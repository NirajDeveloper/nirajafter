/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export let skin = {
    "logo": "/assets/images/dana-logo.png",
    "home": "pzv_home.png",
    "automotive_id": 168,
    "root":"",
    "rfqfilename":"Dana-Aftermarket-RFQ-ID-",
    "banner":["2.jpg","3.jpg","4.jpg"],
    "bannerSliderTimer": "5000",
    "stickyAd":[]
}