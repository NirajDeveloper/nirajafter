

/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export let french = {
    langName: 'fr',
    active: false,
    dispName: "French - EUR",
    image: "assets/images/localization/french.png",
    data: {
        CURRENCY: "USD",
        INDEX: {
            TITLE: 'DANA Aftermarket'
        },
        HEADER: {
            SITENAME: 'AFTERMARKET',
            DISCUSSION: 'Discussion Board',
            WHERETOBUY : 'Where to Buy',
            MENUS: {
                HELPNFAQ: {
                    HELPLINE: 'DANA Customer Service 1-800-621-8084',
                    HELP: 'Aidez-moi',
                    FAQ: 'FAQ'
                },
                LANGNCURRENCY: {
                    ENGLISHUSD: 'French - EUR'
                },
                ORDER: {
                    TRACK: 'Piste',
                    ORDERS: 'Ordres',
                    ORDER: 'Commande',
                    LISTS: 'Listes'
                },
                PROFILE: {
                    SIGNIN: 'Se connecter',
                    REGISTER: 'Registre',
                    USERP: "Profil de l'utilisateur",
                    WISHLIST: 'Liste de souhaits',
                    ORDERLIST: "liste d'ordre",
                    MYLIST: 'My List',
                    LIST: 'List',
                    REPORT: 'Report',
                    LOGOUT: 'Logout',
                    CART: 'Cart',
                    GREETINGS_TXT: 'Hi',
                    MYRFQ: 'My RFQ',
                    RFQS: 'RFQs',
                    MYORDERS: 'My Orders',
                    NOTIFICATIONS : 'Notifications!',
                    CUSTNO : 'Cust. No. {{selectedCustid}}',
                    CUSTNO_TXT: 'Cust. No. ',
                    CLIST_CNUM_LBL1: 'Customer',
                    CLIST_CNUM_LBL2: 'No.',
                    CLIST_DCODE_LBL1: 'Dealer',
                    CLIST_DCODE_LBL2: 'Code',
                    CLIST_CNAME_LBL1: 'Customer',
                    CLIST_CNAME_LBL2: 'Name',
                    CLIST_SRCH_PH: 'Search by Cust. No. / Dealer Code / Customer Name'
                },
                ADVANCEDSEARCH: {
                        ADVANCEDSEARCHYMM: 'Automobile par année - Faire - Modèle',
                        INTERCHANGE:'Échange',
                        YEAR:'An',
                        MAKE:'Faire',
                        MODEL:'Modèle'
                },
                INTERCHANGE: {
                    PLACEHDR: "Type de l'échange numéro de pièce",
                    GO:'Aller'
                }
            }
        },
        FOOTER: {
            DANAHCORP: '2016 Dana Limité',
            ABOUT: 'Sur',
            TERMS: "Conditions d'utilisation",
            POLICY: 'Politique de confidentialité',
            CONNECT: 'Connect with Us'

        },
        BREADCRUMB: {
            BACKSECTION: 'Retour aux résultats de la recherche',
            FOR: 'pour',
            RESULTFOR: 'résultats pour',
            OF: 'de',
            ALL: 'Tout',
            SORT: 'Trier par :',
            MYLISTS: 'My Lists',
            HOME: 'Home',
            SND_RQST_FOR_QUOTE: 'Send Request for Quote',
            INVOICEDETAILS: 'Invoice Details',
            MYORDERS: 'My Orders',
            ORDER_DETAILS: 'Order Details',
            ORDER_STATUS: 'Order Status',
            REPORTS: 'Reports'
        },
        LOADING: {
            LOADINGTEXT: 'CHARGEMENT...'
        },
        //-------------------component/sending-----------------//

        SENDING: {
            SENDINGTEXT: 'Envoi postal....'
        },

        // ------------------components/header/searchbar------------------//

        TYPEHEADPOPUP: {
            EXACTMATCH: 'Correspondance exacte',
            CLAOSEMATCH: 'Fermer matchs',
            SUGGESTION: 'Suggestions'
        },
        TYPEAHEAD: {
            SEARCHBYYMM: 'Recherche par Année / Marque / Modèle',
            SEARCH_PLACEHOLDER: "Recherche par numéro de pièce , type de produit, la gamme de produits , les mots-clés , Interchange , Année , Marque, Modèle"
        },

        //--------------------------orderlist----------------------------//

        ORDERLIST: {
            ORDERLISTHASH: 'Liste Commande #',
            PART: ' Partie',
            PARTS: " les pièces",
            FUTUREREFRENCE: "Partagez la liste de commande pour référence ultérieure",
            SHAREORDERLIST: 'Partager Liste des commandes',
            CAPSONPARTS: 'partie',
            CAPSOFFPARTS: 'les pièces',
            REMOVEPART: 'Retirer la partie',
            EDIT: 'Modifier',
            SAVE: 'Sauvegarder',
            REMOVECROSS: 'X',
            ERRORMSG: "Quantité devrait être chiffre positif",
            NOORDERLIST: 'Aucune liste des commandes disponibles'
        },

        //--------------------------orderlist/shareOrderlist----------------------------//

        SHAREORDERLIST: {
            ORDERLISTHASH: 'ListeCommande #',
            SHAREDMSG: 'Une fois partagé , liste de commande ne sera pas modifiable.',
            FROM: 'De',
            FNAME: "Prénom",
            MANDATORY: '*',
            FNAMEMSG: "Entrez votre prénom s'il vous plait",
            LNAME: 'Nom de famille',
            LNAMEMSG: 'Veuillez entrer votre nom de famille ',
            FEMAIL: 'Votre Email ID',
            REQUIREEMAILMSG: "S'il vous plaît Entrez votre Email.",
            ERROREMAILMSG: "S'il vous plaît Entrez Email ID / ID corrects",
            PHONENO: "Ton numéro de téléphone",
            TO: "À:",
            TEMAIL: 'Votre Email ID',
            TEXTAREA: "Salut, j'ai trouvé quelques pièces sur danaaftermarket.com à commander . S'il vous plaît vérifier ma liste d'ordre .",
            CALLBACK: "aimeriez-vous nous rappeler",
            // GETURL:'Get URL',
            // GOTOHOME:'Go to Home',
            SHAREORDERLIST: 'Partager Liste des commandes',
            FIRTNAMEPLACEHDR: 'Prénom',
            LASTNAMEPLACEHDR: 'Nom de famille',
            YOUREMAILPLACEHDR: "Votre Email ID",
            PHONENOPLACEHDR: 'Ton numéro de téléphone',
            TOEMAILPLACEHDR: "Pour Envoyer ID",
            TEXTAREAPLACEHDR: 'Tout message',
            INVALID: 'Please enter a valid email ID!',
            SUCCESS: 'Your list has been shared successfully!',
            TOADDRESSLABEL: 'To: Type in email address',
            CLR: 'Clear',
            SND_COPY_OF_EMAIL: 'Send me a copy of this email',
            SND_EMAIL: 'SEND EMAIL',
            SHARE: 'Share',
            VIA_EMAIL: 'via Email',
            PLACEHOLDER: "Recipient Email ID's separated by comma.",
            BODY_PLACEHOLDER: 'Type your personal message here.'
        },
        //--------------------------orderlist/sharedOrderlist----------------------------//

        SHAREDORDERLIST: {
            PARTS: 'les pièces'
        },


        //-------------------------orderlist/ShareConfirmation---------------------------//
        SHARECONFIRM: {
            ORDERLIST: 'Liste des commandes',
            PART: 'Partie',
            PARTS: 'les pièces',
            CONFIRMATIONMSG: 'Votre liste de commande est partagée',
            GOTOHOME: 'Aller à la maison'
        },

        //----------------------part/email-------------------------//
        EMAIL: {
            EMILHEADING1: 'Share part ',
            EMILHEADING2: ' via Email',
            FROM: 'From',
            EMAILID: 'Your Email IDs',
            MANDATORY: '*',
            EMAILREQUIRE: 'Please enter a valid Email.',
            EMAILVALIDATE: 'Please enter valid Email ID / IDs.',
            TO: 'To :',
            SUCCESSMSG: 'Mail has been sent succesfully.',
            SEND: 'SEND EMAIL',
            COPYME: 'Send me a copy of this Email',
            MULTIEMAILPLACEHOLDER: "Type in email addresses separated by a comma.",
            SUBJECT: 'The following product has been shared with you.',
            BODY: "I found this part on http://phasezero.xyz/. Please checkout it's details." 
        },

        //------------------part/bomtable-----------------//

        // BOMTABLE: {
        //   BOMNO. : 'BOM No',
        //   LEVEL : 'Level',
        //   COMPONENT : 'Component Name',
        //   QTY : 'Quantity',
        // },

        //------------------part/compatiblitytable-----------------//

        COMPATIBILITY: {
            YEAR: 'An',
            MAKE: 'Faire',
            MODEL: 'Modèle',
            FITMENTNOTES: 'Fitment Notes',
            LOCATION: 'Location'
        },

        //------------------part/interchangestable-----------------//

        INTERCHANGES: {
            NAME: 'prénom',
            PARTNO: 'Partie Nombre',
            TYPE: 'Type'
        },

        //------------------part-----------------//

        PART: {
            ELEARN: 'E Apprentissage',
            CATALOG: 'Partie Catalogue',
            SERVICESMANNUAL: 'Service manuel',
            INSTALLATION: 'Partie Installation',
            SPEC: 'Partie Spec',
            TEXT1: 'Illustrated List Partie Dana',
            TEXT2: 'Liste Spicer Essieux moteurs S110 Illustrated partie',
            DESCRIPTION: 'la description',
            EMAIL: 'Email',
            HUBTEXT1: 'Industrie de la technologie du système de plaque tournante de premier plan',
            HUBTEXT2: 'Une maintenance réduite et une meilleure responsabilité',
            TEXT3: 'Il y a une partie avancée de ce modèle',
            TEXT4: '202BN103-25, Heavy Essieu, Steer Axle, Steer Axle Assy E1202I (69 Kpi)',
            ADDTOORDERLIST: 'Ajouter à la commande LIST',
            ADD: 'Ajouter',
            VALIDMSG: 'Quantité devrait être chiffre positif.',
            WARRENTY: 'GARANTIE:',
            TEXT5: '3 ans / 350.000 miles (560.000 kilomètres)',
            WARRENTYMATRIX: 'Voir la Garantie Matrice',
            MDS: 'Diagramme de modèle et spécifications',
            PAIRSWITH: 'Paires avec',
            PARTDETAIL: ' Spicer, Steer Axle, E1002I, E-series, 10,000lbs, Heavy Duty KingPin mixte, 2-Drawkeys, ...',
            ALOSBOUGHT: 'Vous ont également acheté',
            IDENTIFYYPART: 'Identifier votre part',
            SMDTAB: 'Spécifications & Schéma Modèle',
            CWATAB: 'Compatible avec les applications',
            EVBOMTAB: 'Éclaté & BOM',
            INTERCHANGES: 'Interchangeable',
            BOMNO: 'Nombre BOM',
            LEVEL: 'Niveau',
            COMPONENTPARTNO: 'Composant Numéro de pièce',
            COMPONENT: 'Nom du composant',
            QTY: 'Quantité',
            qty:'qté',
            POPDESCUSSION: 'Discussions populaires',
            VIEWALL: 'Tout voir (31)',
            SECTIONTITLE: 'Est-ce le bon choix pour ma flotte sur route?',
            //SECTIONDESCRIPTION: 'Dana/Spicer has always had a good product to my knowledge and usually backed it up better on parts/warranty.
            //    BTW, noticed one of TMCs new trucks the other day..... white in color. Had to take the second look'
            READMORE: 'Lire la suite...',
            ANSBY: 'Réponse de',
            BLACK900: 'noirw900',
            DAYS: ', 7 il y a quelques jours',
            INTERCHANGES_TABLE: {
                COL1: 'Fabricant',
                COL3: 'Pied Remarques',
                COL2: "Numéro de pièce du fabricant",
                COL4: 'Dana Marque',
                COL5: 'Dana Numéro de pièce',
                COL6: 'type de produit',
                SUPERSEDED: 'REMPLACÉ'
            },
            CHECKAVBLT: 'CHECK AVAILABILITY',
            ADDTOLIST: 'ADD TO LIST',
            PRINT :'Print',
            PARTTOOLTIPMSG1 : "As a registered user, after you have added parts to list, you can send RFQ. To order this part, contact customer service representative at 1-800-621-8084",
            PARTTOOLTIPMSG2 :  "Add parts to list, save it or share it for requesting a quote."
        },

        //------------- searchresults/filter-----------------------//

        FILTER: {
            FILTERALLCATEGORY: 'CATÉGORIES',
            REFINE: 'AFFINER PAR',
            TOOLTIP:"Sélectionnez une catégorie pour voir les filtres"
        },

        //------------- searchresults/partcard-----------------------//

        PARTCARD: {
            CORRESMATCH: 'ÉCHANGE',
            ADDTOORDERLIST: 'Ajouter à la liste Commandez',
            ADDTOLIST: 'ADD TO LIST',
            ADDTOCART: 'Add to Cart',
            REMOVEITEM: 'REMOVE ITEM',
            ADD: 'Ajouter',
            SHOWMSG: 'Ajouté à la liste Commandez.',
            ADDEDTOLIST: 'Added to list!',
            VALIDMSG: 'Quantité devrait être chiffre positif.',
            INVALID_PACKAGE_QTY: 'Available only in packages of ',
            SPECIFICATIONS: 'Caractéristiques',
            CALL: "S'il vous plaît contactez Dana Helpline number : 1-800-621-8084",
            QTY: 'QTY:',
            CHKAVL: 'CHECK AVAILABILITY',
            SAVELIST: 'SAVE LIST',
            SHARELIST: 'SHARE LIST',
            CHECKAVAILABILITY: 'CHECK AVAILABILITY',
            GOTOLIST: 'GO TO LIST',
            ADDEDTOCART : 'Added to cart.',
            CREATENEWLIST : 'Create New List',
            UNIT_PRICE_LBL: 'Unit Price:',
            CORE_UNIT_LBL: 'Core Unit Deposit:',
            STK_PRICE_NOTE: '* stock order price',
            USD_TXT: 'USD'
        },

        //------------- searchresults/partcard/popover-----------------------//
        POPOVER: {
            WishList: 'Ajouter à la liste de souhaits',
            //CONTENT: 'content',
            //TITLE: 'Popup Title:',
            SPECIFICATIONS: 'Caractéristiques',
            MMS: 'MMS',
            TM: 'TM',
            HUB: 'Hub'
        },


        //------------- searchresults/selected-filter-----------------------//

        SELECTEDFILTER: {
            CLOSE: 'X'
        },

        //------------- searchresults/ymm-----------------------//
        YMM: {
            SEARCHBY: 'Recherché par ',
            SEARCHBYMMY: 'Recherche par Marque / Modèle / Année',
            MAKE: "Faire",
            APPLY: 'APPLIQUER',
            GO: 'Aller',
            VEHICLE: 'VOTRE VEHICULE'
        },


        //------------------ searchresults-----------------------//

        SEARCHRESULT: {
            SHOWING: 'présentation',
            MATCHMSG: "correspondance pour l'échange numéro de pièce",
            ZERORESULT: 'a trouvé 0 résultats.',
            REDEFINEEXPLORE: 'Veuillez affiner votre terme de recherche ou explorer les parties via la barre de recherche.',
            CLEAR_ALL: 'Tout effacer',
            SEARCHFOR_TEXT: 'Votre recherche de'
        },
        
        //till that it's added in devsearch //

        //Again we have to start from//
        //-----------------Login -------------------  //
        LOGIN: {
            LOGINTOPROCEED: "S'il vous plaît vous connecter pour continuer!",
            DANACUSTOMER: 'Dana Client',
            BECOMEDANACUSTOMER: 'Devenez un client Dana',
            REGISTEREDUSER: 'Utilisateur enregistré',
            REGISTERASUSER: "Inscrivez-vous en tant qu'utilisateur",
            QUOTEMSG: "Vous pouvez demander à recevoir un devis auprès des distributeurs autorisés Dana pour une quantité souhaitée. Pour être en mesure de passer des commandes, s'il vous plaît",
            REGISTERASDANACUSTOMER: 'enregistrer en tant que client Dana',
            TOOLTIP1: 'Devenez un client Dana - TBD',
            TOOLTIP2: "Inscrivez-vous en tant qu'utilisateur - TBD",
            CUSTOMERUSER: 'DANA UTILISATEUR',
            ACCOUNTLOGIN: 'Connexion au compte',
            CUSTOMERID: 'N ° de client',
            CUSTOMERID_VALIDATION: 'N ° de client est obligatoire',
            USERNAME: "Nom d'utilisateur",
            USERNAME_VALIDATION: "Nom d'utilisateur est obligatoire",
            PASSWORD: 'Mot de passe',
            PASSWORD_VALIDATION: 'Mot de passe est obligatoire',
            LOGIN: "S'IDENTIFIER",
            FORGOT_PASSWORD: 'Mot de passe oublié',
            REMEMBERME: 'Souviens-toi de moi',
            NON_USERS: 'Dana non utilisateurs',
            REGISTERED_USER: 'UTILISATEUR ENREGISTRÉ',
             NEW_TO: 'New to',
            DANA_AFTERMARKETS: "Dana Aftermarket?",
            INFO_DANAUSER: 'Please sign-up as a registered user to receive quotes from authorized distributors and to track status',
            REGISTERASDANAUSER: 'Sign Up as a Registered User',
            DANACUST: 'Sign up as a Dana Customer',
            INFO_DANACUST: 'Get access to your order lists, check price and place orders.',
            INFO2_DANACUST: 'To become a Dana Customer, please contact Dana sales team @ ',
            CONTACT_NUMBER: '1-800-621-8084 or',
            EMAIL_ADDRESS: 'dananorthamericadc@dana.com',
            SIGNIN: 'Sign In',
            USER_NAME: 'Login ID',
            USERNAME_PHOLDER: 'Enter Login Id',
            USER_NAME_VALIDATION: 'Login Id is mandatory',
            PASSWORD_PHOLDER: 'Enter Password',
            CUSTOMER_ID: 'Customer Number',
            CUSTID_PHOLDER: 'Enter Customer Number',
            CUSTID_INFO: 'Required for Dana Customers only',
            LOGIN_BTN: 'LOGIN',
            FIELD_SHORT_ERROR: "Entered value is too short.",
            INVALID_FORMAT: "Invalid format.",
            LOGIN_ID_ERROR: "Please enter a valid Login ID.",
            INVALID_PASSWORD: "Please enter a valid Password.",
            INVALID_CUST_ID: "Please enter a valid Customer Number.",
            CONNECT_US: 'Connect with Us',
            SESSION_EXPIRED: 'Session Expired! Please login again.'
        },
       
          //------------------ Signup ----------------------------------//
        SIGNUP: {
            TITLE: 'Signup',
            PHONE: 'Téléphone',
            PHONE_MOBILE: '(Mobile)',
            PHONE_VALIDATION_MSG: 'N ° de téléphone est obligatoire',
            PHONE_ERR_MSG: 'Please enter a valid Phone Number.',
            SIGNIN_BTN: 'Existing user? Sign in',
            SIGNUP_REG_USER: 'Sign up as Registered User',
            DUSER_INFO1: 'Create lists for requesting quotes from Dana distributors.',
            DUSER_INFO2: 'Send request for quotes to multiple Dana distributors.',
            DUSER_INFO3: 'Track request status.',
            DUSER_INFO4: 'Find Dana distributors in your area.',
            CONNECT_US: 'Connect with Us',
            LOGIN_ID: 'Login ID *',
            LOGIN_ID_ERR_MSG: 'Please enter a valid Login ID.',
            ID_AVAIL: 'Available!',
            ID_EXISTS1: 'Already in use,',
            ID_EXISTS2: 'try another one!',
            PASSWORD: 'Mot de passe',
            PASSWORD_VALIDATION_MSG: 'Mot de passe est obligatoire',
            PASSWORD_ERR_MSG: 'Please enter a valid Password.',
            PASSWORD_SUGGEST: 'Password must be at least 8 characters in length and contain at least one number and one symbol.',
            TXT_STRONG: 'Strong',
            FIRSTNAME: 'Prénom',
            FIRSTNAME_VALIDATION_MSG: 'Prénom est obligatoire',
            FIRSTNAME_ERR_MSG: 'Please enter First Name.',
            LASTNAME: 'Nom de famille',
            LASTNAME_VALIDATION_MSG: 'Nom est obligatoire',
            LASTNAME_ERR_MSG: 'Please enter Last Name.',
            EMAIL: 'Email',
            EMAIL_VALIDATION_MSG: 'Email est obligatoire',
            EMAIL_ERR_MSG: 'Please enter a valid Email Address.',
            RE_EMAIL: 'Re-type Email *',
            RE_EMAIL_ERR_MSG: 'Emails do not match.',
            ZIP_CODE: 'Zip Code *',
            ZIP_CODE_ERR_MSG: 'Please enter a valid ZIP code.',
            COMPANY_NAME: 'Company Name *',
            COMPANY_NAME_ERR_MSG: 'Please enter Company Name.',
            COUNTRY: 'Country *',
            COUNTRY_ERR_MSG: 'Please select a Country.',
            CREATE_ACCOUNT: 'CRÉER UN COMPTE',
            ALL_FIELDS: 'All fields are required.,',
            USER_INFO : 'As a registered user, here is what you can do while you are on our site.',
            SEARCHPARTS : 'SEARCH PARTS',
            USEOURPOWERFULSEARCH: 'Use our powerful search',
            OPTIONPARTQUICKLY :' options to spot your part quickly.',
            ADDTOLIST : 'ADD TO LIST',
            CREATEANDMANAGE : 'Create and Manage',
            MULTIPLELIST : 'multiple lists. Create RFQs',
            FROMPARTADDEDTOLIST : 'from part added to lists..',
            SENDRFQ : 'SEND RFQ',
            FINDDISTRIBUTORS : 'Find distributors in',
            YOURAREARFQ  : 'your area and send RFQ.',
            TRACKRFQ : 'TRACK RFQ',
            TRACKSTATUS :'Track status of your',
            RFQQUOTES: 'RFQs untill you receive quotes.',
            SIGNUPSUCCESSMSG : 'Hi {{firstname}}! you have signed up successfully.'
        },

        //------------------ FORGOT-PASSWORD ----------------------------------//
        FORGOT_PSWRD: {
            PSWRD_ASSIST:{
                TITLE: 'Password Assistance',
                INSTRUCTION: 'Enter the e-mail address associated with your Aftermarket account.',
                LABEL : 'Email Address',
                ERROR: 'Please enter a valid Email Address.',
                BUTTON: 'CONTINUE',
                FOOTER_TXT1: 'Has your e-mail address changed? If you no longer use the e-mail address associated with your Aftermarket account, you may contact',
                FOOTER_TXT2: 'Customer Service',
                FOOTER_NUM: '1-800-621-8084',
                FOOTER_EMAIL: 'dananorthamericadc@dana.com',
                OR_TXT: 'or',
                FOOTER_TXT3: 'for help restoring access to your account.'
            },
            VERIFY_OTP:{
                TITLE: 'Reset Password',
                INSTRUCTION_TXT1: "For your security, we need to verify your identity.",
                INSTRUCTION_TXT2: 'Please enter the one time temporary password that was sent to your email ',
                LABEL: 'Temporary Password',
                ERROR: 'Invalid Temporary Password.',
                BUTTON: 'CONTINUE',
                RESEND_LNK: 'Resend Temporary Password',
                FOOTER_TXT1: 'Has your e-mail address changed? If you no longer use the e-mail address associated with your Aftermarket account, you may contact',
                FOOTER_TXT2: 'Customer Service',
                FOOTER_TXT3: 'for help restoring access to your account.'
            },
            RESET_PSWRD:{
                TITLE: 'Create New Password',
                PSWRD_SUGGESTION: 'Password must be at least 8 characters in length and contain at least one number and one symbol.',
                LABEL_NEW_PSWRD: 'New Password',
                NEW_PSWRD_ERR: 'Please enter a valid Password.',
                LABEL_CONFIRM_PSWRD: 'Confirm Password',
                CONFIRM_PSWRD_ERR: "Passwords do not match.",
                BUTTON: 'SAVE CHANGES'
            },
            TITLE: 'Signup',
            PHONE: 'Phone *',
            PHONE_ERR_MSG: 'Please enter a valid Phone Number.',
            SIGNIN_BTN: 'Existing user? Sign in',
            SIGNUP_REG_USER: 'Sign up as Registered User',
            DUSER_INFO1: 'Create lists for requesting quotes from Dana distributors.',
            DUSER_INFO2: 'Send request for quotes to multiple Dana distributors.',
            DUSER_INFO3: 'Track request status.',
            DUSER_INFO4: 'Find Dana distributors in your area.',
            CONNECT_US: 'Connect with Us',
            LOGIN_ID: 'Login ID *',
            LOGIN_ID_ERR_MSG: 'Please enter a valid Login ID.',
            ID_AVAIL: 'Available!',
            ID_EXISTS1: 'Already in use,',
            ID_EXISTS2: 'try another one!',
            PASSWORD: 'Password *',
            PASSWORD_ERR_MSG: 'Please enter a valid Password.',
            PASSWORD_SUGGEST: 'Password must be at least 8 characters in length and contain at least one number and one symbol.',
            TXT_STRONG: 'Strong',
            FIRSTNAME: 'First Name *',
            FIRSTNAME_ERR_MSG: 'Please enter First Name.',
            LASTNAME: 'Last Name *',
            LASTNAME_ERR_MSG: 'Please enter Last Name.',
            EMAIL: 'Email *',
            EMAIL_ERR_MSG: 'Please enter a valid Email Address.',
            RE_EMAIL: 'Re-type Email *',
            RE_EMAIL_ERR_MSG: 'Emails do not match.',
            ZIP_CODE: 'Zip Code *',
            ZIP_CODE_ERR_MSG: 'Please enter a valid ZIP code.',
            COMPANY_NAME: 'Company Name *',
            COMPANY_NAME_ERR_MSG: 'Please enter Company Name.',
            COUNTRY: 'Country *',
            COUNTRY_ERR_MSG: 'Please select a Country.',
            CREATE_ACCOUNT: 'SIGN UP',
            ALL_FIELDS: 'All fields are required.'
        },

        //------------------ USER PROFILE ----------------------------------//
        PROFILE_ERROR:{
            CUSTOMER_ID_REQUERIED: 'Customer No. is required for the Login ID you entered!',
            AUTH_LOGIN_FAILED: 'Customer No. you entered is not linked to the Login ID, please verify and try again!',
            INVALID_LOGIN_ID: 'Login ID and Password you entered do not match, please verify and try again!',
            PASSWORD_WRONG: 'Login ID and Password you entered do not match, please verify and try again!',
            LOGIN_FAILED: 'Login ID and Password you entered do not match, please verify and try again!',
            CUSTOMER_ID_EMPTY: 'Customer No. is not required for the Login ID you entered!',
            USER_RESET_PASSWORD_NOT_ALLOWED: 'Sorry, to reset password, a Dana customer should contact Customer service at 800-621-8084.',
            INTERNAL_SERVER_ERROR: 'There is an internal server error, please try again!',
            USER_EXIST: 'User already exist',
            USER_NOT_EXIST: 'User not exist',
            PASSWORD_MISMATCH: 'Password should match',
            INVALID_CREDENTIALS: 'Login ID and Password you entered do not match, please verify and try again!',
            EMAIL_NOT_EXIST: 'Available',
            EMAIL_EXIST: 'Email address already in use.',
            OTP_WRONG: 'Temporary Password you entered is incorrect, please verify and try again!',
            OTP_CORRECT: 'Verification successful!',
            PASSWORD_NOT_RESET: 'Password not changed',
            INVALID_EMAIL_ID: 'E-mail ID you entered is not registered, please verify and try again!',
            OTP_EXPIRED: 'Temporary Password is expired',
            PROFILE:{ 
                PROFILE_UPDATED_SUCCESSFULLY: 'Your Profile has been updated!',
                PSWRD_CHANGED_SUCCESSFULLY : 'Your Password has been changed!',
                INVALID_CREDENTIAL: 'Current Password you entered is incorrect, please verify and try again!',
                INTERNAL_SERVER_ERROR: 'There is an internal server error, please try again!'
            }
        },

        PROFILE: {
            TITLE:'My Account',
            BC_HOME:'Home',
            TAB_PROFILE: 'My Profile',
            TAB_PASSWORD: 'Change Password',
            PROFILE_TITLE: 'Personal Information',
            PASSWORD_TITLE: 'Change Password',
            PHONE: 'Phone (Mobile)',
            PHONE_ERR_MSG: 'Please enter a valid Phone Number.',
            PHONE_WORK: 'Phone (Work)',
            PHONE_WORK_ERR_MSG: 'Please enter a valid Phone Number.',
            ADDRESS_1: 'Address 1',
            ADDRESS_1_ERR_MSG: 'Please enter Address.',
            ADDRESS_2: 'Address 2',
            FAX: 'Fax',
            OPTIONAL: 'Optional',
            LOGIN_ID: 'Login ID',
            LOGIN_ID_ERR_MSG: 'Please enter a valid Login ID.',
            ID_AVAIL: 'Available!',
            ID_EXISTS1: 'Already in use,',
            ID_EXISTS2: 'try another one!',
            PASSWORD_SUGGEST: 'Password must be at least 8 characters in length and contain at least one number and one symbol.',
            FIRSTNAME: 'First Name',
            FIRSTNAME_ERR_MSG: 'Please enter First Name.',
            LASTNAME: 'Last Name',
            LASTNAME_ERR_MSG: 'Please enter Last Name.',
            EMAIL: 'Email',
            EMAIL_ERR_MSG: 'Please enter a valid Email Address.',
            ZIP_CODE: 'Zip Code',
            ZIP_CODE_ERR_MSG: 'Please enter a valid ZIP code.',
            COMPANY_NAME: 'Company Name',
            COMPANY_NAME_ERR_MSG: 'Please enter Company Name.',
            CITY: 'City',
            CITY_ERR_MSG: 'Please enter a valid City.',
            CRRENT_PSWRD: 'Current Password *',
            CRRENT_PSWRD_ERR_MSG: 'Please enter a valid Password.',
            NEW_PSWRD: 'New Password *',
            NEW_PSWRD_ERR_MSG: 'Please enter a valid Password.',
            RE_PSWRD: 'Re-enter New Password *',
            RE_PSWRD_ERR_MSG: 'Passwords do not match.',
            STATE: 'State',
            STATE_ERR_MSG: 'Please select a State.',
            COUNTRY: 'Country',
            COUNTRY_ERR_MSG: 'Please select a Country.',
            PREFER_MODE: 'Preferred mode of Communication',
            PREFER_MODE_ERR_MSG: 'Please select a Preferred mode of Communication.',
            SAVE_CHANGES: 'SAVE CHANGES',
            STARRED_FIELDS: '* fields are required.',
            ALL_FIELDS: 'All fields are required.',
            REPORTS: 'Reports',
            D_CUSTOMER: 'Dana Customer',
            DOWNLOAD_EXCEL: 'Download',
            SHOP_USER: 'Shop User',
            CREATEUSER :'Create User',
            CREATECUSTOMERUSER : 'Create Customer User',
            CUSTOMERNUMER : 'Customer No.',
            CREATE : 'CREATE',
            MOBILEPHONE : 'Mobile Phone',
            WORKPHONE : 'Work Phone',
            EMAILONLY : 'Email'
        },

        //-------------------------Pricing and Availability -------------//
        REPORT:{
            'CUST_USER' :{
                customerNumber: 'Customer Number',	
                companyName: 'Company Name',
                dealerCode: 'Dealer Code',
                userId: 'User ID',	
                firstName: 'First Name',
                lastName: 'Last Name',
                emailId: 'Email Address',
                lastLogInDate: 'Last LogIn Date',
                CanFinalizeOrder: 'Can Finalize Order',
                CanSeePrice: 'Can See Price'
            },
            'SHOP_USER' :{
                userId: 'User ID',	
                firstName: 'First Name',
                lastName: 'Last Name',
                emailId: 'Email Address',
                lastLogInDate: 'Last LogIn Date'
            }     
        },

        PRICINGANDAVAILABILITY: {
            INSTRUCTIONRTB: 'Sélectionnez le type de commande et entrez la quantité pour vérifier la disponibilité et ajoutez la partie au panier ultérieurement.',
            ORDERTYPE: "Type d'ordre:",
            STOCKORDER: 'Commandez',
            ORDERTYPE1: 'Commandez',
            ORDERTYPE2: "Ordonnance d'urgence",
            ORDERTYPE3: 'Ordre de test',
            ORDERTYPE1_DELIVERY: 'La livraison normale, selon la disponibilité',
            ORDERTYPE2_DELIVERY: 'livraison accélérée, les frais applicables',

            NORMALDELIVERY: 'La livraison normale, selon la disponibilité',
            EMERGENCYORDER: "Ordonnance d'urgence",
            FASTTRACKCHARGESMSG: 'livraison accélérée, les frais applicables',
            DESIREDQTY: 'quantité souhaitée',
            CHECKAVAILABILITY_S: 'Check Price & Availability',
            CHECKAVAILABILITY: 'VOIR LES DISPONIBILITÉS',
            UPDATEAVAILABILITY: 'MISE À JOUR DISPONIBILITÉ',
            QTYVALIDATIONMSG: 'Quantité devrait être chiffre positif',
            PARTNO: 'Numéro de pièce',
            CHANGE: 'Changement',
            UNITOFMEASUREMENT: 'UOM',
            PACKAGE: 'Paquet',
            EACH: 'EA',
            TOTALQTYAVAILABLE: 'Quantité totale disponible',
            FROM: 'de',
            LOCATION: 'emplacement',
            LOCATIONERRORMSG: 'Lieu choisi ne satisfait pas la quantité désirée',
            EMG_QTY_AVAIL_ERR: 'Your desired quantity is not available, please contact customer service at',
            LOCATIONHEADER: 'Emplacement',
            QTYHEADER: 'Quantité disponible',
            QTY_AVAIL: 'Available',
            SHIPPINGDATEHEADER: "Date d'envoi estimée",
            SHIPPING_DATE: 'Shipping Date',
            UNITPARTPRICE: 'Unité Référence Prix',
            EXTENDEDPRICE: 'Prix prolongée ',
            COREUNITPRICE: 'Unit Core Deposit* ',
            CORECHARGE: 'charge de base',
            PACKAGEPRICE: 'Prix du coffret',
            ADDTOCART: 'AJOUTER AU PANIER',
            ADDTOLIST:'ADD TO LIST',
            ADD: 'AJOUTER ',
            TOTHECART: 'AU PANIER + ',
            ADDTOBACKORDER: 'POUR SAUVEGARDER COMMANDER',
            PART_NOT_AVAILABLE: 'Oops! partie non disponible :(',
            PART_NOT_AVAILABLE_INFO: "Vous pouvez choisir d'ajouter la quantité désirée comme Backorder",
            CONTACTREPSENT : 'Contact your Dana representative to order this part or contact customer service at',
            ACCT_ORDER_AUTHORISED :  'It seems your account is not authorized to order this part',
            AUTHORIZATION_REQD_FOR:'Authorization required for',
            PLACING_STK_ORDER: 'placing stock orders',
            SELECT_LIST:'Select a List',
            NO_LIST: 'No list!',
            CREATE_NEW_LIST: 'Create new lists',
            CROSSVILLE : 'Crossville',
            STANDARDLEADTIME :'Standard Lead Time',
            ESTIMATED_DATE: 'Estimated shipping date:',
            BTO_MSG: ' can be built by',
            UPTO : 'Up to ',
            BTO_TOOLTIP: 'Custom built for you!',
            SLTI_MSG: 'Add {{availability}} remaining from ',
            PARTIAL_MSG: 'Out of {{total}}(qty), only {{partial}}  will be picked from this location to match desired quantity.',
            SYSTEM_ERR_MSG: 'Application has encountered an error while retrieving availabilty.',
            SYSTEM_PRICING_ERR_MSG: 'Application has encountered an error while retrieving price.',
            TRY_AGAIN: 'TRY AGAIN',
            DESIREQTY : 'Desire Quantity',
            AVAILABILITY: {
                ADD_TO_CART_AS_BACK_ORDR: 'ADD TO CART AS BACK ORDER',
                UPDATE_TO_CART_AS_BACK_ORDER: 'UPDATE TO CART AS BACK ORDER',
                ADD: 'ADD ',
                UPDATE: 'UPDATE ',
                ADD_ONLY: 'ADD ONLY ',
                UPDATE_ONLY: 'UPDATE ONLY ',
                TO_CART: ' TO THE CART',
                TO_THE_CART: ' TO THE CART + ',
                TO_BACK_ORDER: ' TO BACK ORDER',
                AUTHORIZATION_REQD: 'Authorization required to check availability.',
                CONTACT_REP: 'Please contact your Sales Representative to add this product line.',
                NOTE:'Pricing Note:',
                MSG_1:'Pricing displayed represents "Standard Net Price" and may not reflect program or promotional offerings.',
                MSG_2:'For any questions, please contact Customer Service at 800-621-8084.',
                AVAIL_NOTE: 'Availabilty Note:',
                AVAIL_NOTE_CONTENT: 'Does not reserve inventory until the checkout process is complete.'
            }
        },

        //------------------ Preview Order ----------------------------------//

        PREVIEWORDER: {
            CUSTOMERNAME: 'Bill-to Customer Name',
            MAILINGADDRESS: 'Mail-to Address',
            SHIPPINGADDRESS: 'Select Ship-to Address',
            BILLTOADDRESS: 'Bill-to Address (Invoice to be sent to)',
            ADVANCEDSEARCH: 'ADVANCED SEARCH',
            SHIPPINDADDWARN: 'Select a Ship-to address for placing order!',
            EMAILADDRESS: 'Select mail-to email addresses (It will be used for order related notifications, update and correspondence)',
            ADDEMAIL: 'ADD NEW EMAIL ADDRESS',
            SHIPPINGMETHOD: 'Shipping method',
            CARRIER: 'Carrier',
            SHIPPINGDATE: 'Requested Shipping Date',
            ADD_NEW: 'ADD NEW',
            ENTER_VALID_SO_NO: 'Please enter a valid SO number',
            SHIP_COMPLETE: 'Partial shipment allowed?',
            NO: 'No',
            YES: 'Yes',
            SHIPPINGINSTRUCTION: 'Shipping instruction (optional)',
            PURCHAGE_ORDR_NUM: 'Purchase Order number (optional)',
            PRIMARY_NUM: 'Primary PO No.',
            SECONDARY_NUM: 'Secondary PO No. (Optional)',
            CA_NUMBER: 'Carrier Account No.',
            ORDER_SUMMARY: 'Stock order summary',
            NUM_OF_PARTS: 'No. of Item(s)',
            TOTAL_WEIGHT: 'Total Weight (Kg/lbs)',
            ORDER_TOTAL: 'order total',
            PLACE_ORDER: 'Place Order',
            CONT_SHOPPING: 'CONTINUE SHOPPING',
            EDIT_ORDR_LIST: 'EDIT ORDER LIST',
            ORDR_PROCESSING: 'Processing Your Stock Order...',
            SHIP_TO_ADDRESS: 'Advanced Search - Ship-to Address',
            SHIP_TO_THIS: 'SHIP TO THIS',
            REVERT_CART: 'Revert Cart',
            ITEMS_AVAILABILITY_INFO: 'Availability of items being ordered, will be re checked once order is replaced',
            STOCK_ORDER: 'Stock Order',
            ADDNEWSHIPTOADDRESS : 'ADD NEW SHIP-TO ADDRESS',
            SHIPPINGAVAILABLITYDETAILS : 'As per availablity, for one of the part of(s) in your cart, the shipping date is',
            SHIPPINGAVAILABLITYDETAILS1 : 'therefore, requested shipping date can not be earlier',
            APRROVALMSGFROMDANA : 'To edit or cancel the orders placed, please contact customer service at',
            EMAILACKNWMSG : "When your order is placed, we'll send you an e-mail message acknowledging receipt of your order.",
            CONTACTCUSTOMERSRV: "Need help? Contact Customer Service at",
            CS_NUM: "1-800-621-8084",
            CONTACT_EMAIL: 'dananorthamericadc@dana.com',
            OR: 'or',
            SP_SHIPPING : "Specify Shipping",
            CUSTOMERNO : 'Customer No.',
            CUSERNAME : 'Customer Name',
            VALIDENTERPOMSG : 'Please enter a valid PO number',
            BILLTO: 'Bill-to (Invoice to be sent to)'
        },

        //--------------- Post Order -----------------------//

        POSTORDER: {
            SEARCH_PLACEHOLDER: "Search Orders by PO number, Invoice number, Process number, Order number",
            HOME: 'Home',
            MACCOUNT: 'My Account',
            MYORDERS: 'My Orders',
            RESULT_LBL: "Search Results",
            RESULT_MSG: "order(s) found",
            TAB1: "Recent Orders (Last 2 months)",
            TAB2: "Specify a date range",
            TAB3: "Open Orders",
            TAB4: "Back Orders",
            TAB5: "Invoices",
            PONUMBER: "PO No.",
            ORDER_DATE: "Order Date",
            INVOICE_DATE: "Invoice Date",
            INVOICE_NUMBER: "Invoice No.",
            PROCESS_NUMBER: "Process No.",
            ORDER_NUMBER: "Order No.",
            DETAIL_BTN: "Deatil",
            SOLDTO: "Sold To",
            SHIPTO: "Ship To",
            PARTNUMBER: "Part No.",
            DESC: "Description",
            QTY_REQ: "Open Quantity",
            EXT_PRICE: "Extended Price",
            PRICE: "Price",
            PROMISE_DATE: "Estimated Shipping Date",
            DELIVERY_DATE: "Delivery Date",
            PACKING_SLIPS: "Packing Slips",
            PACKING_SLIPS_BTN: "PACKING SLIP",
            ORDERSTATUS: "Order Status",
            ACTION: "Action",
            VIEW_STATUS: "VIEW STATUS",
            ORDER_DETAILS_BY_DATE: {
                ACTION: 'Action',
                VIEW_STATUS: 'VIEW STATUS'
            },
            ORDER_STATUS: {
                IN_PROGRESS: 'In Process',
                R_FOR_SHIPPING: 'Ready for Shipping',
                NO_STATUS_AVAIL: 'No status available'
            },
            EMAIL:{
                LABEL_EMAIL: 'Email',
                ERR_MSG: 'There is some server error in sending email.',
                EMAIL_TO: 'To :',
                FORM_ERR_MSG: 'Please enter a valid email.',
                BTN_TXT: 'SEND EMAIL',
                SUCCESS_MSG: 'Mail has been sent succesfully.',
                PLACEHOLDER: "Recipient Email ID's separated by comma."
            }
        },

        //------------ backorder.html----------
        BACKORDER : {
            NO_DATA_AVAIL : 'No data available!',
            NO_RECORD_FOUND: 'Information requested is not available, please verify your access permissions'
        },

        //------------ Invoice -------------------//

        INVOICE: {
            INVOICE_NUMBER: "Invoice Number",
            INVOICE_DATE: "Invoice Date",
            INVOICE_TYPE: "Invoice Type",
            INVOICE_DESC: "Description",
            INV_COMP_NAME: "Dana Heavy Vehicle Systems Group, LLC",
            PRINT_INV: 'Print',
            AFTERMARKET: 'AFTERMARKET',
            INV_HEAD: 'Invoice',
            REPRINT: 'Reprint',
            DATE: 'Date',
            CUST_NUM: 'Customer No.',
            SHIP_TO_NUM: 'Ship to No.',
            ORDER_NUMBER: 'Order No.',
            ORD_DATE: 'Order Date',
            INV_NUM: 'Invoice No.',
            DEALER_PO: 'Dealer PO',
            DEALER_CODE: 'Dealer Code',
            SHIPMENT_NUM: 'Shipment / BOL No.',
            SOLDTO: 'Sold to',
            SHIPTO: 'Ship to',
            CUSTOMER_PO: 'Customer PO',
            TERMS: 'Payment Terms',
            SHIP_VIA: 'Carrier',
            PICK_NUM: 'Pick No.',
            PART_NUM: 'Part No.',
            CUSTOMER_PART: 'Customer Part',
            PART_DESC: 'Part Description',
            QTY_ORD: 'Qty Ordered',
            QTY_SHIP: 'Qty Ship',
            UNITPRICE: 'Unit Price (USD)',
            EXTENDEDPRICE: 'Extended Price (USD)',
            REMIT_TO: 'Remit to',
            DANA_LMTD: 'Dana Limited',
            EXPEDITE_WAY: '88714 Expedite Way',
            CHICAGO_IL: 'Chicago IL',
            US: 'US',
            TOTAL: 'Total (USD):',
            FREIGHT_CHARGES: 'Freight Charges (USD):',      
            ITEM_TOTAL: 'Item Total (USD):',
            ALL_CLAIM_AGAINST: 'All Claims against the carrier (damages, shortages, etc) must be noted on the freight bill upon delivery.',
            CLAIM_MUST_MADE: 'Claims must be made within 30 days from receipt of goods, providing above Shipment Number.'
        },

        // --------------------- invoicedetails.......................//

        INVOICEDETAILS: {
            I_DETAILS: 'Invoice Details',
            BACK: 'BACK',
            SOLDTO: 'SOLD TO',
            SHIPTO: 'SHIP TO',
            BILLTO: 'BILL TO',
            PRO_NUM: 'Process No.',
            ORD_NUM: 'Order No.',
            ORD_TYPE: 'Order Type',
            CARRIER: 'Carrier',
            TERMS: 'Payment Terms',
            ACCOUNT: 'Carrier Account No.',
            TRACKING_NUM: 'Tracking No.',
            INV_NUM: 'Invoice No.',
            INV_DATE: 'Invoice Date',
            PO_NUM: 'PO No.',
            PART_NUM: 'Part No.',
            DESC: 'Description',
            QTY_ORD: 'Qty Ordered',
            QTY_SHIPD: 'Qty Shipped',
            UNT_PRICE: 'Unit Price',
            EXT_PRICE: 'Extended Price',
            WEIGHT: 'Weight',
            STATUS: 'Status',
            TOTAL: 'Total',      
            ITEM_TOTAL: 'Item Total:',
            UOM: 'LB'
        },

        //----------------------Invoice-list,invoice-list.html

        INVOICELIST: {
            FRM_DATE: 'From Date',
            TO_DATE: 'To Date',
            GET_CREDIT_HIST: 'Get Credit History'
        },

        //----------orderdetails(post-order/orderdetails/detail.html)

        ORDERDETAILS: {
            ORDER_DETAILS: 'Order Details',
            BACK: 'BACK',
            PRINT: 'Print',
            DOWNLOAD_ORDER: 'Download',
            SOLDTO: 'SOLD TO',
            SHIPTO: 'SHIP TO',
            BILLTO: 'BILL TO',
            PRO_NUM: 'Process No.',
            ORD_NUM: 'Order No.',
            ORD_TYPE: 'Order Type',
            CARRIER: 'Carrier',
            TERMS: 'Terms',
            ACCOUNT: 'Account',
            INV_NUM: 'Invoice No.',
            INV_DATE: 'Invoice Date',
            PART_NUM: 'Part No.',
            DESC: 'Description',
            QTY_ORDERED: 'Qty Ordered',
            QTY_SHIPPED: 'Qty Shipped',
            UNITPRICE: 'Unit Price',
            EXT_PRICE: 'Extended Price',
            WEIGHT: 'Weight',
            UOM: 'LB',
            WEIGHT_UOM: 'Weight UOM',
            STATUS: 'Status',
            TOTAL: 'Total'
        },

        //----------ordr-dtls-ByDtRange.html

        ORDERDETAILS_BY_DATE: {
            FROM_DATE: 'From Date',
            TO_DATE: 'To Date',
            GET_ORDER_LIST: 'Get Order List'
        },

        //----------order-status(orderstatus.html)-------------

       ORDERSTATUS: {
            ALL: 'All',
            ESTSHIPPING: 'Est. Shipping',
            AWAITINGPROCESSING: 'Awaiting Processing',
            INPROCESS: 'In Process',
            TRACKING: 'Tracking',
            NORESULTSFOUND: 'No results found!',
            SEARCHBYPART: 'Search by Part No.',
            PRIMARYPONUMBER: 'PO No.',
            ORDER_STATUS: 'Order Status',
            BACK: 'BACK',
            ORDER_ID: 'Order No.',
            TOT_ITEMS: 'Total Items(s)',
            HIDE_PARTS_CS: 'Hide parts Completely Shipped',
            LINE_NO: 'Line No.',
            PN_PN: 'Part No./Part Name',
            ORD_QTY: 'Qty Ordered',
            PROCCESSING: 'Processing',
            STAGE_1: 'Stage 1',
            STAGE_2: 'Stage 2',
            SHIPPING: 'Shipping',
            SHIPPING_INFO: 'The grey progress bar represents total quantity ordered whereas the green bar and dots represent quantity shipped on different dates.',
            EST_SHIPPING: 'Est. shiping',
            SHIPPED: 'shipped',
            ON: 'on',
            NO_DATA: 'No data available!',
            OVERALL_STATUS: 'Overall Status',
            STATUS: {
                ORDER_CANCEL: "Order Canceled",
                ORDER_NO_STATUS: "Order Status Not Available",
                ORDER_RECEIVED: "Awaiting Processing",
                ORDER_BEING_PROCESSED: "In Process",
                ORDER_PARTIALLY_SHIPPED: "In Process",
                ORDER_COMPLETELY_SHIPPED: "Shipped"

            },
            DETAILSTATUS: 'Status: ',
            DETAILSTATUSMSG: 'Each box depicts a quantity and its shipment statuses; "Awaiting Processing" status with a white box, "In Process" status with a blue box and "Shipped" status with a green box. To know more, mouse hover respective box',
            PARTS_SHIPPED: 'Part(s) Shipped',
            PARTS_PROCESS: 'Part(s) In Process',
            PARTS_AWAITING: 'Part(s) Awaiting Processing',
            PRINT: 'Print Page',
            EMAIL: 'Email',
            QTY_AWAITING: 'Qty Awaiting Processing',
            QTY_PROCESS: 'Qty In Process',
            QTY_SHIPPED: 'Qty Shipped'
        },


        //----------packingslip(packingslip.html)-------------

        PACKINGSLIP: {
            PACK_SLIP: 'Packing Slips for Order Number ',
            ORD_NUM: 'Order No.',
            PRINT_P_SLIP: 'Print',
            DEL_NUMS: 'Delivery Numbers',
            DANA_H_VEH: 'Dana Heavy Vehicle Systems Group, LLC',
            AFTERMARKET: 'AFTERMARKET',
            PACKINGSLIP: 'PACKING SLIP',
            REPRINT: 'REPRINT',
            DELIVERY_NUM: 'Delivery Number',
            PLANTCODE: 'Plant Code',
            BILL_TO_C_NO: 'Bill to Cust. No.',
            SHIP_TO_C_NO: 'Ship to Cust. No.',
            SHIP_DATE: 'Ship Date',
            SHIP_VIA: 'Carrier',
            SHIPPING_INSTR: 'Shipping Instructions',
            CAR_NUM: 'Carton No.',
            LINE_NUM: 'Line No.',
            CUST_PART_NUM: 'Customer Part No.',
            PRIMARY_PO_NUM: 'Customer PO No.',
            DANA_PART_NUM: 'Dana Part No.',
            QTY_SHIPPED: 'Qty Shipped',
            UNIT_WEIGHT: 'Unit Weight ',
            SHIPMENT_ID: 'Tracking No.',
            SUPPILER_ID: 'Supplier ID',
            SHIPPING_PONIT: 'Shipping point',
            INCO_TERMS: 'Incoterms',
            SHIPPING_COND: 'Shipping Conditions',
            SHIPTOPARTY: 'Ship-to-party',
            DOCUMENTDATE: 'Document Date',
            DELIVERYNO: 'Delivery No.',
            INFORMATION: 'Information',
            SOLDTOPARTY_ADDR: 'SOLD TO:',
            SHIP_ADDR: 'SHIP TO:',
            UOM: 'LB'
        }, 

        
         //----------Add new ship to address//
        SHIPADDR: {
            MANDATORY: '*',
            HEADER: "Add New Ship-to Address",
            NAME: "Name",
            STATE: "State",
            COUNTRY: "Country",
            CITY: "City",
            ZIN: "Zip Code",
            STREET_ADDRESS: "Address Line 1",
            STREET_ADDRESS2: "Address Line 2",
            STATEERRMSG: 'Please enter State',
            CITYERRMSG: 'Please enter City',
            COUNTRYERRMSG: 'Please enter Country',
            PINEERRMSG: 'Please enter a valid ZIP code.',
            STREET1ERRMSG: 'Please enter Street Address',
            SAVE: "Save",
            SHIP_NAME: 'Name',
            SHIPERRMSG: 'Please Enter Ship To Name'
        },
        
          // my list/ mylist.html
        MYLIST: {
            MLIST: 'My Lists',
            PLIST: 'Print',
            APLIST: 'Add part to list',
            ADDALL_TOCART: 'ADD ALL ITEMS TO CART',
            ADDALL_TORFQ: 'ADD ALL ITEMS TO RFQ',
            ADDTOCART: 'ADD TO CART',
            ADDTORFQ: 'ADD TO RFQ',
            LITEM1: 'Truck 01 list(2)',
            LITEM2: 'ABC  list(2)',
            LITEM3: 'ANZ list(3)',
            SELECTEDITEM: '2 item(s) selected of 2 items in the list',
            RQSIBTN: 'Request Quote for selected items',
            SRNO: 'Serial Number',
            PARTNO_NAME: 'Part No./Part Name',
            PARTNAME: 'Part Name',
            PARTNO: 'Part Number',
            QTY: 'Quantity',
            QTY_UPDATE_SUCCESS : 'Quantity has been updated successfully!',
            REMOVE: 'Remove',
            MMDDYY: 'Added on MM/DD/YYYY',
            RFQS: 'Request for Quote Status',
            NEWRFQ: 'RFQ',
            RFQID: 'RFQ ID',
            REQID: 'Req ID',
            REQDATE: 'Req Date',
            QRBY: 'Quote is requested by',
            DISTRIBUTOR: 'Ditributor',
            LNAME: 'Last Name',
            RFQSTATUS: 'RFQ Status',
            RECQUOTE: 'Quote Recieved (Y/N)',
            DETAILBTN: 'Detail',
            YES: 'Yes',
            NO: 'No',
            CREATENL: 'Create List',
            MANAGEL: 'Manage List',
            COREPRICE: "Unit Core Deposit (USD)",
            UNITPRICE: "Unit Price (USD)",
            ZEROPARTS: '0 parts in RFQ!',
            ADDPARTS_TOPROCEED: 'Add parts to proceed.',
            DATEADDED: "Date Added",
            DELETE_CONFIRMATION: "Do you really want to delete",
            FROM: "from",
            DELETE: "Delete",
            CANCEL: "Cancel",
            CONFIRMATION: "Confirmation",
            CREATE_NEW: "CREATE NEW LIST",
            LIST_LBL: "List (Number of items)",
            GOTOCART: "GO TO CART",
            LIST_SEARCH_ERR: "No List(name) matched your search!",
            EMPTY_MSG: "Please add parts to the list through part search.",
            NO_RESULT: "No results found!",
            ADDED_RFQ: "Added to RFQ!",
            ADDED_CART: "Added to Cart!",
            AUTH_REQ: "Please contact your Sales Representative to add this product line.",
            NO_PARTS: "No of Item(s) Added",
            SEND_RFQ_BTN1: "SELECT APPROVED",
            SEND_RFQ_BTN2: "DISTRIBUTORS TO SEND RFQ TO",
            VIEW_PARTS_ADDED: "VIEW ITEMS ADDED",
            HIDE_PARTS_ADDED: "HIDE ITEMS ADDED",
            RFQ_SLNO: "Serial No.",
            RFQ_PART: "Part No./Part Name",
            RFQ_PART1: "Part No./",
            RFQ_PART2: "Part Name",
            RFQ_RQTY: "Desired Quantity",
            ADDED_CART1: "1 item(s) added to the cart!",
            CART_MSG1: " item(s) added to the cart!",
            CART_MSG_ONLY: " only ",
            CART_MSG_OUTOF: "Out of ",
            CART_MSG_AUTH: " Authorization required to order this part(s)."
            
            
        },
        
         //---------------mylist/createlist/createlist.html-------------------

        // CREATENEWLIST: {
        //     CANBUSE: 'This list can be used for requesting quotation only from Dana distributor.',
        //     CREATENL: 'Create New List',
        //     LISTNAME: 'List Name',
        //     PLACEHRD: 'Untitle list number',
        //     CREATELISTBTN: 'CREATE LIST'
        // },
        CREATENEWLIST: {
            CREATENL: 'CREATE LIST',
            SHOPLIST1: 'Give your list a name (',
            SHOPLIST2: ' part(s) added)',
            CANBUSE: 'This list can be used for placing stock order only',
            SCANBUSE: 'You may use list to create and send RFQ to authorized Dana distributors / dealers.',
            LISTNAME: 'List Name',
            CREATELISTBTN: 'CREATE ',
            SAVECONTINUE: 'SAVE & CONTINUE',
            CANCEL: 'CANCEL',
            CONFIRM: 'Currently your list is stored in a temporary space, if you don’t save it now, it will be deleted permanently. Please click ‘SAVE’ to revert and save it or click ‘DELETE’ to confirm deletion.',
            GOTOLIST: "Go To My List",
            SUCCESS: "Success",
            SUCCESS_MSG1: "Your List is saved as",
            SUCCESS_MSG2: 'list, You can go to "My List" to modify it.',
            SHORT_MSG: "List name is too short.",
            LONG_MSG: "List name is too long.",
            REQUIRED_MSG: "List name is required."
        },
        
         //---------------mylist/managelist/managelist.html-------------------

        // MANAGELIST: {
        //     MANAGEML: 'Manage My List',
        //     DEFAULT: 'Default',
        //     DELETE: 'Delete',
        //     LIST: 'List',
        //     LIST1: 'Truck 01 list(2)',
        //     LIST2: 'ABC  list(2)',
        //     LIST3: 'ANZ list(3)',
        //     SUBMIT: 'Submit'
        // },
        LISTSETTING: {
            LISTSET: 'Manage List',
            PLACEHDR: 'Search List Name',
            DELETE: 'Select to Delete',
            SUBMIT: 'Submit',
            ERR_SEARCH: 'No List(name) matched your search!',
            ERR_INVALID: 'Please enter a valid List Name',
            SUCCESS_MSG: 'The List Name has been updated.',
            ERR_EXISTS: 'The list you had entered, already exists!',
            DELETED_MSG: 'Selected List(s) has been deleted.'
        },
        
        //--------------mylist/requestquote/sendrequestquote.html

        SENDREQQUOTE: {
            YRQR: 'Your request for quote is ready',
            SREQUEST: 'Send Request',
            SELITEM: ' 2 out of the 2 items selected from Truck 01 list',
            BTL: 'Back to list',
            MYLOC: 'My Location',
            ZIPCODE: 'Zip code to detemine to your ship to- location (as per address)',
            FAIRP: 'Fairport',
            RADCOVER: 'Radious coverd in and around zip code',
            DDL1: '25',
            DDL2: '30',
            DDL3: '35',
            MILES: 'miles',
            MYPREF: 'My Preferences',
            SDAT: 'spacify a date and time to recieve quotation by',
            EST: 'Est',
            SMNTQ: 'spacify maximum number of the quotation you would like to recieve for an item',
            TNQMYLOC: 'The number of quotation you would recieve for an item will be in the range of 1 to 3 depending upon disributors in and arround your location.',
            REQTOGGLE: ' Let me select disributor(s) send requests  for quote to',
            RFQ: 'RFQ',
            SAD: 'Select Approved Distributors',
            VPA: 'VIEW PARTS ADDED',
            D_COVERED : 'Distance Covered',
            DLR_TYPE : 'Dealer Type',
            ALL:'All',
            DISTRIBUTORS: 'Distributors',
            DEALERS: 'Dealers',
            SORT_BY: 'Sort By',
            NEAREST: 'Nearest',
            BY_NAME: 'By Name',
            EP_LIST: 'EDIT PART LIST',
            IWT_RECIEVE_QI: 'I want to receive quote(s) in',
            SND_RFQ_TO_AD : 'SEND RFQ TO APPROVED DISTRIBUTORS',
            D_SELECTED : 'Dealers Selected',
            RFQC_SUCCESSFULLY: 'Request for quote created successfully!',
            CS_BY_GTS: 'You can check the status by going to "Request for Quote Status" section. ',
            CRFQ_STATUS : 'CHECK RFQ STATUS',
            RFQ_DETAIL: 'RFQ DETAIL',
            GB_TO_MYLIST: 'GO BACK TO MY LIST',
            CANCELRFQ: 'CANCEL RFQ',
            CANCELLEDRFQ: 'CANCELED'
 
        },

        //-----------------mylist/requestquote/addedrfq/addedrfq.html

        ADDEDRFQ: {
            RFQ: 'RFQ',
            ADDED_PARTS: 'Added Parts',
            SN: 'Line item',
            PN_PN : 'Part No./Part Name',
            QTY: 'Quantity'
        },

        //-----------------mylist/rfqdetails/rfqdetails.html

         RFQDETAILS: {
            AFTERMARKET: 'AFTERMARKET',
            NOP: 'No of parts',
            QR_BY: 'Quote requested by',
            SN: 'Line item',
            PN_PN: 'Part No. / Part Name',
            DES_QTY: 'Desired Quantity'
        },

        //-----------------mylist/sessionlist/sessionlist.html

        SESSIONLIST: {
            DELETE: 'DELETE',
            SAVE: 'SAVE'
        },

        
         //--------------mylist/sendquoteconfirm/quoteconfirmation.html

        QUOTECONFIRM: {
            SUCCESSMSG: '3 Requests for  Quote sent successfully ! ',
            YCCSTATUS: ' you can check the status by going to "Request for Quote Status" section.',
            FAQSTATUSBTN: ' check RFQ status',
            RFQID: 'RFQ ID:',
            ID1: '345643224',
            PARTTYPTEMAIL: 'AXDE Autoparts, Phone +0 000 000 000, Email:sales@axde.com',
            CHECKBTN: 'check RFQ status',
            GOBACKBTN: 'Go back to my list',
            CONTINUESHOP: 'continue shopping'
        },

         DEALERLOCATOR: {
            SEARCHDEALERSHEADER: 'Search Dealers By Location',
            ZIPCODE: 'Specify Zip Code',
            USELOCATION: 'USE MY LOCATION',
            LOCATION: 'Search Approved Distributors',
            DEALERSNEARYOU: ' Distributor(s) Near You',
            ERRMSG: 'We are unable to find a distributor for entered zip code.',
            RFQERRMSG1: 'We are unable to find a distributor who can supply all of the parts in your list.',
            RFQERRMSG2: 'This might happen if parts in your list belong to multiple product lines or we dont have a distributor in your vicinity yet. Please try editing your part list or increasing distance covered.',
            RESULTS: 'Results ({{value}})',
            PRODUCTCATEGORY: {
                L06: "Automotive Axle",
                L05: "Automotive Driveshaft",
                P07: "Commercial Vehicle Drive Axle",
                L03: "Commercial Vehicle Drive Axle",
                P03: "Commercial Vehicle Drive Axle",
                L01: "Commercial Vehicle Driveshaft",
                L02: "Commercial Vehicle Steer Axle",
                P05: "Commercial Vehicle Steer Axle",
                P08: "Commercial Vehicle Steer Axle",
                L04: "Central Tire Inflation System (CTIS(tm))",
                P01: "Crate Axle",
                P02: "Crate Axle",
                L11: "GWB Industrial Driveshafts",
                L14: "SVL",
                L13: "SVL",
                L10: "Transfer Case"
            },
            UNIT: {
                mi: "Miles"
            },
            DISTANCECOVERED: 'Distance Covered',
            VALIDATEZIPCODE: 'Please enter a valid zipcode!',
            WD: 'WD',
            OE: 'OE'
        },

        
         //---------------wheretobuy/wheretobuy.html

        WHERETOBUY: {
            SEARCHDEALERSHEADER: 'Search Dealers By Location',
            ZIPCODE: 'Specify Zip Code',
            USELOCATION: 'USE MY LOCATION',
            ENTER_VALID_ZIPCODE: 'Please enter a valid zipcode!',
            DIST_COVERED: 'Distance Covered',
            DEALER_TYPE: 'Dealer Type',
            ALL: 'All',
            DISTRIBUTORS: 'Distributors',
            DEALERS: 'Dealers',
            SORT_BY: 'Sort By',
            NEAREST: 'Nearest',
            NAME: 'Name',
            PRODUCTS: 'Product',
            ALL_PRODUCTS: 'All Products',
            LOCATION: 'Search Approved Distributors',
            DEALERSNEARYOU: ' Distributor(s) Near You',
            ERRMSG: 'We are unable to find a distributor for entered zip code.',
            RFQERRMSG1: 'We are unable to find a distributor who can supply all of the parts in your list.',
            RFQERRMSG2: 'This might happen if parts in your list belong to multiple product lines or we dont have a distributor in your vicinity yet. Please try editing your part list or increasing distance covered.',
            RESULTS: 'Results ({{value}})',
            PRODUCTCATEGORY: {
                L06:"Automotive Axle",
                L05:"Automotive Driveshaft",
                P07:"Commercial Vehicle Drive Axle",
                L03:"Commercial Vehicle Drive Axle",
                P03:"Commercial Vehicle Drive Axle",
                L01:"Commercial Vehicle Driveshaft",
                L02:"Commercial Vehicle Steer Axle",
                P05:"Commercial Vehicle Steer Axle",
                P08:"Commercial Vehicle Steer Axle",
                L04:"Central Tire Inflation System (CTIS(tm))",
                P01:"Crate Axle",
                P02:"Crate Axle",
                L11:"GWB Industrial Driveshafts",
                L14:"SVL",
                L13:"SVL",
                L10:"Transfer Case"
            },
            UNIT: {
                mi: "Miles"
            }
        },
        
         //-------------------MYRFQ

        MYRFQ: {
            DETAIL: 'DETAIL',
            RFQID: 'RFQ ID',
            DD: 'Approved Distributor',
            RFQD: 'RFQ Deadline',
            RFQC: "RFQ Closed on",
            RFQI: 'RFQ initiated on',
            RFQS: 'RFQ Status',
            SENDDR: 'Send Dealer a reminder',
            CONFIRM: 'Confirm quote receipt',
            NOITEMSOPENRFQ: 'No Open RFQ(s)',
            //NOITEMSCLOSEDRFQ: 'No Closed RFQ(s)',
            NOITEMSCLOSEDRFQ: 'You currently have no closed RFQ(S)',
            REMINDERSENT: 'Reminder Sent!',
            PHONE : 'Phone',
            MOBILE : 'Mobile',
            EMAIL :' Email',
            APPROVED_DIST_MSG1 : 'Approved Distributors use their own communication medium e.g. email to send quotes. To follow up, you can click SEND REMINDER button. Please acknowledge and notify Approved Distributor' ,
            APPROVED_DIST_MSG2 : 'as well as Dana Aftermarket when you receive a quote by clicking CONFIRM RECEIPT button; an RFQ is moved from ‘Open RFQ’ to ‘Closed RFQ’ section thereupon.' ,
            SEARCHBYRFQ : 'Search by RFQ ID',
            STATUS: {
                DRAFT: 'Draft',
                SENT_TO_DISTRIBUTOR: 'Sent to Distributor',
                OPENED_BY_DISTRIBUTOR: 'Opened by Distributor',
                QUOTE_SENT_BY_DISTRIBUTOR: 'Quote Sent by Distributor',
                CLOSED: 'Closed',
                CANCELLED: 'Canceled',
                REMINDER: 'Reminder',
                CONFIRM_RECEIPT: 'Confirm Receipt'
            }
        },
        
         MANAGERFQ: {
            DETAILS: 'RFQ Details',
            TITLE: 'Manage RFQ',
            RFQID: 'RFQ ID',
            DATERCD: 'Date Received',
            DATECLD: 'Date Closed',
            DUEIN: 'Due In',
            RFQS: 'RFQ Status',
            ACTION: 'Action Quote Sent',
            NOITEMSOPENRFQ: 'You currently have no pending request(s) for quote. Enjoy your day!',
            //NOITEMSCLOSEDRFQ: 'No closed RFQ(s)',
            NOITEMSCLOSEDRFQ: 'You currently have no closed RFQ(S)',
            NOPARTS: 'No of parts',
            QUOTEREQBY: 'Quote requested by',
            SNO: 'Serial No.',
            PARTNOPARTNAME: 'Part no. / Part Name',
            DESQTY: 'Desired Quantity',
            NEWRFQS: 'NEW RFQS',
            OPENRFQS: 'OPEN RFQS',
            OVERDUERFQ: 'OVERDUE RFQ',
            PRINT: 'Print',
            ERRMSG: 'No Search Results!',
            SEARCHFOR: 'Search for RFQ ID',
            ALL: 'All',
            NEW: 'New',
            OPEN: 'Open',
            QUOTESENT: 'Quote Sent',
            QUOTESENT1: 'Quote Sent!',
            STATUS: {
                NEW: 'NEW',
                OPEN: 'OPEN',
                QUOTE_SENT: 'QUOTE SENT',
                CLOSED_BY_CLIENT: 'CLOSED BY CLIENT',
                CANCELLED_BY_CLIENT: 'CANCELED BY CLIENT',
                OVERDUE: 'OVERDUE',
                REMINDER: 'REMINDER'
            },
            DOWNLOADEXCEL: 'Download',
            DOWNLOADPDF: 'Download',
            APPROVED_DIST_MSG1: 'For sending quote to requester, please use your own communication systems e.g. email. Once a quote is sent,',
            APPROVED_DIST_MSG2: ' please click "QUOTE SENT" button to notify the requester and Dana Aftermarket'
  
        },
         PAGINATION: {
            FIRST: 'First',
            LAST: 'Last'
        },
        
        
        COMPLETEORDER : {
            ORDERNO : 'Order No.',
            VIEWORDDETAIL :'View Order Detail',
            PROCESSNO : 'Process No.',
            ORDERSUCESSMSG : 'Your order is placed. Thank you for your order!',
            ORDERSHIPEDMSG : 'It is being processed now. ',
            ORDERPROCCMSG : 'Your Order ID is not generated due to some internal error!',
            GETSTATUSHBYCUSTSUPPORT :'Your order processing in progress, you may wish to get the status of the order by contacting the customer support.',
            GOTOCART :  'GO TO CART',
            CONTACT_C_SERVICE: 'To edit or cancel the orders placed, please contact Customer Service.',
            CONTINUNINGSHOPING : 'CONTINUE SHOPPING'      
        },
 

       INCOMPLETEORDER : { 
            ORDERCOMP : '',
            LISTBELLOWIS : '',
           ORDERAVLISSUEACTION : 'For the items listed below, the availability has changed. Please take corrective action to proceed.',
            PARTNO : 'Part No./Part Name',
            ORDERQTY : 'Quantity',
            AVLQTY : 'Available Qty',
            BACKORDERQTY : 'Back Order',
            PLANTCODE : 'Plant Name',
            UNITPRICEUSD : 'Unit Price (USD)',
            EXTPRICEUSD : 'Extended Price (USD)',
            CORECHARGETOTLUSD : 'Unit Core Deposit (USD)',
            PRODTOTALUSD : 'Product Total (USD)',
            EDIT : "Edit Quantity",
            DELETE : "Delete",
            PROCEED_WITH_CO: 'Proceed with completed orders',
            ADD_IT_AS_BO: 'ADD IT AS A BACK ORDER',
            P_WITH_AVAIL: 'PROCEED WITH AVAILABLE',
            SO_SUMMARY: 'Stock Order Summary',
            EO_SUMMARY: 'Emergency Order Summary',
            NO_PARTS: 'No. of Item(s)',
            OT_USD: 'Order Total (USD)',
            PROCEED: 'PLACE ORDER',
            C_AND_GTC: 'GO TO CART',
            C_AND_CONT_SHOPPING: 'CONTINUE SHOPPING',
            NO_ITEM_IN_CART: 'There is no item in the Cart',
            GO_BACK_HOME: 'GO BACK TO HOME' 
 
        },

        PLACEORDER : {
            ORDERID : 'Order ID',
            VIEWORDERDETAILS : 'View Order Detail',
            SAVEITASORDERLIST : 'Save it as Order List',
            EMIALWITHSHIPMENTTRCKDETAILS : 'It is being processed now. when item(s) are shipped you shall recieve an email with shipment tracking details.',
            PLACEANOTHERORDER : 'Place Another Order',
            CONTINUEORDER :    'Continue Order',
            ACKDRECIEPTOFYOURORDER : "When your order is placed, we will send you an email message acknowledging reciept of your order.",
            NEEDHELPCONTCUSTCARESERVICE : 'To edit or cancel the orders placed, please contact Customer Service at',
            DANANORTHAMARICADCDEMAILID : 'dananorthamericadc@dana.com'
           
        },


        POERROR : {
           ERRORINPLACINGORDER : 'Error while Placing Order!'
 
        },

        ADVANCEDSEARCHPOPUP : {
            ADDRESS :  'Address',
            LINE1   :  'Line1',
            LINE2   :  'Line2',
            COUNTRY :  'Country'
        },

        PROCESSORDERPOPUP : {
            VERIFYINGAVLMSGPART1 : 'we are verifying availablity of',
            VERIFYINGAVLMSGPART2 : 'part(s) and other details one last time',
            VERIFYINGAVLMSGPART3 : 'Please stand by. Refreshing your browser would ',
            VERIFYINGAVLMSGPART4 : 'interrupt the process.'
        },
 
       PROCEEDMSG : {
            PROCEEDMSG1 : 'To edit or cancel the orders placed, please contact Customer Service ',
            PROCEEDMSG2 : "I understand, please do not repeat this alert!",
            PROCESSWITHORDER : 'Process with the Order'
        },
 
       SHIPPEDLIST : {
            USERCANCHANGE : 'user can change here!',
            QTY : 'Quantity',
            BACKORDERQTY : 'Back Order Quantity',
            UNITPRICEUSD : 'Unit Price (USD)',
            EXTENDEDPRICEUSD : 'Extended Price (USD)',
            CORECHAREGES : 'Core Deposit Total (USD)',
            PRODUCTOTUSD : 'Product Total (USD)',
            UOM : 'UOM',
            ADD : 'Add',
            UPDATE : 'Update'
    }, 

   ORDERMGMT : {
          BACK: 'Back ',
          ORDER : 'Order',
          UNIT : 'Unit',
          PRICEUSD :'Price (USD)',
          EXTENDED : 'Extended',
          CORECHARGES : 'Unit Core',
          DEPOSITUSDL :'Deposit (USD)',
          TOTALUSD : 'Total (USD)',
          ORDER_TOTAL: 'Order Total',
          PRODUCT :'Product',
          REQUESTED : 'Requested',
          SHIPTODATE : 'Ship Date',
          EDITQTY : 'Edit Quantity',
          REMOVE : 'Remove',
          ITEMCARTSTATUS: 'Your Shopping Cart is empty!',
          EMERGENCYORDERCART : 'Emergency Order Cart',
          HELPMSGTOCLINT : 'Pricing Note: Pricing displayed represents "Standard Net Price" and may not reflect program or promotional offerings. For any questions, please contact Customer Service at 1-800-621-8084.  Availability Note: Does not reserve inventory until the checkout process is complete.',
          HELPMSGTOCLINT1: 'Pricing displayed represents your current, agreed, every-day, “Standard Net Price", and does not reflect any special / promotional pricing or discounts that may be available. If you have questions, please contact Customer Service at 1-800-621-8084.',
          STOCKORDERSUMMARY : 'Stock Order Summary',
          EMERGENCYORDERSUMMARY :  'Emergency Order Summary',
          NOOFPARTS :  'No. of Item(s)',
          NUM_OF_PARTS: 'Total No. of Parts',
          CONTINUESHOPING : 'CONTINUE SHOPPING',
          SHIPFROM : 'Ship from',
          LOCATION : 'Location',
          CHECKOUT  : 'CHECKOUT',
          STOCKORDERCART : 'Stock Order Cart',
          CARTREVIEW: 'Shopping Cart Review',
          UOM: 'UOM',
          OUTOFSTOCK : ' OUT OF STOCK',
          CONFIRM_CHANGES: 'CONFIRM CHANGES'

           
 
    },

     expresscheckout : {
        MAINHEADER : 'Fast Order Entry',
        SEARCHBYPART : 'Search by Part No.',
        SERIALNO : 'Line No.',
        PARTNO : 'Part No/Part Name',
        QTY : 'Quantity',
        REQTOSHIPDATE : 'Requested Ship Date',
        UOM : 'UOM',
        EXTWGTH : 'Ext. Weight(Kg)',
        UNITPRICE: 'Unit Price (USD)',
        UNITCORE : 'Unit Core Deposit (USD)',
        EXTPRICE : 'Ext. Price (USD)',
        AVL : 'Available',
        ADDEDASBACKORDER :'added as Back Order',
        SELECTTOREMOVE : 'Select to remove',
        FOOTERMSG1 :'Pricing displayed represents "Standard Net Price" and may not reflet or promotional offerings. For any questions, please contact Customer Service at 1-800-621-8084.',
        FOOTERMSG2 :'Does not reserve inventory until the checkout is complete.',
        EXECEDWRNMSG1: 'Please verify Order Total',
        EXECEDWRNMSG2: '',
        MARKITEMREMOVEMSG: 'You have marked item(s) to remove.',
        REMOVESELECTED: 'REMOVE SELECTED',
        TOTALORDERQTY : 'Total Quantity',
        PRICINGNOTE: "Pricing Notes",
        AVLNOTE : 'Availability Note',
        CUSTPARTNO : "Cust. Part No."
       
    },
 
    ERRORMSG :{
        PARTNOTVALID:'Part is not valid!',
        NOTRTBERRORMSG: 'Please contact your Sales Representative <br>to add this product line',
        SUPPORTMSG: 'To edit or cancel the orders placed, please contact Customer Service.',
        DISCLAIMER: 'Disclaimer:',
        PROMOTIONALMSG:'Promotional pricing/discounts, if any, are not included in Order Total.',
        RTBSTASTUSTOKNOW :'To know your RTB status, please contact your Sales Representative.',
        SUPPORTMSGFORADDRES : 'To add a new address, please contact your Sales Representative.',
        EMIALADDRESSSELECTMSG : 'Please select at least one email address',
        EMERGENCYORDEREXPRESSSHIPING :'For emergency order, only Express shipping and FedEx as carrier is available',
        NOTSELETEDITEMFORNOTLOGGED :'A list is a temporary place to store your selected items, if you are not logged in.',
        FAILEDTOADDPRODUCTTOCART : 'Failed to add product to cart',
        CONTACT_REP : 'Please contact your Sales Representative to add this product line.',
        PASSWORDCHNAGEMSG : 'Your password has been changed. Try signing in with it here.',
        TEMPPASSWORDCHANGEMSG :'Temporary password has been resent',
        VALIDPAARTNOERRORMSG:  'Please enter a valid part number',
        OPPSSOMETHINGWENTWRONG : 'Opps! Something went wrong',
        TRYAGAIN : 'Try Again',
        SHAREVIAMAIL :'Share via mail',
        AFTERREMOVEMSG : "{{partno}} is removed from Cart.",
        PARTADDEDSUCCESSMSG : 'Part number {{partnumber}} added to the cart successfully',
        LISTTOPROCEEDMSG : 'Please create a list to proceed!',
        ORDERSTATUS : 'Your Order is not complete yet!'
   
    },
    
 
    SHIPTOSOLDPOPUP :{
         SIGNINSUCCESSFULL : 'Signin successful!',
         YOUHAVESIGNEDWITHA :  'You have signed with a',
         YOUHAVESELETEDA :     'You have selected a',
         SHIPTOCUSTOMER : 'Ship-to Customer Number',
         SOLDTOCUSTOMERTOPROCEED : 'please select an associated Sold-to customer to proceed.',
         ASSOCIATEDSLODTOCUSTOMER : 'Associated Sold-to customers ',
         SELECT :'SELECT'
 
    },
 
    RFQLIST :{
        RFQID213224234 : 'RFQ ID 213224234',
        TODAY24HRSAGO : 'Today 2hrs ago',
        NEW : 'New',
        URLCOPIEDCLIPBOARD : 'URL is copied in clipboard',
        YOURORDERLISTURL : 'Your Orderlist URL'
    },
 
    common : {
        COPYURL : 'Copy URL',
        URL : 'URL' ,
        SOLDTO : 'Sold-to',
        SHIPTO : 'Ship-to',
        EXT : 'Ext.',
        WEIGHT :'Weight',
        KG : '(kg)',
        CONFIRMCHANGES : ' CONFIRM CHANGES',
        ENTERZIPCODE:  'Enter zip code'
    },
 
    BOM : {
        BOMNO : 'BOM No',
        LEVEL : 'Level',
        COMONENTNAME : 'Component Name',
        QTY : 'Quantity'
    },
      PLACEHOLDERALL : {
        SEARCHORDERBYPARTNO : 'Search Orders by Parts No./Part Name',
        SEARCHPARTNO : 'Search by Part No./Part Name',
        SEARCHLISTBYNAME : 'Search list by list name',
        SEARCHLISTBYPARTNO : 'Search Lists by Part No./Part Name'
    },
   TOOLTIP : {
         PLEASEVERFYPARTQTY : '请验证零件数量'
   }


        
    }
}
