/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export let localization = [
  english,
  french,
  chinese
]

import {english} from './en.js';
import {french} from './fr.js';
import {chinese} from './zh.js';
