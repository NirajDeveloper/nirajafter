/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export let chinese = {
    active: false,
    langName: 'zh',
    dispName: "Chinese - CNY",
    image: "assets/images/localization/china.gif",
    data: {
        CURRENCY: "USD",
        INDEX: {
            TITLE: 'DANA售後市場'
        },
        HEADER: {
            SITENAME: '售后市场',
            DISCUSSION: "讨论区",
             WHERETOBUY: '去哪买',
            MENUS: {
                HELPNFAQ: {
                    HELPLINE: '达纳 热线 1-800-621-8084',
                    HELP: "帮帮我",
                    FAQ: '常问问题'
                },
                LANGNCURRENCY: {
                    ENGLISHUSD: 'Chinese - CNY'
                },
                ORDER: {
                    TRACK: '跟踪',
                    ORDERS: '命令',
                    ORDER: '订购',
                    LISTS: '清单'
                },
                PROFILE: {
                    ZERO_NOTIFICATION: '0通知！',
                    SIGNIN: '签到',
                    REGISTER: '寄存器',
                    USERP: '用户资料',
                    WISHLIST: '愿望清单',
                    ORDERLIST: '订单',
                    MYLIST: '我的列表',
                    LIST: '列舉',
                    REPORT: '彙報',
                    LOGOUT: '登出',
                    CART: '大车',
                    GREETINGS_TXT: '嗨!',
                    MYRFQ: '我的詢價',
                    RFQS: '詢價',
                    MYORDERS: '我的订单',
                    NOTIFICATIONS : '通知',
                    CUSTNO : '卡斯特無 {{selectedCustid}}',
                    CUSTNO_TXT: '卡斯特無 ',
                    CLIST_CNUM_LBL1: '顧客',
                    CLIST_CNUM_LBL2: '不是!',
                    CLIST_DCODE_LBL1: '零售商',
                    CLIST_DCODE_LBL2: '略碼',
                    CLIST_CNAME_LBL1: '顧客',
                    CLIST_CNAME_LBL2: '名稱',
                    CLIST_SRCH_PH: '由卡斯特否/經銷商代碼/客戶名稱搜索'
                },
                ADVANCEDSEARCH: {
                        ADVANCEDSEARCHYMM: '汽车按年 - 制作 - 型号',
                        INTERCHANGE:'立交',
                        YEAR:'年',
                        MAKE:'使',
                        MODEL:'模型'
                },
                INTERCHANGE: {
                    PLACEHDR: '键入交换部件号',
                    GO:'走'
                }
            }
        },
        FOOTER: {
            DANAHCORP: '2016 达纳有限公司',
            ABOUT: '关于',
            TERMS: '使用条款',
            POLICY: '隐私政策',
            CONNECT: '聯繫我們'

        },
        BREADCRUMB: {
            BACKSECTION: '返回搜索结果',
            FOR: '对于',
            RESULTFOR: '结果',
            OF: '的',
            ALL: '所有',
            SORT: '排序方式 :',
            MYLISTS: '我的列表',
            HOME: '家',
            SND_RQST_FOR_QUOTE: '发送请求给报价单',
            MYORDERS : '我的订单',
            INVOICEDETAILS: '发票明细',
            ORDER_DETAILS: '订单详细信息',
            ORDER_STATUS: '订单状态',
            REPORTS: '报告'
        },
        LOADING: {
            LOADINGTEXT: '炉料...'
        },

        //-------------------component/sending-----------------//

        SENDING: {
            SENDINGTEXT: '邮件....'
        },

        // ------------------components/header/searchbar------------------//

        TYPEHEADPOPUP: {
            EXACTMATCH: '完全符合',
            CLAOSEMATCH: '关闭匹配',
            SUGGESTION: '建议'
        },
        TYPEAHEAD: {
            SEARCHBYYMM: '按年份/品牌/型号搜索',
            SEARCH_PLACEHOLDER: "搜索产品型号，产品类型，产品线，关键字，立交，年份，品牌，型号"

        },

        //--------------------------orderlist----------------------------//

        ORDERLIST: {
            ORDERLISTHASH: '订单 ＃',
            PART: ' 部分',
            PARTS: ' 部分',
            FUTUREREFRENCE: '分享以供将来参考顺序列表',
            SHAREORDERLIST: '分享订单清单',
            CAPSONPARTS: '部分',
            CAPSOFFPARTS: '部分',
            REMOVEPART: '删除部分',
            EDIT: '编辑',
            SAVE: '保存',
            REMOVECROSS: 'X',
            ERRORMSG: '量应为正数',
            NOORDERLIST: '没有可用的订单列表'
        },

        //--------------------------orderlist/shareOrderlist----------------------------//

        SHAREORDERLIST: {
            ORDERLISTHASH: '订单 ＃',
            SHAREDMSG: '一旦共享，订单列表将不会被编辑',
            FROM: '从',
            FNAME: '名字',
            MANDATORY: '*',
            FNAMEMSG: '请输入您的名字',
            LNAME: '姓',
            LNAMEMSG: '请输入您的姓氏 ',
            FEMAIL: '您的电子邮件ID',
            REQUIREEMAILMSG: '请输入您的电子邮件',
            ERROREMAILMSG: '请输入正确的电子邮件 ID /标识',
            PHONENO: '你的电话号码',
            TO: '至:',
            TEMAIL: '您的电子邮件ID',
            TEXTAREA: '嗨，我发现danaaftermarket.com一些地区订购。请看看我的订单列表。',
            CALLBACK: '您希望我们回电',
            // GETURL:'Get URL',
            // GOTOHOME:'Go to Home',
            SHAREORDERLIST: '分享订单清单',
            FIRTNAMEPLACEHDR:'名字',
            LASTNAMEPLACEHDR:'姓',
            YOUREMAILPLACEHDR:"您的电子邮件标识",
            PHONENOPLACEHDR:'你的电话号码',
            TOEMAILPLACEHDR:"以电子邮件ID",
            TEXTAREAPLACEHDR:'任何消息',
            INVALID: '请输入一个有效的电子邮件ID！',
            SUCCESS: '名单已成功共享！',
            TOADDRESSLABEL: '名单已成功共享！',
            CLR: '明确',
            SND_COPY_OF_EMAIL: '我发送此电子邮件的副本',
            SND_EMAIL: '发电子邮件',
            SHARE: '分享',
            VIA_EMAIL: '通过电子邮件',
            PLACEHOLDER: "收件人的电子邮件ID的以逗号分隔",
            BODY_PLACEHOLDER: '在这里输入您的个人信息'   
        },

         //--------------------------orderlist/sharedOrderlist----------------------------//

        SHAREDORDERLIST:{
             PARTS: '部分'
        },

        //-------------------------orderlist/ShareConfirmation---------------------------//
        SHARECONFIRM: {
            ORDERLIST: '订单 ',
            PART: '部分',
            PARTS: '部分',
            CONFIRMATIONMSG: '您的订单列表共享',
            GOTOHOME: '回家'
        },

        //----------------------part/email-------------------------//
        EMAIL: {
            EMILHEADING1: '电子邮件部分',
            EMILHEADING2: '电子邮件部分',
            FROM: '从',
            EMAILID: '您的电子邮件标识',
            MANDATORY: '*',
            EMAILREQUIRE: '请输入您的电子邮件',
            EMAILVALIDATE: '请输入正确的电子邮件 ID /标识',
            TO: '至:',
            SUCCESSMSG: '邮件发送成功',
            SEND: '发送',
            COPYME: '向我发送此电子邮件的副本',
            MULTIEMAILPLACEHOLDER:"收件人的电子邮件ID以逗号分隔.",
            SUBJECT  : '以下产品已与您共享',
            BODY  : "我发现这部分在http：// phasezeroxyz /结帐请它的详细信息" 
        },

        //------------------part/bomtable-----------------//

        // BOMTABLE: {
        //   BOMNO. : 'BOM No',
        //   LEVEL : 'Level',
        //   COMPONENT : 'Component Name',
        //   QTY : 'Quantity'
        // },

        //------------------part/compatiblitytable-----------------//

        COMPATIBILITY: {
            YEAR: '年',
            MAKE: '使',
            MODEL: '模型',
            FITMENTNOTES: '装备注释',
            LOCATION: '位置'
        },

        //------------------part/interchangestable-----------------//

        INTERCHANGES: {
            NAME: '名称',
            PARTNO: '零件号',
            TYPE: '类型'
        },

        //------------------part-----------------//

        PART: {
            ELEARN: '线上学习',
            CATALOG: '零件目录',
            SERVICESMANNUAL: '服务手册',
            INSTALLATION: '部分安装',
            SPEC: '部分规格',
            TEXT1: '画报零件清单 Dana',
            TEXT2: '斯派塞驱动轴S110 画报零件清单',
            DESCRIPTION: '描述',
            EMAIL: '电子邮件',
            HUBTEXT1: '行业领先的中枢系统技术',
            HUBTEXT2: '减少了维护和改进的责任',
            TEXT3: '目前这款机型的先进部分',
            TEXT4: '202BN103-25，重轴，转向轴，转向桥总成E1202I（69 KPI）',
            ADDTOORDERLIST: '加入顺序列表',
            ADD: '加',
            VALIDMSG: '量应为正数。',
            WARRENTY: '保证:',
            TEXT5: '3年/350000英里（560000公里）',
            WARRENTYMATRIX: '见保修矩阵',
            MDS: '模型图和规格',
            PAIRSWITH: '与对',
            PARTDETAIL: ' 斯派塞，转向轴，E1002I，E系列，10,000lbs，重型主销联合，2- Drawkeys，...',
            ALOSBOUGHT: '您还购买了',
            IDENTIFYYPART: '找出你的一部分',
            SMDTAB: '规格及模型图',
            CWATAB: '与应用程序兼容',
            EVBOMTAB: '分解图和BOM',
            INTERCHANGES: '互通式立交',
            BOMNO : 'BOM号码',
            LEVEL : '水平',
            COMPONENTPARTNO: '组件部件号',
            COMPONENT : '组件名称',
            QTY : '数量',
            qty:'数量',
            POPDESCUSSION: '热门讨论',
            VIEWALL: '查看全部（31）',
            SECTIONTITLE: '这是我在公路车队正确的选择？',
            //SECTIONDESCRIPTION: 'Dana/Spicer has always had a good product to my knowledge and usually backed it up better on parts/warranty.
            //    BTW, noticed one of TMCs new trucks the other day..... white in color. Had to take the second look'
            READMORE: '阅读更多...',
            ANSBY: '通过回答',
            BLACK900: '黑w900',
            DAYS: ', 7 几天前',
            INTERCHANGES_TABLE: {
                COL1: '生产厂家',
                COL3: '脚注',
                COL2: "制造商的零件号",
                COL4: '达纳品牌',
                COL5: '达纳型号',
                COL6: '产品类别',
                SUPERSEDED: '取代'
            },
            CHECKAVBLT: '检查可用性',
            ADDTOLIST: '添加到列表',
            PRINT :'打印',
            PARTTOOLTIPMSG1 : "作为注册用户，您已经添加的部分后列出，您可以发送询价要订购这一部分，",
            PARTTOOLTIPMSG2 :  "添加零件清单，保存或分享请求报价"
        },

        //------------- searchresults/filter-----------------------//

        FILTER: {
            FILTERALLCATEGORY: '类别',
            REFINE: '缩小搜索范围',
            TOOLTIP:"选择一个类别，看看过滤器"
        },

        //------------- searchresults/partcard-----------------------//

        PARTCARD: {
            CORRESMATCH: '互换',
            ADDTOORDERLIST: '加入订单列表',
            ADDTOLIST: '添加到列表',
            ADDTOCART: '添加到购物车',
            REMOVEITEM: '除去项目',
            ADD: '加',
            SHOWMSG: '加入订单列表',
            ADDEDTOLIST: '添加到列表！',
            VALIDMSG: '量应为正数.',
            INVALID_PACKAGE_QTY: 'Available only in packages of ',
            SPECIFICATIONS: '产品规格',
            CALL: '请联系 达纳热线号码：1-800-621-8084',
            QTY: '数量:',
            CHKAVL: '检查可用性',
            SAVELIST: '保存列表',
            SHARELIST: '共享列表',
            CHECKAVAILABILITY: '检查可用性',
            GOTOLIST: '转到列表',
            ADDEDTOCART : '添加到购物车.',
            CREATENEWLIST : '创建新列表',
            UNIT_PRICE_LBL: '单价:',
            CORE_UNIT_LBL: '核心单位存款:',
            STK_PRICE_NOTE: '* 股票价格秩序',
            USD_TXT: '美元'
        },

        //------------- searchresults/partcard/popover-----------------------//
        POPOVER: {
            WishList: '加入收藏',
            //CONTENT: 'content',
            //TITLE: 'Popup Title:',
            SPECIFICATIONS: '产品规格',
            MMS: 'MMS',
            TM: 'TM',
            HUB: 'Hub'
        },


        //------------- searchresults/selected-filter-----------------------//

        SELECTEDFILTER: {
            CLOSE: 'X'
        },

        //------------- searchresults/ymm-----------------------//
        YMM: {
            SEARCHBY: '通过搜索 ',
            SEARCHBYMMY: '通过制作/模型/年的搜索',
            MAKE: "使",
            APPLY: '应用',
            GO: '走',
            VEHICLE:'您的车辆'
        },


        //------------------ searchresults-----------------------//

        SEARCHRESULT: {
            SHOWING:'显示',
            MATCHMSG: '匹配交换部件号',
            ZERORESULT: '找到0个结果.',
            REDEFINEEXPLORE: '请细化搜索词，或探索通过搜索栏的部件.',
            CLEAR_ALL: '全部清除',
            SEARCHFOR_TEXT: '您的搜索'
        },
        
         //till that it's added in devsearch //

         //Again we have to start from//

        //-----------------Login -------------------  //
        LOGIN: {
            LOGINTOPROCEED: '请登录才能继续进行！',
            DANACUSTOMER: '达纳客户',
            BECOMEDANACUSTOMER: '成为达纳客户',
            REGISTEREDUSER: '注册用户',
            REGISTERASUSER: '注册成为用户',
            QUOTEMSG: '您可以要求从授权分销商达纳收到报价为所需的数量。为了能够下订单，请',
            REGISTERASDANACUSTOMER: '注册成为达纳客户',
            TOOLTIP1:'成为达纳客户 - TBD',
            TOOLTIP2:'注册成为用户 - TBD',
            CUSTOMERUSER: 'DANA 用户',
            ACCOUNTLOGIN: '帐号登录',
            CUSTOMERID: '客户ID',
            CUSTOMERID_VALIDATION: '客户ID是强制性',
            USERNAME: '用户名',
            USERNAME_VALIDATION: '用户名是强制性的',
            PASSWORD: '密码',
            PASSWORD_VALIDATION: '密码是必需的',
            LOGIN: '登录',
            FORGOT_PASSWORD: '忘记密码',
            REMEMBERME: '记住我',
            NON_USERS: 'Dana 非用户',
            REGISTERED_USER: '登记用户',
            NEW_TO: '新到',
            DANA_AFTERMARKETS: "Dana售後市場？",
            INFO_DANAUSER: '請註冊為註冊用戶，以從授權分銷商處接收報價並跟踪狀態',
            REGISTERASDANAUSER: '註冊為註冊用戶',
            DANACUST: '註冊為Dana客戶',
            INFO_DANACUST: '訪問您的訂單，檢查價格和下訂單。',
            INFO2_DANACUST: '要成為Dana客戶，請聯繫Dana銷售團隊',
            CONTACT_NUMBER: '1-800-621-8084或',
            EMAIL_ADDRESS: 'dananorthamericadc@dana.com',
            SIGNIN: '簽到',
            USER_NAME: '登錄ID',
            USERNAME_PHOLDER: '輸入登錄ID',
            USER_NAME_VALIDATION: '輸入登錄ID',
            PASSWORD_PHOLDER: '輸入密碼',
            CUSTOMER_ID: '顧客號碼',
            CUSTID_PHOLDER: '顧客號碼',
            CUSTID_INFO: '僅適用於Dana客戶',
            LOGIN_BTN: '登录',
            FIELD_SHORT_ERROR: "輸入的值過短",
            INVALID_FORMAT: "無效的格式",
            LOGIN_ID_ERROR: "請輸入有效的登錄ID",
            INVALID_PASSWORD: "請輸入有效密碼",
            INVALID_CUST_ID: "請輸入正確的客戶號碼",
            CONNECT_US: '聯繫我們',
            SESSION_EXPIRED: '會話過期！'
        },
        
        //------------------ Signup ----------------------------------//
        SIGNUP: { 
            TITLE: '註冊',
            PHONE: '电话 *',
            PHONE_MOBILE: '(运动物体)',
            PHONE_ERR_MSG: '请输入一个有效的电话号码',
            SIGNIN_BTN: '现有用户？',
            SIGNUP_REG_USER: '注册成为注册用户',
            DUSER_INFO1: '从达纳经销商报价请求创建列表',
            DUSER_INFO2: '发送请求报价到多个分销商达纳',
            DUSER_INFO3: '跟踪请求状态',
            DUSER_INFO4: '查找经销商达纳您所在地区',
            CONNECT_US: '联系我们',
            LOGIN_ID: '登录ID *',
            LOGIN_ID_ERR_MSG: '请输入有效的登录ID',
            ID_AVAIL: '可得到',
            ID_EXISTS1: '已在使用中，',
            ID_EXISTS2: '尝试一个又一个！',
            PASSWORD: '密码',
            PASSWORD_VALIDATION_MSG: '密码是必需的',
            PASSWORD_ERR_MSG: '请输入有效密码',
            PASSWORD_SUGGEST: '密码长度必须至少为8个字符，并且至少包含一个数字和一个',
            TXT_STRONG: '强大',
            FIRSTNAME: '名字',
            FIRSTNAME_VALIDATION_MSG: '名字是强制性',
            FIRSTNAME_ERR_MSG: '请输入名字',
            LASTNAME: '姓',
            LASTNAME_ERR_MSG: '请输入姓氏',
            EMAIL: '电子邮件*',
            EMAIL_ERR_MSG: '请输入有效的电子邮件地址',
            RE_EMAIL: '重新写邮件 *',
            RE_EMAIL_ERR_MSG: '电子邮件内容不符',
            ZIP_CODE: '邮政编码 *',
            ZIP_CODE_ERR_MSG: '请输入有效的邮政编码',
            COMPANY_NAME: '公司名 *',
            COMPANY_NAME_ERR_MSG: '请输入公司名称',
            COUNTRY: '国家 *',
            COUNTRY_ERR_MSG: '请选择一个国家',
            CREATE_ACCOUNT: '创建帐号',
            ALL_FIELDS: '各个领域都需要.',
            USER_INFO : '作为注册用户，这里是当你在我们网站上你可以做什么',
            SEARCHPARTS : '搜索产品',
            USEOURPOWERFULSEARCH: '使用我们的强大的搜索',
            OPTIONPARTQUICKLY :' 选项可以快速识别你的一部分',
            ADDTOLIST : '添加到列表',
            CREATEANDMANAGE : '创建和管理',
            MULTIPLELIST : '多個列表。創建RFQ',
            FROMPARTADDEDTOLIST : '從添加到列表的部分。 。',
            SENDRFQ : '發送詢價',
            FINDDISTRIBUTORS : '查找經銷商',
            YOURAREARFQ  : '您的區域並發送詢價。',
            TRACKRFQ : 'TRACK詢價',
            TRACKSTATUS :'跟踪您的狀態',
            RFQQUOTES: 'RFQ，直到你收到報價。',
            SIGNUPSUCCESSMSG : '嗨! {{firstname}}! 您已成功註冊。'
           
        },
        
        //------------------ FORGOT-PASSWORD ----------------------------------//
        FORGOT_PSWRD: {
            PSWRD_ASSIST:{
                TITLE: '密碼幫助',
                INSTRUCTION: '輸入與售後市場帳戶相關聯的電子郵件地址。',
                LABEL : '電子郵件地址',
                ERROR: '請輸入有效的電子郵件地址。',
                BUTTON: '繼續',
                FOOTER_TXT1: '貴的e-mail地址改變了嗎？',
                FOOTER_NUM: '1-800-621-8084',
                FOOTER_EMAIL: 'dananorthamericadc@dana.com',
                OR_TXT: '或',
                FOOTER_TXT2: '顾客服务',
                FOOTER_TXT3: '寻求帮助恢复访问您的帐户'
            },
            VERIFY_OTP:{
                TITLE: '重设密码',
                INSTRUCTION_TXT1: "为了您的安全，我们需要验证您的身份",
                INSTRUCTION_TXT2: '请输入发送到您的电子邮件一次性临时密码',
                LABEL: '临时密码',
                ERROR: '临时密码无效。',
                BUTTON: '繼續',
                RESEND_LNK: '重新发送临时密码',
                FOOTER_TXT1: '您的电子邮件地址是否已更改？如果您不再使用与售后市场帐户关联的电子邮件地址，您可以联系',
                FOOTER_TXT2: '客户服务',
                FOOTER_TXT3: '以帮助恢复对您帐户的访问权限。'
            },
            RESET_PSWRD:{
                TITLE: '创建新密码',
                PSWRD_SUGGESTION: '码长度必须至少为8个字符 和包含至少一个数字和一个符号.',
                LABEL_NEW_PSWRD: '新密码',
                NEW_PSWRD_ERR: '请输入有效密码。',
                LABEL_CONFIRM_PSWRD: '确认密码',
                CONFIRM_PSWRD_ERR: "密码不匹配。",
                BUTTON: '保存更改'
            },
            TITLE: '注册',
            PHONE: '电话 *',
            PHONE_ERR_MSG: '请输入一个有效的电话号码',
            SIGNIN_BTN: '现有用户？签到',
            SIGNUP_REG_USER: '注册为注册用户',
            DUSER_INFO1: '创建从Dana分销商请求报价的列表。',
            DUSER_INFO2: '向多个Dana经销商发送报价请求。',
            DUSER_INFO3: '跟踪请求状态。',
            DUSER_INFO4: '查找您所在地区的Dana经销商。',
            CONNECT_US: '联系我们',
            LOGIN_ID: '登录ID *',
            LOGIN_ID_ERR_MSG: '请输入有效的登录ID。',
            ID_AVAIL: '可用！',
            ID_EXISTS1: '已在使用中，',
            ID_EXISTS2: '尝试另一个！',
            PASSWORD: '密码*',
            PASSWORD_ERR_MSG: '请输入有效密码。',
            PASSWORD_SUGGEST: '密码长度必须至少为8个字符，并且至少包含一个数字和一个符号。',
            TXT_STRONG: '强大',
            FIRSTNAME: '名字 *',
            FIRSTNAME_ERR_MSG: '请输入名字',
            LASTNAME: '姓 *',
            LASTNAME_ERR_MSG: '请输入姓氏',
            EMAIL: '电子邮件*',
            EMAIL_ERR_MSG: '请输入有效的电子邮件地址',
            RE_EMAIL: '重新写邮件 *',
            RE_EMAIL_ERR_MSG: '电子邮件内容不符',
            ZIP_CODE: '邮政编码 *',
            ZIP_CODE_ERR_MSG: '请输入有效的邮政编码',
            COMPANY_NAME: '公司名 *',
            COMPANY_NAME_ERR_MSG: '请输入公司名称',
            COUNTRY: '农村的 *',
            COUNTRY_ERR_MSG: '请选择一个国家',
            CREATE_ACCOUNT: '报名',
            ALL_FIELDS: '各个领域都需要'
        },

        //------------------ USER PROFILE ----------------------------------//
        PROFILE_ERROR:{
            CUSTOMER_ID_REQUERIED: '客户没有需要你输入登录ID！',
            AUTH_LOGIN_FAILED: '客户没有您输入不挂登录ID，请确认并重',
            INVALID_LOGIN_ID: '您输入登录ID和密码不匹配，请确认并重试',
            PASSWORD_WRONG: '您输入登录ID和密码不匹配，请确认并重试',
            LOGIN_FAILED: '您输入登录ID和密码不匹配，请确认并重试',
            CUSTOMER_ID_EMPTY: '客户编号不要求你输入登录ID！',
            USER_RESET_PASSWORD_NOT_ALLOWED: '对不起，重置密码, 一个达纳客户应与客户服务 800-621-8084.',
            INTERNAL_SERVER_ERROR: '有一个内部服务器错误, 请再试一次!',
            USER_EXIST: '用户已经存在',
            USER_NOT_EXIST: '用户不存在',
            PASSWORD_MISMATCH: '密码应匹配',
            INVALID_CREDENTIALS: '您输入登录ID和密码不匹配，请确认并重试',
            EMAIL_NOT_EXIST: '可得到',
            EMAIL_EXIST: '电子邮件地址已在使用',
            OTP_WRONG: '您输入临时密码不正确，请确认并重试！',
            OTP_CORRECT: '验证成功！',
            PASSWORD_NOT_RESET: '不更改密码',
            INVALID_EMAIL_ID: '您输入的电子邮件ID没有被注册，请确认并重试！',
            OTP_EXPIRED: '临时密码已过期',
            PROFILE:{ 
                PROFILE_UPDATED_SUCCESSFULLY: '你的个人资料已经更新！',
                PSWRD_CHANGED_SUCCESSFULLY : '您的密码已被更改！',
                INVALID_CREDENTIAL: '您输入当前的密码不正确，请确认并重试！',
                INTERNAL_SERVER_ERROR: '有一个内部服务器错误，请重试！'
            }
        },

        PROFILE: {
            TITLE:'我的帐户',
            BC_HOME:'家',
            TAB_PROFILE: '我的简历',
            TAB_PASSWORD: '更改密码',
            PROFILE_TITLE: '个人信息',
            PASSWORD_TITLE: '更改密码',
            PHONE: 'Phone手机',
            PHONE_ERR_MSG: '请输入一个有效的电话号码',
            PHONE_WORK: '工作电话',
            PHONE_WORK_ERR_MSG: '请输入一个有效的电话号码',
            ADDRESS_1: '地址 1',
            ADDRESS_1_ERR_MSG: '请输入地址',
            ADDRESS_2: '地址 2',
            FAX: '传真',
            OPTIONAL: '选修的',
            LOGIN_ID: '登录ID',
            LOGIN_ID_ERR_MSG: '请输入有效的登录ID',
            ID_AVAIL: '可得到!',
            ID_EXISTS1: '已在使用中，',
            ID_EXISTS2: '尝试一个又一个！',
            PASSWORD_SUGGEST: '密码长度必须至少为8个字符，并且至少包含一个数字和一个',
            FIRSTNAME: '名字',
            FIRSTNAME_ERR_MSG: '请输入名字',
            LASTNAME: '姓',
            LASTNAME_ERR_MSG: '请输入姓氏',
            EMAIL: '搪瓷',
            EMAIL_ERR_MSG: '请输入有效的电子邮件地址',
            ZIP_CODE: '邮政编码',
            ZIP_CODE_ERR_MSG: '请输入有效的邮政编码.',
            COMPANY_NAME: '公司名',
            COMPANY_NAME_ERR_MSG: '请输入公司名称.',
            CITY: '市',
            CITY_ERR_MSG: '请输入有效的城市.',
            CRRENT_PSWRD: '当前密码 *',
            CRRENT_PSWRD_ERR_MSG: '请输入有效密码.',
            NEW_PSWRD: '新密码 *',
            NEW_PSWRD_ERR_MSG: '请输入有效密码.',
            RE_PSWRD: '重新输入新密码 *',
            RE_PSWRD_ERR_MSG: '密码不匹配.',
            STATE: '州',
            STATE_ERR_MSG: '请选择一个国家.',
            COUNTRY: '国家',
            COUNTRY_ERR_MSG: '请选择一个国家.',
            PREFER_MODE: '沟通的首选方式',
            PREFER_MODE_ERR_MSG: '请选择通信的首选模式.',
            SAVE_CHANGES: '保存更改',
            STARRED_FIELDS: '* 字段为必填.',
            ALL_FIELDS: '各个领域都需要.',
            REPORTS: '报告',
            D_CUSTOMER: '达纳客户',
            DOWNLOAD_EXCEL: '下载',
            SHOP_USER: '店的',
            CREATEUSER :'创建用户',
            CREATECUSTOMERUSER : '创建客户用户',
            CUSTOMERNUMER : '客户编号.',
            CREATE : '创建',
            MOBILEPHONE : '移动电话',
            WORKPHONE : '工作电话',
            EMAILONLY : '搪瓷'
        },
        
        //-------------------------Pricing and Availability -------------//
        REPORT:{
            'CUST_USER' :{
                customerNumber: '顾客号码',	
                companyName: '公司名',
                dealerCode: '经销商代码',
                userId: '用户名',	
                firstName: '名字',
                lastName: '姓',
                emailId: '电子邮件',
                lastLogInDate: '上次登录日期',
                CanFinalizeOrder: '可以敲定订单',
                CanSeePrice: '可以看到价格'
            },
            'SHOP_USER' :{
                userId: '用户名',	
                firstName: '名字',
                lastName: '姓',
                emailId: '电子邮件地址',
                lastLogInDate: '上次登录日期'
            }     
        },
        
        PRICINGANDAVAILABILITY: {
            INSTRUCTIONRTB: '为了选择类型并输入数量，检查可用性和部分随后添加到购物车.',
            ORDERTYPE: "订单类型:",
            STOCKORDER: '股票订单',
            ORDERTYPE1: '股票订单',
            ORDERTYPE2: '紧急命令',
            ORDERTYPE3: '测试订单',
            ORDERTYPE1_DELIVERY: '正常分娩，按可用性',
            ORDERTYPE2_DELIVERY: '快速轨道交付，需付费',
            
            NORMALDELIVERY: '正常分娩，按可用性',
            EMERGENCYORDER: '紧急命令',
            FASTTRACKCHARGESMSG: '快速轨道交付，需付费',
            DESIREDQTY: '所需数量',
            CHECKAVAILABILITY_S: '查看价格和可用性',
            CHECKAVAILABILITY: '检查可用性',
            UPDATEAVAILABILITY: '更新可用性',
            QTYVALIDATIONMSG: '量应为正数',
            PARTNO: '一部分 数',
            CHANGE: '更改',
            UNITOFMEASUREMENT:'UOM',
            PACKAGE: '包',
            EACH: 'EA',
            TOTALQTYAVAILABLE: '总数量',
            FROM: '从',
            LOCATION: '位置',
            LOCATIONERRORMSG:'选定的位置不满足所需量',
            EMG_QTY_AVAIL_ERR: '你所需的数量不可用，请联系客户服务',
            LOCATIONHEADER: '位置',
            QTYHEADER:'现有数量',
            QTY_AVAIL: '可得到',
            SHIPPINGDATEHEADER: '预计运送日期',
            SHIPPING_DATE: '发货日期',
            UNITPARTPRICE: '单位单价 ',
            EXTENDEDPRICE: '扩展价格 ',
            COREUNITPRICE: '单位核心存款* ',
            CORECHARGE: '核心充电 ',
            PACKAGEPRICE: '套餐价 ',
            ADDTOCART: '添加到购物车',
            ADDTOLIST:'添加到列表',
            ADD: '加 ',
            TOTHECART: '至购物车 + ',            
            ADDTOBACKORDER: '要晚点才能',
            PART_NOT_AVAILABLE: '哎呀！不可用部分 :(',
            PART_NOT_AVAILABLE_INFO: '您可以选择添加所需数量的延交订单',
            CONTACTREPSENT : '请联系您的达纳代表订购该部分或联系客服',
            ACCT_ORDER_AUTHORISED :  '看来你的帐户无权下令这部分',
            AUTHORIZATION_REQD_FOR:'需要授权',
            PLACING_STK_ORDER: '配售股票订单',
            SELECT_LIST:'选择一个列表',
            NO_LIST: '没有列表！',
            CREATE_NEW_LIST: '创建新的列表',
            CROSSVILLE : '克罗斯维尔',
            STANDARDLEADTIME :'标准交付周期',
            ESTIMATED_DATE: '预计运送日期：',
            BTO_MSG: ' 可以通过',
            UPTO : '取决于 ',
            BTO_TOOLTIP: '定制的为您服务！',
            SLTI_MSG: ' 其余的车',
            PARTIAL_MSG: '在......之外 {{total}}(qty), 只有 {{partial}}  将从此位置挑选以匹配所需数量.',
            SYSTEM_ERR_MSG: '应用遇到了一个错误，而检索库存情况.',
            SYSTEM_PRICING_ERR_MSG: '应用遇到了一个错误，而价格检索.',
            TRY_AGAIN: '再试一次',
            DESIREQTY : '欲望数量',
            AVAILABILITY: {
                ADD_TO_CART_AS_BACK_ORDR: ' 将从此位置挑选以匹配所需数量',
                UPDATE_TO_CART_AS_BACK_ORDER: '更新到订单',
                ADD: '加 ',
                UPDATE: '现代化 ',
                ADD_ONLY: '只添加 ',
                UPDATE_ONLY: '仅更新 ',
                TO_CART: ' 至购物车',
                TO_THE_CART: ' 至购物车 + ',
                TO_BACK_ORDER: '要晚点才能',
                AUTHORIZATION_REQD: '需要授权才能查询.',
                CONTACT_REP: '请联系您的销售代表加入该产品线.',
                NOTE:'定价注:',
                MSG_1:'显示的定价标准代表净价而不代表程序或促销.',
                MSG_2:'如有任何疑问，请致电客户服务 800-621-8084.',
                AVAIL_NOTE: '房态注意：',
                AVAIL_NOTE_CONTENT: '直到结账过程完成后不保留库存.'
            }
        },


        //------------------ Preview Order ----------------------------------//

        PREVIEWORDER: {
            CUSTOMERNAME: '收单客户名称',
            MAILINGADDRESS: '邮件地址]',
            SHIPPINGADDRESS: '选择收货地址',
            BILLTOADDRESS: ' 帐单邮寄地址 (发票被发送到)',
            ADVANCEDSEARCH: '高级搜索',
            SHIPPINDADDWARN: '选择一个收货人地址为订货！',
            EMAILADDRESS: '选择邮件到电子邮件地址 (它将被用于订单相关的通知, 更新和信件)',
            ADDEMAIL: '新增电子邮件地址',
            SHIPPINGMETHOD: '邮寄方式',
            CARRIER: '带菌者',
            SHIPPINGDATE: '请求的装运日期',
            ADD_NEW: '添新',
            ENTER_VALID_SO_NO: '请输入有效的SO号码',
            SHIP_COMPLETE: '允许分批装运？',
            NO: '不',
            YES: '是!',
            SHIPPINGINSTRUCTION: '运输说明 (选修的)',
            PURCHAGE_ORDR_NUM: '订购单号码(选修的)',
            PRIMARY_NUM: '主要采购订单号.',
            SECONDARY_NUM: '二次订单号码. (选修的)',
            CA_NUMBER: '运营商帐号.',
            ORDER_SUMMARY: '股票订单汇总',
            NUM_OF_PARTS: '没有项目(s)',
            TOTAL_WEIGHT: '总重量 (Kg/lbs)',
            ORDER_TOTAL: '合计订单',
            PLACE_ORDER: '下订单',
            CONT_SHOPPING: '继续购物',
            EDIT_ORDR_LIST: '编辑订单列表',
            ORDR_PROCESSING: '处理您的库存订单...',
            SHIP_TO_ADDRESS:'高级搜索 - 收货地址',
            SHIP_TO_THIS:'船舶此',
            REVERT_CART: '恢复车',
            ITEMS_AVAILABILITY_INFO: '可用性项目被责令, 一旦订单被替换将被重新检查',
            STOCK_ORDER: '股票订单',
            ADDNEWSHIPTOADDRESS : '添加新收货地址',
            SHIPPINGAVAILABLITYDETAILS : '根据可用性，为您的购物车的部分OFS之一，出货日期',
            SHIPPINGAVAILABLITYDETAILS1 : '因此，要求出货日期不能早',
            APRROVALMSGFROMDANA : '编辑或取消所下订单的，请联系客户服务',
            EMAILACKNWMSG : "当您的订单时，我们会向您发送电子邮件确认收到你的.",
            CONTACTCUSTOMERSRV: "需要帮助? 客户服务中心联系",
            SP_SHIPPING : "指定送货",
            CONTACT_EMAIL: 'dananorthamericadc@dana.com',
            CS_NUM: "1-800-621-8084",
            OR: '要么',
            CUSTOMERNO : '客户编号.',
            CUSERNAME : '顾客姓名',
            VALIDENTERPOMSG : '请输入有效的订单号',
            BILLTO: '记账到 (发票被发送到)'
        },
        
        //--------------- Post Order -----------------------//
        
        POSTORDER: {
            SEARCH_PLACEHOLDER: "通过订单号，发票号，进程号，订单号搜索订单",
            HOME: '家',
            MACCOUNT: '我的帐户',
            MYORDERS: '我的订单',
            RESULT_LBL: "搜索结果s",
            RESULT_MSG: "订购(小号) 创建",
            TAB1: "最近的订单 (过去2个月)",
            TAB2: "指定日期范围",
            TAB3: "打开订单",
            TAB4: "返回订单",
            TAB5: "发票",
            PONUMBER: "订单号码.",
            ORDER_DATE: "订购日期",
            INVOICE_DATE: "发票日期",
            INVOICE_NUMBER: "发票号码.",
            PROCESS_NUMBER: "过程中没有.",
            ORDER_NUMBER: "订单号.",
            DETAIL_BTN: "详情",
            SOLDTO: "卖给",
            SHIPTO: "运送到",
            PARTNUMBER: "型号.",
            DESC: "描述",
            QTY_REQ: "不限产品数量",
            EXT_PRICE: "扩展价格",
            PRICE: "价格",
            PROMISE_DATE: "预计运送日期",
            DELIVERY_DATE: "邮寄日期",
            PACKING_SLIPS: "装箱单",
            PACKING_SLIPS_BTN: "装箱单",
            ORDERSTATUS: "订单状态",
            ACTION: "行动",
            VIEW_STATUS: "查看状态",
            ORDER_DETAILS_BY_DATE: {
                ACTION: '行动',
                VIEW_STATUS: '查看状态'
            },
            ORDER_STATUS: {
                IN_PROGRESS: '在流程',
                R_FOR_SHIPPING: '准备运输',
                NO_STATUS_AVAIL: '无可用状态'
            },
            EMAIL:{
                LABEL_EMAIL: '搪瓷',
                ERR_MSG: '有一个在发送邮件服务器的一些错误.',
                EMAIL_TO: '至 :',
                FORM_ERR_MSG: '请输入一个有效的电子邮件地址.',
                BTN_TXT: '发电子邮件',
                SUCCESS_MSG: '邮件已成功发送.',
                PLACEHOLDER: "收件人的电子邮件ID的以逗号分隔."
            }
        },

        //------------ backorder.html----------
        BACKORDER : {
            NO_DATA_AVAIL : '无可用数据!',
            NO_RECORD_FOUND: '请求的信息不可用, 请验证您的访问权限'
        },
        //------------ Invoice -------------------//
        INVOICE: {
            INVOICE_NUMBER: "发票号码",
            INVOICE_DATE: "发票日期",
            INVOICE_TYPE: "发票类型",
            INVOICE_DESC: "描述",
            INV_COMP_NAME: "达纳重型车辆系统集团有限责任公司",
            PRINT_INV: '打印',
            AFTERMARKET: '售后市场',
            INV_HEAD: '发票',
            REPRINT: '重印',
            DATE: '日期',
            CUST_NUM: '客户编号.',
            SHIP_TO_NUM: '船到无.',
            ORDER_NUMBER: '订单号.',
            ORD_DATE: '订购日期',
            INV_NUM: '发票号码.',
            DEALER_PO: '经销商后',
            DEALER_CODE: '经销商代码',
            SHIPMENT_NUM: '装船 / BOL 无.',
            SOLDTO: '卖给',
            SHIPTO: '运送到',
            CUSTOMER_PO: '客户订单',
            TERMS: '付款条件',
            SHIP_VIA: '支架',
            PICK_NUM: '挑不出.',
            PART_NUM: '部件号.',
            CUSTOMER_PART: '部分客户',
            PART_DESC: '部分说明',
            QTY_ORD: '有序数量',
            QTY_SHIP: '船舶数量',
            UNITPRICE: '单价 (美元)',
            EXTENDEDPRICE: '扩展价格 (美元)',
            REMIT_TO: '汇给',
            DANA_LMTD: '达纳有限公司',
            EXPEDITE_WAY: '88714 加快路',
            CHICAGO_IL: '伊利诺伊州芝加哥',
            US: 'US',
            TOTAL: '总额 (美元):',
            FREIGHT_CHARGES: '运费 (美元):',      
            ITEM_TOTAL: '项目总 (美元):',
            ALL_CLAIM_AGAINST: '对承运人的所有版权声明 (索赔, 短缺, 等等) 必须在交付时的运费单注意.',
            CLAIM_MUST_MADE: '要求必须在收到商品后30天内提出，提供上述货物'
        },

        // --------------------- invoicedetails.......................//

        INVOICEDETAILS: {
            I_DETAILS: '发票明细',
            BACK: '背面',
            SOLDTO: '卖给',
            SHIPTO: '运送到',
            BILLTO: '记账到',
            PRO_NUM: '过程中没有.',
            ORD_NUM: '订单号.',
            ORD_TYPE: '订单类型',
            CARRIER: '带菌者',
            TERMS: '付款条件',
            ACCOUNT: '運營商帳號.',
            TRACKING_NUM: '追踪號碼.',
            INV_NUM: '發票號碼.',
            INV_DATE: '发票日期',
            PO_NUM: '訂單號碼.',
            PART_NUM: '型號.',
            DESC: '描述',
            QTY_ORD: '有序數量',
            QTY_SHIPD: '發貨數量',
            UNT_PRICE: '单价',
            EXT_PRICE: '扩展价格',
            WEIGHT: '重量',
            STATUS: '狀態',
            TOTAL: '總',      
            ITEM_TOTAL: '项目总：',
            UOM: 'LB'
        },

        //----------------------Invoice-list,invoice-list.html

        INVOICELIST: {
            FRM_DATE: '从日期e',
            TO_DATE: '迄今',
            GET_CREDIT_HIST: '获取信用记录'
        },

        //----------orderdetails(post-order/orderdetails/detail.html)

        ORDERDETAILS: {
            ORDER_DETAILS: '訂單詳細信息',
            BACK: '背面',
            PRINT: '打印',
            DOWNLOAD_ORDER: '下載',
            SOLDTO: '賣給',
            SHIPTO: '運送到',
            BILLTO: '記賬到',
            PRO_NUM: '过程中没有.',
            ORD_NUM: '订单号.',
            ORD_TYPE: '訂單類型',
            CARRIER: '带菌者',
            TERMS: '術語',
            ACCOUNT: '帳戶',
            INV_NUM: '發票號碼.',
            INV_DATE: '發票日期',
            PART_NUM: '型號.',
            DESC: '描述',
            QTY_ORDERED: '有序數量',
            QTY_SHIPPED: '發貨數量',
            UNITPRICE: '单价',
            EXT_PRICE: '扩展价格',
            WEIGHT: '重量',
            UOM: 'LB',
            WEIGHT_UOM: '重量計量單位',
            STATUS: '狀態',
            TOTAL: '狀態'
        },

        //----------ordr-dtls-ByDtRange.html

        ORDERDETAILS_BY_DATE: {
            FROM_DATE: '從日期',
            TO_DATE: '迄今',
            GET_ORDER_LIST: '獲取訂單清單'
        },

        //----------order-status(orderstatus.html)-------------
        ORDERSTATUS: {
            ALL: '所有',
            ESTSHIPPING: '东。航运',
            AWAITINGPROCESSING: '等待处理',
            INPROCESS: '过程中',
            TRACKING: '跟踪',
            NORESULTSFOUND: '未找到结果!',
            SEARCHBYPART: '按零件号搜索.',
            PRIMARYPONUMBER: '订单号码.',
            ORDER_STATUS: '订单状态',
            BACK: '背部',
            ORDER_ID: '订单号.',
            TOT_ITEMS: '总项目',
            HIDE_PARTS_CS: '隐藏零件完全出货',
            LINE_NO: '行号.',
            PN_PN: '零件号/零件名称',
            ORD_QTY: '数量有序',
            PROCCESSING: '处理',
            STAGE_1: '阶段1',
            STAGE_2: '阶段2',
            SHIPPING: '运输',
            SHIPPING_INFO: '灰色进度条表示订购的总数量，绿色条和点表示不同日期发货的数量.',
            EST_SHIPPING: '东。航运',
            SHIPPED: '发货',
            ON: '上',
            NO_DATA: '无可用数据!',
            OVERALL_STATUS: '总体状态',
            STATUS: {
                ORDER_CANCEL: "订单已取消",
                ORDER_NO_STATUS: "订单状态不可用",
                ORDER_RECEIVED: "等待处理",
                ORDER_BEING_PROCESSED: "过程中",
                ORDER_PARTIALLY_SHIPPED: "In Process",
                ORDER_COMPLETELY_SHIPPED: "发货"

            },
            DETAILSTATUS: '状态: ',
            DETAILSTATUSMSG: '每个框描述一个数量及其装运状态; “等待处理”状态为白色框，“处理中”状态为蓝色框，“出货”状态为绿色框。要了解更多，鼠标悬停在相应的框中',
            PARTS_SHIPPED: '部件已发货',
            PARTS_PROCESS: '过程中的部件',
            PARTS_AWAITING: '等待处理的部分',
            PRINT: '打印页面',
            EMAIL: '电子邮件',
            QTY_AWAITING: '等待处理的数量',
            QTY_PROCESS: '数量正在处理中',
            QTY_SHIPPED: '数量已发货'
        },


        //----------packingslip(packingslip.html)-------------

        PACKINGSLIP: {
            PACK_SLIP: '装箱单的订单号 ',
            ORD_NUM: '订单号.',
            PRINT_P_SLIP: '打印',
            DEL_NUMS: '交货编号',
            DANA_H_VEH: '达纳重型车辆系统集团有限责任公司',
            AFTERMARKET: '售后市场',
            PACKINGSLIP: '裝箱單',
            REPRINT: '重印',
            DELIVERY_NUM: '交貨編號',
            PLANTCODE: '工廠代碼',
            BILL_TO_C_NO: '條例草案沒有卡斯特.',
            SHIP_TO_C_NO: '船到卡斯特無.',
            SHIP_DATE: '發貨日期',
            SHIP_VIA: '带菌者',
            SHIPPING_INSTR: '發貨說明',
            CAR_NUM: '紙箱無.',
            LINE_NUM: '號線.',
            CUST_PART_NUM: '客戶部件號.',
            PRIMARY_PO_NUM: '客戶訂單號碼.',
            DANA_PART_NUM: '達納型號.',
            QTY_SHIPPED: '發貨數量',
            UNIT_WEIGHT: '單位重量 ',
            SHIPMENT_ID: '裝運ID',
            SUPPILER_ID: '供應商ID',
            SHIPPING_PONIT: '裝運點',
            INCO_TERMS: '國際貿易術語',
            SHIPPING_COND: '國際貿易術語',
            SHIPTOPARTY: '船到黨',
            DOCUMENTDATE: '文檔日期',
            DELIVERYNO: '交貨編號.',
            INFORMATION: '信息',
            SOLDTOPARTY_ADDR: '賣給:',
            SHIP_ADDR: '運送到:',
            UOM: 'LB'
        }, 

         //----------Add new ship to address//
         SHIPADDR: {
            MANDATORY: '*',
            HEADER: "添加新的送货地址",
            NAME: "名称",
            STATE: "州",
            COUNTRY: "国家",
            CITY: "市",
            ZIN: "邮政编码",
            STREET_ADDRESS: "地址欄1",
            STREET_ADDRESS2: "地址第2行",
            STATEERRMSG: '請輸入狀態',
            CITYERRMSG: '请输入城市',
            COUNTRYERRMSG: '请输入国家',
            PINEERRMSG: '請輸入PIN碼',
            STREET1ERRMSG: '请输入街道地址',
            SAVE: "保存",
            SHIP_NAME: '名稱',
            SHIPERRMSG: '请输入收货方名称'
        },
        
        // my list/ mylist.html
        MYLIST: {
            MLIST: '我的列表',
            PLIST: '打印',
            APLIST: '部分添加到列表',
            ADDALL_TOCART: '全部添加货品放入购物车',
            ADDALL_TORFQ: '添加的所有项目询价',
            ADDTOCART: '添加到购物车',
            ADDTORFQ: '加入询价',
            LITEM1: '卡车01名单(2)',
            LITEM2: 'ABC  list(2)',
            LITEM3: 'ANZ list(3)',
            SELECTEDITEM: '2 项目(s) 2项在列表中选择',
            RQSIBTN: '申请报价为所选项目',
            PARTNO_NAME: '零件号/零件名称',
            SRNO: '编号',
            PARTNAME: '零件名称',
            PARTNO: '零件号',
            QTY: '数量',
            QTY_UPDATE_SUCCESS : '数量已成功更新！',
            REMOVE: '清除',
            MMDDYY: '新增的 MM/DD/YYYY',
            RFQS: '请给报价状态',
            NEWRFQ: 'RFQ',
            RFQID: 'RFQ ID',
            REQID: 'Req ID',
            REQDATE: 'Req Date',
            QRBY: '报价被要求',
            DISTRIBUTOR: '经销商',
            LNAME: '姓',
            RFQSTATUS: '询价状态',
            RECQUOTE: '收到报价 (和/ñ)',
            DETAILBTN: '详情',
            YES: '是!',
            NO: '不',
            CREATENL: '创建列表',
            MANAGEL: '管理列表',
            COREPRICE: "单位核心存款 (美元)",
            UNITPRICE: "单价 (美元)",
            ZEROPARTS: '0 部件在RFQ!',
            ADDPARTS_TOPROCEED: '添加部分进行.',
            DATEADDED: "添加日期",
            DELETE_CONFIRMATION: "你真的要删除",
            FROM: "從",
            DELETE: "删",
            CANCEL: "取消",
            CONFIRMATION: "鞏故",
            CREATE_NEW: "創建新列表",
            LIST_LBL: "目錄 (項目數)",
            GOTOCART: "轉到購物車",
            LIST_SEARCH_ERR: "沒有列表（名稱）匹配您的搜索!",
            EMPTY_MSG: "您尚未向此列表中添加任何項目!",
            NO_RESULT: "未找到結果!",
            ADDED_RFQ: "已添加到RFQ!",
            ADDED_CART: "添加到購物車!",
            AUTH_REQ: "請聯繫您的銷售代表添加此產品線.",
            NO_PARTS: "项目数量已添加",
            SEND_RFQ_BTN1: "選擇已批准",
            SEND_RFQ_BTN2: "经销商发送求购意向，",
            VIEW_PARTS_ADDED: "查看項目已添加",
            HIDE_PARTS_ADDED: "查看項目已添加",
            RFQ_SLNO: "查看項目已添加z.",
            RFQ_PART: "零件号/零件名称",
            RFQ_PART1: "部件號./",
            RFQ_PART2: "零件名稱",
            RFQ_RQTY: "所需數量",
            ADDED_CART1: "1 項目添加到購物車!",
            CART_MSG1: " 項目添加到購物車!",
            CART_MSG_ONLY: " 僅 ",
            CART_MSG_OUTOF: "在......之外 ",
            CART_MSG_AUTH: " 訂購此部件需要授權."
            
            
        },
        
         //---------------mylist/createlist/createlist.html-------------------

        // CREATENEWLIST: {
        //     CANBUSE: 'This list can be used for requesting quotation only from Dana distributor.',
        //     CREATENL: 'Create New List',
        //     LISTNAME: 'List Name',
        //     PLACEHRD: 'Untitle list number',
        //     CREATELISTBTN: 'CREATE LIST'
        // },
        CREATENEWLIST: {
            CREATENL: '创建列表',
            SHOPLIST1: '给你的列表中的名称 (',
            SHOPLIST2: '部分加',
            CANBUSE: '该列表可用于放置库存订单仅',
            SCANBUSE: '您可以使用列表创建和发送RFQ给授权的Dana经销商/经销商.',
            LISTNAME: '列表名称',
            CREATELISTBTN: '创建 ',
            SAVECONTINUE: '保存并继续',
            CANCEL: '取消',
            CONFIRM: '目前您的列表存储在临时空间中，如果您现在不保存它，它将被永久删除。请点击“保存”以还原并保存，或点击“删除”以确认删除.',
            GOTOLIST: "转到我的列表",
            SUCCESS: "成功",
            SUCCESS_MSG1: "您的列表保存为",
            SUCCESS_MSG2: '列表，你可以去“我的列表”修改它.',
            SHORT_MSG: "列表名称太短.",
            LONG_MSG: "列表名称过长.",
            REQUIRED_MSG: "列表名称是必需的."
        },

        //---------------mylist/managelist/managelist.html-------------------

        // MANAGELIST: {
        //     MANAGEML: 'Manage My List',
        //     DEFAULT: 'Default',
        //     DELETE: 'Delete',
        //     LIST: 'List',
        //     LIST1: 'Truck 01 list(2)',
        //     LIST2: 'ABC  list(2)',
        //     LIST3: 'ANZ list(3)',
        //     SUBMIT: 'Submit'
        // },
        LISTSETTING: {
            LISTSET: '管理列表',
            PLACEHDR: '搜索列表名称',
            DELETE: '选择删除',
            SUBMIT: '提交',
            ERR_SEARCH: '没有列表名符合您搜索!',
            ERR_INVALID: '请输入一个有效的列表名称',
            SUCCESS_MSG: '列表名称已更新.',
            ERR_EXISTS: '你已经进入名单，已经存在!',
            DELETED_MSG: '选择列表已被删除.'
        },


        //--------------mylist/requestquote/sendrequestquote.html

        SENDREQQUOTE: {
            YRQR: '您的报价请求已准备就绪',
            SREQUEST: '发送请求',
            SELITEM: ' 2出从卡车01列表中选择2个项目',
            BTL: '返回目录',
            MYLOC: '我的位置',
            ZIPCODE: '邮编为detemine你的船于─位置 (按地址)',
            FAIRP: '费尔波特',
            RADCOVER: '在邮政编码和周围的暴力覆盖',
            DDL1: '25',
            DDL2: '30',
            DDL3: '35',
            MILES: '英里',
            MYPREF: '我的偏好',
            SDAT: '间隔接收报价的日期和时间',
            EST: '是',
            SMNTQ: '间隔您想要接收的报价的最大数量',
            TNQMYLOC: '您会收到一个项目的报价的数量将是在1至3的范围取决于.',
            REQTOGGLE: ' 让我选择分销商发出请求报价至',
            RFQ: 'RFQ',
            SAD: '选择批准分销商',
            VPA: '查看部件已添加',
            D_COVERED : '覆盖距离',
            DLR_TYPE : '经销商类型',
            ALL:'所有',
            DISTRIBUTORS: '配电盘',
            DEALERS: '零售商',
            SORT_BY: '排序方式',
            NEAREST: '迩',
            BY_NAME: '按名字',
            EP_LIST: '编辑零件清单',
            IWT_RECIEVE_QI: '我想收到报价',
            SND_RFQ_TO_AD : '发送询价批准分销商',
            D_SELECTED : '经销商精选',
            RFQC_SUCCESSFULLY: '已成功创建报价请求!',
            CS_BY_GTS: '您可以通过转到“请求报价状态”部分检查状态. ',
            CRFQ_STATUS : '检查RFQ状态',
            RFQ_DETAIL: 'RFQ 详情',
            GB_TO_MYLIST: '回到我的清单',
            CANCELRFQ: '取消 RFQ',
            CANCELLEDRFQ: '取消'
 
        },

        //-----------------mylist/requestquote/addedrfq/addedrfq.html

          ADDEDRFQ: {
            RFQ: 'RFQ',
            ADDED_PARTS: '增加的部分',
            SN: '線項目',
            PN_PN : '零件号/零件名称',
            QTY: '数量'
        },

        //-----------------mylist/rfqdetails/rfqdetails.html

         RFQDETAILS: {
            AFTERMARKET: '售后市场',
            NOP: '没有零件',
            QR_BY: '报价所要求',
            SN: '线项目',
            PN_PN: '型号/零件名称',
            DES_QTY: '所需数量'
        },

        //-----------------mylist/sessionlist/sessionlist.html

        SESSIONLIST: {
            DELETE: '删',
            SAVE: '保存'
        },


        //--------------mylist/sendquoteconfirm/quoteconfirmation.html

        QUOTECONFIRM: {
            SUCCESSMSG: '3报价请求成功发送! ',
            YCCSTATUS: ' 您可以通过转到“请求报价状态”部分检查状态.',
            FAQSTATUSBTN: '检查RFQ状态',
            RFQID: 'RFQ ID：',
            ID1: '345643224',
            PARTTYPTEMAIL: 'AXDE 汽车零件，电话+0 000 000 000, Email:sales@axde.com',
            CHECKBTN: '检查RFQ状态',
            GOBACKBTN: '回到我的清单',
            CONTINUESHOP: '继续购物'
        },

         //---------------dealerlocator/dealer-locator.html

        DEALERLOCATOR: {
            SEARCHDEALERSHEADER: '搜索经销商按地点',
            ZIPCODE: '指定邮政编码',
            USELOCATION: '使用我的位置',
            LOCATION: '搜索批准的分销商',
            DEALERSNEARYOU: ' 您附近的经销商',
            ERRMSG: '我们无法找到经销商输入的邮政编码.',
            RFQERRMSG1: '我们无法找到一个经销商，可以提供您列表中的所有部件.',
            RFQERRMSG2: '如果列表中的部分属于多个产品系列，或者我们在您附近没有经销商，则可能会发生这种情况。请尝试编辑您的零件清单或增加覆盖的距离.',
            RESULTS: '结果 ({{value}})',
            PRODUCTCATEGORY: {
                L06: "汽车车轴",
                L05: "汽车传动轴",
                P07: "商用车驱动桥",
                L03: "商用车驱动桥",
                P03: "商用车驱动桥",
                L01: "商用车驱动轴",
                L02: "商用车转向桥",
                P05: "商用车转向桥",
                P08: "商用车转向桥",
                L04: "中央轮胎通货膨胀系统（CTIS（tm））",
                P01: "条板轴",
                P02: "条板轴",
                L11: "GWB工业传动轴",
                L14: "SVL",
                L13: "SVL",
                L10: "分动箱"
            },
            UNIT: {
                mi: "英里"
            },
            DISTANCECOVERED: '覆盖距离',
            VALIDATEZIPCODE: '请输入有效的邮政编码!',
            WD: 'WD',
            OE: 'OE'
        },

        //---------------wheretobuy/wheretobuy.html

       WHERETOBUY: {
            SEARCHDEALERSHEADER: '搜索经销商按位置',
            ZIPCODE: '指定邮编',
            USELOCATION: '使用我的位置',
            ENTER_VALID_ZIPCODE: '请输入有效的邮政编码！',
            DIST_COVERED: '覆盖距离',
            DEALER_TYPE: '经销商类型',
            ALL: '所有',
            DISTRIBUTORS: '配电盘',
            DEALERS: '零售商',
            SORT_BY: '排序方式',
            NEAREST: '迩',
            NAME: '名称',
            PRODUCTS: '产品',
            ALL_PRODUCTS: '所有产品',
            LOCATION: '搜索批准分销商',
            DEALERSNEARYOU: ' 您附近的经销商',
            ERRMSG: '我们无法找到输入的邮政编码分销商.',
            RFQERRMSG1: '我们无法找到一个经销商，可以提供您列表中的所有部件.',
            RFQERRMSG2: '如果列表中的部分属于多个产品系列，或者我们在您附近没有经销商，则可能会发生这种情况。请尝试编辑您的零件清单或增加覆盖的距离.',
            RESULTS: '结果 ({{value}})',
            PRODUCTCATEGORY: {
                L06:"汽车车桥",
                L05:"汽车传动轴",
                P07:"商用车驱动桥",
                L03:"商用车驱动桥",
                P03:"商用车驱动桥",
                L01:"商用车传动轴",
                L02:"商用车转向轴",
                P05:"商用车转向轴",
                P08:"商用车转向轴",
                L04:"中央轮胎通货膨胀系统 (CTIS(tm))",
                P01:"周转箱桥",
                P02:"周转箱桥",
                L11:"GWB工业传动轴",
                L14:"SVL",
                L13:"SVL",
                L10:"分动箱"
            },
            UNIT: {
                mi: "英里"
            },
            DISTANCECOVERED: 'Distance Covered',
            VALIDATEZIPCODE: 'Please enter a valid zipcode!',
            WD: 'WD',
            OE: 'OE'
        },

        //-------------------MYRFQ

        MYRFQ: {
            DETAIL: '产品详细介绍',
            RFQID: 'RFQ ID',
            DD: '批准的分销商',
            RFQD: '询价截止日期',
            RFQC: "RFQ逢",
            RFQI: 'RFQ启动了',
            RFQS: '询价状态',
            SENDDR: '经销商发送提醒',
            CONFIRM: '确认收到报价',
            NOITEMSOPENRFQ: '没有公开询价(小号)',
            //NOITEMSCLOSEDRFQ: 'No Closed RFQ(s)',
            NOITEMSCLOSEDRFQ: '您目前没有任何封闭的询价(小号)',
            REMINDERSENT: '提醒已发送！',
            PHONE : '电话',
            MOBILE : '移动',
            EMAIL :' 搪瓷',
            APPROVED_DIST_MSG1 : '批准的分销商使用自己的通信介质 e。 G。电子邮件发送报价. 要进行后续操作，您可以单击“发送提醒”按钮。请确认并通知批准的分销商' ,
            APPROVED_DIST_MSG2 : '以及Dana售后市场，当您通过点击确认收据按钮收到报价.' ,
            SEARCHBYRFQ : '通过RFQ ID搜索',
            STATUS: {
                DRAFT: '草案',
                SENT_TO_DISTRIBUTOR: '发送到分销商',
                OPENED_BY_DISTRIBUTOR: '通过经销商开业',
                QUOTE_SENT_BY_DISTRIBUTOR: '通过报价发送经销商',
                CLOSED: '关门',
                CANCELLED: '取消',
                REMINDER: '提醒',
                CONFIRM_RECEIPT: '确认收据'
            }
        },

        MANAGERFQ: {
            DETAILS: 'RFQ详细信息',
            TITLE: '管理RFQ',
            RFQID: 'RFQ ID',
            DATERCD: '接收日期',
            DATECLD: '关闭日期',
            DUEIN: '由于在',
            RFQS: '询价状态',
            ACTION: '动作发送报价',
            NOITEMSOPENRFQ: '您目前没有任何悬而未决的要求(小号) 询价享受你的一天!',
            //NOITEMSCLOSEDRFQ: 'No closed RFQ(s)',
            NOITEMSCLOSEDRFQ: '您目前没有任何封闭的询价(小号)',
            NOPARTS: '没有零件',
            QUOTEREQBY: '报价所要求',
            SNO: '序列号.',
            PARTNOPARTNAME: '零件号/零件名称',
            DESQTY: '所需数量',
            NEWRFQS: '新的询价',
            OPENRFQS: '公开询价',
            OPENRFQ: '打开RFQ',
            CLOSERFQ : '关闭询价',
            OVERDUERFQ: '逾期RFQ',
            PRINT: '打印',
            ERRMSG: '没有搜索结果！',
            SEARCHFOR: '搜索RFQ ID',
            ALL: '所有',
            NEW: '新',
            OPEN: '开门',
            QUOTESENT: '发送报价',
            QUOTESENT1: '发送报价!',
            MANAGERFQ : '管理',
            STATUS: {
                NEW: '新',
                OPEN: '开门',
                QUOTE_SENT: '发送报价',
                CLOSED_BY_CLIENT: '客户关闭',
                CANCELLED_BY_CLIENT: '已取消客户',
                OVERDUE: '过期的',
                REMINDER: '提醒'
            },
            DOWNLOADEXCEL: '下载',
            DOWNLOADPDF: '下载',
            APPROVED_DIST_MSG1: '要向申请人发送报价，请使用您自己的通信系统，例如电子邮件。一旦报价发送,',
            APPROVED_DIST_MSG2: ' 请点击 “报价发送”按钮通知请求者和Dana售后市场'
  
        },
        
         PAGINATION: {
            FIRST: '首先',
            LAST: '持续'
        },
        
          COMPLETEORDER : {
            ORDERNO : '订单号.',
            VIEWORDDETAIL :'查看订单详情',
            PROCESSNO : '过程中没有.',
            ORDERSUCESSMSG : '您的订单被放置感谢您的订单！',
            ORDERSHIPEDMSG : '它现在正在处理. ',
            ORDERPROCCMSG : '您的订单号是不是由于某些内部错误产生的！',
            GETSTATUSHBYCUSTSUPPORT :'您的订单处理正在进行中，您可能希望通过联系客户支持获得订单的状态.',
            GOTOCART :  '进入购物车',
            CONTACT_C_SERVICE: '编辑或取消所下订单的，请联系客服.',
            CONTINUNINGSHOPING : '继续购物'      
        },
 

        INCOMPLETEORDER : { 
            ORDERCOMP : '',
            LISTBELLOWIS : '',
           ORDERAVLISSUEACTION : '对于下面列出的项目,可用性已更改.请采取纠正措施继续.',
            PARTNO : '零件号/零件名称',
            ORDERQTY : '数量',
            AVLQTY : '可用数量',
            BACKORDERQTY : '返回订单r',
            PLANTCODE : '植物名称',
            UNITPRICEUSD : '单价 (美元)',
            EXTPRICEUSD : '扩展价格 (美元)',
            CORECHARGETOTLUSD : '单位核心存款 (美元)',
            PRODTOTALUSD : '产品总计 (美元)',
            EDIT : "编辑数量",
            DELETE : "删",
            PROCEED_WITH_CO: '与完成的订单继续',
            ADD_IT_AS_BO: '将其添加为返回订单',
            P_WITH_AVAIL: '处理可用',
            SO_SUMMARY: '库存订单摘要',
            EO_SUMMARY: '紧急订单摘要',
            NO_PARTS: '项目数量',
            OT_USD: '合计订单 (美元)',
            PROCEED: '下订单',
            C_AND_GTC: '进入购物车',
            C_AND_CONT_SHOPPING: '继续购物',
            NO_ITEM_IN_CART: '没有项目在购物车',
            GO_BACK_HOME: '返回首页' 
 
        },

       PLACEORDER : {
            ORDERID : '订单ID',
            VIEWORDERDETAILS : '查看订单详情',
            SAVEITASORDERLIST : '将其保存为订单清单',
            EMIALWITHSHIPMENTTRCKDETAILS : '您的订单处理正在进行中，您可能希望通过联系客户支持获得订单的状态.',
            PLACEANOTHERORDER : '其他订单',
            CONTINUEORDER :    '继续订购',
            ACKDRECIEPTOFYOURORDER : "当您的订单下达，我们将向您发送一封电子邮件，确认您的订单.",
            NEEDHELPCONTCUSTCARESERVICE : '编辑或取消所下订单的，请联系客户服务',
            DANANORTHAMARICADCDEMAILID : 'dananorthamericadc@dana.com'
           
        },


        POERROR : {
           ERRORINPLACINGORDER : '错误放置订购'

        },

        ADVANCEDSEARCHPOPUP : {
            ADDRESS :  '地址',
            LINE1   :  '线路1',
            LINE2   :  '2号线',
            COUNTRY :  '国家'
        },

       PROCESSORDERPOPUP : {
            VERIFYINGAVLMSGPART1 : '我们正在核实的可用性',
            VERIFYINGAVLMSGPART2 : '部分和其他细节最后一次',
            VERIFYINGAVLMSGPART3 : '通过刷新浏览器，请立会',
            VERIFYINGAVLMSGPART4 : '中断过程.'
        },
 
       PROCEEDMSG : {
            PROCEEDMSG1 : '编辑或取消所下订单的，请联系客服',
            PROCEEDMSG2 : "我明白了，请不要重复此警报！",
            PROCESSWITHORDER : '与订单处理'
        },
 
       SHIPPEDLIST : {
            USERCANCHANGE : '用户可以在这里改变！',
            QTY : '数量',
            BACKORDERQTY : '返回订单数量',
            UNITPRICEUSD : '单价 (美元)',
            EXTENDEDPRICEUSD : '扩展价格 (美元)',
            CORECHAREGES : '核心存款总额 (美元)',
            PRODUCTOTUSD : '产品总计 (美元)',
            UOM : 'UOM',
            ADD : '加',
            UPDATE : '现代化'
    }, 

    ORDERMGMT : {
          BACK: '背部 ',
          ORDER : '订购',
          UNIT : '单元',
          PRICEUSD :'价格 ()',
          EXTENDED : '拉',
          CORECHARGES : '核心单位',
          DEPOSITUSDL :'存款 (美元)',
          TOTALUSD : '总 (美元)',
          ORDER_TOTAL: '合计订单',
          PRODUCT :'产品',
          REQUESTED : '请求',
          SHIPTODATE : '发货日期',
          EDITQTY : '编辑数量',
          REMOVE : '清除',
          ITEMCARTSTATUS: '你的购物车是空的！',
          EMERGENCYORDERCART : '紧急订购车',
          HELPMSGTOCLINT : '定价注意：显示的价格代表“标准净价”，可能不反映计划或促销产品。对于任何问题，请联系客户服务，电话：1-800-621-8084。可用性注意：在结帐过程完成之前，不会预留广告资源.',
          HELPMSGTOCLINT1: '显示的价格代表您当前的，约定的，每天的“标准净价”，并不反映任何特殊/促销价格或可用的折扣如果您有任何问题，请联系客户服务1-800-621- 8084.',
          STOCKORDERSUMMARY : '库存订单摘要',
          EMERGENCYORDERSUMMARY :  '紧急订单摘要',
          NOOFPARTS :  'No. of Item(s)',
          NUM_OF_PARTS: '没有总. 零件',
          CONTINUESHOPING : '继续购物',
          SHIPFROM : '从船舶',
          LOCATION : '位置',
          CHECKOUT  : '查看',
          STOCKORDERCART : '股票订购车',
          UOM: 'UOM',
          OUTOFSTOCK : '缺货',
          CONFIRM_CHANGES: '确认更改'

           
 
    },
      expresscheckout : {
        MAINHEADER : '快速订单输入',
        SEARCHBYPART : '按零件号搜索零件.',
        SERIALNO : '号线.',
        PARTNO : '型号/零件名称',
        QTY : '数量',
        REQTOSHIPDATE : '要求交货日期',
        UOM : 'UOM',
        EXTWGTH : '分机重量(公斤)',
        UNITPRICE: '单价 (美元)',
        UNITCORE : '核心单位存款 (美元)',
        EXTPRICE : '外部价格 (美元)',
        AVL : '可得到',
        ADDEDASBACKORDER :'添加为返回订单',
        SELECTTOREMOVE : '选择要删除',
        FOOTERMSG1 :'显示的定价标准代表净价 而不代表或促销产品. 如有任何疑问，请致电客户服务 1-800-621-8084.',
        FOOTERMSG2 :'直到结账完成后不保留库存.',
        EXECEDWRNMSG1: '请确认订单总额',
        EXECEDWRNMSG2: '',
        MARKITEMREMOVEMSG: '您已标记的项目删除.',
        REMOVESELECTED: '删除选定',
        TOTALORDERQTY : '总数（量',
        PRICINGNOTE: "定价说明",
        AVLNOTE : '可用性说明',
        CUSTPARTNO : "卡斯特. 型号."
       
    },
 
    ERRORMSG :{
        PARTNOTVALID:'第一部分是无效的！',
        NOTRTBERRORMSG: '请联系您的销售代表brto添加此产品线',
        SUPPORTMSG: '编辑或取消所下订单的，请联系客服',
        DISCLAIMER: '放弃:',
        PROMOTIONALMSG:'促销定价/折扣，如果有的话，不包括在订单总额',
        RTBSTASTUSTOKNOW :'要知道你的RTB状态，请联系您的销售代表',
        SUPPORTMSGFORADDRES : '要添加新地址，请联系您的销售代表',
        EMIALADDRESSSELECTMSG : '请选择至少一个电子邮件地址',
        EMERGENCYORDEREXPRESSSHIPING :'对于紧急命令，只有快递和联邦快递为载体。提供',
        NOTSELETEDITEMFORNOTLOGGED :'名单存储您所选项目的临时地方，如果您还没有登录',
        FAILEDTOADDPRODUCTTOCART : '无法将产品添加到购物车',
        CONTACT_REP : '请联系您的销售代表加入该产品线',
        PASSWORDCHNAGEMSG : '您的密码已被更改尝试此处用它登录',
        TEMPPASSWORDCHANGEMSG :'临时密码已经重新发送',
        VALIDPAARTNOERRORMSG:  '请输入有效的零件号',
        OPPSSOMETHINGWENTWRONG : '哎呀！',
        TRYAGAIN : '再试一次',
        SHAREVIAMAIL :'通过邮件分享',
        AFTERREMOVEMSG : "{{PARTNO}}从购物车中移除",
        PARTADDEDSUCCESSMSG : '部件号{{部分号码}}添加到购物车成功',
        LISTTOPROCEEDMSG : '请创建一个列表进行！',
        ORDERSTATUS : '您的订单尚未完成！'
   
    },
    
 
    SHIPTOSOLDPOPUP: {
            SIGNINSUCCESSFULL: '签到成功！',
            YOUHAVESIGNEDWITHA: '您已经签署了',
            YOUHAVESELETEDA: '您已选择了',
            SHIPTOCUSTOMER: '收货地址客户编号',
            SOLDTOCUSTOMERTOPROCEED: '请选择相关的「待售客户」以继续.',
            ASSOCIATEDSLODTOCUSTOMER: '关联的客户 ',
            SELECT: '选择'
        },
 
    RFQLIST :{
        RFQID213224234 : 'RFQ ID 213224234',
        TODAY24HRSAGO : '今天2小时前',
        NEW : '新',
        URLCOPIEDCLIPBOARD : '网址 在剪贴板复制',
        YOURORDERLISTURL : '您的订单列表网址'
    },
 
    common : {
        COPYURL : '复制网址',
        URL : '网址' ,
        SOLDTO : '卖给',
        SHIPTO : '运送到',
        EXT : '分机',
        WEIGHT :'重量',
        KG : '公斤',
        LB: '(磅)',
        CONFIRMCHANGES : '确认更改',
        ENTERZIPCODE:  '输入邮政编号',
        DISPLAY : '显示',
        OF  : '的',
        CHECKAVL : '检查可用性...',
        QTYAVL : '数量。可用',
        EXPAND : '扩大',
        COLLAPSE : '崩溃'
    },
 
    BOM : {
        BOMNO : '在良好的',
        LEVEL : '水平',
        COMONENTNAME : '组件名称',
        QTY : '数量'
    },

    PLACEHOLDERALL : {
        SEARCHORDERBYPARTNO : '按部件搜索订单编号/部件名称',
        SEARCHPARTNO : '按部件号/部件名搜索',
        SEARCHLISTBYNAME : '按列表名称搜索列表',
        SEARCHLISTBYPARTNO : '按部件号/部件名搜索列表',
        SEARCHORDERBYPARTNUM : '按部件号搜索订单',
        SEARCHBUCUST : '按剪切号，用户ID，名称，电子邮件或登录日期搜索',
        SEARCHBYUSEID : '按用户ID，姓名，电子邮件或登录日期搜索'

    },
     TOOLTIP : {
         PLEASEVERFYPARTQTY : '请验证零件数量',
         PARTCARD1  : '作为注册用户，在您将零件添加到列表后，您可以发送RFQ。要订购此部件，请致电1-800-621-8084与客户服务代表联系',
         PARTCARD2 :  '将零件添加到列表中，保存或共享以请求报价。'
   }
    
    }
}
