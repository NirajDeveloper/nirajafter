/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


window.env = {
    activeAPIBase : 'http://52.9.252.59/',
    endPoint : '/search-service/api',
    pricingEndPoint: '/pricing-service/api',
    availabilityEndPoint: '/availability-service/api',
    postOrderEndPoint: '/order-service/api/v1/postOrder',
    orderServiceEndPoint: '/order-service/api/v1',
    cartEndpoint: 'http://52.8.125.250:8082/order-service/api/v1/'
}