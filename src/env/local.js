/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

window.env = {
    activeAPIBase : 'http://52.9.144.171',
    activeAuthBase : 'http://52.9.144.171',
    //pricingAPIBase : 'http://52.9.144.171',
    pricingAPIBase : 'http://52.52.26.6',
    availabilityAPIBase : 'http://52.9.144.171',
    orderServiceAPIBase : 'http://52.9.144.171', 
    authEndPoint:'/auth-service',
    endPoint : '/search-service/api',
    pricingEndPoint: '/pricing-service/api',
    availabilityEndPoint: '/availability-service/api',
    postOrderEndPoint: '/order-service/api/v1/postOrder',
    orderServiceEndPoint: '/order-service/api/v1',
    cartEndpoint: 'http://52.9.144.171/order-service/api/v1/',
    cartPriceEndpoint: 'http://52.9.144.171/pricing-service/api/v1/'
}