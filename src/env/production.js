window.env = {
    activeAPIBase : 'http://52.53.236.6',
    endPoint : '/search-service/api',
    pricingEndPoint: '/pricing-service/api',
    availabilityEndPoint: '/availability-service/api',
    postOrderEndPoint: '/order-service/api/v1/postOrder',
    orderServiceEndPoint: '/order-service/api/v1',
    cartEndpoint: 'http://52.8.125.250:8082/order-service/api/v1/'
}