/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/
export function config($logProvider, $sceDelegateProvider, $translateProvider, AftermarketConstants, $httpProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
    $sceDelegateProvider.resourceUrlWhitelist(['**']);
    
    sessionStorage.categoryHint = "Select a category to see filters.";
    $httpProvider.defaults.headers.common.Authorization = "";
    if(localStorage.bearerToken){
        let authorization = localStorage.bearerToken;
        $httpProvider.defaults.headers.common.Authorization = authorization;
    }
    // Set options third-party lib
    //let activeLang = {};
    //removed as per sonar
    angular.forEach(AftermarketConstants.localization,(language)=>{
        $translateProvider.translations(language.langName, language.data);
        if(language.active){
            $translateProvider.preferredLanguage(language.langName);
        }
    }); 
    if(localStorage.lang) {
        $translateProvider.preferredLanguage(localStorage.lang);
    }
    
    $httpProvider.interceptors.push('httpInterceptor');
    $httpProvider.interceptors.push('AuthInterceptor');
        /*$translateProvider.translations('fr', {
            HEADLINE: 'Hallo, Das ist mein super App !',
            INTRO_TEXT: 'And it has i18n support!'
        });*/
    //$translateProvider.preferredLanguage(AftermarketConstants.localization.lang);
}

config.$inject = ['$logProvider', '$sceDelegateProvider', '$translateProvider', 'AftermarketConstants', '$httpProvider'];
