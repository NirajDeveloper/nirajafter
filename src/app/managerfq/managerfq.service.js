/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class ManageRFQService {
    constructor(dataServices, $q) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            dataServices,
            $q
        });
        vm._openRFQ = {};
        vm._closedRFQ = {};
    }
    get openRFQ() {
        return this._openRFQ;
    }
    set openRFQ(obj) {
        this._openRFQ = obj;
    }

    get closedRFQ() {
        return this._closedRFQ;
    }

    set closedRFQ(obj) {
        this._closedRFQ = obj;
    }

    getOpenRFQ(obj) {
        let vm = this;
        let { dataServices, $q } = vm.DI();
        return $q(function (resolve, reject) {
            dataServices.openRFQ(obj).then(function(response) {
                vm.openRFQ = response;
                resolve(response);                
            });
        });
    }

    getClosedRFQ(obj) {
        let vm = this,
            {
                dataServices,
                $q
            } = vm.DI();
        
        return $q(function (resolve, reject) {
            dataServices.closedRFQ(obj).then(function(response) {
                vm.closedRFQ = response;
                resolve(response);
            });
        });        
    }

    updateRFQStatus(payload) {
        let vm = this,
            {
                dataServices,
                $q
            } = vm.DI();
        return $q(function (resolve, reject) {
            dataServices.updateRFQStatus(payload).then(function(response) {
                resolve(response);
            });
        });
    }

    getUserDetails(userId) {
        let vm = this;
        let { dataServices, $q } = vm.DI();

        return $q(function(resolve, reject) {
            dataServices.fetchUserProfileByUserId(userId).then(function(response) {
                resolve(response);
            });
        });
    }

    getRFQReceiver(rfqId) {
        let vm = this;
        let { dataServices, $q } = vm.DI();
        return $q(function(resolve, reject) {
            dataServices.getRFQReceiver(rfqId).then(function(response) {
                resolve(response);
            });
        });
    }
}