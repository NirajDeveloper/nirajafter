/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

//Author: Gautham Manivannan

export class ManageRFQController {
  constructor($rootScope, $translate, $scope, $filter, ManageRFQService, authenticationService, dataServices, AftermarketConstants) {
    'ngInject';
    let vm = this;
    vm.DI = () => ({ $rootScope, $translate, $scope, $filter, ManageRFQService, authenticationService, dataServices, AftermarketConstants });
    vm.websiteLogo = AftermarketConstants.skin.logo;
    vm.init("open");
    vm.loading = false;
  }

  init(type) {
    let vm = this;
    vm.reset();
    let payload = vm.getPayload();
    if (type === "open")
      vm.getOpenRFQ(payload);
    else if (type === "closed")
      vm.getClosedRFQ(payload);
  }

  reset() {
    let vm = this;
    vm.searchUid = "";
    vm.selectedRow = 0;
    vm.index = 1;
    vm.size = 10;
    vm.RFQDetails = {};
    vm.aggrReq = true;
    vm.sortOrder = true;
    vm.sortAttr = "DATE_INITIATED";
  }
  navOpenRFQ() {
    let vm = this;
    let payload = vm.getPayload();
    vm.getOpenRFQ(payload);
  }

  navClosedRFQ() {
    let vm = this;
    let payload = vm.getPayload();
    vm.getClosedRFQ(payload);
  }

  sortOpenRFQ(key) {
    let vm = this;
    vm.sortOrder = vm.getSortOrder(key);
    vm.sortAttr = vm.getSortAttr(key);
    vm.index = 1;
    let payload = vm.getPayload();
    vm.getOpenRFQ(payload);
  }

  sortClosedRFQ(key) {
    let vm = this;
    vm.sortOrder = vm.getSortOrder(key);
    vm.sortAttr = vm.getSortAttr(key);
    vm.index = 1;
    let payload = vm.getPayload();
    vm.getClosedRFQ(payload);
  }

  filterOpenRFQ() {
    let vm = this;
    let { ManageRFQService } = vm.DI();
    let payload = vm.getPayload();
    vm.getOpenRFQ(payload);
  }

  filterClosedRFQ() {
    let vm = this;
    let { ManageRFQService } = vm.DI();
    let payload = vm.getPayload();
    vm.getClosedRFQ(payload);
  }

  onOpenClear() {
    let vm = this;
    if(vm.searchUid === "")
      vm.filterOpenRFQ();
  }

  onClosedClear() {
    let vm = this;
    if(vm.searchUid === "")
      vm.filterClosedRFQ();
  }


  getSortOrder(key) {
    let vm = this;
    let bool;
    let sortAttr = vm.getSortAttr(key);
    if (vm.sortAttr === sortAttr) {
      bool = (vm.sortOrder) ? false : true;
    }
    else {
      bool = false;
    }
    return bool;
  }

  getSortAttr(key) {
    let val;
    switch (key) {
      case 0:
        val = "RFQ_ID";
        break;
      case 1:
        val = "DATE_INITIATED";
        break;
      case 2:
        val = "DUE_IN";
        break;
      case 3:
        val = "STATUS";
        break;
      default:
        val = "DATE_INITIATED";
        break;
    }

    return val;
  }

  status(key) {
    let rStr;
    switch (key) {
      case "NEW":
        rStr = "MANAGERFQ.STATUS.NEW";
        break;
      case "DRAFT":
        rStr = "MANAGERFQ.STATUS.DRAFT";
        break;
      case "OPEN":
        rStr = "MANAGERFQ.STATUS.OPEN";
        break;
      case "QUOTE_SENT":
        rStr = "MANAGERFQ.STATUS.QUOTE_SENT";
        break;
      case "CLOSED_BY_CLIENT":
        rStr = "MANAGERFQ.STATUS.CLOSED_BY_CLIENT";
        break;
      case "CANCELLED_BY_CLIENT":
        rStr = "MANAGERFQ.STATUS.CANCELLED_BY_CLIENT";
        break;
      default:
        rStr = "";
        break;
    }
    return rStr;
  }

  getPayload() {
    let vm = this;
    let from = 10 * (vm.index - 1);
    let obj = {
      from: from,
      size: vm.size,
      sortParam: vm.sortAttr,
      sortDsc: vm.sortOrder,
      aggrReq: vm.aggrReq,
      filter: {}
    };


    if (vm.searchUid !== "")
      obj.filter.rfqUid = vm.searchUid;

    if (vm.searchStatus !== "")
      obj.filter.receiverStatus = vm.searchStatus;

    return obj;
  }


  getOpenRFQ(payload) {
    let vm = this,
      {
        ManageRFQService,
        authenticationService
      } = vm.DI();
    vm.loading = true;
    let openRFQ = ManageRFQService.getOpenRFQ(payload);
    openRFQ.then(function (resolve) {
      vm.loading = false;
      vm.openRFQ = ManageRFQService.openRFQ;
      vm.RFQDetails = {};
      vm.RFQUserDetails = {};
      if(vm.openRFQ)
        vm.details(vm.openRFQ.list[0], 0);
    });
  }

  getClosedRFQ(payload) {
    let vm = this,
      {
        ManageRFQService,
        authenticationService
      } = vm.DI();
      
    vm.currentDate = new Date().valueOf();
    let closedRFQ = ManageRFQService.getClosedRFQ(payload);
    vm.loading = true;
    closedRFQ.then(function (resolve) {
      vm.loading = false;
      vm.closedRFQ = ManageRFQService.closedRFQ;
      vm.RFQDetails = {};
      vm.RFQUserDetails = {};
      if(vm.closedRFQ)
        vm.details(vm.closedRFQ.list[0], 0);
    });
  }

  details(obj, index) {
    let vm = this,
      {
        $rootScope, ManageRFQService
      } = vm.DI();

    vm.selectedRow = index;
    let userDetails = ManageRFQService.getUserDetails(obj.userId);
    userDetails.then(function (response) {
      vm.RFQUserDetails = response;
    });

    let rfqDetails = ManageRFQService.getRFQReceiver(obj.id);
    rfqDetails.then(function (response) {
      vm.RFQDetails = response;
    });


    if (obj.status === "NEW") {
      vm.updateStatus(obj, "OPEN");
    }
    /*vm.RFQDetails = obj;
    if (obj.reciverStatus === "new")
      vm.updateStatus(obj, "OPEN");
    let userDetails = ManageRFQService.getUserDetails(obj.userId);
    userDetails.then(function (response) {
      vm.RFQUserDetails = response;
    });*/
  }

  updateStatus(obj, type) {
    let vm = this;
    let { ManageRFQService, $scope } = vm.DI();
    let payload = {
      "rfqReciverId": obj.id,
      "receiverStatus": type
    };

    obj.status = type;
    let updateStatus = ManageRFQService.updateRFQStatus(payload);
    updateStatus.then(function (response) {
    });

  }

  downloadRFQ() {
    let vm = this;
    let { dataServices, AftermarketConstants } = vm.DI();

    dataServices.downloadRFQ(vm.RFQDetails.id).then(function (response) {
      var b = new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      saveAs(b, AftermarketConstants.skin.rfqfilename + vm.RFQDetails.rfqUniqueId + ".xlsx");
      //this is FileSaver.js function
      /*var anchor = angular.element('<a/>');
        anchor.css({display: 'none'}); // Make sure it's not visible
        angular.element(document.body).append(anchor); // Attach to document
        anchor.attr({
            href: 'data:attachment/csv;charset=utf-8,' + encodeURIComponent(response),
            target: '_blank',
            download: "samplefile.xlsx"
        })[0].click();
        anchor.remove(); // Clean it up afterwards*/
    });
  }
}