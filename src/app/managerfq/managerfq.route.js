/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

//author: Gautham Manivannan


export function routeConfig($stateProvider){
	'ngInject';
	$stateProvider
	.state('managerfq',{
		url: '/managerfq',
		parent: 'aftermarket',
		templateUrl: 'app/managerfq/managerfq.html',
		controller: 'ManageRFQController',
		controllerAs: 'mrfq',
	});
}