/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Gautham Manivannan -->

import {routeConfig} from './managerfq.route';
import {ManageRFQController} from './managerfq.controller';
import {ManageRFQService} from './managerfq.service';

angular.module('aftermarket.managerfq', [])
.config(routeConfig)
.controller('ManageRFQController', ManageRFQController)
.service('ManageRFQService', ManageRFQService);
