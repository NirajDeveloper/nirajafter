/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// var devServer = "http://52.8.125.250:8080",
//     qaServer = "http://52.53.236.6";
// var orderServiceBaseAPI = "http://52.8.125.250:8082";
//activeAPIBase = devServer,
//activeAPIBase = qaServer,
//apiConfig.REQPOINT() = activeAPIBase + '/search-service/api';




var cId = 1;

export let apiConfig = {

    'BASEURL': '',
    'ENDPOINT': '',
    'AUTH_BASEURL': '',
    'PRICINGURL': '',
    'AVAILABILITYURL': '',
    'ORDERSERVICEURL': '',
    'AUTH_ENDPOINT': '',
    'AUTH_VERENDPOINT': '',
    'PRICING_ENDPOINT': '',
    'AVAILABILITY_ENDPOINT': '',
    'REGISTER_ENDPOINT': '',
    'ORDER_ENDPOINT': '',
    'ORDER_SERVICE_ENDPOINT': '',
    'RFQ_SERVICE_ENDPOINT': '',
    'DL_SERVICE_ENDPOINT': '',
    'REQPOINT': function () {
        return apiConfig.BASEURL + apiConfig.ENDPOINT;
    },
    'PRICING': function () {
        return apiConfig.PRICINGURL + apiConfig.PRICING_ENDPOINT;
    },
    'AVAILABILITY': function(){
        return apiConfig.AVAILABILITYURL + apiConfig.AVAILABILITY_ENDPOINT;
    },
    'REGISTER': function() {  
        return apiConfig.BASEURL + apiConfig.REGISTER_ENDPOINT;
    },
    'LOGIN': function() {  
        return apiConfig.AUTH_BASEURL + apiConfig.AUTH_ENDPOINT;
    },
    'AUTH_SERVICE': function() {
         return apiConfig.AUTH_BASEURL + apiConfig.AUTH_ENDPOINT + apiConfig.AUTH_VERENDPOINT;
    },
    'ORDER_URL': function() {
        return apiConfig.ORDERSERVICEURL + apiConfig.ORDER_ENDPOINT;
    },
    'PACKING_URL': function() {
        return apiConfig.ORDERSERVICEURL + '/order-service/api/v1/postorder';
    },
    'ORDERSERVICE': function() {
        return apiConfig.ORDERSERVICEURL + apiConfig.ORDER_SERVICE_ENDPOINT;
    },    
    'RFQ': function() {
        return  apiConfig.RFQ_SERVICE_ENDPOINT+ "rfq";
    },
    'DRAFT': function() {
        return  apiConfig.RFQ_SERVICE_ENDPOINT+ "draftrfq";
    },
    'RFQADDITEM': function() {
        return apiConfig.RFQ_SERVICE_ENDPOINT + "rfq/addItem/"; 
    },
    'RFQDELITEM': function() {
        return  apiConfig.RFQ_SERVICE_ENDPOINT+ "rfq/del/rfqItem";
    },
    'OPENRFQ': function() {
        return apiConfig.RFQ_SERVICE_ENDPOINT+ "rfq/open";
    },
    'CLOSEDRFQ': function() {
        return apiConfig.RFQ_SERVICE_ENDPOINT+ "rfq/closed";
    },
    'SHAREORDERLIST': function() {
        return apiConfig.RFQ_SERVICE_ENDPOINT+ "shareOrderlist";
    },
    'GETDEALERLOCATOR': function() {
        return  apiConfig.DL_SERVICE_ENDPOINT+ "dealerlocator";
    },
    'GETWHERETOBUY': function() {
        return  apiConfig.DL_SERVICE_ENDPOINT+ "wheretobuy";
    },
     'CART': function(){
        return apiConfig.CART_ENDPOINT;
    },
    'EXPRESS_CHECKOUT_URL': function() {
        return apiConfig.EXPRESS_CHECKOUT;
    },
    'CARTPRICE': function(){
        return apiConfig.CARTPRICE_ENDPOINT;
    },
    'SHARERFQ': function() {
        return apiConfig.RFQ_SERVICE_ENDPOINT + "rfq/share";
    },
    'UPDATERFQSTATUS': function() {
        return apiConfig.RFQ_SERVICE_ENDPOINT + "rfq/status";
    },
    'YMM_SEARCH': {
        'url': '',
        'setUrl': function (searchString, category, prodLine, year, make, model, from, size) {
            this.url = apiConfig.REQPOINT() + '/result';
            this.data = {
                "q": searchString,
                "cats": [category, null, prodLine],
                "year": year,
                "make": make,
                "model": model,
                "from": from,
                "size": size
            }
        },
        'method': 'POST',
        'data': {}
    },

    'ORDER_LIST': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.REQPOINT() + '/orderList/create';
        },
        'method': 'POST',
        'data': {}
    },
    'SHARE_LIST': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.REQPOINT() + '/saveOrderList';
            this.data = payload
        },
        'method': 'POST',
        'data': {}
    },

    'SHARED_ORDERLIST': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.REQPOINT() + '/orders/' + id;
        },
        'method': 'GET',
        'data': {}
    },

    'SHARE_ORDERLIST': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.SHAREORDERLIST();
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },

    'AUTO_SEARCH': {
        'url': '',
        'setUrl': function (param, category, scope) {
            let catNames = [];

            catNames[0] = scope ? scope.name : null;
            this.url = apiConfig.REQPOINT() + '/suggest';
            this.data = {
                "q": param,
                "cats": [category, null, null],
                "catNames": catNames
            }
        },
        'method': 'POST',
        'data': {}
    },
    'CAT_SEARCH': {
        'url': '',
        'setUrl': function (param, scope, from, size, productCategory, filterObjectArray, year, make, model, ymm, cat2, sort, catNames, intChgPartSearch) {
            this.url = apiConfig.REQPOINT() + '/result';
            this.data = {
                "q": param,
                "from": from,
                "size": size,
                "cats": [scope ? scope : 0, cat2 ? cat2 : 0, productCategory ? productCategory : 0],
                "filter": filterObjectArray,
                "year": year,
                "make": make,
                "model": model,
                "ymm": ymm,
                "sort": sort ? sort : null,
                "catNames": catNames,
                "intChgPartSearch":intChgPartSearch 
            }
        },
        'method': 'POST',
        'data': {}
    },
    'PART': {
        'url': '',
        'setUrl': function (param, icNumber) {
            this.url = apiConfig.REQPOINT() + '/part/' + param + "?interChgNumber=" + icNumber;
        },
        'method': 'GET',
        'data': {}
    },
    'PART_BY_PNUM': {
        'url': '',
        'setUrl': function (param) {
            this.url = apiConfig.REQPOINT() + '/part/partnumber/' + param;
        },
        'method': 'GET',
        'data': {}
    },
    'APPINFO': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.REQPOINT() + '/appInfo';
        },
        'method': 'GET'
    },
    'EMAIL_PART': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.REQPOINT() + '/partDetail/email';
        },
        'method': 'POST',
        'data': {
            "partDetailUrl": "",
            "emailDTO": {
                "from": "",
                "toEmails": [],
                "subject": "",
                "body": ""
            }

        }

    },
    'PART_AVILABILITY': {
        'url': '',
        'setUrl': function () {  
            this.url = apiConfig.AVAILABILITY() + '/v1/part/partnumber/availability';
            //availability-service/api/v1/part/partNumber/availability
        },
        'setData': function (cId, oType, qty, partNumber, corePartNumber, partCats, packageQty) {
            this.data = {
                "partNumber": partNumber,
                "customerCid": cId,
                "orderType": oType,
                "quantityReq": qty,
                //"corePartNumber": corePartNumber,
                "productCat": partCats,
                "packageQty": packageQty
            };
        },
        'method': 'POST',
        'data': {}
    },
    'PART_PRICING': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.PRICING() + '/v1/part/partnumber/pricing';
            //pricing-service/api/v1/part/partNumber/pricing  
                                 
        },
        'setData': function (cId, oType, qty,partNumber, corePartNumber, partCats) {
            this.data = {
                "partNumber": partNumber,
                "customerCid": cId,
                "orderType": oType,
                "quantityReq": qty
                //"corePartNumber": partNumber
            };
        },
        'method': 'POST',
        'data': {}
    },
    'ADD_TO_CART': {
        'url': '',
        'setUrl': function (payload) {
           // this.url = apiConfig.CART()+'cart';
            this.url = apiConfig.CART()+'cart';
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'REGISTER_USER': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.REGISTER() + '/v1/register/';
        },
        'setData': function (payload) {
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'LOGIN_USER': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.LOGIN() + '/login';
            this.data = payload;
        },
        'setData': function (payload) {
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'CREATE_UPDATE_SHOP_USER': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.AUTH_SERVICE() + '/anonymous/user/register';
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'CREATE_UPDATE_USER': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.AUTH_SERVICE() + '/user';
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'CHECK_USER_STATUS': {
        'url': '',
        'setUrl': function (loginId) {
            this.url = apiConfig.AUTH_SERVICE() + '/account/status?loginId=' + loginId;
        },
        'method': 'GET'
    },
    'VERIFY_ENTITLEMENT_ACCESS': {
        'url': '',
        'setUrl': function (resource, action) {
            this.url = apiConfig.AUTH_SERVICE() + '/isEntitled/' + resource + '/' + action;
        },
        'method': 'GET'
    },
    'VERIFY_ENTITLEMENT_RTB_ACCESS': {
        'url': '',
        'setUrl': function (partNumber) {
            this.url = apiConfig.AUTH_SERVICE() + '/isEntitledForRTB/' + partNumber;
        },
        'method': 'GET'
    },
    'CHECK_USER_EXISTENCE': {
        'url': '',
        'setUrl': function (loginId) {
            this.url = apiConfig.AUTH_SERVICE() + "/isUserExists?loginId=" + loginId;
        },
        'method': 'GET'
    },
    'CHECK_EMAIL_EXISTENCE': {
        'url': '',
        'setUrl': function (emailId) {
            this.url = apiConfig.AUTH_SERVICE() + "/isEmailExists?emailId=" + emailId;
        },
        'method': 'GET'
    },
    'GET_COUNTRY_LIST': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.AUTH_SERVICE() + "/country/list";
        },
        'method': 'GET'
    },
    'GET_STATE_LIST': {
        'url': '',
        'setUrl': function (countryCode) {
            this.url = apiConfig.AUTH_SERVICE() + "/state/list?code="+ countryCode;
        },
        'method': 'GET'
    },
    'GET_FP_OTP': {
        'url': '',
        'setUrl': function (loginId) {
            this.url = apiConfig.AUTH_SERVICE() + "/anonymous/user/fp?loginId=" + loginId;
        },
        'method': 'GET'
    },
    'VERIFY_FP_OTP': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.AUTH_SERVICE() + "/account/anonymous/verifyOtp";
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'CHANGE_PASSWORD': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.AUTH_SERVICE() + "/user/cp";
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'RESET_PASSWORD': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.AUTH_SERVICE() + "/anonymous/user/rp";
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'DOWNLOAD_REPORT': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.AUTH_SERVICE() + "/download";
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },    
    'GET_SOLD_TO_TOKEN': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.AUTH_SERVICE() + "/customer/billTo/"+ payload;
        },
        'method': 'GET',
        'data': {}
    },
    'LOGOUT': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.LOGIN() + "/logmeout";
        },
        'method': 'POST',
        'data': {}
    },
    'GET_USER_PROFILES_FOR_REPORT': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.AUTH_SERVICE() + "/users";
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'GET_LOGED_IN_USER_PERMISSIONS': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.AUTH_SERVICE() + "/user/acl";
        },
        'method': 'GET',
        'data': {}
    },
    'SWITCH_CUSTOMER': {
        'url': '',
        'setUrl': function (customerId) {
            this.url = apiConfig.AUTH_SERVICE() + "/customer/switch/"+ customerId;
        },
        'method': 'GET',
        'data': {}
    },
    'GET_USER_PROFILE_BY_ID': {
        'url': '',
        'setUrl': function (loginId) {
            this.url = apiConfig.AUTH_SERVICE() + "/user?loginId=" + loginId;
        },
        'setHeaders':function(bearerToken){
            this.headers={'Authorization' : bearerToken};
        },
        'method': 'GET'
    },
    'GET_CUST_LIST_BY_ID': {
        'url': '',
        'setUrl': function (loginId) {
            this.url = apiConfig.AUTH_SERVICE() + "/session/customer/list";
        },
        'method': 'GET'
    },
    'GET_CART': {
        'url': '',
        'setUrl': function (cart_type, order_type) {
            this.url = apiConfig.CART() + cart_type + "/" + order_type +'/cart/';
        },
        'method': 'GET'
        //'headers': {'tenant_id':1}
    },
    'GET_CARTBY_ID': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.CART() + "cartline/" + id;
            //this.url = "http://52.9.144.171/order-service/api/v1/cartline/1283";
        },
        'method': 'GET'
        //'headers': {'tenant_id':1}
    },
    'GET_CART_PRICE': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.CARTPRICE() + 'part/pricing';
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'UPDATE_CART': {
        
        'url': '',
        'setUrl': function (payload, id) {
           // this.url = apiConfig.CART() + 'cart/'+ id;
            //this.url = apiConfig.CART() + 'express-cart/SO/cart';
            if(id){
                this.url = apiConfig.CART() + 'cart/'+ id;
            }else{
               this.url = apiConfig.CART() + 'express-cart/SO/cart';
            }
            

            this.data = payload;
        },
        'method': 'PUT',
        'data': {}
    },
    'DELETE_CART': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.CART() + 'cartline/' + id;
        },
        'method': 'DELETE'
    },
    'DELETE_ORDERLIST_ITEM': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.CART() + 'orderline/' + id;
        },
        'method': 'DELETE'
    },
    'CHECK_OUT': {
        'url': '',
        'setUrl': function (cart_type, orderType) {
           if(orderType=='express-cart'){
             this.url = apiConfig.CART() + 'order/header/express-cart/'+ cart_type;
           }else{
             this.url = apiConfig.CART() + 'order/header/normal-cart/'+ cart_type;
           }
            
        },
        'method': 'GET'
    },
    'ORDER_HISTORY': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.ORDER_URL() + '/orderHist/';
        },
        'setData': function (payload) {
             this.data = payload; 
        },
        'method': 'POST',
        'data': {}
    },
    'ORDER_DETAILS': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.ORDER_URL() + '/orderHist/'+ id;
        },
        
        'method': 'GET',
        'data': {}
    },
    'BACK_ORDER_LIST': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.ORDER_URL() + '/backOrderHist/';
        },
        'setData': function (cId) {
             this.data = {
                "customerId": cId,
             }
        },
        'method': 'POST',
        'data': {}
    },
    'INVOICE_LIST': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.ORDER_URL() + '/invoiceHist/';
        },
        'setData': function (cId,fd,td) {
             this.data = {
                "customerId": cId,
                "fromDate": fd,
                "thruDate": td
             }
        },
        'method': 'POST',
        'data': {}
    },
    'INVOICE_DETAILS': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.ORDER_URL() + '/invoiceHist/' + id;
        },
        'setData': function (payload) {
            this.data = payload;
        },
        'method': 'GET',
        'data': {}
    },
    'DOWNLOAD_ORDER': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.ORDER_URL() + "/downloadOrderDtl/" + payload;
        },
        'method': 'GET',
        'data': {},
        'responseType': "arraybuffer"
    },
    'DOWNLOAD_POST_ORDER': {
        'url': '',
        'setUrl': function (payload, endUrl) {
            this.url = apiConfig.ORDER_URL() + endUrl;
            this.data = payload;
        },
        'method': 'POST',
        'data': {},
        'responseType': "arraybuffer"
    },
    'SEND_INVOICE_TO_EMAILS': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.ORDER_URL() + "/sendinvoice";
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'SEND_DELIVERYNO_TO_EMAILS': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.ORDER_URL() + "/sendpckslip";
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'GET_DELIVERY_NUMBERS': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.PACKING_URL() + '/packingslip/' + id;
        },
        'method': 'GET',
        'data': {}
    },
    'GET_PACKING_SLIPS': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.PACKING_URL() + '/packingslipdtls/' + id;
        },
        'method': 'GET',
        'data': {}
    },
    'GET_ORDER_STATUS': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.PACKING_URL() + '/orderstatus/' + id;
        },
        'method': 'GET',
        'data': {}
    },
    'CART_SEARCH': {
        'url': '',
        'setUrl': function (srchStr) {
            this.url = apiConfig.CART() + cart_type + "/" + order_type +'/cart';
        },
        'method': 'GET'
        //'headers': {'tenant_id':1}
    },
    'ADD_SHIP_ADDR': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.CART() + "addShipAddr";
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'GET_SHIP_ADDR': {
        'url': '',
        'setUrl': function (id) {
            this.url = apiConfig.CART() + "customer/shiptodetails/" +id;
        },
        'method': 'GET'
    },
    'CART_COUNT': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.CART() + "cart/count";
        },
        'method': 'GET'
    },
    'GET_CARRIER': {
        'url': '',
        'setUrl': function (cid, type) {
            this.url = apiConfig.CART() + "carrier/"+ type + "/" + cid;
        },
        'method': 'GET'
    },
    'PLACE_ORDER': {
        'url': '',
        'setUrl': function (payload, order_type, cartType) {
            if(cartType=="express-cart"){
             this.url = apiConfig.CART() + "express-cart/SO/order";
            }else{
             this.url = apiConfig.CART() + "normal-cart/"+order_type+"/order";
            }
           
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'CREATE_ORDER_LIST': {
        'url': '',
        'setUrl': function (name) {
            this.url = apiConfig.ORDERSERVICE() + "/order-service/api/v1/orderlist";
        },
        'setData': function (payload) {
            this.data = payload;
        },
        'method': 'PUT',
        'data': {}
    },
    'FETCH_LIST_ITEMS': {
        'url': '',
        'setUrl': function (id) {
             this.url = apiConfig.ORDERSERVICE() + "/order-service/api/v1/orderlistdetails/" + id;
        },
        'method': 'GET',      
        'data': {}
    },
    'GET_LISTS': {
        'url': '',
        'setUrl': function (type) {
           this.url = apiConfig.ORDERSERVICE() + "/order-service/api/v1/orderlist";
        },
        'method': 'POST',
        'data': {
            "cartType":"ORDER_LIST",
            "countReq" :1,
            "latestOnTop": 1
        }
    },
    'UPDATE_ORDERLIST_ITEM': {
        'url': '',
        'setUrl': function (name) {
            this.url = apiConfig.ORDERSERVICE() + "/order-service/api/v1/orderList?lName=" + name;

        },
        'setData': function (payload) {
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'UPDATE_LISTITEM_QTY': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.ORDERSERVICE() + "/order-service/api/v1/orderline/modify";
        },
        'setData': function (payload) {
            this.data = payload;
        },
        'method': 'PUT',
        'data': {}
    },
    'DELETE_ORDER_LIST': {
        'url': '',
        'setUrl': function (id) {
             this.url = apiConfig.ORDERSERVICE() + "/order-service/api/v1/orderlist/" + id ;
          },
        'method': 'DELETE',
        'data': {}
    },
    'RENAME_ORDER_LIST': {
        'url': '',
        'setUrl': function (payload) {
             this.url = apiConfig.ORDERSERVICE() + "/order-service/api/v1/orderlist/rename";
             this.data = payload;
          },
        'method': 'POST',
        'data': {}
    },
    // 'DELETE_ORDERLIST_ITEM': {
    //     'url': '',
    //     'setUrl': function (type) {
    //        this.url = apiConfig.ORDERSERVICE() + "/order-service/api/v1/carts?cart_type=" + type ;
    //     },
    //     'method': 'GET',
    //     'data': {}
    // },
    'ADD_ORDERLIST_ITEM': {
        'url': '',
        'setUrl': function (id) {
              this.url = apiConfig.ORDERSERVICE() + "/order-service/api/v1/orderlist/" + id ;
        },
        'setData': function (payload) {
            this.data = payload;
        },
        'method': 'PUT',
        'data': {}
    },
    'PART_AVILABILITY_BULK': {
        'url': '',
        'setUrl': function (payload) {  
            this.url = apiConfig.AVAILABILITY() + '/v1/part/availability';
            this.data = payload
        },
        'method': 'POST',
        'data': {}
    },
    'PART_AVILABILITY_BULK_EO': {
        'url': '',
        'setUrl': function (payload) {  
            this.url = apiConfig.AVAILABILITY() + '/v1/part/availabilitybyplant';
            this.data = payload
        },
        'method': 'POST',
        'data': {}
    },
    'ADD_TO_RFQ': {
        'url': '',
        'setUrl': function (id,payload) {
           // this.url = apiConfig.CART()+'cart';
            this.url = apiConfig.RFQADDITEM() + id;
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'GET_RFQ_RECEIVER': {
        'url': '',
        'setUrl': function(rfqId) {
            this.url = apiConfig.RFQ() + '/receiver/' + rfqId;
        },
        'method': 'GET'
    },
    'GET_RFQ_DETAILS': {
        'url': '',
        'setUrl': function(rfqId) {
            this.url = apiConfig.RFQ() + '/user/' + rfqId;
        },
        'method': 'GET'
    },
    'ADD_RFQ_RECEIVER': {
        'url': '',
        'setUrl': function (rfqId,custId) {
           // this.url = apiConfig.CART()+'cart';
            this.url = apiConfig.RFQ()+'/addReceiver/'+rfqId+'/'+custId;
        },
        'method': 'GET',
        'data': {}
    },
    'DELETE_RFQ_RECEIVER': {
        'url': '',
        'setUrl': function (custId) {
           // this.url = apiConfig.CART()+'cart';
            this.url = apiConfig.RFQ()+'/del/rfqReciver/'+custId;
        },
        'method': 'GET'
    },
    'DOWNLOAD_RFQ': {
        'url': '',
        'setUrl': function(id) {
            this.url = apiConfig.RFQ_SERVICE_ENDPOINT + 'downloadRfq/'+id;
        },
        'responseType': "arraybuffer"
    },
    'RFQ_NOTIFICATION': {
        'url': '',
        'setUrl': function(id) {
            this.url = apiConfig.RFQ() + '/notification';
        }
    },
    'GET_DRAFT_LIST': {
        'url': '',
        'setUrl': function () {
            this.url = apiConfig.DRAFT();
        },
        'method': 'GET',
        'data': {}
    },
    'DEL_RFQ_ITEM': {
        'url': '',
        'setUrl': function(id) {
            this.url = apiConfig.RFQDELITEM() + "/" +id;
        },
        'method': "GET",
    },
    'BULK_RTB_CHECK': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.AUTH_SERVICE() + '/isEntitledForRTB/bulk';
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'GET_DEALER_LOCATOR': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.GETDEALERLOCATOR();
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'GET_WHERE_TO_BUY': {
        'url': '',
        'setUrl': function(payload) {
            this.url = apiConfig.GETWHERETOBUY();
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'OPEN_RFQ': {
        'url': '',
        'setUrl': function(payload) {
            this.url = apiConfig.OPENRFQ();
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'CLOSED_RFQ': {
        'url': '',
        'setUrl': function(payload) {
            this.url = apiConfig.CLOSEDRFQ();
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'SHARE_RFQ': {
        'url': '',
        'setUrl': function(rfqId, quoteBy) {
            this.url = apiConfig.SHARERFQ() + '/' + rfqId + '/' + quoteBy;
        },
        'method': 'GET',
    },
    'UPDATE_RFQ_STATUS': {
        'url': '',
        'setUrl': function(payload) {
            this.url = apiConfig.UPDATERFQSTATUS();
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    'MERGE_CART': {
        'url': '',
        'setUrl': function (payload) {
            this.url = apiConfig.CART() + 'merge/cart';
            //this.url = "http://10.18.22.218:8080/order-service/api/v1/merge/cart"
            this.data = payload;
        },
        'method': 'POST',
        'data': {}
    },
    
    'GET_PART_DETAILS': {
        'url': '',
        'setUrl': function (payload) {
           this.url=  apiConfig.EXPRESS_CHECKOUT_URL() + "customer/part-details";           
           this.data=payload;
        },
        'method': 'POST',
        'data':{}
    }
}
