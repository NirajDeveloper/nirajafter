/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function dataServices() {

    this.$get = dataService;

    function dataService($log, $q, $http, apiConfig, appInfoService, $rootScope) {
        'ngInject';
        // Add all the dependencies that need to be used in individual functions here.
        let DI = {
            log: $log,
            http: $http,
            q: $q,
            apiConfig: apiConfig,
            appInfoService: appInfoService,
            $rootScope: $rootScope
        };

        return {
            //register your api calls here
            partSearch: partSearch(DI),
            autoSearch: autoSearch(DI),
            catSearch: catSearch(DI),
            part: part(DI),
            partByPartNum: partByPartNum(DI),
            appInfo: appInfo(DI),
            ymmSearch: ymmSearch(DI),
            orderList: orderList(DI),
            shareList: shareList(DI),
            sharedOrderList: sharedOrderList(DI),
            shareOrderList: shareOrderList(DI),
            emailPart: emailPart(DI),
            partAvailability: partAvailability(DI),
            partPricing: partPricing(DI),
            addToCart: addToCart(DI),
            registerUser: registerUser(DI),
            getCart: getCart(DI),
            getCartPrice: getCartPrice(DI),
            updateCart: updateCart(DI),
            deleteCart: deleteCart(DI),
            getOrderHistory: getOrderHistory(DI),
            getOrderDetails: getOrderDetails(DI),
            getInvoiceList: getInvoiceList(DI),
            getInvoice: getInvoice(DI),
            checkUserExistance: checkUserExistance(DI),
            createOrUpdateShopUser: createOrUpdateShopUser(DI),
            createOrUpdateUser: createOrUpdateUser(DI),
            fetchUserProfileByUserId: fetchUserProfileByUserId(DI),
            checkOut: checkOut(DI),
            cartSearch: cartSearch(DI),
            addShipAddr: addShipAddr(DI),
            getShipAddr: getShipAddr(DI),
            cartCount: cartCount(DI),
            getBackOrderList: getBackOrderList(DI),
            getInvoiceDetails: getInvoiceDetails(DI),
            getCarrier: getCarrier(DI),
            placeOrder: placeOrder(DI),
            getLists: getLists(DI),
            createList: createList(DI),
            fetchListItems: fetchListItems(DI),
            updateOrderlistItem: updateOrderlistItem(DI),
            deleteList: deleteList(DI),
            deleteItemFromOrderlist: deleteItemFromOrderlist(DI),
            addItemToOrderList: addItemToOrderList(DI),
            getEntitlementAccess: getEntitlementAccess(DI),
            getRtbEntitlementAccess: getRtbEntitlementAccess(DI),
            bulkRTBCheck: bulkRTBCheck(DI),
            getOtpForFp: getOtpForFp(DI),
            verifyOtpForFp: verifyOtpForFp(DI),
            resetPswrdForFp: resetPswrdForFp(DI),
            changePswrdForCp: changePswrdForCp(DI),
            bulkAvailabilityCheck: bulkAvailabilityCheck(DI),
            fetchCustListByUserId: fetchCustListByUserId(DI),
            getAllUserProfilesForReport: getAllUserProfilesForReport(DI),
            getAllPermissionsOfLoggedInUser: getAllPermissionsOfLoggedInUser(DI),
            addToRFQ: addToRFQ(DI),
            getDraftList: getDraftList(DI),
            deleteRFQItem: deleteRFQItem(DI),
            getDealerLocatorData: getDealerLocatorData(DI),
            getPackingSlips: getPackingSlips(DI),
            getDeliveryNumbers: getDeliveryNumbers(DI),
            getOrderStatus: getOrderStatus(DI),
            switchCustomer: switchCustomer(DI),
            openRFQ: openRFQ(DI),
            closedRFQ: closedRFQ(DI),
            shareRFQ: shareRFQ(DI),
            updateRFQStatus: updateRFQStatus(DI),
            mergeCart: mergeCart(DI),
            getCartById: getCartById(DI),
            checkEmailExistance: checkEmailExistance(DI),
            fetchCountryList: fetchCountryList(DI),
            fetchStateListByCountry: fetchStateListByCountry(DI),
            downloadReportByUserType: downloadReportByUserType(DI),
            renameList: renameList(DI),
            fetchNewTokenForSoldToCustomer: fetchNewTokenForSoldToCustomer(DI),
            updateListItemQuantity: updateListItemQuantity(DI),
            addRFQReceiver: addRFQReceiver(DI),
            logoutFromApp: logoutFromApp(DI),
            deleteRFQReceiver: deleteRFQReceiver(DI),
            downloadOrderDetails: downloadOrderDetails(DI),
            downloadPostOrderData: downloadPostOrderData(DI),
            sendInvoiceToEmails: sendInvoiceToEmails(DI),
            sendDeliveryNoToEmails: sendDeliveryNoToEmails(DI),
            getWhereToBuyData: getWhereToBuyData(DI),
            getRFQReceiver: getRFQReceiver(DI),
            getRFQDetails: getRFQDetails(DI),
            downloadRFQ: downloadRFQ(DI),
            getRFQNotification: getRFQNotification(DI),
            bulkAvailabilityCheckEo: bulkAvailabilityCheckEo(DI),
            getPartDetails : getPartDetails(DI),
            
            
        }
    }
    dataService.$inject  = ['$log', '$q', '$http','apiConfig', 'appInfoService'];
}

//import your api request files here
import { partSearch } from './requests/part-search.request';
import { autoSearch } from './requests/auto-search.request';
import { catSearch } from './requests/cat-search.request';
import { part } from './requests/part.request';
import { partByPartNum } from './requests/part-by-pnum.request';
import { appInfo } from './requests/app-info.request';
import { ymmSearch } from './requests/ymm-search.request';
import { orderList } from './requests/order-list.request';
import { shareList } from './requests/share-list.request';
import { sharedOrderList } from './requests/shared-order-list.request';
import { shareOrderList } from './requests/share-order-list.request';
import { emailPart } from './requests/email-part.request';
import { partAvailability } from './requests/part-availability.request';
import { partPricing } from './requests/part-pricing.request';
import { addToCart } from './requests/order-mgt/add-to-cart.request';
import { registerUser } from './requests/user-mgt/register.request';
import { getCart } from './requests/order-mgt/get-cart.request';
import { getCartPrice } from './requests/order-mgt/get-cart-price.request';
import { updateCart } from './requests/order-mgt/update-cart.request';
import { deleteCart } from './requests/order-mgt/delete-cart.request';
import { checkOut } from './requests/order-mgt/check-out.request';
import { cartSearch } from './requests/order-mgt/cart-search.request';
import { addShipAddr } from './requests/order-mgt/add-ship-address.request';
import { getShipAddr } from './requests/order-mgt/get-ship-address.request';
import { cartCount } from './requests/order-mgt/cart-total-count.request';
import { getCarrier } from './requests/order-mgt/get-carrier.request';
import { placeOrder } from './requests/order-mgt/place-order.request';
import { bulkAvailabilityCheck } from './requests/order-mgt/bulkAvailabilityCheck.request';
import { bulkAvailabilityCheckEo } from './requests/order-mgt/bulkAvailabilityCheckEo.request';
import { getPartDetails } from './requests/order-mgt/get-part-details.request';

import { getOrderHistory } from './requests/order-history/get-order-list.request';
import { getOrderDetails } from './requests/order-history/get-order-details.request';
import { getInvoiceList } from './requests/order-history/get-invoice-list.request';
import { getInvoice } from './requests/order-history/get-invoice.request';
import { checkUserExistance, createOrUpdateUser, createOrUpdateShopUser, fetchUserProfileByUserId, getEntitlementAccess, getRtbEntitlementAccess,getOtpForFp, verifyOtpForFp, resetPswrdForFp, changePswrdForCp, fetchCustListByUserId, getAllUserProfilesForReport, getAllPermissionsOfLoggedInUser, bulkRTBCheck, switchCustomer, checkEmailExistance, fetchCountryList, fetchStateListByCountry, downloadReportByUserType, fetchNewTokenForSoldToCustomer, logoutFromApp } from './requests/user-mgt/user-profile.request';
import { getBackOrderList } from './requests/order-history/get-backorder.request';
import { getInvoiceDetails } from './requests/order-history/get-invoice-details.request';
import { getPackingSlips } from './requests/order-history/get-packing-slips.request';
import { getDeliveryNumbers } from './requests/order-history/get-delivery-numbers.request';
import { getOrderStatus } from './requests/order-history/get-orderstatus.request';
import { downloadOrderDetails } from './requests/order-history/download-order-details.request';
import { downloadPostOrderData } from './requests/order-history/download-post-order-data.request';
import { sendInvoiceToEmails } from './requests/order-history/send-invoice-to-emails.request';
import { sendDeliveryNoToEmails } from './requests/order-history/send-deliveryno-to-emails.request';
 
import { getLists } from './requests/order-list/get-lists.request';
import { createList } from './requests/order-list/create-list.request';
import { fetchListItems } from './requests/order-list/fetch-list-items.request';
import { updateOrderlistItem } from './requests/order-list/update-item-orderlist.request';
import { deleteList } from './requests/order-list/delete-orderlist.request';
import { renameList } from './requests/order-list/rename-orderlist.request';
import { deleteItemFromOrderlist } from './requests/order-list/delete-item-orderlist.request';
import { addItemToOrderList } from './requests/order-list/add-item-orderlist.request';
import { updateListItemQuantity } from './requests/order-list/update-listitem-quantity.request';

import { addToRFQ } from './requests/shop-user/add-to-rfq.request';
import { getDraftList } from './requests/shop-user/get-draft-list.request';
import { deleteRFQItem } from './requests/shop-user/del-rfq-item.request';
import { getDealerLocatorData } from './requests/shop-user/get-dealer-locator.request'; 
import { openRFQ } from './requests/shop-user/open-rfq.request';
import { closedRFQ } from './requests/shop-user/closed-rfq.request';
import { shareRFQ } from './requests/shop-user/share-rfq.request';
import { updateRFQStatus } from './requests/shop-user/update-rfq-status.request';
import { addRFQReceiver } from './requests/shop-user/add-rfq-receiver.request';
import { deleteRFQReceiver } from './requests/shop-user/delete-rfq-receiver.request';
import { getWhereToBuyData } from './requests/shop-user/get-where-to-buy-data.request';
import { getRFQReceiver } from './requests/shop-user/get-rfq-receiver.request';
import { getRFQDetails } from './requests/shop-user/get-rfq-details.request';
import { downloadRFQ } from './requests/shop-user/download-rfq.request';
import { getRFQNotification } from './requests/shop-user/get-rfq-notification.request';

import { mergeCart } from './requests/order-mgt/merge-cart.request';
import { getCartById } from './requests/order-mgt/get-cartbBy-id.request';
