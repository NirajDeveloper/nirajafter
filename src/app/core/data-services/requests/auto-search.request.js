/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function autoSearch(DI) {
    return function (searchString, category, scope) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.AUTO_SEARCH.setUrl(searchString, category, scope);
            DI.http(DI.apiConfig.AUTO_SEARCH).then(function (response) {
                response.data.APIResponse.resultSetLimit = 10;
                resolve(response.data.APIResponse);
            }, function (error) {
                reject(error);
            });
        });
    }
}