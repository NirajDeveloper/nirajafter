/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function downloadRFQ(DI) {
    return function (id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.DOWNLOAD_RFQ.setUrl(id);
            DI.http(DI.apiConfig.DOWNLOAD_RFQ).then(function (response) {
                resolve(response.data);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}