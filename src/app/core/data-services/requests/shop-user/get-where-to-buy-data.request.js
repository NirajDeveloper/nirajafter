/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function getWhereToBuyData(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_WHERE_TO_BUY.setUrl(payload);
            DI.http(DI.apiConfig.GET_WHERE_TO_BUY).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}