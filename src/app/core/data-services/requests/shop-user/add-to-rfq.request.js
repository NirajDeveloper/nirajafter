/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function addToRFQ(DI) {
    return function (id, payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.ADD_TO_RFQ.setUrl(id, payload);
            DI.http(DI.apiConfig.ADD_TO_RFQ).then(function (response) {
                resolve(response.data);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}