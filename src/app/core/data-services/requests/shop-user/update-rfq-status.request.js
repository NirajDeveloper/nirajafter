/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function updateRFQStatus(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.UPDATE_RFQ_STATUS.setUrl(payload);
            DI.http(DI.apiConfig.UPDATE_RFQ_STATUS).then(function (response) {
                resolve(response.data);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}