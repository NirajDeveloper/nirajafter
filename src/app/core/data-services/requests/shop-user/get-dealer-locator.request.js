/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function getDealerLocatorData(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_DEALER_LOCATOR.setUrl(payload);
            DI.http(DI.apiConfig.GET_DEALER_LOCATOR).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}