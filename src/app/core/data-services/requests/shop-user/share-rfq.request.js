/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function shareRFQ(DI) {
    return function (rfqId,quoteBy) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.SHARE_RFQ.setUrl(rfqId,quoteBy);
            DI.http(DI.apiConfig.SHARE_RFQ).then(function (response) {
                resolve(response.data);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}