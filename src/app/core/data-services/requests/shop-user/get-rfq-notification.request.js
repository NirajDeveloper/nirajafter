/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function getRFQNotification(DI) {
    return function () {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.RFQ_NOTIFICATION.setUrl();
            DI.http(DI.apiConfig.RFQ_NOTIFICATION).then(function (response) {
                resolve(response.data);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}