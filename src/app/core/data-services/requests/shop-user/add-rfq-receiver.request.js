/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function addRFQReceiver(DI) {
    return function (rfqId,custId) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.ADD_RFQ_RECEIVER.setUrl(rfqId,custId);
            DI.http(DI.apiConfig.ADD_RFQ_RECEIVER).then(function (response) {
                resolve(response.data);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}