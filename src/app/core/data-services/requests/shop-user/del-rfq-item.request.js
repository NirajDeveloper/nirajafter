/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function deleteRFQItem(DI) {
    return function (id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.DEL_RFQ_ITEM.setUrl(id);
            DI.http(DI.apiConfig.DEL_RFQ_ITEM).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}