/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function partAvailability(DI) {
    return function (partNumber, cId, oType, qty, pNumber, pCats, packageQty) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.PART_AVILABILITY.setUrl();
            DI.apiConfig.PART_AVILABILITY.setData(cId, oType, qty, partNumber, pNumber, pCats, packageQty);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.PART_AVILABILITY).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}