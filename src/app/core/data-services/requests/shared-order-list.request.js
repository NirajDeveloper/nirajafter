/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function sharedOrderList(DI) {
    return function (id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.SHARED_ORDERLIST.setUrl(id);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.SHARED_ORDERLIST).then(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    }
}
