/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function emailPart(DI) {
    return function (url, from, to, sub, body, isCCEnabled, userId) {
        return DI.q(function (resolve, reject) {
            let reqParams = {
                "partDetailUrl": url,
                "userId": userId,
                "emailDTO": {
                    "from": from,
                    "toEmails": to,
                    "subject": sub,
                    "body": body,
                    "isCCEnabled": isCCEnabled
                }

            }
            DI.apiConfig.EMAIL_PART.setUrl();
            angular.extend(DI.apiConfig.EMAIL_PART.data, reqParams);
            DI.http(DI.apiConfig.EMAIL_PART).then(function (response) {
                if(response.status=== 200){
                    resolve({"success":true});
                }                
            }, function (error) {
                reject(error);
            });
        });
    }
}