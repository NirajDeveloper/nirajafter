/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function getCartById(DI) {
    return function (id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_CARTBY_ID.setUrl(id);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.GET_CARTBY_ID).then(function (response) {
                
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    }
}