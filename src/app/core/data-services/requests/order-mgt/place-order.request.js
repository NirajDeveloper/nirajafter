/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function placeOrder(DI) {
    return function (payload, orderType, cartType) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.PLACE_ORDER.setUrl(payload, orderType=='STK'? 'SO':'EO', cartType);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.PLACE_ORDER).then(function (response) {
                resolve(response);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}