/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function bulkAvailabilityCheckEo(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.PART_AVILABILITY_BULK_EO.setUrl(payload);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.PART_AVILABILITY_BULK_EO).then(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    }
}