/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function addToCart(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.ADD_TO_CART.setUrl(payload);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.ADD_TO_CART).then(function (response) {
               // resolve(response.data.APIResponse);
                resolve(response.data);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}