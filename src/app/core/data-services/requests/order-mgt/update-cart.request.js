/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function updateCart(DI) {
    return function (payload, id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.UPDATE_CART.setUrl(payload, id);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.UPDATE_CART).then(function (response) {
                resolve(response);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}