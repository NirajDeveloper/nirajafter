/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function addShipAddr(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.ADD_SHIP_ADDR.setUrl(payload);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.ADD_SHIP_ADDR).then(function (response) {
                resolve(response.data.APIResponse);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}