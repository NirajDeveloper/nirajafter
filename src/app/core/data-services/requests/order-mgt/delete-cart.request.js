/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function deleteCart(DI) {
    return function (id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.DELETE_CART.setUrl(id);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.DELETE_CART).then(function (response) {
                resolve(response.data);
                //resolve();
            }, function (error) {
                reject(error);
            });
        });
    }
}