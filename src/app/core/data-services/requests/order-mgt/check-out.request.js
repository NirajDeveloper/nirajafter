/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function checkOut(DI) {
    return function (orderType, cartType) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.CHECK_OUT.setUrl(orderType=='STK'? 'SO':'EO', cartType);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.CHECK_OUT).then(function (response) {
                
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    }
}