/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function registerUser(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.REGISTER_USER.setUrl();
            DI.apiConfig.REGISTER_USER.setData(payload);
            DI.http(DI.apiConfig.REGISTER_USER).then(function (response) {
                resolve(response.data.APIResponse);
            }, function (error) {
                reject(error);
            });
        });
    }
}