/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function createOrUpdateUser(DI) {
    return function (user) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.CREATE_UPDATE_USER.setUrl(user);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.CREATE_UPDATE_USER).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function createOrUpdateShopUser(DI) {
    return function (user) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.CREATE_UPDATE_SHOP_USER.setUrl(user);
            DI.http(DI.apiConfig.CREATE_UPDATE_SHOP_USER).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function checkUserExistance(DI) {
    return function (userId) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.CHECK_USER_EXISTENCE.setUrl(userId);
            DI.http(DI.apiConfig.CHECK_USER_EXISTENCE).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function checkEmailExistance(DI) {
    return function (emailId) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.CHECK_EMAIL_EXISTENCE.setUrl(emailId);
            DI.http(DI.apiConfig.CHECK_EMAIL_EXISTENCE).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function fetchCountryList(DI) {
    return function () {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_COUNTRY_LIST.setUrl();
            DI.http(DI.apiConfig.GET_COUNTRY_LIST).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function fetchStateListByCountry(DI) {
    return function (countryCode) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_STATE_LIST.setUrl(countryCode);
            DI.http(DI.apiConfig.GET_STATE_LIST).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function fetchUserProfileByUserId(DI) {
    return function (userId) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_USER_PROFILE_BY_ID.setUrl(userId);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.GET_USER_PROFILE_BY_ID).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function getEntitlementAccess(DI) {
    return function (resource, action) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.VERIFY_ENTITLEMENT_ACCESS.setUrl(resource, action);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.VERIFY_ENTITLEMENT_ACCESS).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function getRtbEntitlementAccess(DI) {
    return function (partNumber) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.VERIFY_ENTITLEMENT_RTB_ACCESS.setUrl(partNumber);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.VERIFY_ENTITLEMENT_RTB_ACCESS).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function getOtpForFp(DI) {
    return function (loginId) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_FP_OTP.setUrl(loginId);
            //DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.GET_FP_OTP).then(function (response) {

                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function verifyOtpForFp(DI) {
    return function (otpDetails) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.VERIFY_FP_OTP.setUrl(otpDetails);
            //DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.VERIFY_FP_OTP).then(function (response) {

                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function resetPswrdForFp(DI) {
    return function (pswrdDtls) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.RESET_PASSWORD.setUrl(pswrdDtls);
            //DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.RESET_PASSWORD).then(function (response) {

                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function changePswrdForCp(DI) {
    return function (pswrdDtls) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.CHANGE_PASSWORD.setUrl(pswrdDtls);
            DI.http(DI.apiConfig.CHANGE_PASSWORD).then(function (response) {

                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function fetchCustListByUserId(DI) {
    return function () {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_CUST_LIST_BY_ID.setUrl();
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.GET_CUST_LIST_BY_ID).then(function (response) {
               // DI.log.debug("response :"+ response.data);
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function getAllUserProfilesForReport(DI) {
    return function (userDetails) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_USER_PROFILES_FOR_REPORT.setUrl(userDetails);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.GET_USER_PROFILES_FOR_REPORT).then(function (response) {

                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function getAllPermissionsOfLoggedInUser(DI) {
    return function () {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_LOGED_IN_USER_PERMISSIONS.setUrl();
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.GET_LOGED_IN_USER_PERMISSIONS).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function bulkRTBCheck(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.BULK_RTB_CHECK.setUrl(payload);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.BULK_RTB_CHECK).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function switchCustomer(DI) {
    return function (customerId) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.SWITCH_CUSTOMER.setUrl(customerId);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.SWITCH_CUSTOMER).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function downloadReportByUserType(DI) {
    return function (userDtls) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.DOWNLOAD_REPORT.setUrl(userDtls);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.DOWNLOAD_REPORT).then(function (response) {

                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function fetchNewTokenForSoldToCustomer(DI) {
    return function (custNum) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_SOLD_TO_TOKEN.setUrl(custNum);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.GET_SOLD_TO_TOKEN).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}

export function logoutFromApp(DI) {
    return function () {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.LOGOUT.setUrl();
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.LOGOUT).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}