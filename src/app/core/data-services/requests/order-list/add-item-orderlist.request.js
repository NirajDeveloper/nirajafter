/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function addItemToOrderList(DI) {
    return function (payload, id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.ADD_ORDERLIST_ITEM.setUrl(id);
            DI.apiConfig.ADD_ORDERLIST_ITEM.setData(payload);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.ADD_ORDERLIST_ITEM).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}
