/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function fetchListItems(DI) {
    return function (id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.FETCH_LIST_ITEMS.setUrl(id);
            //DI.apiConfig.FETCH_LIST_ITEMS.setData({"cartType":cartType,"cartName":name});
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.FETCH_LIST_ITEMS).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}
