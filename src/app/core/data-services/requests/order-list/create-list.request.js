/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function createList(DI) {
    return function (payload, id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.CREATE_ORDER_LIST.setUrl(id);
            DI.apiConfig.CREATE_ORDER_LIST.setData(payload);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.CREATE_ORDER_LIST).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}
