/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function partPricing(DI) {
    return function (partNumber, cId, oType, qty, pNumber, pCats) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.PART_PRICING.setUrl();
             DI.apiConfig.PART_PRICING.setData(cId, oType, qty,partNumber, pNumber, pCats);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.PART_PRICING).then(function (response) {
                //DI.log.debug("response :", response.data.APIResponse);
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}