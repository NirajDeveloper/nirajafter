/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function appInfo(DI) {
    return function (partNumber) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.APPINFO.setUrl();
            DI.http(DI.apiConfig.APPINFO).then(function (response) {
                
                resolve(response.data.APIResponse);
            }, function (error) {
                reject(error);
            });
        });
    }
}