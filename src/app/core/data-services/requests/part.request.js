/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function part(DI) {
    return function (partNumber, icNumber) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.PART.setUrl(partNumber, icNumber);
            DI.http(DI.apiConfig.PART).then(function (response) {
                resolve(response.data.APIResponse);
            }, function (error) {
                reject(error);
            });
        });
    }
}