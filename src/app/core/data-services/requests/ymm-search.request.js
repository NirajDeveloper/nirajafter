/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function ymmSearch(DI) {
    return function (searchString, category,prodLine,year,make,model,from, size ) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.YMM_SEARCH.setUrl(searchString,category,prodLine,year,make,model,from, size);
            DI.http(DI.apiConfig.YMM_SEARCH).then(function (response) {
                resolve(response.data.APIResponse);
            }, function (error) {
                reject(error);
            });
        });
    }
}