/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function getInvoiceList(DI) {
    return function (payload, fDate, tDate) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.INVOICE_LIST.setUrl();
            DI.apiConfig.INVOICE_LIST.setData(payload, fDate, tDate);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.INVOICE_LIST).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}
