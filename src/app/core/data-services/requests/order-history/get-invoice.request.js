/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function getInvoice(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.INVOICE_DETAILS.setUrl(payload.id);
            DI.apiConfig.INVOICE_DETAILS.setData(payload);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.INVOICE_DETAILS).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}
