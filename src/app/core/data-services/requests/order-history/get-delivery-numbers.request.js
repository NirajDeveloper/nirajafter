/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

//getDeliveryNumbers
export function getDeliveryNumbers(DI) {
    return function ( id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_DELIVERY_NUMBERS.setUrl(id);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.GET_DELIVERY_NUMBERS).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}