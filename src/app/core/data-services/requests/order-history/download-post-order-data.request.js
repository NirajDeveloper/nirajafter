export function downloadPostOrderData(DI) {
    return function (tab, postData) {
        return DI.q(function (resolve, reject) {
            if(tab == 'TAB1'){
                DI.apiConfig.DOWNLOAD_POST_ORDER.setUrl(postData, "/downloadRecentOrderDtl");
            }else if(tab == 'TAB2'){
                DI.apiConfig.DOWNLOAD_POST_ORDER.setUrl(postData, "/downloadRecentOrderDtl");
            }else if(tab == 'TAB3'){
                DI.apiConfig.DOWNLOAD_POST_ORDER.setUrl(postData, "/downloadBackOrderDtl");
            }else if(tab == 'TAB4'){
                DI.apiConfig.DOWNLOAD_POST_ORDER.setUrl(postData, "/downloadViewCredits");
            }else{

            }
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.DOWNLOAD_POST_ORDER).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}