/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function downloadOrderDetails(DI) {
    return function (ordernumber) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.DOWNLOAD_ORDER.setUrl(ordernumber);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.DOWNLOAD_ORDER).then(function (response) {

                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}