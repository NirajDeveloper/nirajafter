/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function getOrderHistory(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.ORDER_HISTORY.setUrl();
            DI.apiConfig.ORDER_HISTORY.setData(payload);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.ORDER_HISTORY).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}