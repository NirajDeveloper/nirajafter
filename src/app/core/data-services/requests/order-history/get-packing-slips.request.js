/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

//getDeliveryNumbers
export function getPackingSlips(DI) {
    return function ( id) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.GET_PACKING_SLIPS.setUrl(id);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.GET_PACKING_SLIPS).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}