export function sendDeliveryNoToEmails(DI) {
    return function (payload) {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.SEND_DELIVERYNO_TO_EMAILS.setUrl(payload);
            DI.http.defaults.headers.common.Authorization = localStorage.bearerToken;
            DI.http(DI.apiConfig.SEND_DELIVERYNO_TO_EMAILS).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}