/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function orderList(DI) {
    return function () {
        return DI.q(function (resolve, reject) {
            DI.apiConfig.ORDER_LIST.setUrl();
            DI.http(DI.apiConfig.ORDER_LIST).then(function (response) {
                resolve(response.data);
            }, function (error) {
                reject(error);
            });
        });
    }
}