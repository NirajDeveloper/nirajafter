/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


import {dataServices} from './data-services/data-services.provider';
import {apiConfig} from './data-services/api.config';
import {AppInfoService} from './app-info.service';
import {AppInfoServiceProvider} from './app-info.provider';

angular.module('aftermarket.core', [])
    .constant('apiConfig', apiConfig)
    .provider('dataServices', dataServices)
    .service('appInfoService', AppInfoService);
    //.provider('appInfoService', AppInfoServiceProvider);