/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function AppInfoServiceProvider(apiConfig) {
    let vm = this;

    this.$get = appInfoService;
    function appInfoService() {
        let vm = this;
        return {
            //register your api calls here
            getAppInfo: vm.getAppInfo,
            setAppInfo: vm.setAppInfo,
            getCat: vm.getCat
        }
    }
    this.getAppInfo = function () {
        return vm;
    }


    this.setAppInfo = function (info) {
        this._info = info;
    }


    this.initializeAppInfo = function () {
        let vm = this;
        let initInjector = angular.injector(['ng']);
        let $http = initInjector.get('$http');

        $http(apiConfig.APPINFO).then((response) => {
            vm._info = response.data.APIResponse;
        }, (error) => {
            angular.noop();
        });
    }

    this.getCat = function (id) {
        angular.forEach(this._info.cats, (cat) => {
            if (cat.id === id) {
                return cat;
            }
        });

    }
}

AppInfoServiceProvider.$inject = ['apiConfig'];
