/*Author : Shaifali Jaiswal*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function pageFooterDirective(){

	let directive = {
		restrict:'E',
		templateUrl: 'app/components/footer/footer.html',
		controller: 'PageFooterController',
		controllerAs: 'pgFdr',
        bindToController: true
	};

	return directive;
}