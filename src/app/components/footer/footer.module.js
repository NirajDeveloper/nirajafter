/*Author : Shaifali Jaiswal*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


import { PageFooterController } from './footer.controller';
import { pageFooterDirective } from './footer.directive';

angular.module('aftermarket.footer', ['aftermarket.core'])
    .controller('PageFooterController', PageFooterController)
    .directive('pageFooter',pageFooterDirective);