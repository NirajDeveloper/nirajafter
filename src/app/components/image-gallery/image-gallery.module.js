/*
Author : Gautham Manivannan
*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import { imageGalleryDirective } from './image-gallery.directive';
import { imageGalleryController } from './image-gallery.controller';

angular.module('aftermarket.imageGallery', ['aftermarket.core'])
    .controller('imageGalleryController', imageGalleryController)
    .directive('imageGallery', imageGalleryDirective)
    .directive('ngImageOnLoad', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind("load", function (e) {
                    let mWidth = element.parent().parent().parent()[0].offsetWidth - 20;
                    let mHeight = element.parent().parent().parent()[0].offsetHeight - 60;
                    let width = element[0].offsetWidth;
                    let height = element[0].offsetHeight;

                    if(width > mWidth) {
                        height = (mWidth/width)*height;
                        width = mWidth; 
                    }

                    if(height > mHeight) {
                        width = (mHeight/height)*width;
                        height = mHeight;
                    }

                    element.css('width', width+'px');
                    element.css('height', height+'px');
                    let cWidth = width + 20;
                    let cHeight = height + 60;
                    element.parent().parent().css('width', cWidth +'px');
                    element.parent().parent().css('height', cHeight +'px');
                })
            }
        }
    });