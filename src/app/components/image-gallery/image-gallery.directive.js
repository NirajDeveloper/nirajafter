export
    function imageGalleryDirective() {
    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/image-gallery/image-gallery.html',
        scope: {
            data: '=',
            index: '=',
            name: '='
        },
        controller: 'imageGalleryController',
        controllerAs: 'image',
        bindToController: true
    };
    return directive;
}

