export class imageGalleryController {
    constructor($scope, $rootScope) {
        let vm = this;

        vm.DI = () => ({
            $scope, $rootScope
        });
        vm.show = false;
        vm.cdn_url = $rootScope.protocol + "://d1irm9ez93jyd6.cloudfront.net/store_imgs/";
        vm.img_url = "";
        vm.thumbnails = [];


        $scope.$watch(() => vm.data, function (nv, ov) {
            if (vm.data !== undefined && vm.data.length > 0)
                vm.showGallery();
            else 
                vm.show = false;
        });

        $scope.$watch(() => vm.index, function (nv, ov) {
            vm.getImg(vm.index);
        });

    }


    showGallery() {
        let vm = this;
        vm.show = true;
        vm.thumbnails = [];
        for (let i = 0; i < vm.data.length; i++) {
            vm.thumbnails.push(vm.cdn_url + vm.data[i]);
        }
        vm.getImg(vm.index);
    }

    hideGallery() {
        let vm = this;
        vm.data = [];
    }


    getImg(index) {
        let vm = this;
        vm.img_url = vm.thumbnails[index];
    }
}

imageGalleryController.$inject = ['$scope', '$rootScope'];