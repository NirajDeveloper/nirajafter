/*
    Author : Rohit Rane
*/

/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class ImageZoomController {
    constructor($log, $document, $window, $rootScope) {
        let vm = this;
        vm.DI = () => ({ $log, $document, $window, $rootScope });

        vm.showZoom = false;

        vm.containerDimensions = angular.fromJson(vm.containerDimensions);
        let activeImg = $document[0].getElementById("active-img");
        angular.element(activeImg).css("width", vm.containerDimensions.width - 8 + "px");
        angular.element(activeImg).css("height", vm.containerDimensions.height - 4 + "px");

        vm.zoomLevel = vm.zoomIndex;

        activeImg.onerror = function () {
            this.src = vm.protocol +"://placehold.it/1000x1000/dbdbdb/0099CC/?text=NO+IMAGE";
        }


    }
    enlarge(event) {
        let vm = this;
        let {$document, $window} = vm.DI();
        vm.showZoom = true;
        if (angular.isUndefined(event)) {
            event = vm.event;
        } else {
            vm.event = event;
        }

        let zoomLevel = vm.zoomIndex;
        let imgUrl = vm.src;

        let activeImg = $document[0].getElementById("active-img");
        let crossSection = angular.fromJson(vm.lensDimensions);
        let imgCont = $document[0].getElementById("img-container");
        
        let rect2 = imgCont.getBoundingClientRect();
        crossSection.yOffset = rect2.top + $document[0].documentElement.scrollTop + crossSection.height / 2;
        crossSection.xOffset = rect2.left + crossSection.width / 2;


        let lensCenterY = event.pageY - crossSection.yOffset;
        let lensCenterX = event.pageX - crossSection.xOffset;
        vm.lensCenterY = lensCenterY;
        vm.lensCenterX = lensCenterX;
        lensCenterX < 0 ? lensCenterX = 0 : angular.noop();
        lensCenterY < 0 ? lensCenterY = 0 : angular.noop();
        lensCenterX + crossSection.width > activeImg.offsetWidth ? vm.showZoom = true : angular.noop();
        lensCenterY + crossSection.height > activeImg.offsetHeight ? vm.showZoom = true : angular.noop();

        vm.repositionLens(lensCenterX, lensCenterY, crossSection);

        let zoom = $document[0].getElementById("zoom");
        let zoomElement = angular.element(zoom);
        zoomElement.css("height", crossSection.height * zoomLevel + "px");
        zoomElement.css("width", crossSection.width * zoomLevel + "px");

        let startX = -lensCenterX * zoomLevel;
        let startY = -(lensCenterY) * zoomLevel;

        let zoomedImgHgt = activeImg.offsetHeight * zoomLevel;
        let zoomedImgWdt = activeImg.offsetWidth * zoomLevel;

        zoomElement.css("background", generateBgString(imgUrl, startX, startY, zoomedImgHgt, zoomedImgWdt, vm.protocol));

    }

    repositionLens(lensCenterX, lensCenterY, crossSection) {
        let vm = this;
        let {$document} = vm.DI();
        let lens = $document[0].getElementById("lens");
        let lensElement = angular.element(lens);

        lensElement.css("height", (crossSection.height * (vm.zoomLevel / vm.zoomIndex)) + 'px');
        lensElement.css("width", (crossSection.width * (vm.zoomLevel / vm.zoomIndex)) + 'px');
        lensElement.css("top", lensCenterY + 'px');
        lensElement.css("left", lensCenterX + 'px');
        let activeImg = $document[0].getElementById("active-img");
        (lensCenterX + crossSection.width - 10 > activeImg.offsetWidth ) ? vm.showZoom = false : angular.noop();
        lensCenterY + crossSection.height > activeImg.offsetHeight ? vm.showZoom = false : angular.noop();

    }

    mouseleft() {
        let vm = this;
        vm.showZoom = false;
    }

    _mouseWheelHandler(e) {
        e.preventDefault();
        let vm = this;
        if (e.deltaY > 0) {
            vm.zoomIndex++;
        } 
    }

    /*Attach the below function to ng-mouseover event*/
    zoomInOrOut() {
        let vm = this;
        let {$document} = vm.DI();
        //Mouse wheel event
        let lens = $document[0].getElementById("lens");
        angular.element(lens).on("wheel", function (e) {
            e.preventDefault();
            if (e.deltaY > 0) {
                vm.zoomIndex++;
            } else {
                vm.zoomIndex--;
            }
            vm.enlarge();
        });
    }

    showZoomOrNot() {
        let vm = this;
        let showZoomSection = vm.showZoomSection?angular.fromJson(vm.showZoomSection) : false;
        return showZoomSection && vm.showZoom;
    }

    preventRightClick(e){
        e.preventDefault();
    }

    preventRightClick(e){
        e.preventDefault();
    }
}

function generateBgString(imgUrl, startX, startY, zoomedImgHgt, zoomedImgWdt, httpProtocol) {
    return "url(" + httpProtocol + "://" + imgUrl + ") " + startX + "px" + " " + startY + "px/" + zoomedImgWdt + "px " + zoomedImgHgt + "px " + "no-repeat";
}

ImageZoomController.$inject = ['$log', '$document', '$window', '$rootScope'];
