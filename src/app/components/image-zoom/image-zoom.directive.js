/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function imageZoomDirective() {

    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/image-zoom/image-zoom.html',
        scope: {
            src: '@src',
            zoomIndex: '@zoomIndex',
            lensDimensions: '@lensDimensions',
            containerDimensions: '@containerDimensions',
            showZoomSection: '@showZoomSection',
            protocol: '@'
        },
        controller: 'ImageZoomController',
        controllerAs: 'imgZoom',
        bindToController: true
    };

    return directive;
}