/*
Author : Rohit Rane
*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


import { imageZoomDirective } from './image-zoom.directive';
import { ImageZoomController } from './image-zoom.controller';

angular.module('imageZoom', ['aftermarket.core'])
    .directive('imageZoom', imageZoomDirective)
    .controller('ImageZoomController', ImageZoomController);