/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function SendingDirective($rootScope) {
    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/sending/sending.html',
        scope: {},
        controller: "SendingController",
        controllerAs: 'vm',
        replace: true,
        link: function (scope, element, attrm, vm) {
            
        }
    };
    return directive;
}

SendingDirective.$inject = ['$rootScope'];