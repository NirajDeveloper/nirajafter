/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


import { SendingDirective } from './sending.directive';
import { SendingController } from './sending.controller';

angular.module('sending', [])
	.directive('sendingDirective', SendingDirective)
	.controller('SendingController', SendingController);