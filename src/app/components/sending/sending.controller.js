/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class SendingController {
    constructor($scope, $rootScope, $log, $translate) {
        let vm = this;
        vm.DI = () => ({ $scope, $rootScope, $log });
        vm.sending = true;

		$translate('SENDING.SENDINGTEXT').then((sendingtxt)=>{
			vm.sendingtxt = sendingtxt;
		});
    }
}


SendingController.$inject = ['$scope', '$rootScope', '$log', '$translate'];
