/*
Author : Gautham Manivannan
*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import { dealerlocatorDirective } from './dealer-locator.directive';
import { dealerlocatorController } from './dealer-locator.controller';
import { dealerlocatorService } from './dealer-locator.service';

angular.module('aftermarket.dealerLocator', ['aftermarket.core'])
    .service('dealerlocatorService',dealerlocatorService)
    .controller('dealerlocatorController', dealerlocatorController)
    .directive('dealerLocator', dealerlocatorDirective);