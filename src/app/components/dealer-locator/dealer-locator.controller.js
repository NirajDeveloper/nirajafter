
export class dealerlocatorController {
    constructor($scope, $rootScope, appInfoService, authenticationService, dealerlocatorService, $q) {
        let vm = this;
        vm.DI = () => ({
            $scope, $rootScope, appInfoService, authenticationService, dealerlocatorService, $q
        });
        vm.profile = {};
        vm.profile.zip = "48174";
        vm.info = {};
        vm.mapOptions = {};
        vm.map = null;
        if (appInfoService.appInfo && appInfoService.appInfo.cdnBaseurl) {
              vm.imageBaseUrl = appInfoService.appInfo.cdnBaseurl;
        }

        let intvl = setInterval(() => {
            if (appInfoService.appInfo) {
                clearInterval(intvl);
                vm.info = appInfoService.appInfo;
                vm.profile = JSON.parse(JSON.stringify(authenticationService.currentUserProfileData));
                vm.init();
            }
        }, 100);

        vm.reset();

        vm.dealerType = [{
            index: 0,
            name: "All",
            value: "All"
        }, {
            index: 1,
            name: "Distributor",
            value: "DISTRIBUTOR"
        }, {
            index: 2,
            name: "Dealer",
            value: "DEALER"
        }];
        vm.defaultZoomLevel = 5;
        vm.zoomLevel = [10, 9, 8, 7, 6];
        vm.payloadObj = {};
        vm.resetFilter();
    }

    init() {
        let vm = this;
        vm.data = [];

        vm.resetCircle();
        vm.resetMarkers();
        vm.isEmptyZip = vm.isEmptyZipCode(vm.profile.zip);
        vm.isValidZip = vm.isValidZipCode(vm.profile.zip);
        vm.recenterMap();
        if (!vm.isEmptyZip && vm.isValidZip) {
            vm.isLoading = true;
            vm.recenterMap(vm.profile.zip);
            vm.getData();
        }
    }

    resetFilter() {
        let vm = this;
        vm.distance = "0";
        vm.type = "0";
        vm.sort = "";
        vm.productType = "0";
    }

    recenterMap(zip) {
        let vm = this;
        let positionService = vm.geoPosition(zip);
        positionService.then(function (response) {
            vm.plotMap(response);
        });
    }

    /*moreDetails(index){
     document.getElementById("qty_" + index).style.display='block';
     document.getElementById("less_" + index).style.display='block';
     document.getElementById("more_" + index).style.display='none';
    }
    lessDetails(index){
      document.getElementById("qty_" + index).style.display='none';
      document.getElementById("less_" + index).style.display='none';
      document.getElementById("more_" + index).style.display='block';
    }*/

    geoPosition(zip) {
        let vm = this,
            {
                $q
            } = vm.DI();
        return $q(function (resolve, reject) {
            if (zip) {
                let geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'address': zip }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        let obj = {
                            zoom: vm.zoomLevel[parseInt(vm.distance)],
                            center: new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng())
                        };
                        vm.profile.latitude = results[0].geometry.location.lat();
                        vm.profile.longitude = results[0].geometry.location.lng();
                        resolve(obj);
                    }
                });
            }
            else {
                let obj = {
                    zoom: vm.defaultZoomLevel,
                    center: new google.maps.LatLng(34.5133, -94.1629)
                };
                resolve(obj);
            }
        });
    }

    getData() {
        let vm = this;
        let { dealerlocatorService } = vm.DI();
        let service;
        let payload = {};
        switch (vm.mapType) {
            case 'WHERETOBUY':
                payload = {
                    "compId": 1,
                    "zip": vm.profile.zip,
                    "rfqId": null,
                    "distance": vm.info.whereToBuyDistance[parseInt(vm.distance)],
                    "type": vm.dealerType[parseInt(vm.type)].value,
                    "unit": "mi",
                    "rtbs": (vm.productType === "0") ? null : [vm.productType]
                };
                service = dealerlocatorService.getWhereToBuyData(payload);
                break;
            case 'SENDRFQ':
                payload = {
                    "compId": 1,
                    "zip": vm.profile.zip,
                    "rfqId": vm.rfqId,
                    "distance": vm.info.whereToBuyDistance[parseInt(vm.distance)],
                    "type": vm.dealerType[parseInt(vm.type)].value,
                    "unit": "mi"
                };
                service = dealerlocatorService.getDealerLocatorData(payload);
                break;
            default:
                payload = {
                    "compId": 1,
                    "zip": vm.profile.zip,
                    "rfqId": null,
                    "distance": vm.info.whereToBuyDistance[parseInt(vm.distance)],
                    "type": vm.dealerType[parseInt(vm.type)].value,
                    "unit": "mi",
                    "rtbs": (vm.productType === "" || vm.productType === null || vm.productType === undefined) ? vm.info.whereToBuyProductLine : [vm.productType]
                };
                service = dealerlocatorService.getWhereToBuyData(payload);
                break;
        }

        service.then(function (response) {
            vm.isLoading = false;
            vm.data = vm.serialize(angular.copy(dealerlocatorService.data));

            vm.plotCircle();
            vm.plotMarkers();
        });
    }

    serialize(data) {
        for(let i=0;i<data.length;i++) {
            data[i].index = i;
        }
        return data;
    }

    selectDealer(obj) {
        let vm = this;

        if (!obj.selected) {
            obj.selected = true;
            vm.dealerSelected = obj;
        }
    }

    sortItems() {
        let vm = this;
        let { dealerlocatorService } = vm.DI();
        switch (vm.sort) {
            case "default":
                vm.data = angular.copy(dealerlocatorService.data);
                break;
            case "name":
                let data = angular.copy(dealerlocatorService.data);
                vm.data = data.sort(function (a, b) {
                    var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                    if (nameA < nameB)
                        return -1
                    if (nameA > nameB)
                        return 1
                    return 0;
                });
                break;
            default:
                vm.data = angular.copy(dealerlocatorService.data);
                break;
        }
        vm.plotMarkers();
    }

    plotMap(obj) {
        let vm = this;
        let elem = document.getElementById("dealer-map");
        vm.mapOptions = angular.extend(obj, {
            mapTypeId: 'roadmap',
            mapTypeControl: false,
            mapTypeControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_TOP
            }
        });
        if (vm.map === null) {
            vm.map = new google.maps.Map(elem, vm.mapOptions);
        }
        else {
            vm.map.setOptions(vm.mapOptions);
        }

    }

    plotCircle() {
        let vm = this;
        let positionService = vm.geoPosition(vm.profile.zip);
        positionService.then(function (response) {
            vm.circle = new google.maps.Circle({
                center: response.center,
                radius: vm.info.whereToBuyDistance[parseInt(vm.distance)] * 1609.344,
                strokeColor: '#0093c6',
                fillColor: "#000000",
                fillOpacity: 0.05,
                strokeOpacity: 0.6,
                strokeWeight: 1,
                map: vm.map
            });

        });
    }

    resetCircle() {
        let vm = this;

        if (vm.circle !== undefined) {
            vm.circle.setMap(null);
        }
    }

    plotMarkers() {
        let vm = this;
        vm.marker = [];
        let image = 'assets/images/drop-pin-edited.png';
        for (let i = 0; i < vm.data.length; i++) {
            let addedZero = (i < 9) ? "0" : "";
            vm.marker[i] = new google.maps.Marker({
                position: new google.maps.LatLng(vm.data[i].lat, vm.data[i].lgt),
                map: vm.map,
                icon: image,
                label: {
                    text: addedZero + (i + 1).toString(),
                    color: 'white',
                    fontWeight: 'bold'
                }
            });
            google.maps.event.addListener(vm.marker[i], 'mouseover', (function (marker, i) {
                return function () {
                    vm.infoWindowTrigger(i);
                }
            })(vm.marker[i], i));
        }
    }

    resetMarkers() {
        let vm = this;
        if (vm.marker !== undefined) {
            for (let i = 0; i < vm.marker.length; i++) {
                vm.marker[i].setMap(null);
            }
        }
    }

    infoWindowTrigger(i) {
        let vm = this;
        let marker = vm.marker[i];
        if (vm.infowindowTrigger) {
            vm.infowindowTrigger.close();
        }
        vm.infowindowTrigger = new google.maps.InfoWindow();
        vm.infowindowTrigger.setContent(vm.infoWindowElem(vm.data[i]));
        vm.infowindowTrigger.open(vm.map, marker);
    }

    infoWindowElem(data) {
        let vm = this;
        let address = "";
        address += (data.addressLine1 !== "" || data.addressLine1 !== null) ? data.addressLine1 : '';
        address += (data.addressLine2 !== "" || data.addressLine2 !== null) ? ' ' + data.addressLine2 + '<br/>' : '<br/>';
        address += (data.city !== "" || data.city !== null) ? data.city + ' ' : '';
        address += (data.state !== "" || data.state !== null) ? data.state + ' ' : '';
        address += (data.zip !== "" || data.zip !== null) ? data.zip : '';
        let phone = (data.phone === null) ? '' : data.phone;
        let email = (data.email === null) ? '' : data.email;
        let website = (data.website === null) ? '' : data.website;

        let fromLatlng, toLatlng;
        fromLatlng = vm.profile.latitude + "," + vm.profile.longitude;
        toLatlng = data.lat + "," + data.lgt;

        let elements = '<div class="map-info-window">';
        elements += '<h6>' + parseFloat(data.distance).toFixed(1) + ' Miles</h6>';
        elements += '<h5>' + data.name + '</h5>';
        elements += '<address>' + address + '</address>';
        elements += (phone === '') ? '' : '<p><i class="fa fa-phone" aria-hidden="true" style="margin-right: 5px;"></i>' + phone + '</p>';
        elements += (email === '') ? '' : '<p><i class="fa fa-envelope-o" aria-hidden="true" style="margin-right: 5px;"></i>' + email + '</p>';
        elements += (website === '') ? '' : '<a href="' + website + '" target="_blank">VIEW DEALERS WEBSITE</a>';
        elements += '<a href="https://www.google.co.in/maps/dir/' + fromLatlng + '/' + toLatlng + '" target="_blank">DIRECTIONS</a></div>';
        return elements;
    }

    isValidZipCode(zip) {
        return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zip) || (/^\d+(-\d+)*$/.test(zip) && zip.length === 10) || (/[A-Za-z]\d[A-Za-z] ?\d[A-Za-z]\d/.test(zip));
    }

    isEmptyZipCode(zip) {
        if (zip === undefined || zip === '')
            return true;
        else
            return false;
    }

    filterCollapse() {
        let vm = this;
        vm.isFilterCollapsed = (vm.isFilterCollapsed) ? false : true;
    }

    showThumbnail(imgs, index, title) {
        let vm = this;
        vm.galleryIndex = angular.copy(index);
        vm.galleryImgs = angular.copy(imgs);
        vm.galleryTitle = angular.copy(title);
    }

    geocodeLatLng(lat, lng) {
        let vm = this;
        let { $q } = vm.DI();
        let geocoder = new google.maps.Geocoder();
        let latlng = new google.maps.LatLng(lat, lng);
        return $q(function (resolve, reject) {
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        resolve(results);
                    }
                }
            });
        });

    }

    geocodeZip(zip) {
        let vm = this;
        let { $q } = vm.DI();
        let geocoder = new google.maps.Geocoder();
        return $q(function (resolve, reject) {
            geocoder.geocode({ 'address': zip }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    let obj = {
                        latitude: results[0].geometry.location.lat(),
                        longitude: results[0].geometry.location.lng()
                    };
                    resolve(obj);
                }
            });
        });
    }

    getBrowserLocation() {
        let vm = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                let latlngService = vm.geocodeLatLng(position.coords.latitude, position.coords.longitude);
                latlngService.then(function (data) {
                    vm.profile.zip = data[0]["address_components"][7]["long_name"];
                    document.getElementById("zip_code_model").value = vm.profile.zip;
                    vm.init();
                });
            });
        }
    }

    clear() {
        let vm = this;
        vm.reset();
        vm.init();
    }

    reset() {
        let vm = this;
        vm.data = [];
        vm.profile.zip = '';
        vm.isFilterCollapsed = false;
        vm.isEmptyZip = true;
        vm.isValidZip = false;
        vm.isLoading = false;
    }
}

dealerlocatorController.$inject = ['$scope', '$rootScope', 'appInfoService', 'authenticationService', 'dealerlocatorService', '$q']; 