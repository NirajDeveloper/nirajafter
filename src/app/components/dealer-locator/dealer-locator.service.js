/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class dealerlocatorService {
    constructor(dataServices, $q) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            dataServices,
            $q
        });

    }
    get data() {
        return this._dealerLocatorData;
    }
    set data(data) {
        this._dealerLocatorData = data;
    }

    getWhereToBuyData(payload) {
        let vm = this;
        let { dataServices, $q } = vm.DI();
        return $q(function (resolve, reject) {
            dataServices.getWhereToBuyData(payload).then(function (response) {
                vm.data = response;
                resolve(response);
            });
        });

    }

    getDealerLocatorData(payload, selected) {
        let vm = this;
        let { dataServices, $q } = vm.DI();

        return $q(function (resolve, reject) {
            dataServices.getDealerLocatorData(payload).then(function (response) {
                vm.data = response;
                resolve(response);
            });
        });
    }


}