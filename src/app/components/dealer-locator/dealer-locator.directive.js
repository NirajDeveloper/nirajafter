export
    function dealerlocatorDirective() {
    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/dealer-locator/dealer-locator.html',
        scope: {
            mapType:"@",
            rfqId:"@",
            data: "=",
            dealerSelected: "=",
            dealerClass:"@"
        },
        controller: 'dealerlocatorController',
        controllerAs: 'locator',
        bindToController: true
    };
    return directive;
}

