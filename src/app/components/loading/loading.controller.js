/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class LoadingController {
    constructor($scope,$rootScope,$log, $translate) {
        let vm = this;
        vm.DI = () => ({ $scope,$rootScope,$log });
        vm.loading = false;
    }
}

LoadingController.$inject = ['$scope', '$rootScope', '$log', '$translate'];
