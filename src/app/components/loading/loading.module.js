/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


import { LoadingDirective } from './loading.directive';
import { LoadingController } from './loading.controller';

angular.module('loading', [])
	.directive('loadingDirective', LoadingDirective)
	.controller('LoadingController', LoadingController);