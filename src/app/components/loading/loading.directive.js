/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function LoadingDirective($rootScope) {
    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/loading/loading.html',
        scope: {},
        controller: "LoadingController",
        controllerAs: 'vm',
        replace: true,
        link: function (scope, element, attrm, vm) {
            $rootScope.$on('showLoading', function(event, flag){
                vm.loading = flag;
            });
        }
    };
    return directive;
}

LoadingDirective.$inject = ['$rootScope'];