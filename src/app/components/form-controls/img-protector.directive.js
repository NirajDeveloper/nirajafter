
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function imgProtectorDirective() {

    let directive = {
        restrict: 'E',
        link: function (scope, elem) // removed 'attrs' parameter as per sonar 
        {
            elem.bind('contextmenu', function (event) {
                scope.$apply(function () {
                    event.preventDefault();
                });
            });
        }
    };

    return directive;
}