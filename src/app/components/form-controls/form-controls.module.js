/*
Author : Rohit Rane
*/

/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


import {noSplCharDirective} from './no-spl-char.directive';
import {rightClickDirective} from './right-click.directive';
import {imgProtectorDirective} from './img-protector.directive';
import {keyEnterDirective} from './key-enter.directive';

angular.module('form-controls', [])
  .directive('noSplChar', noSplCharDirective)
  .directive('rightClick', rightClickDirective)
  .directive('img', imgProtectorDirective)
  .directive('keyEnter', keyEnterDirective);
