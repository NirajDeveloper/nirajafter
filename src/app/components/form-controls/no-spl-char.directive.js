/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function noSplCharDirective() {

    let directive = {
        restrict: 'A',
        scope: {
            ngModel: '='
            },
        link: function(scope, elem) {
            //fix for PZVAF-669
            // var elToAppend = angular.element('<div class="caret-wrapper"><span class="glyphicon glyphicon-triangle-top up-counter" aria-hidden="true" id="upCounter"></span> <span class="glyphicon glyphicon-triangle-bottom down-counter" aria-hidden="true" id="downCounter"></span></div>');
            // elem.after(elToAppend);
            // scope.isFocused = false;
            // elem.bind('focus', function(){
            //     scope.isFocused = true;
            // });
            // debugger;
            // elem.next().children().eq(0).on('click', function(event){
            //     event.stopPropagation();
            //     if(!scope.isFocused){
            //         elem[0].focus();
            //     }
            //     scope.$apply(function () {
            //         if(angular.isNumber(scope.ngModel)){
            //             scope.ngModel ++;
            //         }else{
            //             scope.ngModel = 1;
            //         }
                    
            //     });
            // });
            // elem.next().children().eq(1).on('click', function(event){
            //     event.stopPropagation();
            //     if(!scope.isFocused){
            //         elem[0].focus();
            //     }
            //     if(scope.ngModel > 1){
            //         scope.$apply(function () {
            //             scope.ngModel --;
            //         });
            //     }
            // });
            // console.log("document.getElementById('upCounter') :"+ document.getElementById('upCounter'));
            elem.bind('keypress', (e) => {
                var regex = new RegExp("^[0-9]+$");
                var charCode = !e.charCode ? e.which : e.charCode;
                // check for tab & backspace for Firefox:
                if ([0, 8].indexOf(charCode) !== -1) return;
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }

                e.preventDefault();
                return false;
            });
        }
    };

    return directive;
}