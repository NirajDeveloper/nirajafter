/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function keyEnterDirective() {
    'ngInject';
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    if(attrs.value=="" || attrs.value==0)return;
                    scope.$eval(attrs.keyEnter);
                });

                event.preventDefault();
            }
        });
    };
}