export function pageHeaderOrderDirective() {
    'ngInject';

    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/header/header1.html',
        controller: 'PageHeaderController',
        controllerAs: 'pgHdr',
        bindToController: true
    };

    return directive;
}