/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class PageHeaderController {
    constructor($rootScope, $timeout, $state, $document, AftermarketConstants, $location, $scope, SearchBarService, $translate, dataServices, authenticationService, AUTH_EVENTS) {

        let vm = this;
        vm.DI = () => ({ $location, SearchBarService, $scope, dataServices, $rootScope, authenticationService, $state,AUTH_EVENTS });
        vm.websiteLogo = AftermarketConstants.skin.logo;
        vm.rootUrl = AftermarketConstants.skin.root;
        $rootScope.$on("realignMegaMenu", () => {
            $timeout(() => {
                let hiddenLeft = $document[0].getElementById('hidden-left');
                angular.element(hiddenLeft).css("float", "left");
            }, 150);
        });
        $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            //assign the "from" parameter to something
            $rootScope.previousParams = from? from:'';
            $rootScope.previousStatename = from? from.name:'';
            $rootScope.previousStateParams = fromParams? fromParams: "";
        });
        vm.userPermissionsList = authenticationService.userPermissionsList;
        vm.custName = authenticationService.currentUserProfileData.firstName;
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            vm.customerId = authenticationService.associatedCustData.activeCustId;
        }
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            vm.isHomePage = toState.name === "home" ? true : false;
        })
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });
        vm.isHomePage = $state.is("home") ? true : false;
        //vm.isCurrentUserExist=true;
        vm.isCurrentUserExist = !vm.isEmptyObj($rootScope.currentUserProfileData);
        $rootScope.$on('auth-login-success', function () {
            vm.isCurrentUserExist = true;
        });
        let deregistrationCallback = $rootScope.$on("gotCartCount", function (event) {
            vm.getCartCount();
        });
        vm.getCartCount();
        $scope.$on("$destroy", deregistrationCallback);
    }
     getCartCount() {
        let vm = this;
        let { dataServices, $state, $scope } = vm.DI();
        if ($state.current.name == "home" || $state.current.name == "part" || $state.current.name == "ordermgt" || $state.current.name == "searchResults" || $state.current.name == "mylist") {
            dataServices.cartCount().then(function (response) {
                if(response && response.data && response.data.count){
                    vm.cartCount = response.data.count;
                }
            }, function (error) {

            });
        }
    }
    getCartDetails() {
        let vm = this;
        let { $log, dataServices, $state, $scope } = vm.DI();
        $scope.$emit("gotCartCount");
        $state.go("ordermgt");
    }
    expresscheckout(){
        let vm = this;
        let { $state} = vm.DI();
        $state.go("expresscheckout");
    }
    wheretobuy(){
        let vm = this;
        let { $state} = vm.DI();
        $state.go("wheretobuy");
    }
     isEmptyObj(obj) {
        for (var prop in obj) { 
            if (obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    };

    go() {
        let vm = this;
        let {$scope, $rootScope } = vm.DI();
        $scope.$emit("reachedhome");
        $rootScope.$emit("$resetYMM");
        $rootScope.$emit('$resetAdvancedSearch');
    }    
}

PageHeaderController.$inject=['$rootScope', '$timeout', '$state', '$document', 'AftermarketConstants', '$location', '$scope', 'SearchBarService', '$translate', 'dataServices', 'authenticationService'];

