/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class TypeaheadPopupController {
    constructor($log, $translate) {

        let vm = this;
        //Add all the DI this the vm model so that u can use them in the controller functions.
        vm.DI = {
            log: $log
        };

        vm.logger = $log;
        
    }
}

TypeaheadPopupController.$inject = ['$log', '$translate'];