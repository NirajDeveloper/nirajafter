/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function searchBarDirective() {

    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/header/search-bar/search-bar.html',
        scope: {
            creationDate: '='
        },
        controller: 'SearchBarController',
        controllerAs: 'searchBar',
        bindToController: true,
        replace:true
    };

    return directive;
}