/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class advncdSrchController {
    constructor($state, $rootScope, $timeout, SearchBarService, $translate) {
        let vm = this;

        vm.interChange = {
            submit: function(valid, formText) {
                if (valid) {
                    vm.interChange.errMsg = "";
                    let paramObj = {
                        from: 0,
                        'str': formText,
                        'ics': true,
                        cat1: 0
                    };
                    $rootScope.$emit("showAll", true);
                    SearchBarService.srchStr = formText;
                    $timeout(() => {
                        $state.go("searchResults", paramObj);
                    });
                } else {
                
                    $translate('ERRORMSG.VALIDPAARTNOERRORMSG').then((ERRORMSG)=>{
                       vm.interChange.errMsg=ERRORMSG;
                    });
                    //vm.interChange.errMsg = "Please enter a valid part number.";
                }
            },
            errMsg: ""
        }

        vm.advancedMenu = [{
            'name': 'automotive',
            'displayName': "HEADER.MENUS.ADVANCEDSEARCH.ADVANCEDSEARCHYMM",
            'templateUrl': 'app/components/header/menus/advanced-search/selectors/automotive.html',
            'classNme': 'active'
        }, {
            'name': 'interchange',
            'displayName': "HEADER.MENUS.ADVANCEDSEARCH.INTERCHANGE",
            'templateUrl': 'app/components/header/menus/advanced-search/selectors/interchange.html',
            'classNme': ''
        }];



        vm.activeSearch = vm.advancedMenu[0];

        $rootScope.$on('$resetAdvancedSearch', function() {
            vm.changeAdvancedSearch(0, vm.advancedMenu[0]);
        });


    }

    changeAdvancedSearch(index, menuOption) {
        let vm = this;
        vm.activeSearch = menuOption;
        angular.forEach(vm.advancedMenu, (advancedMenu) => {
            advancedMenu.classNme = '';
        });
        vm.advancedMenu[index].classNme = 'active';
    }

    interChangeSearch() {

    }

}

advncdSrchController.$inject = ['$state', '$rootScope', '$timeout', 'SearchBarService', '$translate'];