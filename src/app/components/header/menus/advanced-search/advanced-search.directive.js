
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function advncdSrchDirective() {
    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/header/menus/advanced-search/advanced-search.html',
        controller: 'AdvancedSearchController',
        controllerAs: 'asc',
        bindToController: true,
        replace:true
    };

    return directive;
}
