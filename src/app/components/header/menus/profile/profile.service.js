/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class ProfileService {

    constructor() {
        'ngInject';
        this._shopUser = false;
        this._customer = false;
        this._admin = false;
        this._loggedIn = false;
        this._userNme = "";
    }

    get userNme() {
        return this._userNme;
    }
    set userNme(flag) {
        this._userNme = flag;
    }
    
    get loggedIn() {
        return this._loggedIn;
    }
    set loggedIn(flag) {
        this._loggedIn = flag;
    }

    get shopUser() {
        return this._shopUser;
    }
    set shopUser(flag) {
        this._shopUser = flag;
    }

    get customer() {
        return this._customer;
    }
    set customer(flag) {
        this._customer = flag;
    }

    get admin() {
        return this._admin;
    }
    set admin(flag) {
        this._admin = flag;
    }

    setType(obj) {
        this.loggedIn = false;
        this.admin = false;
        this.shopUser = false;
        this.customer = false;
        this.userNme = "";
        if (obj !== undefined) {
            this.loggedIn = true;
            this.userNme = obj.userId;
            switch (obj.userType) {
                case 'SHOPUSER':
                    this.shopUser = true;
                    break;
                case 'admin':
                    this.admin = true;
                    break;
                case 'customer':
                    this.customer = true;
                    break;
                default:
                    break;
            }
        }
    }
}