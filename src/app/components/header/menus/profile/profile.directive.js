/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function profileMenuDirective() {

    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/header/menus/profile/profile.html',
        scope: {
            currentUserProfileData: '='
        },
        controller: ProfileMenuController,
        controllerAs: 'profile',
        bindToController: true,
        replace: true
    };

    return directive;
}
class ProfileMenuController {
    constructor($uibModal, $log, $rootScope, $window, AftermarketConstants, $location, AUTH_EVENTS, dataServices, authenticationService, $state, $scope, OrderListService, $timeout, ProfileService, SocketIOService, MyRFQService, ManageRFQService, $interval) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $uibModal, $log, $rootScope, $window, AftermarketConstants, $location, dataServices, authenticationService, $state, $scope, OrderListService, $timeout, ProfileService, AUTH_EVENTS, SocketIOService, MyRFQService, ManageRFQService, $interval });

        SocketIOService.init();
        vm.isCurrentUserExist = !vm.isEmptyObj($rootScope.currentUserProfileData);
        if (localStorage.associatedCustData) {
            vm.refreshAssociatedCustomer();
        }

        vm.currentUserProfileData = $rootScope.currentUserProfileData;
        vm.userPermissionsList = authenticationService.userPermissionsList;
        vm.userShortInfo = authenticationService.currentUserShortInfo;
        vm.mylistopen = false;
        vm.searchCustomer = undefined;
        vm.custDrpDnStatus = {
            isOpen: false
        };
        vm.profileDrpDnStatus = {
            isOpen: false
        };
        vm.goto = $state.params.goto;
        vm.gotoRFQ();
        
        vm.refUrl = $state.params.refUrl;
        vm.redirectUrl();
        let updateCustData = $rootScope.$on("ASSOCIATED_CUSTOMER", () => {
            if (localStorage.associatedCustData) {
                vm.refreshAssociatedCustomer();
            }
        });

        $scope.$on("$destroy", updateCustData);

        let openSignin = $rootScope.$on("openSignin", () => {
            vm.openSignInPopup();
        });
        $scope.$on("$destroy", openSignin);

        // let loginExpired = $rootScope.$on("loginExpired",()=>{
        //     vm.openLoginExpiredPopup();
        // });

        let deregistrationCallback = $rootScope.$on("gotCartCount", function (event) {
            vm.getCartCount();
        });

        $scope.$on("$destroy", deregistrationCallback);

        let destroyLogout = $rootScope.$on("logout", function (event, data) {
            vm.logout();
            vm.openLoginExpiredPopup();
        });

        $scope.$on("$destroy", destroyLogout);

        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });

        $scope.$on("$destroy", destroyUserPermissions);

        let custListApiCall = $rootScope.$on("CALL_CUSTLIST_API", function (event) {
            let custNum, dealerCode, custName;
            if(authenticationService.associatedCustData && authenticationService.associatedCustData){
                custNum = authenticationService.associatedCustData.activeCustId;
                dealerCode = authenticationService.associatedCustData.dealerCode;
                custName = authenticationService.associatedCustData.customerName;
            }
            vm.getCustomerList(custNum, custName, dealerCode);
        });

        $scope.$on("$destroy", custListApiCall);

        let addedTempItem = $rootScope.$on("addedTempItem", function (evt, args) {
            let num = args.length;
            if (num === 0) {
                vm.showTempListItemCount = false;
                vm.tempListItemCount = 0;
            }
            else {
                vm.showTempListItemCount = true;
                vm.tempListItemCount = num;
            }
            vm.tempListItemData = args;
        });

        $scope.$on("$destroy", addedTempItem);

        let loginSuccess = $rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
            $timeout(function () {
                vm.lists = OrderListService.myLists;
            }, 4000);
        });

        $scope.$on("$destroy", loginSuccess);


        let refreshList = $rootScope.$on('refreshList', function () {
            vm.lists = OrderListService.myLists;
        });

        $scope.$on("$destroy", refreshList);

        let rfqDetails = $rootScope.$on('rfqNotfDetails', function () {
            vm.getRFQDetails();
        });

        $scope.$on("$destroy", rfqDetails);


        $timeout(function () {
            vm.lists = OrderListService.myLists;
        }, 2000);

        vm.showTempListItemCount = false;
        vm.tempListItemCount = 0;
        //let tempListItems = JSON.parse((sessionStorage["tempItemList"]===undefined)?[]:sessionStorage["tempItemList"]);
        let tempListItems = [];
        if (sessionStorage["tempItemList"] !== undefined)
            tempListItems = JSON.parse(sessionStorage["tempItemList"]);
        $rootScope.$broadcast("addedTempItem", tempListItems);
        vm.getCartCount();
        vm.getRFQDetails();
        vm.currentDate = new Date().valueOf();
        vm.newOpenRFQCount = 0;
    }
    // Start: new implementation
    openSignInPopup() {
        let vm = this,
            size = "lg";
        let { $log, $uibModal, $uibModalInstance, authenticationService, dataServices, ProfileService, $rootScope, $timeout, $scope, $state } = vm.DI();
        let uibLoginInstance = $uibModal.open({
            templateUrl: 'app/userprofile/login/login.html',
            controller: 'LoginController',
            controllerAs: 'login',
            size: size,
            backdrop: 'static',
            animation: false,
            windowClass: 'my-modal-popup sign-in-modal',
            resolve: {
                currentUserProfileData: function () {
                    return $rootScope.currentUserProfileData;
                }
            }
        });
        uibLoginInstance.result.then(function (resData) {
            if (resData.action == 'logged-in') {
                uibLoginInstance.close();
                let timer = $timeout(() => {
                    if (sessionStorage["tempItemList"] !== undefined) {
                        if (sessionStorage["saveList"] === "true") {
                            sessionStorage["saveList"] = undefined;
                            vm.saveTempList();
                        }

                        if (sessionStorage["shareList"] === "true") {
                            sessionStorage["shareList"] = undefined;
                            vm.shareTempList();
                        }
                    }
                    vm.gotoRFQ();
                    
                }, 1000);
                vm.getRFQDetails();
                $scope.$emit("gotCartCount");
                vm.userShortInfo = resData.userShortInfo;
                authenticationService.setCurrentUserShortInfo(vm.userShortInfo);
                vm.isCurrentUserExist = true;
                vm.shipToBillToInfo = angular.copy(authenticationService.shipToSoldToInfo);
                if (vm.shipToBillToInfo && vm.shipToBillToInfo.billTo && vm.shipToBillToInfo.billTo.length > 1) {
                    vm.openShipToSoldToPopUp(vm.shipToBillToInfo, true);
                } else {
                    // by default select the first soldTo address
                    if (vm.shipToBillToInfo && vm.shipToBillToInfo.billTo && vm.shipToBillToInfo.billTo.length == 1) {
                        authenticationService.shipToSoldToInfo.selectedSoldTo = vm.shipToBillToInfo.billTo[0];
                    } else {
                        authenticationService.shipToSoldToInfo.selectedSoldTo = undefined;
                    }
                    localStorage.shipToSoldToInfo = JSON.stringify(authenticationService.shipToSoldToInfo);
                    $rootScope.$emit("SHIP_TO_SOLD_TO_INFO");
                    //vm.redirectUrl();
                }
                if (vm.isCurrentUserExist && vm.userShortInfo.roles.find(function (role) { return role == 'PZV_SHOP_USER'; })) {
                    // do something for shop user if needed
                    vm.hasUserCustList = false;
                } else {
                    vm.getCustomerList(resData.customerId, resData.customerName, resData.dealerCode);
                    // vm.associatedCustData = {};
                    // vm.associatedCustData.activeCustId = resData.customerId;
                    // vm.associatedCustData.customerName = resData.customerName;
                    // vm.associatedCustData.dealerCode = resData.dealerCode;
                    // authenticationService.setAssociatedCustIdToUser(vm.associatedCustData); // to get  custId in diff controller
                    // dataServices.fetchCustListByUserId().then(function (response) {
                    //     vm.customerList = response;
                    //     vm.associatedCustData.custList = vm.customerList;
                    //     authenticationService.setAssociatedCustIdToUser(vm.associatedCustData);
                    // }, function (error) {
                    //     console.log("did not get customer list");
                    // });
                }

                
                //call this api to get looged in user profile info
                dataServices.fetchUserProfileByUserId(resData.userId).then(function (usrProfileRes) {
                    if (usrProfileRes) {
                        authenticationService.setCurrentUserProfileData(usrProfileRes);
                        vm.currentUserProfileData = usrProfileRes;
                        //vm.isCurrentUserExist = !vm.isEmptyObj(vm.currentUserProfileData);
                        $rootScope.$emit("partCardLogIn");
                        $rootScope.$emit("partDetailLogIn");
                        $scope.$emit("wheretobuy");
                    } else {
                        console.log("not getting data from profile service");
                    }
                    if (vm.shipToBillToInfo && vm.shipToBillToInfo.billTo && vm.shipToBillToInfo.billTo.length > 1) {
                       //do something
                    } else {
                        vm.redirectUrl();
                    }
                }, function (error) {
                    console.log("did not get user profile");
                });
            } else if (resData.action == 'forgot-password') {
                vm.openPswrdAssistancePopUp();
            } else if (resData.action == 'open-register') {
                vm.openRegistrationPopUp();
            } else if ('close-pop-up') {

            } else {

            }
        });
        uibLoginInstance.closed.then(function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    }
    redirectUrl(){
        let vm = this;
        let { $state, authenticationService, $window, $rootScope} = vm.DI();
        if (vm.refUrl) {
            if (authenticationService.currentUserProfileData.userType !== undefined) {
                let url = window.location.href;
                let arr = url.split("/");
                $window.location.href = $rootScope.protocol +'://'+ arr[2]+ '/' + vm.refUrl+"?extlink=true";
            }
            else {
                vm.openSignInPopup();
            }
        }
    }
    gotoRFQ() {
        let vm = this;
        let { $state, authenticationService } = vm.DI();
        if (vm.goto === "rfq") {
            if (authenticationService.currentUserProfileData.userType !== undefined) {
                if (authenticationService.userPermissionsList.RequestQuote) {
                    $state.go("myrfq");
                }
                else if (authenticationService.userPermissionsList.QuoteManagement) {
                    $state.go("managerfq");
                }
            }
            else {
                vm.openSignInPopup();
            }
        }
    }

    openRegistrationPopUp() {
        let vm = this,
            size = "lg";
        let { $log, $uibModal, authenticationService, dataServices, $rootScope, $scope, AftermarketConstants } = vm.DI();
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/signup/signup.html',
            controller: 'SignupController',
            controllerAs: 'signup',
            size: size,
            animation: false,
            windowClass: 'my-modal-popup sign-up-modal',
            resolve: {
                items: function () {
                    return vm.items;
                }
            }
        });

        uibModalInstance.result.then(function (userData) {
            // if usser created successfully then only show user registerd successfully message/pop-up
            if (userData.isUserCreated) {
                vm.openRegistrationSuccessPopup(userData); // showing registration successful message for a while
                // automatically login
                let loginPostData = {};
                loginPostData.tenant = AftermarketConstants.appSettings.tenant; //AftermarketConstants.appSettings.tenant
                loginPostData.customerId = null;
                loginPostData.user = userData.userInfo.userId;
                loginPostData.password = userData.userInfo.password;
                authenticationService.login(loginPostData).then(function (loginRes) {
                    if (loginRes && (loginRes.status == 'SUCCESS' || loginRes.action == "success-regiter")) {
                        let bearerToken = "Bearer " + loginRes.message;
                        localStorage.bearerToken = bearerToken.toString();
                        // call api to get user profile details
                        let permissionsObj = {};
                        //convert array to json obj
                        if (loginRes.permissions) {
                            for (let i = 0; i < loginRes.permissions.length; i++) {
                                permissionsObj[loginRes.permissions[i]] = true;
                            }
                        }
                        vm.userShortInfo = {};
                        vm.userShortInfo.firstName = loginRes.firstName;
                        vm.userShortInfo.lastName = loginRes.lastName;
                        vm.userShortInfo.roles = loginRes.roles;
                        authenticationService.setCurrentUserShortInfo(vm.userShortInfo);
                        authenticationService.setCurrentUserPermissions(permissionsObj);
                        vm.isCurrentUserExist = true;
                        $scope.$emit("gotCartCount");
                        $rootScope.$emit("CURRENT_USER_PERMISSIONS"); // catch this event where it is required
                        dataServices.fetchUserProfileByUserId(loginPostData.user).then(function (usrProfileRes) {
                            // user profile details available now
                            authenticationService.setCurrentUserProfileData(usrProfileRes);// set user data to rootscope, cookie, sessionStorage
                            vm.currentUserProfileData = usrProfileRes;
                            // vm.isCurrentUserExist = !vm.isEmptyObj(vm.currentUserProfileData);
                            $rootScope.$emit("auth-login-success");
                            $rootScope.$emit("showWhereToBuyBtn");
                            $rootScope.$emit("partCardLogIn");
                            // console.log("$rootScope.currentUserProfileData : " + JSON.stringify($rootScope.currentUserProfileData));
                        }, function (error) {
                            // vm.logInErrMsg = usrProfileRes.message;
                            // vm.isLogInFailed = true;
                            console.log("not logged In");
                        });
                    } else {
                        // set error message here
                        // vm.logInErrMsg = loginRes.message;
                        // vm.isLogInFailed = true;
                        vm.dataLoading = false;
                    }
                }, function (error) {
                    // vm.logInErrMsg = "Server Error";
                    // vm.isLogInFailed = true;
                    console.log("not logged In");
                });
            } else if (userData.action == "open-signin-popup") {
                vm.openSignInPopup();
            }

        }, function () {

        });
    }

    openRegistrationSuccessPopup(userData) {
        let vm = this,
            size = "lg";
        let { $log, $uibModal, authenticationService } = vm.DI();
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/signup/signup-success-pop-up/signupSuccessPopUp.html',
            controller: 'SignupSuccessController',
            controllerAs: 'registerdsuccess',
            size: size,
            animation: false,
            windowClass: 'my-modal-popup registerd-success-modal',
            resolve: {
                userDetails: function () {
                    return userData.userInfo;
                }
            }
        });

        uibModalInstance.result.then(function (resData) {

        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    }

    openShipToSoldToPopUp(shipToBillToInfo, justLoggedIn) {
        let vm = this,
            size = "lg";
        let { $log, $uibModal, $rootScope, $state, authenticationService, AUTH_EVENTS } = vm.DI();
        shipToBillToInfo.justLoggedIn = justLoggedIn;
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/userprofile/login/ship-to-sold-to/shipToSoldToPopUp.html',
            controller: 'ShipToSoldToController',
            controllerAs: 'shipToSoldToCtrl',
            backdrop: 'static',
            size: size,
            animation: false,
            windowClass: 'my-modal-popup shipto-soldto-modal',
            resolve: {
                shipToBillToInfo: function () {
                    return shipToBillToInfo;
                }
            }
        });

        uibModalInstance.result.then(function (justLoggedIn) {
            if (!justLoggedIn) {
                $rootScope.$emit(AUTH_EVENTS.loginSuccess);
                $state.go('home', {}, { reload: true });
            } else {
                // do something if needed
            }
            vm.redirectUrl();
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
            vm.redirectUrl();
        });
    }

    openLoginExpiredPopup() {
        let vm = this,
            size = "lg";
        let { $log, $uibModal, authenticationService } = vm.DI();
        let userData = {};
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/userprofile/login/login-expired/loginExpiredPopUp.html',
            controller: 'LoginExpiredController',
            controllerAs: 'loginExpired',
            size: size,
            animation: false,
            windowClass: 'my-modal-popup login-expired-modal'
        });

        uibModalInstance.result.then(function () {
            // vm.openSignInPopup(); // un-comment it later 
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    }

    logMeOut() {
        let vm = this;
        let { $log, $rootScope, dataServices, authenticationService, $state } = vm.DI();
        dataServices.logoutFromApp().then(function (response) {
            if(response && response.status == 'SUCCESS'){
                vm.isCurrentUserExist = false;
                vm.currentUserProfileData = undefined;
                vm.selectedCustId = '';
                vm.customerList = [];
                vm.hasUserCustList = false;
                vm.userShortInfo = undefined;
                authenticationService.clearCurrentUserProfileData();
                authenticationService.clearAssociatedCustIdToUser();
                authenticationService.clearCurrentUserPermissions();
                authenticationService.clearCurrentUserShortInfo();
                authenticationService.clearShipToSoldToInfo();
                vm.shopUser = false;
                vm.customer = false;
                vm.admin = false;
                $rootScope.$broadcast("addedTempItem", []);
                $state.go('home', {}, { reload: true });
            }else{
                console.log("Error while logging out");
                vm.logout();
            }
        }, function (error) {
            vm.logout();
            console.log("Server error while logging out");
        });
    }

    logout() {
        let vm = this;
        let { $log, $rootScope, dataServices, authenticationService, $state } = vm.DI();
        vm.isCurrentUserExist = false;
        vm.currentUserProfileData = undefined;
        vm.selectedCustId = '';
        vm.customerList = [];
        vm.hasUserCustList = false;
        vm.userShortInfo = undefined;
        authenticationService.clearCurrentUserProfileData();
        authenticationService.clearAssociatedCustIdToUser();
        authenticationService.clearCurrentUserPermissions();
        authenticationService.clearCurrentUserShortInfo();
        authenticationService.clearShipToSoldToInfo();
        localStorage.checkedOutCartType = 'EO';
        vm.shopUser = false;
        vm.customer = false;
        vm.admin = false;
        $rootScope.$broadcast("addedTempItem", []);
        $state.go('home', {}, { reload: true });
    }

    isEmptyObj(obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    };

    navigateTOProfilePage() {
        let vm = this;
        let { $log, $rootScope, $location, $state } = vm.DI();
        $state.go("userProfile");
    }

    openPswrdAssistancePopUp() {
        let vm = this,
            size = "md";
        let { $log, $uibModal, authenticationService } = vm.DI();
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/userprofile/login/password-assistance/passwordAssistancePopUp.html',
            controller: 'PasswordAssistController',
            controllerAs: 'pswrdAssist',
            size: size,
            backdrop: 'static',
            animation: false,
            windowClass: 'my-modal-popup pswrd-assist-modal',
            resolve: {
                items: function () {
                    return vm.items;
                }
            }
        });

        uibModalInstance.result.then(function (resData) {
            if (resData.action == 'verify-otp') {
                vm.openVerifyOtpPopUp(resData);
            } else if ('close-pop-up') {

            } else {

            }
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    }

    openVerifyOtpPopUp(data) {
        let vm = this,
            size = "md";
        let { $log, $uibModal, authenticationService } = vm.DI();
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/userprofile/login/verify-otp/verifyOtpPopUp.html',
            controller: 'VerifyOtpController',
            controllerAs: 'verifyOtp',
            size: size,
            backdrop: 'static',
            animation: false,
            windowClass: 'my-modal-popup verify-otp-modal',
            resolve: {
                userData: function () {
                    return data;
                }
            }
        });

        uibModalInstance.result.then(function (resData) {
            if (resData.action == 'show-success-pop-up') {
                vm.openChangePswrdScuccessPopUp();
                vm.openSignInPopup();
            } else if ('close-pop-up') {

            } else {

            }
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    }

    openCreatePasswordPopUp(loginId) {
        let vm = this,
            size = "md";
        let { $log, $uibModal, authenticationService } = vm.DI();
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/userprofile/login/create-password/createPasswordPopUp.html',
            controller: 'CreatePasswordController',
            controllerAs: 'createPswrd',
            size: size,
            backdrop: 'static',
            animation: false,
            windowClass: 'my-modal-popup create-pswrd-modal',
            resolve: {
                loginId: function () {
                    return loginId;
                }
            }
        });

        uibModalInstance.result.then(function (resData) {
            if (resData.action == 'show-success-pop-up') {
                vm.openChangePswrdScuccessPopUp();
                vm.openSignInPopup();
            } else if ('close-pop-up') {

            } else {

            }
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    }

    openChangePswrdScuccessPopUp() {
        let vm = this,
            size = "lg";
        let { $log, $uibModal, authenticationService } = vm.DI();
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/userprofile/login/pswrd-changed-success/PasswordChangedSuccessPopUp.html',
            controller: 'PasswordChangedSuccessController',
            controllerAs: 'pswrdChanged',
            size: size,
            backdrop: true,
            animation: false,
            windowClass: 'my-modal-popup pswrd-changed-success-modal',
            resolve: {
                items: function () {
                    return vm.items;
                }
            }
        });

        uibModalInstance.result.then(function (resData) {
            // do something if needed
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    }

    onChangeOfSelectedCustomer(custId) {
        let vm = this;
        let { $log, dataServices, $state, $rootScope, authenticationService, AUTH_EVENTS } = vm.DI();

        // call api to get new token for new customer
        dataServices.switchCustomer(custId).then(function (response) {
            if (response && response.status == 'SUCCESS') {
                vm.selectedCustId = custId;
                vm.associatedCustData.activeCustId = custId;
                vm.associatedCustData.customerName = response.customer.customerName;
                vm.associatedCustData.dealerCode = response.customer.dealerCode;
                vm.associatedCustData.custList = vm.customerList;
                authenticationService.setAssociatedCustIdToUser(vm.associatedCustData);
                
                let bearerToken = "Bearer " + response.message;
                localStorage.bearerToken = bearerToken.toString();
                let permissionsObj = {};
                if (response.permissions) {
                    for (let i = 0; i < response.permissions.length; i++) {
                        permissionsObj[response.permissions[i]] = true;
                    }
                }
                authenticationService.setCurrentUserPermissions(permissionsObj);
                authenticationService.setShipToSoldToInfo(response.customer, response.billTo, true);
                vm.shipToBillToInfo = angular.copy(authenticationService.shipToSoldToInfo);
                if (vm.shipToBillToInfo && vm.shipToBillToInfo.billTo && vm.shipToBillToInfo.billTo.length > 1) {
                    vm.custDrpDnStatus.isOpen = false;
                    vm.openShipToSoldToPopUp(vm.shipToBillToInfo, false);
                } else {
                    // by default select the first soldTo address
                    if (vm.shipToBillToInfo && vm.shipToBillToInfo.billTo && vm.shipToBillToInfo.billTo.length == 1) {
                        authenticationService.shipToSoldToInfo.selectedSoldTo = vm.shipToBillToInfo.billTo[0];
                    } else {
                        authenticationService.shipToSoldToInfo.selectedSoldTo = undefined;
                    }
                    localStorage.shipToSoldToInfo = JSON.stringify(authenticationService.shipToSoldToInfo);
                    $rootScope.$emit("SHIP_TO_SOLD_TO_INFO");
                    $rootScope.$emit(AUTH_EVENTS.loginSuccess);
                    $state.go('home', {}, { reload: true });
                }
            } else {
                console.log("token did not changed for new customer");
            }
        }, function (error) {
            console.log("server error");
        });
    }

    navigateToReport() {
        let vm = this;
        let { $log, dataServices, $state, $scope } = vm.DI();
        $state.go("report");
    }

    refreshAssociatedCustomer() {
        let vm = this
        let { $log, authenticationService } = vm.DI();
        let associatedCustData = angular.copy(authenticationService.associatedCustData);
        vm.associatedCustData = {};
        if (associatedCustData) {
            vm.selectedCustId = associatedCustData.activeCustId;
            vm.associatedCustData.activeCustId = associatedCustData.activeCustId;
            vm.associatedCustData.customerName = associatedCustData.customerName;
            vm.associatedCustData.dealerCode = associatedCustData.dealerCode;
            vm.customerList = associatedCustData.custList;
            vm.hasUserCustList = true;
        } else {
            vm.hasUserCustList = false;
        }
    }
    closeAllDrpDnsOfHeader() {
        let vm = this;
        vm.custDrpDnStatus.isOpen = false;
        vm.profileDrpDnStatus.isOpen = false;
    }

    openLoginDrpDn() {
        let vm = this;
        vm.profileDrpDnStatus.isOpen = true
        vm.custDrpDnStatus.isOpen = false;
    }

    closeCustDrpDn() {
        let vm = this;
        vm.profileDrpDnStatus.isOpen = false
        vm.custDrpDnStatus.isOpen = false;
    }

    closeCustDrpDn() {
        let vm = this;
        vm.profileDrpDnStatus.isOpen = false
        vm.custDrpDnStatus.isOpen = false;
    }

    openCustDrpDn() {
        let vm = this;
        vm.custDrpDnStatus.isOpen = true
        vm.profileDrpDnStatus.isOpen = false;
    }

    switchToUniqueCustomer(filteredArr){
        let vm = this;
        if(filteredArr && filteredArr.length == 1 && (vm.selectedCustId !== filteredArr[0].customerNumber)){
            vm.onChangeOfSelectedCustomer(filteredArr[0].customerNumber);
        }
    }

    getCustomerList(custId, custName, dealerCode){
        let vm = this;
        let {authenticationService, dataServices} = vm.DI();
        vm.associatedCustData = {};
        vm.associatedCustData.activeCustId = custId;
        vm.associatedCustData.customerName = custName;
        vm.associatedCustData.dealerCode = dealerCode;
        authenticationService.setAssociatedCustIdToUser(vm.associatedCustData);
        dataServices.fetchCustListByUserId().then(function (response) {
            vm.customerList = response;
            vm.associatedCustData.custList = vm.customerList;
            authenticationService.setAssociatedCustIdToUser(vm.associatedCustData);
        }, function (error) {
            console.log("did not get customer list");
        });
    }

    //End: new implementation


    getCartDetails() {
        let vm = this;
        let { $log, dataServices, $state, $scope } = vm.DI();
        $scope.$emit("gotCartCount");
        $state.go("ordermgt");
    }
    postOrderList() {
        let vm = this;
        let { $log, dataServices, $state } = vm.DI();
        //            params: { refresh: true },
        $state.go("postorder", { refresh: true });
        //$state.transitionTo("postorder", { refresh: true }, { reload: true, inherit: false, location: true });

    }

    goToListPage(id, name) {
        let vm = this;
        let { $rootScope, $state, OrderListService } = vm.DI();
        OrderListService.defaultListId = id;
        vm.mylistopen = false;
        let obj = { listId: id };
        $state.transitionTo("mylist", obj, { reload: true, inherit: false, location: true });
        //$state.go("mylist");
        //$rootScope.$broadcast('showList', {id:id,name:name});
    }
    gotoMylist(){
        let vm = this;
        let { $state } = vm.DI();
        $state.go("mylist");
    }
    createMyList(evt) {
        let vm = this;
        evt.stopPropagation();
        let { $uibModal, $location } = vm.DI();
        vm.mylistopen = false;
        let modalInstance = $uibModal.open({
            templateUrl: 'app/mylist/createlist/createlist.html',
            controller: 'CreatenewlistController',
            controllerAs: 'newlist',
            size: 'md',
            windowClass: 'my-modal-popup',
            resolve: {
                tempItemList: function () {
                    return null;
                }
            }
        });
    }
    postBackOrderList() {
        let vm = this;
        let { $log, dataServices, $state } = vm.DI();

        $state.go("postorder");
    }
    postInvoiceList() {
        let vm = this;
        let { $log, dataServices, $state } = vm.DI();
        $state.go('invoices');
        //$state.go("postorder");
    }
    getCartCount() {
        let vm = this;
        let { dataServices, $state, $scope } = vm.DI();
        if ($state.current.name == "home" || $state.current.name == "part" || $state.current.name == "ordermgt" || $state.current.name == "searchResults" || $state.current.name == "mylist") {
            dataServices.cartCount().then(function (response) {
                if(response && response.data && response.data.count){
                    vm.cartCount = response.data.count;
                }
            }, function (error) {

            });
        }
    }

    isShopUser(obj) {
        //debugger;
        let vm = this;
        let { $rootScope } = vm.DI();
        if (obj !== undefined && obj.userType === "SHOPUSER") {
            vm.shopUser = true;
        }
        else {
            vm.shopUser = false;
        }
    }

    isCustomer(obj) {
        //debugger;
        let vm = this;
        let { $rootScope } = vm.DI();
        if (obj !== undefined && obj.userType === "CUSTOMER") {
            vm.customer = true;
        }
        else {
            vm.customer = false;
        }
    }

    isAdmin(obj) {
        let vm = this;
        let { $rootScope } = vm.DI();
        if (obj !== undefined && obj.userType === "admin") {
            vm.admin = true;
        }
        else {
            vm.admin = false;
        }
    }

    deleteFromSessionList(item, evt) {
        let vm = this;
        evt.stopPropagation();
        let { $rootScope, OrderListService, dataServices } = vm.DI();
        let tempItemList = [];
        let newTempItemList = [];
        if (sessionStorage["tempItemList"] !== undefined)
            tempItemList = JSON.parse(sessionStorage["tempItemList"]);

        for (let i in tempItemList) {
            if (item.partNo !== tempItemList[i].partNo) {
                newTempItemList.push(tempItemList[i]);
            }
        }
        $rootScope.$broadcast("addedTempItem", newTempItemList);
        $rootScope.$broadcast("removedFromTemp", newTempItemList);
        $rootScope.$broadcast("removedFromTemp1", newTempItemList);
        sessionStorage["tempItemList"] = JSON.stringify(newTempItemList);
    }

    saveList() {
        let vm = this;
        let { $rootScope, $scope } = vm.DI();
        sessionStorage["saveList"] = "true";
        //$scope.$emit("openSignin");
        vm.openSignInPopup();
    }
    shareList() {
        let vm = this;
        let { $rootScope, $scope } = vm.DI();
        sessionStorage["shareList"] = "true";
        //$scope.$emit("openSignin");
        vm.openSignInPopup();
    }

    shareTempList() {
        let vm = this;
        let size = "md";
        let { $rootScope, $uibModal, OrderListService } = vm.DI();
        let TempOrderListService = {};
        TempOrderListService._orderList = JSON.parse(sessionStorage['tempItemList']);
        TempOrderListService.isSessionList = true;
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/mylist/sharelist/share.html',
            size: size,
            controller: 'ShareListController',
            controllerAs: 'SLC',
            backdrop: 'static',
            animation: false,
            //windowClass: 'my-modal-popup rfq-details-modal',
            windowClass: 'share-overlay',
            resolve: {
                listData: function () {
                    return TempOrderListService;
                }
            }
        });
    }

    saveTempList() {
        let vm = this;
        let { $rootScope, $uibModal, $location, OrderListService, dataServices } = vm.DI();
        let tempItemList = [];
        tempItemList = JSON.parse(sessionStorage["tempItemList"]);
        if (tempItemList.length !== 0) {
            let modalInstance = $uibModal.open({
                templateUrl: 'app/mylist/createlist/createlist.html',
                controller: 'CreatenewlistController',
                controllerAs: 'newlist',
                size: 'md',
                windowClass: 'my-modal-popup',
                resolve: {
                    tempItemList: function () {
                        return tempItemList;
                    }
                }
            });
        }
    }

    status(key) {
        let rStr;
        switch (key) {
            case "DRAFT":
                rStr = "MYRFQ.STATUS.DRAFT";
                break;
            case "SENT_TO_DISTRIBUTOR":
                rStr = "MYRFQ.STATUS.SENT_TO_DISTRIBUTOR";
                break;
            case "CANCELLED":
                rStr = "MYRFQ.STATUS.CANCELLED";
                break;
            case "QUOTE_SENT_BY_DISTRIBUTOR":
                rStr = "MYRFQ.STATUS.QUOTE_SENT_BY_DISTRIBUTOR";
                break;
            case "OPENED_BY_DISTRIBUTOR":
                rStr = "MYRFQ.STATUS.OPENED_BY_DISTRIBUTOR";
                break;
            case "CONFIRM_RECEIPT":
                rStr = "MYRFQ.STATUS.CONFIRM_RECEIPT";
                break;
            case "NEW":
                rStr = "MANAGERFQ.STATUS.NEW";
                break;
            case "DRAFT":
                rStr = "MANAGERFQ.STATUS.DRAFT";
                break;
            case "OPEN":
                rStr = "MANAGERFQ.STATUS.OPEN";
                break;
            case "QUOTE_SENT":
                rStr = "MANAGERFQ.STATUS.QUOTE_SENT";
                break;
            case "CLOSED_BY_CLIENT":
                rStr = "MANAGERFQ.STATUS.CLOSED_BY_CLIENT";
                break;
            case "CANCELLED_BY_CLIENT":
                rStr = "MANAGERFQ.STATUS.CANCELLED_BY_CLIENT";
                break;
            case "REMINDER_SENT":
                rStr = "MANAGERFQ.STATUS.REMINDER";
                break;
            default:
                rStr = "";
                break;
        }
        return rStr;
    }

    getRFQDetails() {
        let vm = this;
        let { $rootScope, $scope, MyRFQService, ManageRFQService, authenticationService, $interval, dataServices } = vm.DI();
        vm.newOpenRFQCount = 0;
        vm.rfqMaNotfData = [];
        vm.rfqMyNotfData = [];

        vm.rfqNotfData = [];
        if ((vm.userPermissionsList.RequestQuote !== undefined && vm.userPermissionsList.RequestQuote) || (vm.userPermissionsList.QuoteManagement !== undefined && vm.userPermissionsList.QuoteManagement)) {
            dataServices.getRFQNotification().then(function (response) {
                if(response){
                    vm.rfqNotfData = response;
                    vm.rfqCount = response.length;
                }
                
            });
        }

        /*if (vm.userPermissionsList.RequestQuote !== undefined && vm.userPermissionsList.RequestQuote) {
            dataServices.getRFQNotification().then(function(response) {
                debugger;
            });
            let payload = {
                from: 0,
                size: 10,
                sortParam: "DATE_INITIATED",
                sortDsc: true,
                aggrReq: false
            };
            let openRFQ = MyRFQService.getOpenRFQ(payload);
            openRFQ.then(function (resolve) {
                vm.rfqMyNotfData = MyRFQService.openRFQ.list;
            });
            let payload = {
                from: 1,
                size: 10,
                userType: "SHOPUSER"
            };
            let openRFQ = MyRFQService.getOpenRFQ(payload);
            openRFQ.then(function (resolve) {
                vm.rfqMyNotfData = MyRFQService.openRFQ;
            });
        }

        if (vm.userPermissionsList.QuoteManagement !== undefined && vm.userPermissionsList.QuoteManagement) {

            let intervalObj = $interval(() => {
                if (authenticationService.associatedCustData.activeCustId !== undefined) {
                    $interval.cancel(intervalObj);
                    let payload = {
                        from: 0,
                        size: 10,
                        sortParam: "DATE_INITIATED",
                        sortDsc: true,
                        aggrReq: false
                    };
                    let openRFQ = ManageRFQService.getOpenRFQ(payload);
                    openRFQ.then(function (resolve) {
                        vm.rfqMaNotfData = ManageRFQService.openRFQ.list;
                    });
                    /*let payload = {
                        from: 1,
                        size: 10,
                        userType: "DISTRIBUTOR",
                        customerNumber: authenticationService.associatedCustData.activeCustId
                    };
                    let openRFQ = ManageRFQService.getOpenRFQ(payload);
                    openRFQ.then(function (resolve) {
                        vm.rfqMaNotfData = ManageRFQService.openRFQ;
                    });
    }
}, 1000);

        }*/
    }

    increaseOpenRFQCount(items) {
        let vm = this;
        let {authenticationService } = vm.DI();
        //vm.newOpenRFQCount = 0;
        if (authenticationService.userPermissionsList.QuoteManagement) {
            if (items.reciverStatus === "NEW") {
                vm.newOpenRFQCount++;
            }
        }
        else if (authenticationService.userPermissionsList.RequestQuote) {
            if (items.rfqUserStatus === "QUOTE_SENT_BY_DISTRIBUTOR") {
                vm.newOpenRFQCount++;
            }
        }

    }


}

