/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function orderMenuDirective() {

    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/header/menus/order/order.html',
        controller: OrderMenuController,
        controllerAs: 'orders',
        bindToController: true,
        replace:true
    };

    return directive;
}

class OrderMenuController{
  constructor(){
      angular.noop();
  }
}