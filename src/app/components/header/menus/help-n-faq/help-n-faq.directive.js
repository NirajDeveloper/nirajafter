/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function helpNFaqDirective() {

    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/header/menus/help-n-faq/help-n-faq.html',
        controller: HelpNFaqController,
        controllerAs: 'hnf',
        bindToController: true,
        replace:true
    };

    return directive;
}

class HelpNFaqController{
  constructor(){
      angular.noop();
  }
}