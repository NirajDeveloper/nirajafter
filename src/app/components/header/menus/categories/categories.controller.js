/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class CategoryMenuController {
    constructor($scope, $log, $document, $timeout, $window, $interval, $state, $rootScope, $q, dataServices, appInfoService, BreadCrumbService, SearchBarService, searchNavigationService) {
        let vm = this;

        vm.DI = () => ({ $scope, $log, $document, $timeout, $window, $interval, $state, $rootScope, $q, BreadCrumbService, SearchBarService, searchNavigationService, appInfoService });

        angular.element($window).bind('resize', () => {
            vm._sizeMegaMenuPopover();
        });

        let intervalObj = $interval(() => {
            if (angular.isDefined(appInfoService.appInfo) && angular.isDefined(appInfoService.appInfo.cats)) {
                $interval.cancel(intervalObj);
                vm.categories = appInfoService.appInfo.cats;
                angular.forEach(vm.categories, (cat) => {
                    angular.forEach(cat.children, (child) => {
                        child.displayChildren = [];
                        child.displayChildren[0] = [];
                        let colCnt = 0;
                        angular.forEach(child.children, (gChild, index) => {
                            child.displayChildren[colCnt].push(gChild);
                            if ((index + 1) % 9 === 0 && index + 1 / 9 >= 1) {
                                colCnt++;
                                child.displayChildren[colCnt] = [];
                            }
                        });
                    });

                });
            }
        }, 200);

        $scope.currentIndex = "";
        $scope.setCurrentIndex = function (index) {
            $scope.currentIndex = index;
        }

        $scope.clickHandler = function () {
            angular.noop();
        }

        $rootScope.$on("realignMegaMenu", () => {
            if(vm.categories !== undefined && vm.categories[1] !== undefined){
                vm.categories[1].open = true;
                $timeout(() => {
                    //vm.catHover(vm.categories[0]);
                    //debugger;
                    if(vm.categories !== undefined){
                        vm.categories[1].open = false;
                    }
                });
            }
        });

    }

    catHover(cat) {
        let vm = this;
        //removed $document, $log, $window variables as per sonar
        let { $timeout, $q } = vm.DI();
        let closeOthers = () => {
            return $q((resolve) => {
                cat.open = true;
                angular.forEach(vm.categories, function (item, index, arr) {
                    if (cat.name !== item.name) {
                        item.open = false;
                    }
                    if (index === arr.length - 1) {
                        resolve();
                    }
                });
            });
        }
        vm.hoverTimeout = $timeout(() => {
            closeOthers().then(function () {
                $timeout(() => {
                    vm._sizeMegaMenuPopover();
                }, 150);
            });
        }, 500);


    }

    catLeave(cat) {
        let vm = this;
        let { $timeout} = vm.DI();
        cat.open = false;
        $timeout.cancel(vm.hoverTimeout);

    }

    _sizeMegaMenuPopover() {
        let vm = this;
        let { $document, $window } = vm.DI();
        let subCatList = $document[0].getElementById("subcategories");
        let subCatWidth = $window.innerWidth < 1440 ? $window.innerWidth : 1440;
        angular.element(subCatList).css("width", subCatWidth + "px");
        let subCatMarginLeft = $window.innerWidth < 1440 ? 0 : ($window.innerWidth - 1440) / 2;
        angular.element(subCatList).css("margin-left", subCatMarginLeft + "px");
    }

    setTop(index, parent) {
        let vm = this, { $document, $timeout } = vm.DI();
        if (index > 0) {
            $timeout(() => {
                let hdrStr = "cat_header_" + parent;
                let hdr = $document[0].getElementById(hdrStr);
                let colStr = parent + "_col_" + index;
                let col = $document[0].getElementById(colStr);
                if(hdr){
                    if (parent === "Industrial") {
                        angular.element(col).css("margin-top", hdr.offsetHeight + 3 + "px");
                    } else
                        angular.element(col).css("margin-top", hdr.offsetHeight + 5 + "px");
                }
                
            });
        }
    }

    search(cat1, cat2, cat3) {
        //$state variable removed as per sonar
        let vm = this, {$scope, $rootScope, BreadCrumbService, SearchBarService, searchNavigationService, $timeout, appInfoService} = vm.DI();

        $scope.$emit("reachedhome");
        SearchBarService.productLine = appInfoService.getCat1(cat1.id);
        SearchBarService.sort = null;
        $rootScope.$emit("applyHierarchyScope", cat1);
        BreadCrumbService.showAll = false;
        BreadCrumbService.showOnlyTree = false;
        SearchBarService.srchStr = "";
        SearchBarService.listPreviousFilter = [];
        SearchBarService.selectdeFilters = [];
        $rootScope.$emit("clearCategoryFilter");
        BreadCrumbService.searchToResults = true;
        searchNavigationService.gotoResultsPage(cat1, cat2, cat3, null, null, null, null, null, null, null, "1","");

        $timeout(() => {
            cat1.open = false;
        }, 100);

    }

    linkClicked(evt, cat1) {

        let vm = this, {$scope, $rootScope, BreadCrumbService, SearchBarService, $timeout} = vm.DI();
        SearchBarService.sort = null;
        $scope.$emit("reachedhome");
        $rootScope.$emit("applyHierarchyScope", cat1);
        BreadCrumbService.showAll = false;
        SearchBarService.srchStr = "";
        SearchBarService.listPreviousFilter = [];
        $rootScope.$emit("clearCategoryFilter");
        BreadCrumbService.searchToResults = true;        
        $timeout(() => {
            cat1.open = false;
        }, 100);
    }

    disableRightClick(evt) {
        evt.preventDefault();
    }

} 

CategoryMenuController.$inject = ['$scope', '$log', '$document', '$timeout', '$window', '$interval', '$state', '$rootScope', '$q', 'dataServices', 'appInfoService', 'BreadCrumbService', 'SearchBarService', 'searchNavigationService'];
