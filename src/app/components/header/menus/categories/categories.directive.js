/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function categoryMenuDirective() {
    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/header/menus/categories/categories.html',
        controller: 'CategoryMenuController',
        controllerAs: 'categoryMenu',
        bindToController: true,
        replace: true
    };

    return directive;
}

