
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function interChangeSearchDirective() {
    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/header/menus/interchange-search/interchange-search.html',
        controller: 'interChangeSearchController',
        controllerAs: 'incs',
        bindToController: true,
        replace:true
    };

    return directive;
}
