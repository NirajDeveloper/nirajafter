/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class LangNCurrencyController {
   constructor($translate, $rootScope, AftermarketConstants) {
        let vm = this;
        vm.DI = () => ({ $translate, $rootScope });
        vm.languages = AftermarketConstants.localization;
        vm.languages.activeIndex = 0;
        if(localStorage.lang && localStorage.langIndex) {
           vm.languages.activeIndex = localStorage.langIndex;
        }
        vm.IsLocalToggleAvl=AftermarketConstants.appSettings.language_toggle;
    }

    toggleLang(indx) {
        let vm = this;
        let {$translate, $rootScope} = vm.DI();
        vm.languages.activeIndex = indx;
        $translate.use(vm.languages[vm.languages.activeIndex].langName);
        localStorage.lang = vm.languages[vm.languages.activeIndex].langName;
        localStorage.langIndex = indx;
        $rootScope.$broadcast("localizationChnged");
    }
}

LangNCurrencyController.$inject = ['$translate', '$rootScope', 'AftermarketConstants'];
