/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function langNCurrencyDirective() {

    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/header/menus/lang-n-currency/lang-n-currency.html',
        controller: 'LangNCurrencyController',
        controllerAs: 'lnc',
        bindToController: true,
        replace: true
    };

    return directive;
}

