/*Author:Rohit Rane*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class BreadCrumbController {
    constructor($log, $timeout, $rootScope, $state, $scope, $window, $document, $stateParams, $interval, SearchBarService, BreadCrumbService, appInfoService, searchNavigationService, $translate, AftermarketConstants) {
        let vm = this;
        vm.DI = () => ({ $scope, $rootScope, $state, $window, $document, $timeout, $interval, $stateParams, SearchBarService, appInfoService, BreadCrumbService, searchNavigationService });
        vm.cats = [false, false, false];
        vm.showAll = BreadCrumbService.showAll;
        vm.isBomPart = false;
        vm.rootUrl = AftermarketConstants.skin.root;

        vm._resizeBreadCrumb();

        angular.element($window).bind('resize', () => {
            vm._resizeBreadCrumb();
        });

        $rootScope.$on('checkSearch', function (event, previousSearchString) {
            vm.searchString = previousSearchString;
        });

        let intvl = $interval(() => {
            if (appInfoService.appInfo) {
                $interval.cancel(intvl);
                vm.cats = [$stateParams.cat1 ? appInfoService.getCat1($stateParams.cat1) : false, $stateParams.cat2 ? appInfoService.getCat2($stateParams.cat1, $stateParams.cat2) : false, $stateParams.cat3 ? appInfoService.getCat3($stateParams.cat1, $stateParams.cat2, $stateParams.cat3) : false];
                if ($stateParams.y && $stateParams.mk && $stateParams.md) {

                    vm.ymm = {
                        year: $stateParams.y,
                        make: $stateParams.mk,
                        model: $stateParams.md
                    }
                }
            }

        }, 100);
        if ($state.is('searchResults')) {
            vm.sortItem = vm.sortAttributes[0].displayName;
            if ($stateParams.sort) {
                vm.sortItem = SearchBarService.getParticularSAttr($stateParams.sort).displayName;
            }
            vm.pageState = 'searchResults';
            if ($stateParams.from) {
                vm.pageStart = (Number($stateParams.from) + 1);
                vm.pageEnd = (Number($stateParams.from) + 10);
            }
            else {
                vm.pageStart = "1";
                vm.pageEnd = "10";
            }
            if ($stateParams.mode && $stateParams.mode === "hierarchy") {
                $timeout(() => {
                    vm._intializeCats();
                }, 100);


            } else {

                let intvl = $interval(() => {
                    if (appInfoService.appInfo) {
                        $interval.cancel(intvl);
                        vm.cats = [$stateParams.cat1 ? appInfoService.getCat1($stateParams.cat1) : false, $stateParams.cat2 ? appInfoService.getCat2($stateParams.cat1, $stateParams.cat2) : appInfoService.getCat2WithCat3($stateParams.cat1, $stateParams.cat3), $stateParams.cat3 ? appInfoService.getCat3($stateParams.cat1, $stateParams.cat2, $stateParams.cat3) : false];
                        angular.noop();
                    }

                }, 100);
            }

        } else if ($state.is('part')) {
            if ($stateParams.type === "id") {
                vm.pageState = 'part';
                vm.searchString = SearchBarService.srchStr;

                vm.cats = BreadCrumbService.cats;
            } else if ($stateParams.type === "partnum") {
                vm.isBomPart = true;
            }
        }




        if (BreadCrumbService.searchToResults) {
            vm.showBackButton = true;
        } else {
            vm.showBackButton = false;
        }
        if (BreadCrumbService.showOnlyTree) {
            vm.showPathCats = true;
        } else {
            vm.showPathCats = false;
        }


        let deregistrationCallback = $rootScope.$on("categoryFilterApplied", function () {
            vm._intializeCats();
        });

        let deregistrationCallback2 = $rootScope.$on("clearCategories", function () {
            $timeout(() => {
                vm.cats = [false, false, false];
            }, 100);
        });
        let showAll = $rootScope.$on("showAll", (evt, status) => {
            BreadCrumbService.showAll = status;
            vm.showAll = status;
        });
        //removed event,payload from the below function as per sonar report
        let searchLaunched = $rootScope.$on('searchLaunched', function () {
            $timeout(() => {
                vm._intializeCats();
            }, 200);
        });

        $rootScope.$on('$destroy', function () {
            deregistrationCallback();
            deregistrationCallback2();
            ymmEvent();
            showAll();
            searchLaunched();
        });


    }

    showSubsetInfo(totalResults, resultSetLimit) {
        let retValue;
        (Number(totalResults) > Number(resultSetLimit)) ? retValue = true : retValue = false;
        return retValue;
    }

    goToSearch() {
        let vm = this;
        let {$window} = vm.DI();
        $window.history.back();
    }

    _resizeBreadCrumb() {
        let vm = this;
        let {$window, $document} = vm.DI();

        if ($window.innerWidth > 1440) {
            let mgn = -1 * (($window.innerWidth - 1440) / 2);
            let pdng = -1 * mgn;
            let bdcmb = ($document[0].getElementsByClassName('bread-crumb'))[0];
            angular.element(bdcmb).css("margin-left", mgn + "px");
            angular.element(bdcmb).css("margin-right", mgn + "px");
            angular.element(bdcmb).css("padding-left", pdng + "px");
            angular.element(bdcmb).css("padding-right", pdng + "px");
        }
    }

    sortAction(sortObj) {
        let vm = this;
        //removed scope variable as per sonar report
        let {SearchBarService, $state} = vm.DI();
        if (sortObj.Name === "Relevance") {
            SearchBarService.sort = null;
        } else {
            SearchBarService.sort = {
                sortAttribute: sortObj.Name,
                sortType: sortObj.Type
            };
        }

        let srt = "";
        if (SearchBarService.sort) {
            switch (sortObj.displayName) {
                case 'Popularity':
                    srt = '1';
                    break;
                case 'Part Number : Asc':
                    srt = '2';
                    break;
                case 'Part Number : Desc':
                    srt = '3';
                    break;
                default: angular.noop();
            }
        }


        let paramObj = { "from": "", "size": "", "sort": srt };
        $state.go("searchResults", paramObj);
    }

    showColon(index, cats) {
        let vm = this, resp = true;
        let {appInfoService} = vm.DI();
        if (!(cats[index] && cats[index].id) || (index === cats.length - 1 && vm.searchString === "" && !(vm.ymm && appInfoService.getYMMCatId().toString() === cats[0].id.toString())) || (!(cats[index + 1] && cats[index + 1].id) && !(cats[index + 2] && cats[index + 2].id) && vm.searchString === "" && !(vm.ymm && appInfoService.getYMMCatId().toString() === cats[0].id.toString()))) {
            resp = false;
        }
        /*else if (index === cats.length - 1 && vm.searchString === "" && !(vm.ymm && appInfoService.getYMMCatId().toString() === cats[0].id.toString())) {
            resp = false;
        } else if (!(cats[index + 1] && cats[index + 1].id) && !(cats[index + 2] && cats[index + 2].id) && vm.searchString === "" && !(vm.ymm && appInfoService.getYMMCatId().toString() === cats[0].id.toString())) {
            resp = false;
        }*/
        return resp;
    }

    showYmm() {
        let vm = this, retVal = false;
        let {appInfoService} = vm.DI();
        if (vm.ymm && appInfoService.getYMMCatId().toString() === cats[0].id.toString() && vm.resultLength) {
            retVal = true;
        }
        return retVal;
    }

    search(level = -1, type = 1) {
        let vm = this, {$state, $stateParams, appInfoService, SearchBarService, BreadCrumbService, searchNavigationService} = vm.DI();

        if (level === -1) {
            if (SearchBarService.productLine && SearchBarService.productLine.id) {
                SearchBarService.productLine = appInfoService.getCat1(0);
                SearchBarService.productClass = null;
                SearchBarService.productCategory = null;
                launch();
            }
        }
        else {
            if (type === 1) {
                switch (level) {
                    case 0:
                        if (vm.cats[level + 1] && vm.cats[level + 1].id) {
                            SearchBarService.productLine = appInfoService.getCat1(vm.cats[level].id);
                            SearchBarService.productClass = null;
                            SearchBarService.productCategory = null;
                            SearchBarService.categoryfilters = [];
                            SearchBarService.filters = [];
                            SearchBarService.selectdeFilters = [];
                            launch();

                        }
                        break;
                    case 1:
                        if (vm.cats[level + 1] && vm.cats[level + 1].id) {
                            SearchBarService.productLine = appInfoService.getCat1(vm.cats[level - 1].id);
                            SearchBarService.productClass = appInfoService.getCat2(vm.cats[level - 1].id, vm.cats[level].id);
                            SearchBarService.productCategory = null;
                            launch();
                        }
                        break;
                    case 2:
                        if ($state.is("searchResults")) {
                            angular.noop();
                        } else {
                            SearchBarService.listPreviousFilter = [];
                            SearchBarService.productLine = vm.cats[0];
                            SearchBarService.productClass = vm.cats[1];
                            SearchBarService.productCategory = vm.cats[2];
                            launch();
                        } break;
                    default: angular.noop();
                        break;
                }
            } else {
                BreadCrumbService.showAll = false;
                SearchBarService.srchStr = null;
                switch (level) {
                    case 0:
                        if (vm.categories[level + 1] && vm.categories[level + 1].id) {
                            SearchBarService.productLine = appInfoService.getCat1(vm.categories[level].id);
                            SearchBarService.productClass = null;
                            SearchBarService.productCategory = null;
                            launch();

                        }
                        break;
                    case 1:
                        if (vm.categories[level + 1] && vm.categories[level + 1].id) {
                            SearchBarService.productLine = appInfoService.getCat1(vm.categories[level - 1].id);
                            SearchBarService.productClass = appInfoService.getCat2(vm.categories[level - 1].id, vm.categories[level].id);
                            SearchBarService.productCategory = null;
                            launch();
                        }
                        break;
                    case 2:
                        if ($state.is("searchResults")) {
                            angular.noop();
                        } else {
                            SearchBarService.productLine = vm.categories[0];
                            SearchBarService.productClass = vm.categories[1];
                            SearchBarService.productCategory = vm.categories[2];
                            launch();
                        }
                        break;
                    default: angular.noop();
                        break;
                }
            }

        }

        function launch() {
            let sParams = $stateParams;
            searchNavigationService.gotoResultsPage(SearchBarService.productLine, SearchBarService.productClass, SearchBarService.productCategory, null, SearchBarService.srchStr, null, null, null, null, null, null, sParams.ics);

        }
    }

    _intializeCats() {
        let vm = this;
        let {$interval, $stateParams, SearchBarService, appInfoService, BreadCrumbService} = vm.DI();
        if ($stateParams.cat1) {
            vm.cats[0] = SearchBarService.productLine;
            let intObj = $interval(() => {
                if (appInfoService.appInfo) {
                    vm.cats[1] = SearchBarService.productClass ? SearchBarService.productClass : appInfoService.getCat2WithCat3(SearchBarService.productLine ? SearchBarService.productLine.id : $stateParams.cat1, SearchBarService.productCategory ? SearchBarService.productCategory.id : null);
                    $interval.cancel(intObj);
                    initBCService();
                }
            }, 200);
            vm.cats[2] = SearchBarService.productCategory;
            if (vm.cats[2] && vm.cats[1] && vm.cats[2].id === vm.cats[1].id) {
                vm.cats[2] = null;
            } else if (vm.cats[1] && vm.cats[0] && vm.cats[1].id === vm.cats[0].id) {
                vm.cats[1] = null;
            }
        } else {
            vm.cats = [false, false, false];
        }
        initBCService();
        function initBCService() {
            BreadCrumbService.cats = vm.cats;
        }
    }
    isClickable(level = -1, type = 1) {
        let vm = this, {$state, SearchBarService} = vm.DI();
        let retVal = false;
        if (level === -1) {
            if (SearchBarService.productLine && SearchBarService.productLine.id) {
                retVal = true;
            }
        }
        else {
            if (type === 1) {
                switch (level) {
                    case 0:
                        if (vm.cats[level + 1] && vm.cats[level + 1].id) {
                            retVal = true;
                        }
                        break;
                    case 1:
                        if (vm.cats[level + 1] && vm.cats[level + 1].id) {
                            retVal = true;
                        }
                        angular.noop();
                        break;
                    case 2:
                        if ($state.is("searchResults")) {
                            angular.noop();
                        } else {
                            retVal = true;
                        }
                        break;
                    default: angular.noop();
                        break;
                }
            } else {
                switch (level) {
                    case 0:
                        if (vm.categories[level + 1] && vm.categories[level + 1].id) {
                            retVal = true;
                        }
                        break;
                    case 1:
                        if (vm.categories[level + 1] && vm.categories[level + 1].id) {
                            retVal = true;
                        }
                        angular.noop();
                        break;
                    case 2:
                        if ($state.is("searchResults")) {
                            angular.noop();
                        } else {
                            retVal = true;
                        }
                        break;
                    default: angular.noop();
                        break;
                }

            }

        }
        return retVal;
    }
    goHome() {
        let vm = this;
        let {$scope} = vm.DI();
        $scope.$emit("reachedhome");
    }
}

BreadCrumbController.$inject = ['$log', '$timeout', '$rootScope', '$state', '$scope', '$window', '$document', '$stateParams', '$interval', 'SearchBarService', 'BreadCrumbService', 'appInfoService', 'searchNavigationService', '$translate', 'AftermarketConstants'];
