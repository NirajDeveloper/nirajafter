/*Author:Rohit Rane*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function breadCrumbDirective() {

    let directive = {
        restrict: 'E',
        templateUrl: 'app/components/bread-crumb/bread-crumb.html',
        scope: {
            resultSetLimit: '@resultSetLimit',
            totalResults: '@totalResults',
            resultLength: '@resultLength',
            selMainCategory: '@selMainCategory',
            searchString: '@searchString',
            currentPage: '@currentPage',
            categories: '=categories',
            partNo:'@partNo',
            sortAttributes: '=',
            sortItemChanged: '&'
        },
        controller: 'BreadCrumbController',
        controllerAs: 'bc',
        bindToController: true
    };

    return directive;
}