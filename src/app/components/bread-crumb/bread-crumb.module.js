/*
Author : Rohit Rane
*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


import { breadCrumbDirective } from './bread-crumb.directive';
import { BreadCrumbController } from './bread-crumb.controller';
import { BreadCrumbService } from './bread-crumb.service';

angular.module('breadCrumb', ['aftermarket.core'])
    .directive('breadCrumb', breadCrumbDirective)
    .service('BreadCrumbService', BreadCrumbService)
    .controller('BreadCrumbController', BreadCrumbController);