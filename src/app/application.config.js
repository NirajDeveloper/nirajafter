/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export let appSettings = {
    "currency": "USD",
    "protocol": "https",
    "tenant": "dana.com",
    "order_type":[
        {
            "name": "Stock Order",
            "ordertype": "STK",
            "aclrole":"StockOrder",
            "label": "PRICINGANDAVAILABILITY.ORDERTYPE1",
            "description": "PRICINGANDAVAILABILITY.ORDERTYPE1_DELIVERY",
            "options": {
                "backorder": true
            }
            
        },
        {
            "name": "Emergency Order",
            "ordertype": "EMG",
            "aclrole":"EmergencyOrder",
            "label": "PRICINGANDAVAILABILITY.ORDERTYPE2",
            "description": "PRICINGANDAVAILABILITY.ORDERTYPE2_DELIVERY",
            "options": {
                "backorder": false
            }            
        }
        /*,{
            "name": "Future Order",
            "ordertype": "FO",
            "aclrole":"EmergencyOrder",
            "label": "PRICINGANDAVAILABILITY.ORDERTYPE3",
            "description": "PRICINGANDAVAILABILITY.ORDERTYPE2_DELIVERY",
            "options": {
                "backorder": false
            }
            
        }*/
        
    ],
    "contact_info": {
        "number_info": "United States/Canada  1-800-621-8084",
        "email": "dananorthamericadc@dana.com",
        "extended_hours": "Extended Hours:  Mon-Fri, 8am-6pm EST"        
    },
    "my_list":{
        "price_check": true
    },
    "tenant_address":{
        "companyName": "Dana Heavy Vehicle Systems Group, LLC",
        "addressLine1": '900 Industrial Blvd',
        "addressLine2": 'Crossville, 38555, TN USA',
        "contactNo":"Tel: (800) 621 8084   Fax: (800) 332 6124",
        "emailAddress": "www.dana.com",
        "websiteAddress": "www.spicerparts.com"
    },

    "language_toggle": true

}