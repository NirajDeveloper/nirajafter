/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function routeConfig($stateProvider) {
    $stateProvider
        .state('part', {
            url: '/part/:type/:id?val&cat1&cat2&cat3&str&y&mk&md&ic&cp',
            parent: 'aftermarket',
            templateUrl: 'app/part/part.html',
            controller: 'PartController',
            controllerAs: 'part'
        });
}   

routeConfig.$inject = ['$stateProvider'];