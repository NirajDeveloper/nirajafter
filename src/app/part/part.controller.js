/*Author:Rohit Rane*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class PartController {
    constructor($log, $document, $translate, $stateParams, $location, $scope, $window, $timeout, $uibModal, $interval, SearchBarService, OrderListService, dataServices, AftermarketConstants, authenticationService, AvailabilityService, $rootScope, $state, $sce, $filter) {
        let vm = this;
        vm.DI = () => ({
            $log, $document, $scope, $translate, $stateParams, $location, $window, $timeout, $uibModal, $interval, SearchBarService, OrderListService, dataServices, authenticationService, AvailabilityService, $rootScope, $state, $sce, $filter
        });
        vm.displayAddToCartMsg1 = false;
        vm.displayAddToCartMsg = false;
        vm.showAvailable = true;
        $window.scrollTo(0, 0);
        vm.showAddedList = false;
        vm.showAvblt = false;
        vm.showLists = false;
        vm.isPriceVisible = false;
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });

        if ((authenticationService.userPermissionsList.RequestQuote && authenticationService.userPermissionsList.QuoteManagement === undefined) || (authenticationService.currentUserProfileData.userType === undefined)) {
            vm.showAvblt = false;
        }
        else {
            vm.showAvblt = true;
        }

        vm.customerId = null;
        if (authenticationService.associatedCustData.activeCustId) {
            vm.customerId = authenticationService.associatedCustData.activeCustId;
        }

        if (authenticationService.currentUserProfileData.userType !== undefined) {
            vm.showLists = true;
        }
        let partDetailLogIn = $rootScope.$on("partDetailLogIn", function () {
            vm.showAddedList = false;
            vm.showLists = true;
            if (authenticationService.userPermissionsList.RequestQuote && authenticationService.userPermissionsList.QuoteManagement === undefined) {
                vm.showAvblt = false;
            }
            else {
                vm.showAvblt = true;
            }
            vm.setTooltip();
            vm.displayPrice();
        });
        $rootScope.$on("$destroy", partDetailLogIn);

        vm.myLists = OrderListService.myLists;
        vm.getPart();
        $timeout(function () {
            $scope.$emit("searchbarBlurred");
        });

        let actvPic = $document[0].getElementById("active-pic");

        vm.containerDimensions = {
            height: actvPic ? actvPic.offsetHeight : undefined,
            width: actvPic ? actvPic.offsetWidth : undefined
        }


        vm.lensDimensions = {
            height: 80,
            width: 80
        };

        //orderlist code

        vm.shwOrdrTxt = true;
        vm.shwQty = false;
        vm.shwMsg = false;
        vm.qty = "";

        vm.currentUser = authenticationService.currentUser;
        vm.displayAddToCartMsg = false; //AvailabilityService.addToCartStatus;

        vm.hideForNow = true;

        vm.rootUrl = AftermarketConstants.skin.root;

        angular.element($window).bind('resize', () => {
            vm._resizeImage();
        });

        vm.thumbs = [];

        vm.comptblFilter = {
            'year': $stateParams.y,
            'make': $stateParams.mk,
            'model': $stateParams.md
        };


        vm.isCp = Number($stateParams.cp) ? true : false;
        angular.noop();
        let launchYMMList = $rootScope.$on("displayAddToCart", () => {
            vm.displayAddToCartMsg1 = false;
            vm.displayAddToCartMsg = true;
        });

        $rootScope.$on("$destroy", () => {
            launchYMMList();
            vm.displayAddToCartMsg1 = false;
            vm.displayAddToCartMsg = false;

        });

        let launchAddedMsg = $rootScope.$on("displayAddToCartFrmEmergency", () => {
            vm.displayAddToCartMsg = false;
            vm.displayAddToCartMsg1 = true;
        });

        $rootScope.$on("$destroy", () => {
            launchAddedMsg();
            vm.displayAddToCartMsg1 = false;
            vm.displayAddToCartMsg = false;
        });

        vm.setTooltip();

        let removedFromTemp1 = $rootScope.$on("removedFromTemp1", (evt, args) => {
            vm.showAddedList = false;
            for (let j = 0; j < args.length; j++) {
                if (vm.partData.partNumber === args[j].partNo) {
                    vm.showAddedList = true;
                }
            }
        });

        $rootScope.$on("$destroy", removedFromTemp1);
        vm.checkAvlMsg='';
        vm.qtyAVL='Qty. available:';
        $translate('common.CHECKAVL').then((MSG)=>{
           vm.checkAvlMsg=MSG;
        })
        $translate('common.QTYAVL').then((MSG)=>{
          vm.qtyavl= MSG;
        });




    }

    setInterchange() {
        let vm = this;

        if (vm.partData.interchanges) {
            let interchange = angular.copy(vm.partData.interchanges);
            vm.superSession = false;
            for (let i = 0; i < interchange.length; i++) {
                if (interchange[i].interChgComments === "Supersession") {
                    vm.superSession = true;
                    vm.superSessionPartNumber = interchange[i].partNumber;
                    break;
                }
            }

            if (!vm.superSession) {
                if (vm.partData.interchanges.length > 0) {
                    vm.interChngPart = true;
                }
            }
            else {
                vm.interChngPart = false;
            }
        }
        else {
            vm.interChngPart = false;
        }
    }

    setTooltip() {
        let vm = this;
        let {authenticationService, $translate} = vm.DI();
        //console.log(authenticationService);

        if (authenticationService.currentUserProfileData.userType !== undefined) {
            if (authenticationService.userPermissionsList.RequestQuote) {
                $translate('PART.PARTTOOLTIPMSG1').then((msg) => {
                    vm.tooltip = msg;
                })
                // vm.tooltip = "As a registered user, after you have added parts to list, you can send RFQ. To order this part, contact customer service representative at 1-800-621-8084";
            }
        }
        else {
            $translate('PART.PARTTOOLTIPMSG2').then((msg) => {
                vm.tooltip = msg;
            });

            //vm.tooltip = "Add parts to list, save it or share it for requesting a quote.";
        }
    }

    showMyLists() {
        let vm = this;
        let {OrderListService} = vm.DI();
        vm.myLists = OrderListService.fetchMyLists();
    }

    createNewList() {
        let vm = this;
        let { $uibModal } = vm.DI();
        let modalInstance = $uibModal.open({
            templateUrl: 'app/mylist/createlist/createlist.html',
            controller: 'CreatenewlistController',
            controllerAs: 'newlist',
            size: 'md',
            windowClass: 'my-modal-popup',
            resolve: {
                tempItemList: function () {
                    return null;
                }
            }
        });
        modalInstance.result.then(function (resData) {
            vm.selectedList = resData.listName;
            vm.addToList(resData.code, resData.listName);
        }, function () {
        });
    }

    addToList(id, name) {
        let vm = this;
        let { OrderListService, dataServices } = vm.DI();
        vm.selectedList = name;
        let packageQty = !vm.partData.packageQty ? 1 : vm.partData.packageQty; 
        let temp = [{
            partNo: vm.partData.partNumber,
            quantity: packageQty,
            packageQty: packageQty,
            partName: vm.partData.partDesc,
            uom: vm.partData.packageType ? vm.partData.packageType : "",
            thumbnail: vm.partData.tnImage ? vm.partData.tnImage : "",
            wt: vm.partData.wt ? vm.partData.wt : 0,
            wtUom: vm.partData.wtUom ? vm.partData.wtUom : "",
            catPath: vm.partData.catPath
        }];
        //OrderListService.addItemToOrderList(temp,id);
        // let vm = this;
        // let {dataServices} = vm.DI();
        // let listNumber = listId;
        dataServices.addItemToOrderList(temp, id)
            .then(function (response) {
                //{code: "[524]", mess: "Created"}
                if (response.mess === "Created") {
                    OrderListService.incrementListCount(id);
                    vm.newLineId = response.code.replace(/[\[\]]/g, '');
                }
            }, function (error) {

                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });

    }

    isThumbNailActive(thumb, section = 0) {
        let vm = this,
            status = false;
        switch (section) {
            case 0:
                if (vm.activeThumb.url === thumb.fileName) {
                    status = true;
                }
                break;
            case 1:
                if (vm.activeModal.url === thumb.fileName) {
                    status = true;
                }
                break;
            case 2:
                if (vm.activeBom.url === thumb.fileName) {
                    status = true;
                }
                break;
            default: angular.noop();
        }

        return status;
    }


    in_array(array, id) {
        for (var i in array) {
            if (array[i].partNumber.toString() === id.toString()) {
                return {
                    'quantity': array[i].quantity,
                    'id': array[i].id
                }
            }
        }
        return false;
    }


    change(msg) {
        let vm = this;
        let {OrderListService} = vm.DI();
        if (msg === "shwQty") {
            this.shwOrdrTxt = false;
            this.shwQty = true;
        } else if (msg === "shwMsg") {
            this.shwQty = false;
            this.shwMsg = true;
            let currData = vm.partData;
            let oldItem = false;
            OrderListService.orderList.map(function (item) {
                if (item.partNumber.toString() === currData.partNumber.toString()) {
                    item.quantity = Number(item.quantity) + Number(vm.qty);
                    oldItem = true;
                }
            })

            let imgUrl = "";

            if (vm.thumbs[0] && vm.thumbs[0].fileName) {
                imgUrl = vm.thumbs[0].fileName;
            }

            if (oldItem === false) {
                let temp = {
                    id: OrderListService.orderList ? OrderListService.orderList.length : 0,
                    quantity: vm.qty,
                    addToCart: true,
                    partCategory: currData.categories[0].name,
                    partDesc: currData.partDesc,
                    partNumber: currData.partNumber,
                    partName: currData.partNumber + " " + currData.partDesc,
                    partImageUrl: imgUrl
                };
                OrderListService.orderList.push(temp);
            }
            sessionStorage.orderList = angular.toJson(OrderListService.orderList);
        }
    }
 
    checkAvailability() {
        let vm = this,
            {
                dataServices, $sce, $filter
            } = vm.DI();

        vm.showAvailable = false;
        vm.showAvailableMsg = $sce.trustAsHtml("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i><span class='checking'>"+ vm.checkAvlMsg+"</span>");
        let packageQty = vm.partData.packageQty;
        if(!packageQty || packageQty.toString().trim() == ""){
            packageQty = 1;
        }else{
            packageQty = packageQty;
        }
        dataServices.partAvailability(vm.partData.partNumber, vm.customerId ? vm.customerId : "", "STK", vm.qtyReqd = 9999, vm.partData.partNumber, vm.partData.catPath, packageQty)
            .then(function (response) {
                if (response) {
                    let availQty = response.totalQtyAvail ? response.totalQtyAvail : 0;
                    vm.showAvailableMsg = $sce.trustAsHtml("<span>"+vm.qtyAVL+"<b>" + $filter('number')(availQty) + "</b></span> <a class='glyphicon glyphicon-refresh' ng-click='part.checkAvailability()'></a>");
                } else {
                    vm.showAvailableMsg = $sce.trustAsHtml("<span>"+vm.qtyAVL+"<b>" + 0 + "</b></span> <a class='glyphicon glyphicon-refresh' ng-click='part.checkAvailability()'></a>");
                }

            }, function (error) {
                vm.showAvailableMsg = $sce.trustAsHtml("Error");
            });
    }

    getPart() {
        let vm = this;
        let {$log, $stateParams, $scope, $document, $timeout, $interval, SearchBarService, $rootScope, dataServices} = vm.DI();
        vm.productLine = SearchBarService.productLine;
        $scope.$emit("showLoading", true);
        if ($stateParams.type === "partnum") {
            dataServices.partByPartNum($stateParams.val).then(function (response) {
                vm.partData = response;
                if(response && response.attrs && response.attrs.Weight){
                     let splitwgt=response.attrs.Weight.split(" ");
                     let wgt=parseInt(splitwgt[0])+" "+splitwgt[1];
                     Object.assign( vm.partData.attrs, {'Weight' : wgt});
                      
                }
               
                vm.displayPrice();
                setImages();
                vm._createCompatibilityTab();
                //debugger;
                vm.setInterchange();
                $scope.$emit("showLoading", false);
                $rootScope.$emit("partDataLoaded", vm.partData.partNumber);
                $rootScope.$emit("partDataLoaded", vm.partData.partNumber);
                let temporaryList = JSON.parse(sessionStorage["tempItemList"])
                for (let j = 0; j < temporaryList.length; j++) {
                    if (vm.partData.partNumber === temporaryList[j].partNo) {
                        vm.showAddedList = true;
                    }
                }
            }, function (error) {
                $scope.$emit("showLoading", false);
            });
        } else {
            dataServices.part($stateParams.id, $stateParams.ic ? $stateParams.ic : null).then(function (response) {
                vm.partData = response;
                vm.displayPrice();
                setImages();
             
             if(response && response.attrs && response.attrs.Weight){
                     let splitwgt=response.attrs.Weight.split(" ");
                     let wgt=parseInt(splitwgt[0])+" "+splitwgt[1];
                     Object.assign( vm.partData.attrs, {'Weight' : wgt});
                      
                }
                vm.setInterchange();
                vm._createCompatibilityTab();
                $scope.$emit("showLoading", false);
                let partDetails = {};
                partDetails.partNumber = vm.partData.partNumber;
                partDetails.packageQty = vm.partData.packageQty;
                $rootScope.$emit("partDataLoaded", partDetails);
                //let temporaryList = JSON.parse(sessionStorage["tempItemList"])

                if (sessionStorage["tempItemList"] !== undefined){

                    var temporaryList = JSON.parse(sessionStorage["tempItemList"]);
                }

                if(temporaryList && temporaryList.length){
                    for (let j = 0; j < temporaryList.length; j++) {
                        if (vm.partData.partNumber === temporaryList[j].partNo) {
                            vm.showAddedList = true;
                        }
                    }
                }
                
            }, function (error) {
                $scope.$emit("showLoading", false);
            });
        }

        function setImages() {
            vm.modalImage = [];
            vm.bomImage = [];

            angular.forEach(vm.partData.assets, (thumb) => {
                if (thumb.fileName)
                    thumb.show = true;
                if (thumb.assetType === "primary" || thumb.assetType === "outofpackage") {
                    vm.thumbs.push(thumb);
                } else if (thumb.assetType === "modal") {
                    vm.modalImage.push(thumb);
                } else if (thumb.assetType === "bom") {
                    vm.bomImage.push(thumb);
                }

                vm.activeModal = {};
                vm.activeBom = {};
                vm.activeModal.url = vm.modalImage.length ? vm.modalImage[0].fileName : "";
                vm.activeBom.url = vm.bomImage.length ? vm.bomImage[0].fileName : "";
                angular.noop();

            });
            vm.activeThumb = {
                url: "placehold.it/300x300/dbdbdb/0099CC/?text=NO+IMAGE",
                zoom: false
            };
            angular.forEach(vm.thumbs, (thumb, index) => {
                if (thumb.defaultAssetType) {
                    vm.activeThumb.url = thumb.fileName;
                    vm.activeThumb.zoom = true;
                    delete vm.thumbs[index];
                    vm.thumbs.unshift(thumb);
                }
            });
            let intvl_thumb = $interval(() => {
                let thumbDivs = $document[0].getElementsByClassName("custom-thumbnail");
                angular.forEach(thumbDivs, (thumbDiv) => {
                    var imgs = angular.element(thumbDiv).children();
                    if (imgs.length > 0) {
                        $interval.cancel(intvl_thumb);
                        imgs[0].onerror = function () {
                            angular.element(thumbDiv).css("display", "none");
                            let x = angular.element(this).attr("data-thumb");
                            x = angular.fromJson(x);
                            angular.forEach(vm.thumbs, (thumb) => {
                                if (thumb.fileName === x.fileName) {
                                    thumb.show = false;
                                }
                            });
                        }
                    }

                });

                angular.noop();
            }, 50);
            let intvl = $interval(() => {
                let modal = $document[0].getElementById("modal-image");
                if (modal) {
                    $interval.cancel(intvl);
                    modal.onerror = () => angular.element(modal).css("display", "none");

                }
            }, 50);

            let intvl2 = $interval(() => {
                let bom = $document[0].getElementById("bom-image");
                if (bom) {
                    $interval.cancel(intvl2);
                    bom.onerror = function () {
                        angular.element(bom).css("display", "none");
                    }
                }
            }, 50);

        }
    }
    displayPrice() {
        let vm = this;
        let { authenticationService} = vm.DI();
        if (authenticationService.userPermissionsList.CheckPrice) {
            vm.checkRTB(vm.partData.partNumber);
        } else {
            vm.isPriceVisible = false;
        }
    }

    checkRTB(partNo) {
        let vm = this;
        let { dataServices} = vm.DI();
        let itemArr = [];
        itemArr.push(partNo);
        dataServices.bulkRTBCheck(itemArr).then(function (response) {
            if (response && response.rtbParts && response.rtbParts.length >= 1) {
                vm.getPrice();
            } else {
                vm.isPriceVisible = false;
            }
        }, function (error) {
            vm.isPriceVisible = false;
        });
    }

    getPrice() {
        let vm = this,
            {
                dataServices, authenticationService
            } = vm.DI();
        let customerId = null;
        if (authenticationService.shipToSoldToInfo.selectedSoldTo && authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber) {
            customerId = authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber;
        }
        else if (authenticationService.associatedCustData.activeCustId) {
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        dataServices.partPricing(vm.partData.partNumber, customerId, 'STK', 1, "", []).then(function (partPriceRes) {
            if (partPriceRes && partPriceRes.partPriceDtl) {
                vm.unitPrice = partPriceRes.partPriceDtl.priceDtlMap.STANDARD_BASE ? (partPriceRes.partPriceDtl.priceDtlMap.STANDARD_BASE).toFixed(2) : 0;
                //vm.unitPrice = parseInt(vm.unitPrice);
                vm.coreUnitDeposit = partPriceRes.partPriceDtl.priceDtlMap.CORE_BASE ? (partPriceRes.partPriceDtl.priceDtlMap.CORE_BASE).toFixed(2) : 0;
                //vm.coreUnitDeposit = parseInt(vm.coreUnitDeposit);
                vm.isPriceVisible = true;
            }
            else {
                vm.isPriceVisible = false;
            }
        }, function (error) {
            vm.isPriceVisible = false;
        });
    }

    showThumbNailsSection() {
        let vm = this,
            resp = false;
        angular.forEach(vm.thumbs, (thumb) => {
            resp = resp || thumb.show;
        });
        return resp;
    }

    changeActiveImage(thumb, section = 0) {
        let vm = this;
        switch (section) {
            case 0:
                vm.activeThumb.url = thumb;
                break;
            case 1:
                vm.activeModal.url = thumb;
                break;
            case 2:
                vm.activeBom.url = thumb;
                break;
            default: angular.noop();
                break;
        }

    }

    hasSpecification(attrs) {
        if (attrs) {
            let attrsArr = Object.keys(attrs);
            if (attrsArr.length === 0) {
                return false;
            } else return true
        } else return false;
    }

    hasCompatibility(apps) {
        if (apps) {
            if (apps.length === 0) {
                return false;
            } else return true
        } else return false;
    }

    hasInterchanges(interchanges) {
        if (interchanges) {
            if (interchanges.length === 0) {
                return false;
            } else return true
        } else return false;
    }


    _createCompatibilityTab() {
        let vm = this;
        vm.ymmCompatibilityIndex = 0;
        vm.ymmCompatibilityTab = [], vm.ymmCompatibilityTab1 = [], vm.ymmCompatibilityTab2 = [];
        angular.forEach(vm.partData.apps, (ymm) => {
            vm.ymmFilterYear = (vm.comptblFilter.year === "") ? ymm.year : vm.comptblFilter.year;
            vm.ymmFilterMake = (vm.comptblFilter.make === "") ? ymm.make : vm.comptblFilter.make;
            vm.ymmFilterModel = (vm.comptblFilter.model === "") ? ymm.model : vm.comptblFilter.model;
            if ((ymm.year.match(new RegExp(vm.ymmFilterYear, 'g'))) && (ymm.make.match(new RegExp(vm.ymmFilterMake, 'i'))) && (ymm.model.match(new RegExp(vm.ymmFilterModel, 'i')))) {
                vm.ymmCompatibilityTab.push(ymm);
                (vm.ymmCompatibilityIndex % 10 <= 4) ? vm.ymmCompatibilityTab1.push(ymm) : vm.ymmCompatibilityTab2.push(ymm);
                vm.ymmCompatibilityIndex++;
            }
        });
        vm.partData.apps.currentPage = 1;
        vm.partData.apps.maxSize = 10;
        vm.partData.apps.pageLength = 6;
        vm.partData.apps.colLength = 5;
        vm.partData.apps.totalPages = vm.ymmCompatibilityTab.length;
        vm.partData.apps.showPagination = vm.partData.apps.totalPages > (vm.partData.apps.pageLength - 1) * 2 ? true : false;
    }

    _clearCompatibilityTab() {
        let vm = this;
        vm.comptblFilter.year = "";
        vm.comptblFilter.make = "";
        vm.comptblFilter.model = "";
        vm._createCompatibilityTab();
    }

    _createInterchangesTab() {
        let vm = this;

        vm.ymmInterTab1 = [], vm.ymmInterTab2 = [];

        angular.forEach(vm.partData.interchanges, (ymm, index, compArr) => {
            (index < Math.ceil(compArr.length / 2)) ? vm.ymmInterTab1.push(ymm) : vm.ymmInterTab2.push(ymm);
        });
        vm.partData.interchanges.currentPage = 1;
        vm.partData.interchanges.maxSize = 10;
        vm.partData.interchanges.pageLength = 6;
        vm.partData.interchanges.totalPages = vm.partData.interchanges.length;

        vm.partData.interchanges.showPagination = vm.partData.interchanges.totalPages > (vm.partData.interchanges.pageLength - 1) * 2 ? true : false;

        vm.partData.interchanges.pageChanged = function () {
            angular.noop();
        }
    }
    _resizeImage() {
        let vm = this,
            {
                $document
            } = vm.DI();
        let actvPic = $document[0].getElementById("active-pic");

        vm.containerDimensions = {
            height: actvPic ? actvPic.offsetHeight : undefined,
            width: actvPic ? actvPic.offsetWidth : undefined
        }

    }
    shareViaEmail() {
        let vm = this;
        let {
            $uibModal, $location, $timeout, $document
        } = vm.DI();
        //let parentElem = angular.element($document[0].getElementById('sharePartViaEmail'));
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/part/email/email.html',
            controller: 'EmailController',
            controllerAs: 'email',
            size: 'md',
           // appendTo: parentElem,
            windowClass: 'my-modal-popup email-part-popup',
            resolve: {
                partDetails: function () {
                    return {'url' : $location.absUrl(), 'partNum': vm.partData.partNumber};
                }
            },
            bindToController: true
        });

        uibModalInstance.result.then(function (emailResData) {
            if (emailResData.isEmailSent) {
                vm.isEmailSent = true;
                $timeout(function () {
                    vm.isEmailSent = false;
                }, 3000);
            }
        }, function () {

        });
    }

    isSuperSede() {
        let vm = this;
        let {
            $interval
        } = vm.DI();
        vm.superSedeBadge = false;

        let intvl = $interval(() => {
            if (vm.partData && vm.partData.interchanges) {
                $interval.cancel(intvl);
                angular.forEach(vm.partData.interchanges, (intrChng) => {
                    if (intrChng.interChgComments === 'Supersession') {
                        vm.superSedeBadge = true;
                    }
                });
            }
        }, 50);
    }

    showInterBadge() {
        let vm = this;
        let hasIc = false;
        if (vm.partData && vm.partData.interchanges) {
            hasIc = vm.hasInterchanges(vm.partData.interchanges);
        }

        return hasIc || vm.isCp;
    }

    goToCartPage() {
        let vm = this;
        let {$interval, $state} = vm.DI();

        $state.go('ordermgt');
    }

    addToTempList(data) {
        let vm = this;
        let {$rootScope} = vm.DI();
        vm.showAddedList = true;
        let tempItemList = [];
        if (sessionStorage["tempItemList"] !== undefined)
            tempItemList = JSON.parse(sessionStorage["tempItemList"]);

        let item = {
            partNo: data.partNumber,
            partName: data.partDesc,
            quantity: 1,
            uom: data.packageType ? data.packageType : "",
            thumbnail: data.tnImage ? data.tnImage : "",
            wt: data.wt ? data.wt : 0,
            wtUom: data.wtUom ? data.wtUom : "",
            catPath: data.catPath
        };
        tempItemList.push(item);
        $rootScope.$broadcast("addedTempItem", tempItemList);
        sessionStorage["tempItemList"] = JSON.stringify(tempItemList);
    }

    saveList() {
        let vm = this;
        let { $rootScope, $scope } = vm.DI();
        sessionStorage["saveList"] = "true";
        $scope.$emit("openSignin");
    }
    shareList() {
        let vm = this;
        let { $rootScope, $scope } = vm.DI();
        sessionStorage["shareList"] = "true";
        $scope.$emit("openSignin");
    }
    print(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var popupWin = window.open('', '_blank', 'width=300,height=300');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    }


}
PartController.$inject = ['$log', '$document', '$translate', '$stateParams', '$location', '$scope', '$window', '$timeout', '$uibModal', '$interval', 'SearchBarService', 'OrderListService', 'dataServices', 'AftermarketConstants', 'authenticationService', 'AvailabilityService', '$rootScope', '$state', '$sce','$filter'];

