/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/



import {PartController} from './part.controller';
import {routeConfig} from './part.route';
import {EmailController} from './email/email.controller';

angular.module('aftermarket.part', [])
    .config(routeConfig)
    .controller('PartController', PartController)
    .controller('EmailController', EmailController)
   