/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class EmailController {
    constructor( $uibModalInstance, $rootScope, $document, $timeout, $scope, dataServices, partDetails, $log, $translate) {
        let vm = this;
        vm.showSending = false;
        vm.DI = () => ({ $uibModalInstance, $timeout, $scope, $log, $rootScope, dataServices, partDetails });

        $translate('ERRORMSG.PARTNOTVALID').then((MSG)=>{
                     vm.subject=MSG;
        })
         $translate('ERRORMSG.PARTNOTVALID').then((MSG)=>{
                     vm.body= MSG;
        })
        vm.partNum = partDetails.partNum;
        vm.userId = "";
        vm.isCopied = false;
        if (localStorage["currentUserProfileData"] !== undefined) {
            vm.isUserLogged = true;
            vm.userId = JSON.parse(localStorage.currentUserProfileData).userId;
        } else {
            vm.isUserLogged = false;
            vm.userId = "";
        }
        vm.subject = "The following product has been shared with you.";
        vm.body = "I found this part on http://phasezero.xyz/. Please checkout it's details. ";
    }

    selectCopied(){
       let vm = this; 
    }

    clearEmailTextField(){
        let vm = this;
        vm.to = "";
        vm.isInvalidEmailList = false;
    }

    onChangeOfEmailList(){
        let vm = this;
        vm.isInvalidEmailList = false;
    }

    // send(isFormInvalid) {
    //     let vm = this;
    //     let {$uibModalInstance, $timeout, $scope, dataServices, partDetails} = vm.DI();
    //     if(isFormInvalid){
    //         vm.isInvalidEmailList = true;
    //     }else{
    //         vm.isInvalidEmailList = false;
    //         let recipients = vm.to.split(",");
    //         vm.showSending = true;
    //         dataServices.emailPart(partDetails.url, vm.from, recipients, vm.subject, null, vm.isCopied, vm.userId).then((response) => {
                
    //             if (response.success) {
    //                 vm.showSending = false;
    //                 vm.showSuccess = true;
    //                 vm.cancel(vm.showSuccess);
    //             }
    //         }, (error) => {
    //             vm.showSending = false;
    //         });
    //     }
    // }
    send() {
        let vm = this;
        let {$uibModalInstance, $timeout, $scope, dataServices, partDetails} = vm.DI();
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
        let payload = {};
        let emails, re = /^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z.]{1,5}$/, validityArr, atLeastOneInvalid;
        if(vm.to){
            let commaSep = vm.to.indexOf(',');
            if (commaSep !== -1) {
                if (commaSep !== -1) {
                    emails = vm.to.split(',');
                }
                validityArr = emails.map(function (str) {
                    return re.test(str.trim());
                });
                atLeastOneInvalid = false;
                for (let i = 0; i < validityArr.length ; i++) {
                    if (validityArr[i] === false){
                       atLeastOneInvalid = true; 
                       break;
                    }
                }
                if (atLeastOneInvalid) {
                    vm.isInvalidEmailList = true;
                    return;
                }
            }else if(re.test(vm.to.trim())){
                vm.isInvalidEmailList = false;
                emails = [vm.to.trim()];
            }else{
                vm.isInvalidEmailList = true;
                return;
            }
        }else{
            vm.isInvalidEmailList = true;
            return;
        }

        if(!vm.isInvalidEmailList) {
            dataServices.emailPart(partDetails.url, vm.from, emails, vm.subject, null, vm.isCopied, vm.userId).then((response) => {
                
                if (response.success) {
                    vm.showSending = false;
                    vm.showSuccess = true;
                    vm.cancel(vm.showSuccess);
                }
            }, (error) => {
                vm.showSending = false;
            });
        }
    }
    cancel(isEmailSent) {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        let data  = {};
        data.isEmailSent = isEmailSent;  
        $uibModalInstance.close(data);
    }
}


EmailController.$inject = ['$uibModalInstance', '$rootScope', '$document', '$timeout', '$scope', 'dataServices', 'partDetails', '$log', '$translate'];