/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author : Shaifali Jaiswal*/
export class emergencyOrderController {
    constructor($uibModal, $scope, $log, PricingService, OrderlistModalFactory, $uibModalInstance, AvailabilityService, $filter, $q,$rootScope) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            $scope, $uibModal,$rootScope, $log, $uibModalInstance, PricingService, AvailabilityService, $filter, $q
        });

        this.orderType = PricingService.orderType;
        this.qtyReqd = PricingService.qtyRequired;
        this.partNumber = PricingService.partNumber;
        this.partName = PricingService.partName;
        this.extendedPrice ="";
        this.unitPrice ="";
        this.addedToCart = false;
        this.totalQty = "";
        this.plantNames = [];
        this.showNoAvailable = false;
        this.selectedPlants = [];

        vm.initData();

    }

    qtyMismatch() {
        let vm = this,
            {
                $scope
            } = vm.DI();
        return vm.addedToCart == true;
    }
    DateinDDMMYY(numberOfDaysToAdd) {
        var someDate = new Date();
        // var numberOfDaysToAdd = 6;
        someDate.setDate(someDate.getDate() + numberOfDaysToAdd);

        var dd = someDate.getDate();
        var mm = someDate.getMonth() + 1;
        var y = someDate.getFullYear();

        var someFormattedDate = mm + '/' + dd + '/' + y;
        return someFormattedDate;

    }

    enableUpdateBtn(){
        let vm = this,
            {
                $scope, PricingService
            } = vm.DI();
            if(PricingService.qtyRequired !== vm.qtyRequired)
                return true;
            else
                return false;
    }

    updateData() {
        let vm = this,
            {
                $scope, PricingService
            } = vm.DI();
        vm.plantNames=[];
        PricingService.qtyRequired = vm.qtyReqd;
        vm.showNoAvailable = false;
        vm.initData();
    }

    addToCart() {
        let vm = this,
            {
                $scope, AvailabilityService, $uibModalInstance, $rootScope
            } = vm.DI();
        AvailabilityService.addToCartStatus = true;
        $rootScope.$emit("displayAddToCartFrmEmergency");
        $uibModalInstance.close();
    }

    initData() {
        let vm = this,
            {
                $uibModalInstance, PricingService, AvailabilityService, $scope, $q
            } = vm.DI();


        vm.orderType = PricingService.orderType;
        vm.qtyReqd = PricingService.qtyRequired;
        vm.partNumber = PricingService.partNumber;
        vm.partName = PricingService.partName;


        var deferred = $q.defer();
        var promise = deferred.promise;

        let availability = "";

        AvailabilityService.getAvailData('EMG',vm.qtyReqd)
            .then(function(response) {
                let {
                    $scope
                } = vm.DI();

                vm.availableData = response.data.APIResponse; //.partAvailDtl;

                let totAvailable = 0;
                vm.totalQty = vm.availableData.partAvailDtlList.reduce(function(a, b) {
                    return b['quantityAvbl'] == null ? a : a + b['quantityAvbl'];
                }, 0);

                vm.availableData.partAvailDtlList.map(function(item, index) {
                  let addressString = item.location.city+","+item.location.state+","+item.location.zipCode;
                    vm.plantNames[index] = {
                        plantName: item.location.plantName,
                        address:addressString,
                        leadDate: vm.DateinDDMMYY(item.leadTimeInDays),
                        availability: item.quantityAvbl,
                        selected: false
                    };
                })
                var qtyIndex = null;
                var currSum = 0;

                // var filteredData = vm.availableData.partAvailDtl.every(function(item, index) {
                //     currSum = currSum + item.quantityAvbl;
                //     if (vm.qtyReqd <= currSum) {
                //         qtyIndex = index;
                //         return false;
                //     } else {
                //         return true;
                //     }
                // });
                vm.unitPartPrice =(vm.availableData.partPriceDtl.priceDtlMap.STANDARD_BASE).toFixed(2);
                vm.extendedPrice = (vm.qtyReqd * vm.availableData.partPriceDtl.priceDtlMap.STANDARD_BASE).toFixed(2);
                vm.coreUnitPrice =(vm.availableData.partPriceDtl.priceDtlMap.CORE_BASE).toFixed(2) ;
                vm.unitPrice = (vm.qtyReqd * vm.availableData.partPriceDtl.priceDtlMap.CORE_BASE).toFixed(2);

                var filteredData = vm.availableData.partAvailDtlList;
                // vm.availableData.totalQty = currSum;
                if (qtyIndex == null) {
                    vm.availableData.showBTO = true;
                    vm.availableData.totalQty = currSum;
                    vm.availableData.totalBTO = vm.qtyReqd - currSum;
                    vm.filteredData = vm.availableData.partAvailDtlList;
                } else {
                    vm.availableData.showBTO = false;
                    vm.filteredData = vm.availableData.partAvailDtlList.slice(0, qtyIndex + 1);
                }

            }, function(error) {

            });

        let launchYMMList = $scope.$on("showNoAvailable", () => {
                vm.showNoAvailable = true;
            });

            $scope.$on("$destroy", () => {
                launchYMMList();
            });

    }

    changeAvailType() {
        let vm = this,
            {
                $uibModalInstance, PricingService, AvailabilityService, $filter, $scope
            } = vm.DI();

        PricingService.qtyRequired = vm.qtyReqd;
        PricingService.partNumber = vm.partNumber;
        PricingService.partName = vm.partName;

        if(vm.orderType=="STK"){
            vm.orderType ="EMG"
        }

        AvailabilityService.changeOrderType(vm.orderType );
    }

    cancel() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close();
    }


    checkAvail(mplant) {
        let vm = this,
            {
                $uibModalInstance, PricingService, AvailabilityService, $filter, $scope
            } = vm.DI();
        let currSum = null;
        vm.showNoAvailable = false;
        if (vm.qtyReqd < mplant.availability) {
            vm.addedToCart = true;
        }

        //
        // if(vm.selectedPlants.indexOf(mplant.plantName) !== -1){
        //     vm.selectedPlants[vm.selectedPlants.length ]= mplant.plantName;
        // }
        vm.selectedPlants = $filter('filter')(vm.plantNames, {
            checked: true
        });

        if (vm.selectedPlants.length > 0) {
            var filteredData = vm.selectedPlants.every(function(item, index) {
                currSum = currSum + item.availability;
                if (vm.qtyReqd <= currSum) {

                    this.addedToCart = true;
                    return false;
                } else {
                    this.addedToCart = false;
                    return true;
                }
            }, this);
        }

        if (vm.addedToCart == false) {
            $scope.$emit("showNoAvailable");
        }

    }

}
