/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class orderTypeSelectionController {
    constructor($uibModal, $scope, $log, PricingService, OrderlistModalFactory, $uibModalInstance, AvailabilityService, $filter, $q,$rootScope,dataServices, authenticationService, AftermarketConstants, $state, mode, $timeout) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            $scope, $uibModal,$rootScope, $log, $uibModalInstance, PricingService, AvailabilityService, $filter, $q, dataServices, authenticationService, AftermarketConstants, $state, mode, $timeout
        });

        vm.mode = mode;
        this.orderType = PricingService.orderType;
        this.qtyReqd = PricingService.qtyRequired;
        this.partNumber = PricingService.partNumber;
        this.partName = PricingService.partName;

        this.orderTitle = (this.orderType=='STK')?'Stock Order':'Emergency Order';
        this.extendedPrice ="";
        this.unitPrice ="";
        this.addedToCart = false;
        this.maxLevel = false;
        this.disableAllBox = false;
        this.totalQty = "";
        this.showBTO = false;
        this.plantNames = [];
        this.showNoAvailable = false;
        this.totAvailable = 0;
        this.selectedPlants = [];
        this.addToCartLabel = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.ADDTOCART') : $filter('translate')('PRICINGANDAVAILABILITY.UPDATE_CART');
        this.addToBackOrderLabel;
        this.disableCheckAvailability = true;
        //this.userName = authenticationService._currentUser.username;
        this.userType = PricingService.userType;//authenticationService._currentUser.userType;
        //this.userType = "RTB";
        this.isQtyNtMultplOfPkQty = false;
        this.priceData = false;
        this.priceApiError = {};
        this.availApiError = {};
        this.priceDataLoaded = false;
        this.availabilityDataLoaded = false;
        this.isEmgReqQtyAvail = true;
        this.isDataLoading = true;
        this.orderSettings = AftermarketConstants.appSettings.order_type;
        this.contactInfo = AftermarketConstants.appSettings.contact_info;
        this.backorder =false;
        this.qtyAddedToCart = 0;
        this.errorType = '';
        this.showHeader = false;
        vm.priceData = true;
        vm.userPermissionsList = authenticationService.userPermissionsList;
        vm.initData();

    }

    qtyMismatch() {
        let vm = this;
        return vm.addedToCart == true;
    }
    DateinDDMMYY(estShipDate) {
        var someDate = new Date(estShipDate);
        // var numberOfDaysToAdd = 6;
        //someDate.setDate(someDate.getDate() + numberOfDaysToAdd);

        var dd = someDate.getDate();
        var mm = someDate.getMonth() + 1;
        var y = someDate.getFullYear();

        var someFormattedDate = mm + '/' + dd + '/' + y;
        return someFormattedDate;

    }

    enableUpdateBtn(){
        let vm = this,
            {
                PricingService
            } = vm.DI();
            if(PricingService.qtyRequired !== vm.qtyRequired)
                return true;
            else
                return false;
    }
    // desiredQtyDataChange(){        
    //     let vm = this;
    //     this.addedToCart = false;
    //     for(let obj of vm.plantNames){
    //         obj.checked = false;
    //     }
    //     vm.disableAllBox = true;
    // }
    updateData() {
        let vm = this,
            {
                $scope, PricingService
            } = vm.DI();
        if (PricingService.qtyRequired === vm.qtyReqd) {
            return;
        }
        vm.disableAllBox = false;
        vm.plantNames=[];
        PricingService.qtyRequired = vm.qtyReqd;
        vm.showNoAvailable = false;
        vm.initData();
        $scope.emergencyOrder.$setPristine();
    }
    refetchData(){
        let vm = this,
            {
                $scope, PricingService
            } = vm.DI();
        vm.disableAllBox = false;
        vm.plantNames=[];
        PricingService.qtyRequired = vm.qtyReqd;
        vm.showNoAvailable = false;
        vm.initData();
        $scope.emergencyOrder.$setPristine();
    }
    addToCart(backOrder) {
        let vm = this,
            {
               AvailabilityService, $uibModalInstance, $rootScope, dataServices
            } = vm.DI();
        backOrder = vm.backorder;
        AvailabilityService.addToCartStatus = true;
        $rootScope.$emit("displayAddToCartFrmEmergency");
        //$uibModalInstance.close();
        
        // for(let obj of vm.plantNames){
        //     console.log(obj, obj.checked, obj.plantCd);
        // }
        let payload ;

        if(vm.orderType == "STK"){
            payload = {
               "cartName":"xyz",
               "cartType": "normal-cart",
               "orderType": "SO",
               "lineItems":[
                 {
                   "partNo": vm.partNumber,
                   "partName": vm.partName,
                   "productCat":vm.catPath,
                   "packageQty": vm.packageQty,
                   "checked": true,
                   "qtyReq": backOrder ? vm.qtyReqd : (vm.totalQty > vm.qtyReqd ? vm.qtyReqd: vm.totalQty),
                   "qtyAdded": backOrder ? vm.totalQty : vm.totalQty > vm.qtyReqd ? vm.qtyReqd : vm.totalQty,
                   "backOrderQty":backOrder ? vm.qtyReqd - vm.totalQty : 0,
                   "plantCode": vm.plantNames[0] ? vm.plantNames[0].plantCd : "",  
                   "uom": vm.uom ? vm.uom : "",
                   "wt" : vm.wt ? vm.wt: 0,
                   "wtUom": vm.wtUom ? vm.wtUom : "",
                   "thumbnail": vm.thumbnail? vm.thumbnail: "",
                   "prices":[{
                       "type": "",
                       "unitPrice":vm.unitPartPrice,
                       "extdPrice":vm.coreUnitPrice,
                       "corePrice":vm.extendedPrice,
                       "currency":vm.currency ? vm.currency : "USD"
                   }],
                   "refData": vm.plantNames[0] ? [{
                        "plantCode":vm.plantNames[0].plantCd, 
                        "plantERPName":vm.plantNames[0].plantName, 
                        "estAvlDate":new Date(vm.plantNames[0].leadDate).getTime(),            
                        "qtyAvl":vm.plantNames[0].availability
                   }] : [],
                   "shipToDateReq": vm.plantNames[0] ? new Date(vm.plantNames[0].leadDate).getTime() : new Date().getTime()
                 }
                ]
            };
            dataServices.addToCart(payload).then(function (response) {
                if(response){
                    $rootScope.$emit("gotCartCount");
                    $uibModalInstance.close('Pass');
                }
            }, function (error) {
                $uibModalInstance.close('Fail');
            });
        }else{
            let lineItems = [];
            let tempCount = vm.qtyReqd;
            let actualCount = 0;
            for(let obj of vm.plantNames){
                if(obj.checked){
                    
                    if(obj.partial){
                        actualCount= obj.partialQty;
                        tempCount-= obj.partialQty;
                    }
                    else{ 
                        tempCount>obj.availability ? actualCount=obj.availability : actualCount=tempCount;
                        tempCount-=obj.availability;
                    }
                    
                    let temp = {
                       "partNo":vm.partNumber,
                       "partName": vm.partName,
                       "productCat":vm.catPath,
                       "packageQty": vm.packageQty,
                       "checked": true,
                       "plantCode":obj.plantCd,
                       "qtyReq": actualCount, //vm.qtyReqd,
                       "backOrderQty":(obj.inventoryType === 'BTO' || obj.inventoryType === 'SLTI' )  ? actualCount : 0, 
                       "qtyAdded": (obj.inventoryType === 'BTO' || obj.inventoryType === 'SLTI' )  ? 0 : actualCount,
                       "uom": vm.uom ? vm.uom : "",
                       "wt" : vm.wt ? vm.wt: 0,
                       "wtUom": vm.wtUom ? vm.wtUom : "",
                       "thumbnail": vm.thumbnail? vm.thumbnail: "",
                       "inventoryType": obj.inventoryType,
                       "prices":[{
                           "type": "discounted",
                           "unitPrice":vm.unitPartPrice,
                           "extdPrice":vm.coreUnitPrice,
                           "corePrice":vm.extendeaddPrice,
                           "currency":vm.currency ? vm.currency : "USD"
                       }],
                       "refData": [{
                            "plantCode":obj.plantCd,  
                            "plantERPName":obj.plantName,
                            "qtyAvl":obj.availability,
                            "uom":"po",
                            "estAvlDate":new Date(obj.leadDate).getTime()         
                       }],
                       "shipToDateReq": obj.leadDate ? new Date(obj.leadDate).getTime() : new Date().getTime()
                     };
                    lineItems.push(temp);
                    if(tempCount <= 0){
                        break;
                    }
                }
                if(actualCount >= vm.qtyReqd){
                    break;
                }
            }
            payload = {
               "cartName":"EMG",
               "cartType": "normal-cart",
               "orderType": "EO",
               "lineItems": lineItems
            };
            dataServices.mergeCart(payload).then(function (response) {
                $rootScope.$emit("gotCartCount");
                $uibModalInstance.close('Pass');
            }, function (error) {
                $uibModalInstance.close('Fail');
            });
        }
    }

    initData() {
        let vm = this,
            {
                PricingService, $scope, $q, dataServices, authenticationService, $filter
            } = vm.DI();
        vm.errorType = '';
        vm.isbulkInvAvailbl = false;
        vm.addToCartLabel = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.ADDTOCART') : $filter('translate')('PRICINGANDAVAILABILITY.UPDATE_CART');
        vm.showNoAvailable = false;
        vm.orderType = PricingService.orderType;
        vm.qtyReqd = PricingService.qtyRequired;
        vm.packageQty = PricingService.packageQty;
        vm.minQty = vm.packageQty > 1 ? 0 : 1;
        vm.partNumber = PricingService.partNumber;
        vm.partName = PricingService.partName;
        //vm.partCats = JSON.parse(PricingService.partCats);
        vm.catPath = PricingService.catPath;
        vm.thumbnail = PricingService.thumbnail;
        vm.uom = PricingService.uom;
        vm.wt = PricingService.wt;
        vm.wtUom = PricingService.wtUom;
        vm.plantNames=[];
        vm.priceApiError = {pageNotFound: false, systemError: false};
        vm.availApiError = {pageNotFound: false, systemError: false};
        vm.maxLevel = false;
        vm.showHeader = false;
        //var deferred = $q.defer();
        // var promise = deferred.promise;

        // let availability = "";
        let catArr = [];
        
        let customerId = null;

        if(authenticationService.shipToSoldToInfo.selectedSoldTo && authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber){
            customerId = authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber;
        }
        else if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }

        let packageQty = vm.packageQty;
        if(!packageQty || packageQty.toString().trim() == ""){
            packageQty = 1;
        }else{
            packageQty = packageQty;
        }
        if(vm.userPermissionsList.CheckAvailability) {
             dataServices.partAvailability(vm.partNumber, customerId, vm.orderType, vm.qtyReqd, "", vm.catPath, packageQty)
                .then(function (response) { 
                    vm.isDataLoading = false;
                    
                    if(!response || response.code ==='500'){
                        vm.errorType = 'system';
                    }
                    else if(response.code==='404'){
                        vm.errorType = 'notfound';
                    }     
                 
                    //response.partAvailDtlList = [];
                    //response.totalQtyAvail = 0;
                    vm.availableData = response; 
                    if(vm.orderType=="STK"){
                        vm.totAvailable = response.totalFGQtyAvail? response.totalFGQtyAvail : 0;
                    }
                    else{
                        vm.totAvailable = response.totalFGQtyAvail? response.totalFGQtyAvail : 0;
                    }
                    //totalFGQtyAvail
                    vm.totalQty = response.totalQtyAvail? response.totalQtyAvail : 0;
                    vm.qtyAddedToCart = vm.totalQty;
                    vm.availabilityDataLoaded = true;
                    // if(vm.availableData.partAvailDtlList){
                    //     vm.totalQty = vm.availableData.partAvailDtlList.reduce(function(a, b) {
                    //         return b['quantityAvbl'] == null ? a : a + b['quantityAvbl'];
                    //     }, 0);
                    // }

                    if(vm.orderType=="STK"){
                        vm.addedToCart = true;
                        vm.showBTO = false;
                        if(vm.totalQty < vm.qtyReqd){
                            vm.showBTO = true;
                            let label = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.ADD') : $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.UPDATE')
                            if(vm.totalQty > 0){
                                label = label + $filter('number')(vm.totalQty, 0);
                            }
                            else{
                                vm.addedToCart = false;
                                vm.showNoAvailable = false;
                            }
                            vm.addToCartLabel = label +  $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.TO_CART');
                        } else {
                            
                            vm.qtyAddedToCart  = vm.qtyReqd;
                            let label = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.ADD') : $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.UPDATE')
                            if(vm.qtyReqd > 0){
                                label = label + $filter('number')(vm.qtyReqd, 0);
                            }
                            vm.addToCartLabel = label +  $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.TO_CART');
                            
                        }
                    }
                    else{
                        vm.qtyAddedToCart = 0;
                        if(vm.totalQty < vm.qtyReqd){
                            vm.addedToCart = false;
                            vm.showNoAvailable = false;
                            vm.isEmgReqQtyAvail = false;
                        } else {
                            vm.addedToCart = false;
                            vm.isEmgReqQtyAvail = true;
                        }
                        
                        vm.showBTO = false;
                    }
                    if(vm.availableData.partAvailDtlList) {
                        vm.availableData.partAvailDtlList.map(function(item, index) {
                            let addressString = item.location.city+", "+item.location.state;
                            if(!vm.isbulkInvAvailbl && item.inventoryType !=='SLTI'){
                                vm.isbulkInvAvailbl = item.quantityAvbl > 0
                            }
                            vm.plantNames[index] = {
                                plantName: item.location.plantName,
                                address:addressString,
                                leadDate: vm.DateinDDMMYY(item.estShipDate),
                                availability: item.quantityAvbl,
                                selected: false,
                                checked:false,
                                plantCd: item.location.plantCd,
                                inventoryType: item.inventoryType
                            };
                            if(item.inventoryType === 'FG' || item.inventoryType === 'BULK'){
                                vm.showHeader = true;
                            }
                        })
                    }
                    // var qtyIndex = null;
                    // var currSum = 0;
                    if( vm.showBTO) {
                        if(vm.totalQty == 0){
                        vm.addToBackOrderLabel = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.ADD_TO_CART_AS_BACK_ORDR') : $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.UPDATE_TO_CART_AS_BACK_ORDER');
                        } else {
                            let backOrderCount = vm.qtyReqd - vm.totalQty;
                            let label = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.ADD') : $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.UPDATE')
                            vm.addToBackOrderLabel = label + $filter('number')(vm.totalQty, 0) + $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.TO_THE_CART') + $filter('number')(backOrderCount, 0) + $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.TO_BACK_ORDER');
                        }
                    }

                    //var filteredData = vm.availableData.partAvailDtlList;

                }, function(error) {
                    vm.isDataLoading = false;
                    if (error.status === 500) {
                        vm.availApiError.systemError = true;
                        vm.totalQty = 0;

                        vm.addToBackOrderLabel = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.ADD_TO_CART_AS_BACK_ORDR') : $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.UPDATE_TO_CART_AS_BACK_ORDER');
                    }
                    if (error.status === 404) {
                        //DO WHAT YOU WANT
                        vm.availApiError.pageNotFound = true;
                        vm.totalQty = 0;
                        vm.addToBackOrderLabel = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.ADD_TO_CART_AS_BACK_ORDR') : $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.UPDATE_TO_CART_AS_BACK_ORDER');
                    }
                });
            }
            else {
                //Handle Check Availabilty ACL failure case
                vm.isDataLoading = false;
                if(vm.orderType=="STK"){
                    vm.addedToCart = true;
                    vm.showBTO = false;
                    vm.totalQty = vm.qtyReqd;
                    vm.showNoAvailable = false;                    
                }
                
            }
            
            if(vm.userType ==='RTB' && vm.userPermissionsList.CheckPrice){
                let customerId = null;

                if(authenticationService.shipToSoldToInfo.selectedSoldTo && authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber){
                    customerId = authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber;
                }
                else if(authenticationService.associatedCustData.activeCustId){
                    customerId = authenticationService.associatedCustData.activeCustId;
                }

                dataServices.partPricing(vm.partNumber, customerId, vm.orderType, vm.qtyReqd, "", catArr)
                .then(function (response) {
                    // let {
                    //     $scope
                    // } = vm.DI();
                    vm.priceDataLoaded = true;
                    vm.availablePricingData = response;
                    if(vm.availablePricingData && vm.availablePricingData.partPriceDtl){
                        vm.priceData = true;
                        vm.currency = vm.availablePricingData.partPriceDtl.currency;
                        vm.unitPartPrice =vm.availablePricingData.partPriceDtl.priceDtlMap.STANDARD_BASE ?(vm.availablePricingData.partPriceDtl.priceDtlMap.STANDARD_BASE).toFixed(2) : 0;
                        vm.unitPartPrice = parseFloat(vm.unitPartPrice);
                        vm.coreUnitPrice = vm.availablePricingData.partPriceDtl.priceDtlMap.CORE_BASE ? (vm.availablePricingData.partPriceDtl.priceDtlMap.CORE_BASE).toFixed(2) : 0 ;
                        vm.coreUnitPrice = parseFloat(vm.coreUnitPrice);
                        //vm.extendedPrice;//vm.availablePricingData.partPriceDtl.priceDtlMap.STANDARD_BASE ? (vm.qtyReqd * (vm.availablePricingData.partPriceDtl.priceDtlMap.STANDARD_BASE + parseFloat(vm.coreUnitPrice))).toFixed(2) : 0;
                        vm.extendedPrice = (vm.unitPartPrice + vm.coreUnitPrice) * 1;
                        vm.unitPrice = vm.availablePricingData.partPriceDtl.priceDtlMap.CORE_BASE ? (vm.qtyReqd * vm.availablePricingData.partPriceDtl.priceDtlMap.CORE_BASE).toFixed(2) : 0;
                        if(vm.orderType =='EMG'){
                            // vm.addedToCart = false;
                        }
                    }
                    else {
                        vm.priceData = false;
                        //vm.priceApiError.pageNotFound = true;
                    }
                
                    
                }, function(error) {
                    vm.priceData = false;
                    if (error.status === 500) {
                        //DO WHAT YOU WANT
                        //vm.availableData.showBTO = true;
                        
                        vm.priceApiError.systemError = true;
                    }
                    if (error.status === 404) {
                        //DO WHAT YOU WANT
                        //vm.availableData.showBTO = true;
                        vm.priceApiError.pageNotFound = true;
                    }
                });
            }
            else {
                vm.priceData = false;
                //Handle Check Price ACL failure case
            }

            // let launchYMMList = $scope.$on("showNoAvailable", () => {
            //     vm.showNoAvailable = true;
            // });

            // $scope.$on("$destroy", () => {
            //     launchYMMList();
            // });

    }

    changeAvailType(orderType, orderTypeLbl) {
        let vm = this,
            {
                PricingService, $filter, $scope
            } = vm.DI();
            if(vm.orderType !== orderType) {
                vm.orderType=orderType;
                PricingService.qtyRequired = vm.qtyReqd;
                vm.addedToCart = false;
                PricingService.orderType = orderType;
                vm.orderTitle = $filter('translate')(orderTypeLbl);//(vm.orderType=='STK')?'Stock Order':'Emergency Order';
                vm.initData();
                $scope.emergencyOrder.$setPristine();
            }
    }

    cancel() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close('delete');
    }

    checkBackOrder(mplant){
         let vm = this,
            {
                $filter
            } = vm.DI();
        //mplant.checked = mplant.checked ? false: true;
        let total_formatted;
        if(mplant.checked){
            total_formatted = $filter('number')(vm.totalQty + mplant.availability, 0);
            vm.backorder = true;
            vm.addedToCart = true;
            vm.qtyAddedToCart = vm.totalQty + mplant.availability;
            let label = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.ADD') : $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.UPDATE');
            vm.addToCartLabel = label + total_formatted + $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.TO_CART');
        }
        else{
            total_formatted = $filter('number')(vm.totalQty, 0);
            vm.qtyAddedToCart = vm.totalQty;
            vm.backorder = false;
            if(vm.totalQty === 0){
                vm.addedToCart = false;
                vm.addToCartLabel = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.ADDTOCART') : $filter('translate')('PRICINGANDAVAILABILITY.UPDATE_CART');
            }
            else{
                let label = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.ADD') : $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.UPDATE');
                vm.addToCartLabel = label + total_formatted + $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.TO_CART');
            }
            
        }
        
        
        
        
    }

    checkAvail(mplant) {
        let vm = this,
            {
                $filter, $scope
            } = vm.DI();
        let currSum = 0;
        vm.showNoAvailable = false;
        if (vm.qtyReqd < mplant.availability) {
            vm.addedToCart = true;
            //vm.maxLevel = true;
        }else{
            //vm.maxLevel = true;
        }
        if(mplant.checked === false) {
            mplant.partial = false;
        }
        //
        // if(vm.selectedPlants.indexOf(mplant.plantName) !== -1){
        //     vm.selectedPlants[vm.selectedPlants.length ]= mplant.plantName;
        // }
        vm.selectedPlants = $filter('filter')(vm.plantNames, {
            checked: true
        });

        if(vm.selectedPlants.length==0){
          vm.showNoAvailable = false;
          vm.maxLevel = false;
          this.addedToCart = false;
        }
        else{
            //vm.showNoAvailable = true;
            this.addedToCart = true;
        }

        if (vm.selectedPlants.length > 0) {
            var filteredData = vm.selectedPlants.every(function(item, index) {
                currSum = currSum + item.availability;
                item.partial = false;
                item.partialQty = "";
                if (vm.qtyReqd <= currSum) {
                    this.addedToCart = true;
                    vm.maxLevel = true;
                    if(vm.qtyReqd < currSum){
                        item.partial = true;
                        item.partialQty = item.availability - ( currSum - vm.qtyReqd);
                    } 
                    return false;
                } else {
                    vm.maxLevel = false;
                    //this.addedToCart = false;
                    return true;
                }
            }, this);
        } else {
            this.addedToCart = false;
        }
        if(mplant.checked === false) {
            mplant.partial = false;
            mplant.partialQty = "";
        }
        else {
            if (vm.qtyReqd < currSum) {
                mplant.partial = true;
                mplant.partialQty = mplant.availability - ( currSum - vm.qtyReqd); 
            }
        }
        if(currSum){
            let originalCount  = vm.qtyReqd < currSum ? vm.qtyReqd : currSum;
            vm.qtyAddedToCart = originalCount;
            let label = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.ADD') : $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.UPDATE')
            vm.addToCartLabel = label + $filter('number')(originalCount, 0) + $filter('translate')('PRICINGANDAVAILABILITY.AVAILABILITY.TO_CART');
        }
        else {
            vm.qtyAddedToCart = 0;
            vm.addToCartLabel = vm.mode == 'normal' ? $filter('translate')('PRICINGANDAVAILABILITY.ADDTOCART') : $filter('translate')('PRICINGANDAVAILABILITY.UPDATE_CART');
        }

        
        if (vm.addedToCart == false && vm.selectedPlants.length >0 ) {
            //$scope.$emit("showNoAvailable");
            vm.showNoAvailable = true;
            //vm.maxLevel = false;
        }
    }

    updateCart(backorder){
        let vm = this,
        {
            dataServices, $state, PricingService, $uibModalInstance, $timeout
        } = vm.DI();
        backorder = vm.backorder;
        if(PricingService.orderType == 'STK'){
            //vm.orderTitle
            let payload = [{
               "id": PricingService.partToUpdateId,
               "qtyAdded": vm.totalQty > vm.qtyReqd ? vm.qtyReqd : vm.totalQty,
               "qtyReq": vm.qtyReqd,
               "packageQty": vm.packageQty,
               "backOrderQty":backorder ? vm.qtyReqd - vm.totalQty : 0,
               "checked":false,
               "shipToDateReq": new Date(vm.leadDate).getTime(),
               "prices": [
                    {
                      "unitPrice": vm.unitPartPrice ? parseFloat(vm.unitPartPrice) : 0,
                      "extdPrice": vm.extendedPrice ? parseFloat(vm.extendedPrice) : 0,
                      "corePrice": vm.coreUnitPrice ? parseFloat(vm.coreUnitPrice) : 0,
                      "currency": vm.currency ? vm.currency : "USD"
                    }
                ]
            }];

            dataServices.updateCart(payload, PricingService.cartId).then(function(response){
                if(vm.mode != 'incomplete'){
                    $uibModalInstance.close('Edit');
                    $state.go('ordermgt', { reload: true });
                }else{
                    $uibModalInstance.close(response.data.ids);
                }
                //vm.mode != 'incomplete' ? $state.go('ordermgt', { reload: true }) : $uibModalInstance.close("");
            },function(error){
                
            });
        }else{
            let lineItems = [];
            let tempCount = vm.qtyReqd;
            let actualCount = 0;
            for(let obj of vm.plantNames){
                if(obj.checked){
                    if(obj.partial){
                        actualCount= obj.partialQty;
                        tempCount-= obj.partialQty;
                    }
                    else{ 
                        tempCount>obj.availability ? actualCount=obj.availability : actualCount=tempCount;
                        tempCount-=obj.availability;
                    }
                    

                    let temp = {
                       "id": PricingService.plantCode == obj.plantCd ? PricingService.partToUpdateId : "",
                       "plantCode":obj.plantCd,
                       "partNo":vm.partNumber,
                       "partName": vm.partName,
                       "packageQty": vm.packageQty,
                       "backOrderQty":(obj.inventoryType === 'BTO' || obj.inventoryType === 'SLTI' )  ? actualCount : 0, 
                       "qtyAdded": (obj.inventoryType === 'BTO' || obj.inventoryType === 'SLTI' )  ? 0 : actualCount,
                       "inventoryType": obj.inventoryType,
                       //"qtyAdded": actualCount,
                       "qtyReq": actualCount, //vm.qtyReqd,
                       "checked": true,
                       "uom": vm.uom ? vm.uom : "",
                       "productCat":vm.catPath,
                       //"shipToDateReq": new Date().getTime(),
                       "shipToDateReq": obj.leadDate ? new Date(obj.leadDate).getTime() : new Date().getTime(),
                       "prices":[{
                           "type": "discounted",
                           "unitPrice":vm.unitPartPrice,
                           "extdPrice":vm.coreUnitPrice,
                           "corePrice":vm.extendedPrice,
                           "currency":vm.currency ? vm.currency : "USD"
                       }]
                     };
                    lineItems.push(temp);
                    if(tempCount <= 0){
                        break;
                    }
                }
                if(actualCount >= vm.qtyReqd){
                    break;
                }
            }

            let payload = {
               "cartName":"EMG",
               "cartType": "normal-cart",
               "orderType": "EO",
               "lineItems": lineItems
            };

           /* $timeout(() => {
                $state.go("searchResults", vm.srchParamObj, { reload: true });
            });*/

            
            $timeout(function() {
                dataServices.mergeCart(payload).then(function(response){
                    if(vm.mode != 'incomplete'){
                        $uibModalInstance.close('Edit');
                        //$state.go('ordermgt', { reload: true });
                    }else{
                        //let payload = {"created":[1283],"updated":[1264]};
                        //response = payload;
                        $uibModalInstance.close(response);
                    }
                },function(error){
                    
                });
            }, 1000);
        }
    }

    checkCartQtyVal(fieldId) {
        let vm = this;
        this.addedToCart = false;
        for(let obj of vm.plantNames){
            obj.checked = false;
        }
        vm.disableAllBox = true;
        let fieldVal = document.getElementById(fieldId).value;
        if(fieldVal == "0"){
            vm.qtyReqd = vm.packageQty;
        }
        if (vm.qtyReqd > 0) {
            vm.cartQtyNotAllowed = false;
        }else{
            vm.cartQtyNotAllowed = true;
        }
        if(vm.packageQty > 1 && vm.qtyReqd > 0 && (vm.qtyReqd % vm.packageQty != 0)){
            vm.isQtyNtMultplOfPkQty = true;
        }else{
            vm.isQtyNtMultplOfPkQty = false;
        }
    }

    makeQtyPositive(qtyNtPositive, fieldId){
        let vm = this;
        qtyNtPositive ? vm.qtyReqd = vm.packageQty: angular.noop();
        let fieldVal = document.getElementById(fieldId).value;
        // if(vm.qtyReqd % vm.packageQty != 0){
        //     let qtyFold = Math.floor(vm.qtyReqd/vm.packageQty);
        //     if(qtyFold == 0){
        //         vm.qtyReqd = vm.packageQty;
        //     }
        // }
        if (vm.qtyReqd > 0) {
            vm.cartQtyNotAllowed = false;
        }else{
            vm.cartQtyNotAllowed = true;
        }
        if(vm.packageQty > 1 && vm.qtyReqd > 0 && (vm.qtyReqd % vm.packageQty != 0)){
            vm.isQtyNtMultplOfPkQty = true;
        }else{
            vm.isQtyNtMultplOfPkQty = false;
        }
    }
}

