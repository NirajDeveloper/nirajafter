/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function routerConfig($stateProvider) {
    'ngInject';
    $stateProvider
        .state('pricing', {
            //url: '/search?mode&cat1&cat2&cat3',
            url: '/pricing',
            parent: 'aftermarket',
            templateUrl: '',
            controller: 'pricingController',
            controllerAs: 'pricing',
            resolve: {},
            onEnter: function(SearchBarService){  

            },
            onExit: function(SearchBarService){  

            }
        });
}