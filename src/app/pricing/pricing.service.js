/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class PricingService {

    constructor() {
        this._orderType = "EMG";
        this._qtyReqd = 0;
        this._partNumber = "";
        this._partName = "";
        this.partCats = [];
        this.catPath = "";
        this.uom = "";
        this.wt = "";
        this.wtUom = "";
        this.thumbnail = "";
        this._partToUpdateId = 0;
        this._plantCode = 0;
        this._sumOfTotal = 0;
        this._noOfParts = 0;
        this._cartId = 0;
        this.stkItemSelection = [];
        this.eoItemSelection = [];
        //this.checkoutData1 = 0;
    }

    /*get checkoutData1() {
        return this.checkoutData1;
    }

    set checkoutData1(data) {
        this.checkoutData1 = data;
    }*/

    get _cartId() {
        //this._orderList = sessionStorage.orderList ;
        return this._cartId;
    }

    set _cartId(id) {
        this.__cartId = id;
    }

    get sumOfTotal() {
        //this._orderList = sessionStorage.orderList ;
        return this._sumOfTotal;
    }

    set sumOfTotal(sum) {
        this._sumOfTotal = sum;
    }

    get noOfParts() {
        //this._orderList = sessionStorage.orderList ;
        return this._noOfParts;
    }

    set noOfParts(qty) {
        this._noOfParts = qty;
    }

    get qtyRequired() {
        //this._orderList = sessionStorage.orderList ;
        return this._qtyReqd;
    }

    set qtyRequired(qty) {
        this._qtyReqd = qty;
    }

    get orderType() {
        //this._orderId = angular.fromJson(sessionStorage.orderId);
        return this._orderType;
    }

    set orderType(orderType) {
        this._orderType = orderType;
        
    }

    get partNumber() {
        //this._orderId = angular.fromJson(sessionStorage.orderId);
        return this._partNumber;
    }

    set partNumber(partNo) {
        this._partNumber = partNo;
        
    }

    get partName() {
        //this._orderId = angular.fromJson(sessionStorage.orderId);
        return this._partName;
    }

    set partName(name) {
        this._partName = name;
        
    }

    get partToUpdateId() {
        return this._partToUpdateId;
    }

    set partToUpdateId(id) {
        this._partToUpdateId = id;
    }

    get plantCode() {
        return this._plantCode;
    }

    set plantCode(code) {
        this._plantCode = code;
    }
}



       
        