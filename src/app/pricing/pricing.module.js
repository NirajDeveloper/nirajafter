/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/



import {availabilityDirective} from './avail.directive';
import {stockOrderController} from './stock/stock.controller';
import {PricingService} from './pricing.service';
import {emergencyOrderController} from './emergency/emergencyorder.controller';
import { AvailabilityService } from './availability.service';
import {orderTypeSelectionController} from './availability/orderTypeSelectionController';
angular.module('aftermarket.pricing', [])
    .directive('availabilityDirective', availabilityDirective)
     .controller('stockOrderController', stockOrderController)
     .controller('emergencyOrderController', emergencyOrderController)
     .controller('orderTypeSelectionController', orderTypeSelectionController)
      .service('AvailabilityService', AvailabilityService)
      .service('PricingService', PricingService)
      .filter('plantSelection', ['filterFilter', function (filterFilter) {
    return function plantSelection(input, prop) {
      return filterFilter(input, { selected: true }).map(function (fruit) {
        return fruit[prop];
      });
    };
  }])
    //.config(routerConfig);
