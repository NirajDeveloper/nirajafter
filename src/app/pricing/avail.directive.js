/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export
    function availabilityDirective() {
    'ngInject';

    let directive = {
        restrict: 'E',
        templateUrl: 'app/pricing/avail.html',
        scope: {
            partNumber: '@',
            partName: '@',
            currentUser: '=',
            partCats: '@',
            catPath: '@',
            change: '&',
            partQty: '=',
            partObj: '@'
        },
        controller: availabilityController,
        controllerAs: 'avail',
        bindToController: true,
        // optional compile function
        compile(elem, attrs) {
            //tElement.css('position', 'absolute');
            return this.linkFunction;
        },

        linkFunction(scope, element, attrs, modelCtrl) {

        }
    };
    return directive;
}

class availabilityController {
    constructor($log, $scope, $rootScope, $document, OrderlistModalFactory, $timeout, OrderListService, PricingService, SearchBarService, $uibModal, AUTH_EVENTS, Session, authenticationService, AftermarketConstants, $filter, dataServices) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            $log, $scope, OrderlistModalFactory, $timeout, $document, OrderListService, PricingService, SearchBarService, $uibModal, $rootScope, AUTH_EVENTS, authenticationService, AftermarketConstants, $filter, dataServices
        });
        vm.qtyReqd = 1;
        this.orderType = "STK";
        this.orderLabel = "STOCK ORDER";
        this.userName = 'nondana';
        //this.isGuestUser = false;
        this.isUserLogged;
        this.orderTypeStyle = 'radio';
        vm.isRTBChecked = false;
        vm.isRTBUser = false;
        this.shwMyLists = false;
        this.shwMsg = false;
        this.msgText = "";
        this.lists = OrderListService.myLists;
        this.partData = "";
        
        //this.userType = authenticationService._currentUser.userType
        //if(Session.user !== "") this.userName = Session.user.userName;
        this.orderSettings = AftermarketConstants.appSettings.order_type;
        if (this.orderSettings.length > 2) {
            vm.orderTypeStyle = 'dropdown';
        }
        /*if(sessionStorage["currentUserProfileData"] !== undefined){
           this.isUserLogged = true;
        } else {
            this.isUserLogged = false;
        }*/
        vm.userPermissionsList = authenticationService.userPermissionsList;
        if (vm.userPermissionsList.StockOrder) {
            this.orderType = "STK";
        } else if (vm.userPermissionsList.EmergencyOrder) {
            this.orderType = "EMG";
        } else {
            // please confirm what to do if no permission for either
        }
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
            if (vm.userPermissionsList.StockOrder) {
                vm.orderType = "STK";
            } else if (vm.userPermissionsList.EmergencyOrder) {
                vm.orderType = "EMG";
            } else {
                // please confirm what to do if no permission for either
            }
        });
        vm.minQty = 0;
        $rootScope.$on('partDataLoaded', function (event, partDetails) {
            if (partDetails) {
                if(!partDetails.packageQty || partDetails.packageQty.toString().trim() == ""){
                    vm.packQty = 1;
                    vm.qtyReqd = 1;
                    vm.minQty = 1;
                }else{
                    vm.packQty = partDetails.packageQty;
                    vm.qtyReqd = partDetails.packageQty;
                    vm.minQty = vm.packQty > 1 ? 0 : 1;
                }
            }
            if (localStorage["currentUserProfileData"] !== undefined) {
                vm.isUserLogged = true;
            } else {
                vm.isUserLogged = false;
            }
            
            if (!vm.isRTBChecked && partDetails && partDetails.partNumber) {
                vm.checkRTB(partDetails.partNumber);
            }
            if(vm.partObj && vm.partObj.trim() !== ""){
                vm.partData = JSON.parse(vm.partObj);
            }
        });
        $rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
            vm.isUserLogged = true;
            if (vm.partNumber) {
                vm.checkRTB(vm.partNumber);
            }
        });
        // $rootScope.$on("displayAddToCartFrmEmergency", () => {
        //     this.qtyReqd = "";
        // });
        // vm.minQty = 0;
        // $timeout(function(){ 
        //     vm.packQty = JSON.parse(vm.partObj).packageQty;
        //     if(!vm.packQty){
        //         vm.packQty = 1;
        //         vm.qtyReqd = vm.packQty;
        //         vm.minQty = vm.packQty > 1 ? 0 : 1;
        //     }else{
        //         vm.qtyReqd = vm.packQty;
        //         vm.minQty = vm.packQty > 1 ? 0 : 1;
        //     }
        // }, 1000);
    }
    checkRTB(partNo) {
        let vm = this;
        let { dataServices} = vm.DI();
        if (!vm.isRTBChecked) {
            let itemArr = [];
            itemArr.push(partNo);
            dataServices.bulkRTBCheck(itemArr).then(function (response) {
                if (localStorage["currentUserProfileData"] !== undefined) {
                    var userInfo = JSON.parse(localStorage["currentUserProfileData"]);
                    vm.isRTBChecked = true;
                    if (response && response.rtbParts && response.rtbParts.length > 0) {                        
                        //response.data= {message: "Authorized", status: "SUCCESS"};
                        vm.userType = 'RTB';
                        vm.isRTBUser = true;
                        vm.userName = userInfo.username;
                     } else {
                        vm.userType = 'NON-RTB';
                        vm.isRTBUser = false;
                        vm.userName = userInfo.username;
                     }
                }
            }, function (error) {
            });
        }
    }
    open(type = 'client') {
        let vm = this;
        let { $log, $timeout, $document, $rootScope} = vm.DI();
        $rootScope.$emit("openSignin");
        /*$timeout(function() {
            angular.element($document[0].getElementById('signinpopup')).triggerHandler('click');
        });*/
        // let vm = this,
        //     size = "lg";
        // let { $log, $uibModal, authenticationService } = vm.DI();
        // authenticationService.loginType = type;
        // var uibModalInstance = $uibModal.open({
        //     templateUrl: 'app/userprofile/login/login.html',
        //     controller: 'LoginController',
        //     controllerAs: 'login',
        //     // templateUrl: 'app/orderList/ShareError/shareError.html',
        //     // controller: 'ShareErrorController',
        //     size: size,
        //     windowClass: 'my-modal-popup login-overlay-popup',
        //     resolve: {
        //         items: function() {
        //             return vm.items;
        //         }
        //     }
        // });
    }


    setOrderType(value, lbl) {
        let vm = this;
        let {
            $log, $scope, OrderlistModalFactory, $timeout, PricingService, SearchBarService, $filter
        } = vm.DI();
        if (value) {
            vm.orderType = value;
            vm.orderLabel = $filter('translate')(lbl);
        }
        vm.shwMsg = false;
        PricingService.orderType = vm.orderType;
    }
    showList(val) {
        let vm = this;
        let {
            OrderListService
        } = vm.DI();
        vm.shwMsg = false;
        // if(vm.shwMyLists === true) {
        //      vm.shwMyLists = false;
        // }
        // else {
        vm.shwMyLists = true;
        vm.lists = OrderListService.fetchMyLists();
        //}
    }
    addToCart() {
        let vm = this,
            {
                dataServices,
                $rootScope
            } = vm.DI();
        vm.partData = JSON.parse(vm.partObj);
        let payload = {
            "cartName": "STK",
            "cartType": "normal-cart",
            "orderType": "SO",
            "lineItems": [{
                "partNo": vm.partNumber,
                "partName": vm.partName,
                "productCat": vm.catPath,
                "qtyReq": vm.qtyReqd,
                "packageQty": vm.packQty,
                "qtyAdded": vm.qtyReqd,
                "backOrderQty": 0,
                "checked": true,
                "uom": vm.partData.packageType ? vm.partData.packageType : "",
                "thumbnail": vm.partData.tnImage ? vm.partData.tnImage : "",
                "wt": vm.partData.wt ? vm.partData.wt : 0,
                "wtUom": vm.partData.wtUom ? vm.partData.wtUom : "",              
                "shipToDateReq": new Date().getTime()
            }]
        };

        dataServices.addToCart(payload).then(function (response) {
            $rootScope.$emit("gotCartCount");
            $rootScope.$emit("displayAddToCartFrmEmergency");
            vm.qtyReqd = vm.packQty;
        }, function (error) {
        });
    }
    createNewList() {
        let vm = this;
        let { $uibModal, $location } = vm.DI();
        let modalInstance = $uibModal.open({
            templateUrl: 'app/mylist/createlist/createlist.html',
            controller: 'CreatenewlistController',
            controllerAs: 'newlist',
            size: 'md',
            windowClass: 'my-modal-popup',
            resolve: {
                tempItemList: function () {
                    return null;
                }
            }
        });
        modalInstance.result.then(function (resData) {
            vm.addToList(resData.code, resData.listName);
        }, function () {
        });
    }
    addToList(id, name) {
        let vm = this;
        let { OrderListService, dataServices } = vm.DI();
       //let partData = vm.partData;
        vm.partData = JSON.parse(vm.partObj);
        let temp = [{
            partNo: vm.partNumber,
            quantity: vm.packQty,
            packageQty: vm.packQty,
            partName: vm.partName,
            uom: vm.partData.packageType ? vm.partData.packageType : "",
            thumbnail: vm.partData.tnImage ? vm.partData.tnImage : "",
            wt: vm.partData.wt ? vm.partData.wt : 0,
            wtUom: vm.partData.wtUom ? vm.partData.wtUom : "",
            catPath: vm.catPath
        }];
       
        dataServices.addItemToOrderList(temp, id)
            .then(function (response) {
                if (response.mess === "Created") {
                    OrderListService.incrementListCount(id);
                    vm.shwMsg = true;
                    //vm.msgText = vm.qtyReqd + " qty(s) added to " + name;
                    vm.msgText = 'Added to "' + name + '" list.';
                    vm.shwMyLists = false;
                }
            }, function (error) {

                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });

    }
    showAvailScreen(val) {
        let vm = this,
            size = "md";
        let {
            $log, $scope, OrderlistModalFactory, $timeout, PricingService
        } = vm.DI();
        vm.shwMsg = false;
        vm.partData = JSON.parse(vm.partObj);
        if (vm.qtyReqd > 0 && !vm.isQtyNtMultplOfPkQty) {
            PricingService.qtyRequired = vm.qtyReqd;
            PricingService.partNumber = vm.partNumber;
            PricingService.packageQty = vm.packQty;
            PricingService.partName = vm.partName;
            PricingService.orderType = vm.orderType;
            PricingService.partCats = vm.partCats;
            PricingService.catPath = vm.catPath;
            PricingService.orderTypeLbl = vm.orderLabel;
            PricingService.userType = vm.userType;
            PricingService.uom = vm.partData.packageType ? vm.partData.packageType : "";
            PricingService.thumbnail = vm.partData.tnImage ? vm.partData.tnImage : "",
            PricingService.wt = vm.partData.wt ? vm.partData.wt : 0;
            PricingService.wtUom = vm.partData.wtUom ? vm.partData.wtUom : "";
            let urlModalInstance = null;
            urlModalInstance = $timeout(OrderlistModalFactory.open("md", "app/pricing/availability/orderTypeSelection.html", "orderTypeSelectionController", "orderTypeCtrl", "availabilityOverlay", true), 2000);
        }
        else {
            return;
        }

    }

    checkCartQtyVal(fieldId) {
        let vm = this;
        let fieldVal = document.getElementById(fieldId).value;
        if(fieldVal == "0"){
            vm.qtyReqd = vm.packQty;
        }
        if (vm.qtyReqd > 0) {
            vm.cartQtyNotAllowed = false;
        }else{
            vm.cartQtyNotAllowed = true;
        }
        if(vm.packQty > 1 && vm.qtyReqd > 0 && (vm.qtyReqd % vm.packQty != 0)){
            vm.isQtyNtMultplOfPkQty = true;
        }else{
            vm.isQtyNtMultplOfPkQty = false;
        }
    }

    makeQtyPositive(qtyNtPositive, fieldId){
        let vm = this;
        qtyNtPositive ? vm.qtyReqd = vm.packQty: angular.noop();
        let fieldVal = document.getElementById(fieldId).value;
        // if(vm.cartQty % vm.part.packageQty != 0){
        //     let qtyFold = Math.floor(vm.cartQty/vm.part.packageQty);
        //     vm.cartQty = vm.part.packageQty * qtyFold + vm.part.packageQty;
        // }
        // if(vm.qtyReqd % vm.packQty != 0){
        //     let qtyFold = Math.floor(vm.qtyReqd/vm.packQty);
        //     if(qtyFold == 0){
        //         vm.qtyReqd = vm.packQty;
        //     }
        // }
        if (vm.qtyReqd > 0) {
            vm.cartQtyNotAllowed = false;
        }else{
            vm.cartQtyNotAllowed = true;
        }
        if(vm.packQty > 1 && vm.qtyReqd > 0 && (vm.qtyReqd % vm.packQty != 0)){
            vm.isQtyNtMultplOfPkQty = true;
        }else{
            vm.isQtyNtMultplOfPkQty = false;
        }
    }
}
