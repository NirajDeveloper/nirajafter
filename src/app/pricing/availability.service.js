/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class AvailabilityService {
    constructor($log, $timeout, dataServices, OrderlistModalFactory, $http, PricingService, $q, authenticationService) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            $log, $timeout, dataServices, OrderlistModalFactory, PricingService, $http, $q, authenticationService
        });
        this.availabilityURL = "http://52.8.125.250:8080/";
        this._addToCart = false;

        this.changeOrderType = function(param) {
            let vm = this,
                size = "md";

            let urlModalInstance = null;
            if (param == "SO") {
                urlModalInstance = $timeout(OrderlistModalFactory.open("lg", "app/pricing/stock/stockorder.html", "stockOrderController", "stockController"), 2000);
            } else {
                urlModalInstance = $timeout(OrderlistModalFactory.open("lg", "app/pricing/emergency/emergencyorder.html", "emergencyOrderController", "emergencyController"), 2000);
            }
        }
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        this.data = {
            "cid": null,
            "APIResponse": {
                "customerCid": customerId,
                "partNumber": "SPL55-3X",
                "orderType": "SO",
                "quantityReq": 5,
                "currency": "USD",
                "partUnitPrice": 78,
                "corePartUnitPrice": 70,
                "partAvailDtl": [{
                    "quantityAvbl": 3,
                    "leadTimeInDays": 0,
                    "location": {
                        "plantCd": "",
                        "plantName": "CROSS VILLE",
                        "plantAddress": "",
                        "city": "",
                        "state": "",
                        "zipCode": "",
                        "country": ""
                    }
                }, {
                    "quantityAvbl": 7,
                    "leadTimeInDays": 1,
                    "location": {
                        "plantCd": "",
                        "plantName": "CROSS VILLE",
                        "plantAddress": "",
                        "city": "",
                        "state": "",
                        "zipCode": "",
                        "country": ""
                    }
                }, {
                    "quantityAvbl": 12,
                    "leadTimeInDays": 5,
                    "location": {
                        "plantCd": "",
                        "plantName": "CROSS VILLE",
                        "plantAddress": "",
                        "city": "",
                        "state": "",
                        "zipCode": "",
                        "country": ""
                    }
                }]
            }
        }
    }

    getAvailData(orderType,qtyReqd,partNo,corePartNumber) {
        let vm = this;
        let {
            $http,
            $q, authenticationService
        } = vm.DI();
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        return $http({
            url: this.availabilityURL + '/pricing-availability-service/api/part/x576-4/v1_0/pricing-availability',
            method: 'POST',
            data: {
                "customerCid": customerId,
                "orderType": orderType,
                "quantityReq": qtyReqd,
                "corePartNumber": "234"
            }
        })
    }



    get addToCartStatus() {
        return this._addToCart;
    }

    set addToCartStatus(status) {
        this._addToCart = status;
    }


}
