/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author : Shaifali Jaiswal*/
export class stockOrderController {
    constructor($uibModal, $log, $scope, $rootScope, PricingService, OrderlistModalFactory, $uibModalInstance, AvailabilityService) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            $uibModal, $log, $uibModalInstance, PricingService, AvailabilityService, $scope,$rootScope
        });

        this.orderType = PricingService.orderType;
        this.qtyReqd = PricingService.qtyRequired;
        this.partNumber = PricingService.partNumber;
        this.partName = PricingService.partName;
        this.addedToCart = false;
        this.plantNames = [];
        this.totalQty ="";
        this.updateStock = false;
        // this.availableData = AvailabilityService.data.APIResponse;//.partAvailDtl;
        // this.availableData.partNumber = AvailabilityService.data.APIResponse.partNumber;
        // this.availableData.corePartUnitPrice = AvailabilityService.data.APIResponse.corePartUnitPrice;
        // this.availableData.partUnitPrice = AvailabilityService.data.APIResponse.partUnitPrice;
         this.extendedPrice ="";
        this.unitPrice ="";

        vm.initData();

    }

    DateinDDMMYY(numberOfDaysToAdd) {
        var someDate = new Date();
        // var numberOfDaysToAdd = 6;
        someDate.setDate(someDate.getDate() + numberOfDaysToAdd);

        var dd = someDate.getDate();
        var mm = someDate.getMonth() + 1;
        var y = someDate.getFullYear();

        var someFormattedDate = dd + '/' + mm + '/' + y;
        return someFormattedDate;

    }

    changeAvailType() {
        let vm = this,
            {
                $uibModalInstance, PricingService, AvailabilityService, $filter, $scope
            } = vm.DI();

        PricingService.qtyRequired = vm.qtyReqd;
        PricingService.partNumber = vm.partNumber;
        PricingService.partName = vm.partName;
        $uibModalInstance.close();
        AvailabilityService.changeOrderType('EO');
    }


    updateData(){
      let vm = this,
            {
                $scope,PricingService
            } = vm.DI();
      PricingService.qtyRequired = vm.qtyReqd;
      vm.plantNames=[];
      vm.initData();
    }

        enableUpdateBtn(){
        let vm = this,
            {
                $scope, PricingService
            } = vm.DI();
            if(PricingService.qtyRequired !== vm.qtyRequired)
                vm.updateStock = true;
            else
                 vm.updateStock = false;
    }

    addToCart(){
         let vm = this,
            {
                $scope,AvailabilityService,$uibModalInstance,$rootScope
            } = vm.DI();
        AvailabilityService.addToCartStatus = true;
         $rootScope.$emit("displayAddToCart");
        $uibModalInstance.close();
    }

    initData() {
        let vm = this,
            {
                $uibModalInstance, PricingService, AvailabilityService
            } = vm.DI();

        vm.orderType = PricingService.orderType;
        vm.qtyReqd = PricingService.qtyRequired;
        vm.partNumber = PricingService.partNumber;
        vm.partName = PricingService.partName;

       let availability = "";
        AvailabilityService.getAvailData('STK',vm.qtyReqd)
            .then(function(response) {
                let {
                    $scope
                } = vm.DI();

                vm.availableData = response.data.APIResponse; //.partAvailDtl;

                let totAvailable = 0;
                vm.totalQty = vm.availableData.partAvailDtlList.reduce(function(a, b) {
                    return b['quantityAvbl'] == null ? a : a + b['quantityAvbl'];
                }, 0);

                vm.availableData.partAvailDtlList.map(function(item, index) {
                    vm.plantNames[index] = {
                        plantName: item.location.plantName,
                        leadDate: vm.DateinDDMMYY(item.leadTimeInDays),
                        availability: item.quantityAvbl,
                        selected: false
                    };
                })
                var qtyIndex = null;
                var currSum = 0;

                var filteredData = vm.availableData.partAvailDtlList.every(function(item, index) {
                    currSum = currSum + item.quantityAvbl;
                    if (vm.qtyReqd <= currSum) {
                        qtyIndex = index;
                        return false;
                    } else {
                        return true;
                    }
                });


                vm.unitPartPrice = (vm.availableData.partPriceDtl.priceDtlMap.STANDARD_BASE).toFixed(2) ;
                vm.extendedPrice = (vm.qtyReqd * vm.availableData.partPriceDtl.priceDtlMap.STANDARD_BASE).toFixed(2) ;
                vm.coreUnitPrice = (vm.availableData.partPriceDtl.priceDtlMap.CORE_BASE).toFixed(2) ;
                vm.unitPrice = (vm.qtyReqd * vm.availableData.partPriceDtl.priceDtlMap.CORE_BASE).toFixed(2);


                //var filteredData = vm.availableData.partAvailDtlList;
                // vm.availableData.totalQty = currSum;
                if (qtyIndex == null) {
                    vm.availableData.showBTO = true;
                    vm.availableData.totalQty = currSum;
                    vm.availableData.totalBTO = vm.qtyReqd - currSum;
                    vm.filteredData = vm.availableData.partAvailDtlList;
                } else {
                    vm.availableData.showBTO = false;
                    vm.filteredData = vm.availableData.partAvailDtlList.slice(0, qtyIndex + 1);
                }

            }, function(error) {

            });



        // vm.availableData.partAvailDtl.map(function(item) {
        //     item.leadDate = vm.DateinDDMMYY(item.leadTimeInDays);
        //     item.partNumber = AvailabilityService.data.APIResponse.partNumber;
        // })

        // var qtyIndex = null;
        // var currSum = 0;
        // var filteredData = vm.availableData.partAvailDtl.every(function(item, index) {
        //     currSum = currSum + item.quantityAvbl;
        //     if (vm.qtyReqd <= currSum) {
        //         qtyIndex = index;
        //         return false;
        //     } else {
        //         return true;
        //     }
        // });
        // vm.availableData.totalQty = currSum;
        // if(qtyIndex == null){
        //   vm.availableData.showBTO =true;
        //   vm.availableData.totalQty = currSum;
        //   vm.availableData.totalBTO = vm.qtyReqd - currSum;
        //   vm.filteredData = vm.availableData.partAvailDtl;
        // }
        // else{
        //    vm.availableData.showBTO =false;
        //    vm.filteredData = vm.availableData.partAvailDtl.slice(0, qtyIndex + 1);
        // }

    }

    cancel() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close();
    }

    checkAvailability(val) {
        let vm = this,
            size = "md";
        let {
            $log, $scope, OrderlistModalFactory, $timeout
        } = vm.DI();

        let urlModalInstance = $timeout(OrderlistModalFactory.open("lg", "app/pricing/stock/stockorder.html", "stockOrderController", "stockController"), 2000);

    }
}
