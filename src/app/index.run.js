/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/
export function runBlock($rootScope, $location, $stateParams, $window, SearchBarService, dataServices, OrderListService,  $translate, apiConfig, $state, authenticationService, AUTH_EVENTS, AftermarketConstants) {
    /*Please keep this below code at the top itself and every API call depends on it.*/
    apiConfig.BASEURL =window.env.activeAPIBase;
    apiConfig.ENDPOINT = window.env.endPoint;
    apiConfig.PRICINGURL =window.env.pricingAPIBase;
    apiConfig.AVAILABILITYURL =window.env.availabilityAPIBase;
    apiConfig.ORDERSERVICEURL =window.env.orderServiceAPIBase;
    apiConfig.PRICING_ENDPOINT = window.env.pricingEndPoint;
    apiConfig.AVAILABILITY_ENDPOINT = window.env.availabilityEndPoint;
    apiConfig.REGISTER_ENDPOINT = window.env.registerEndPoint;
    apiConfig.CART_ENDPOINT = window.env.cartEndpoint;
    apiConfig.ORDER_ENDPOINT = window.env.postOrderEndPoint;
    apiConfig.AUTH_BASEURL = window.env.activeAuthBase;
    apiConfig.AUTH_ENDPOINT = window.env.authEndPoint;
    apiConfig.AUTH_VERENDPOINT = window.env.authVerEndPoint;
    apiConfig.CARTPRICE_ENDPOINT = window.env.cartPriceEndpoint;
    apiConfig.ORDER_SERIVCE_ENDPOINT = window.env.orderServiceEndPoint;
    apiConfig.RFQ_SERVICE_ENDPOINT = window.env.shopUserEndpoint;
    apiConfig.DL_SERVICE_ENDPOINT = window.env.dealerLocatorEndpoint;
    apiConfig.EXPRESS_CHECKOUT = window.env.expressCheckoutEndpoint;
    
    $rootScope.protocol = AftermarketConstants.appSettings.protocol;
    /*Please keep this above code at the top itself and every API call depends on it.*/
    if(sessionStorage.orderId){
        OrderListService.orderId = angular.fromJson(sessionStorage.orderId);
    }else{
        dataServices.orderList().then(function (response) {
            OrderListService.orderId = response;
            sessionStorage.orderId = angular.toJson(response);
        }, function (error) {
        });
    }

    if(localStorage.currentUserProfileData){
        $rootScope.currentUserProfileData = JSON.parse(localStorage.currentUserProfileData);
        authenticationService.setCurrentUserProfileData($rootScope.currentUserProfileData);
    }
    if(localStorage.associatedCustData){
        authenticationService.setAssociatedCustIdToUser(JSON.parse(localStorage.associatedCustData));
    }
    if(localStorage.userPermissionsList){
        authenticationService.setCurrentUserPermissions(JSON.parse(localStorage.userPermissionsList));
    }

    if(localStorage.currentUserShortInfo){
        authenticationService.setCurrentUserShortInfo(JSON.parse(localStorage.currentUserShortInfo));
    }

    if(localStorage.shipToSoldToInfo){
        authenticationService.setShipToSoldToInfo(JSON.parse(localStorage.shipToSoldToInfo).customer, JSON.parse(localStorage.shipToSoldToInfo).billTos, true);
    }
    //listen to events of unsuccessful logins, to run the login dialog
      // $rootScope.$on(AUTH_EVENTS.notAuthorized, showNotAuthorized);
      // $rootScope.$on(AUTH_EVENTS.notAuthenticated,showLoginDialog);
      // $rootScope.$on(AUTH_EVENTS.sessionTimeout, showLoginDialog);
      // $rootScope.$on(AUTH_EVENTS.logoutSuccess, showLoginDialog);
      // $rootScope.$on(AUTH_EVENTS.loginSuccess, setCurrentUser);

      function showLoginDialog(ctx) {

              let vm = ctx,
                  size = "md";
              // let {
              //     $log, $uibModal
              // } = vm.DI();
              var modalInstance = $uibModal.open({
                 templateUrl: 'app/userprofile/login/login.html',
                  controller: 'LoginController',
                  controllerAs: 'login',
                  // templateUrl: 'app/orderList/ShareError/shareError.html',
                  // controller: 'ShareErrorController',
                  size: size,
                  windowClass: 'my-modal-popup',
                  resolve: {
                      items: function() {
                          return vm.items;
                      }
                  }
              });
          }

         function  setCurrentUser() {
              $rootScope.currentUser = $rootScope.currentUser;
          }

          function showNotAuthorized() {
              alert("Not Authorized");
          }
    
    // listener for non-authenticated user for navigating to home page
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
        //console.log("stateChangeStart ---> fromState : "+fromState.name+ "; toState : "+toState.name);
        $rootScope.fromState = fromState.name;
        if(fromState.name === 'completedOrder' && (toState.name === 'previewOrder' || toState.name === 'incompleteOrder')){
			if(!confirm("Are you sure you want to leave this page? \n \nYou will loose all changes made since you last saved.")) {
                event.preventDefault();
                $state.go(fromState.name);
            }else{
                $state.go('ordermgt', { 'isOrderPlaced': true });
            }
		}
        // if(fromState.name === 'previewOrder' && toState.name === 'ordermgt'){
		// 	if(!confirm("Are you sure you want to leave this page? \n \nYou will loose all changes made since you last saved.")) {
        //         event.preventDefault();
        //         $state.go(fromState.name);
        //     }else{
        //         //$state.go('ordermgt');
        //     }
		// }
        if (toState.authenticate && !localStorage.currentUserProfileData){
            $state.go("home");
            event.preventDefault(); 
        }
    });

    $rootScope.$on('$locationChangeSuccess', function() {
        angular.noop();
    });

    var windowElement = angular.element($window);
	windowElement.on('beforeunload', function (event) {
		if($location.path() === '/search'){
			sessionStorage.refreshClickedSearch = true;
		}else{
			delete sessionStorage.refreshClickedSearch;
		}
	});
}

runBlock.$inject = ['$rootScope', '$location', '$stateParams', '$window', 'SearchBarService', 'dataServices', 'OrderListService',  '$translate', 'apiConfig', '$state', 'authenticationService', 'AUTH_EVENTS', 'AftermarketConstants'];
