/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function routerConfig($stateProvider, $locationProvider, $urlRouterProvider, AftermarketConstants) {
  $locationProvider.html5Mode(true);

  $stateProvider
    .state('aftermarket', {
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'main',
      url: AftermarketConstants.skin.root
    });

    //$urlRouterProvider.otherwise(AftermarketConstants.skin.root+'/');
    $urlRouterProvider.otherwise('/');
}

routerConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'AftermarketConstants'];