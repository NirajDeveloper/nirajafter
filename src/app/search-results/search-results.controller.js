/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class SearchResultsController {
    constructor($log, $rootScope, $state, $scope, $timeout, $window, $document, $stateParams, $interval, $translate, dataServices, SearchBarService, appInfoService, BreadCrumbService, AftermarketConstants) {

        let vm = this;
        vm.DI = () => ({ $log, $state, $scope, $timeout, $rootScope, $stateParams, $interval, $translate, dataServices, SearchBarService, appInfoService, BreadCrumbService });

        $window.scrollTo(0, 0);

        vm.showYMMFlag = false;
        vm._showYMM();

        vm.automotive_id = AftermarketConstants.skin.automotive_id;
        vm.searchString = "";
        vm.sortAttributes = SearchBarService.sortAttr;

        vm.appliedFilters = angular.fromJson($stateParams.filterObject);

        vm.results = {
            parts: [],
            totalResults: 0
        };
        vm.resultSetLimit = 10;
        vm.resultStartIndex = 0;
        $timeout(function () {
            $scope.$emit("searchbarBlurred");
        });

        if ($stateParams.mode && $stateParams.mode === "hierarchy") {
            vm._hierarchyNavigation();
        }

        if ($stateParams.mode && $stateParams.mode === "bcNavigation") {
            vm._breadCrumbNavigation();
        }

        vm.resultLoading = false;

        let clearedFilterEvt = $rootScope.$on("clearedFilter", () => {
            $timeout(() => {
                $state.go("searchResults", vm.srchParamObj, { reload: true });
            });
        });

        let deregistrationCallback2 = $rootScope.$on('searchLaunched', function (event, payload) {
            vm.resultStartIndex = 0;
            vm.getParts(0, 10, payload);
        });

        vm.stickyAd = false;
        let deregistrationCallback = $rootScope.$on("isHeaderSticky", (evt, isHeaderSticky) => {
            let adSec = $document[0].getElementById("ad-section");
            if (isHeaderSticky.state) {
                vm.stickyAd = true;
                if (angular.isDefined(isHeaderSticky.bottomOffset)) {
                    angular.noop();
                }
            } else {
                vm.stickyAd = false;
                angular.element(adSec).css("bottom", 'auto');
            }
        });

        let removedFromTemp = $rootScope.$on("removedFromTemp", (evt, args) => {
            for(let i=0;i<vm.results.parts.length;i++) {
                vm.results.parts[i].showTempList = false;
                for(let j=0;j<args.length;j++) {
                    if(vm.results.parts[i].partNumber === args[j].partNo) {
                        vm.results.parts[i].showTempList = true;
                    }
                }
            }
        });

        $scope.$on('$destroy', function () {
            deregistrationCallback2();
            deregistrationCallback();
            clearedFilterEvt();
            removedFromTemp();
        });

        $stateParams.cat1 === 0 ? SearchBarService.productLine = { "id": 0 } : angular.noop();
        let ymm = {};
        $stateParams.y ? ymm.year = $stateParams.y : angular.noop();
        $stateParams.mk ? ymm.make = $stateParams.mk : angular.noop();
        $stateParams.md ? ymm.model = $stateParams.md : angular.noop();
        let ymmParams = Object.keys(ymm);
        if (!ymmParams.length) {
            ymm = null;
        }

        if ($stateParams.mode && $stateParams.mode === "hierarchy")
            angular.noop();
        else if (SearchBarService.backBottonPressed) {
            vm.getParts(vm.resultStartIndex, vm.resultSetLimit, angular.fromJson($stateParams.filterObject));
        } /*else if (sessionStorage.refreshClickedSearch) {
            vm.getParts(vm.resultStartIndex, vm.resultSetLimit, angular.fromJson($stateParams.filterObject), null, null, null, ymm);
        }*/ else {
            vm.getParts(vm.resultStartIndex, vm.resultSetLimit, angular.fromJson($stateParams.filterObject), null, null, null, ymm);
        }

        this.sortType = [
            "Relevance",
            "Featured",
            "New Launch",
            "Part Number",
            "Brand Name"
        ];

        if (angular.fromJson($stateParams.ics)) {
            vm.isICSRes = true;
        } else {
            vm.isICSRes = false;
        }
        vm.partNumber = $stateParams.str;

        console.log(appInfoService.getCat3($stateParams.cat1,$stateParams.cat2,$stateParams.cat3));
    }



    ymmSearch(year, make, model) {
        let vm = this;
        let {dataServices, SearchBarService} = vm.DI();
        dataServices.ymmSearch(SearchBarService.srchStr, SearchBarService.productLine, SearchBarService.productCategory, year, make, model, 0, 10)
            .then(function (response) {
                vm.filters = response.filter;
                vm.category = response.partCategoryList;
            });
    }

    nullSearchCategory(category) {
        let vm = this;
        let {$scope} = vm.DI();
        $scope.$emit("nullSearchCategory", category);
    }

    nullSearchSubCategory(category, subCategory) {
        let vm = this;
        let {$scope} = vm.DI();
        $scope.$emit("nullSearchSubCategory", category, subCategory);
    }

    getParts(from, size, payload, year, make, model, ymmObj) {
        let vm = this;
        let {dataServices, $stateParams, SearchBarService, $scope, $rootScope, appInfoService} = vm.DI();
        $scope.$emit("searchbarBlurred");
        vm.searchString = SearchBarService.srchStr;
        vm.productLine = SearchBarService.productLine ? SearchBarService.productLine.id : null;
        vm.pLine = SearchBarService.productLine;
        vm.cat1 = SearchBarService.productLine;
        vm.resultLoading = true;
        let ymm = null;
        if (ymmObj) {
            year = ymmObj.year; make = ymmObj.make; model = ymmObj.model;
            SearchBarService.ymmFilter = ymmObj;
        }
        else if (SearchBarService.productLine && SearchBarService.productLine.id.toString() === (2).toString() && SearchBarService.ymmFilter) {
            ymm = SearchBarService.ymmFilter.year + ' ' + SearchBarService.ymmFilter.make + ' ' + SearchBarService.ymmFilter.model;
        }
        else {
            ymm = null;
            SearchBarService.ymmFilter = null;
        }

        if (SearchBarService.autoSuggestItem && SearchBarService.autoSuggestItem.suggestType === "YMM_SUGGEST") {
            ymm = SearchBarService.autoSuggestItem.suggestId;
        }


        $scope.$emit("showLoading", true);

        let sortObj = null;
        if ($stateParams.sort) {
            let sObj = SearchBarService.getParticularSAttr($stateParams.sort);
            sortObj = {
                "sortAttribute": sObj.Name,
                "sortType": sObj.Type
            }
        }

        let icsFlag = $stateParams.ics ? ($stateParams.ics.toString() === ("true").toString() ? true : false) : false;
        dataServices.catSearch($stateParams.str, $stateParams.cat1 ? $stateParams.cat1 : null, $stateParams.from ? $stateParams.from : 0, $stateParams.size ? $stateParams.size : 10, $stateParams.cat3 ? $stateParams.cat3 : null, angular.fromJson($stateParams.filterObject), year, make, model, ymm, $stateParams.cat2 ? $stateParams.cat2 : null, sortObj, icsFlag).then(function (response) {
            vm.resultLoading = false;
            vm.results.parts = response.parts;
            vm.results.totalResults = response.totalResults;
            vm.resultSetLimit = response.resultSetLimit;
            vm.filters = response.filter;
            if (vm.filters === null) vm.filters = [];
            vm.category = response.partCategoryList;

            vm.currentPage1 = $stateParams.from ? $stateParams.from / 10 : 0;
            vm.currentPage1++;

            if (vm.results.parts) {
                vm.results.parts = vm.results.parts.map(function (part) {
                    let partDesc = (part.partDesc === null)?'':part.partDesc;
                    let partNumber = (part.partNumber === null)?'':part.partNumber;
                    part.displayName = partNumber + ' ' + partDesc;
                    if (part.attrs != null) {
                        if(part.attrs.Weight){
                        let splitwgt=part.attrs.Weight.split(" ");
                        let wgt=parseInt(splitwgt[0])+" "+splitwgt[1];
                        Object.assign(part.attrs, {'Weight' : wgt});
                        }
                     part.attrList = Object.keys(part.attrs);
                    } else {
                        part.attrList = [];
                    }
                    return part;
                });
                
                let ymmCatId = appInfoService.getYMMCatId();
                if (ymmCatId.toString() === $stateParams.cat1.toString() && !response.intChgPartSearch && $stateParams.str && response.parts.length !== 0) {
                    $rootScope.$emit("launchYMMList");
                    $scope.$emit("showLoading", true);
                }
                else {
                    $scope.$emit("showLoading", false);
                }
            }

        }, function () {
            vm.resultLoading = false;
            $scope.$emit("showLoading", false);
        });
    }

    loadMore() {
        let vm = this;
        vm.resultStartIndex = vm.resultStartIndex + vm.resultSetLimit;
        vm.getParts(vm.resultStartIndex, vm.resultSetLimit);
    }

    pageChanged() {
        let vm = this;
        let { $state } = vm.DI();
        let paramObj = { "from": (vm.currentPage1 - 1) * 10, "size": 10 };
        $state.go("searchResults", paramObj);
    }

    disableRightClick(evt) {
        evt.preventDefault();
    }

    clearFilter(idx = -1, parntIdx = -1) {
        let vm = this;
        let {$stateParams, $rootScope} = vm.DI();
        let fltrs = angular.fromJson($stateParams.filters);
        if (idx === -1 && parntIdx === -1) {
            fltrs = null;
            vm.appliedFilters = [];
        } else {
            let pos = fltrs.indexOf(vm.appliedFilters[parntIdx].values[idx]);
            fltrs.splice(pos, 1);
            vm.appliedFilters[parntIdx].values.splice(idx, 1);
            if (!vm.appliedFilters[parntIdx].values.length) {
                vm.appliedFilters.splice(parntIdx, 1);
            }
        }

        $rootScope.$broadcast("clearAttrFilter", vm.appliedFilters);

        let fltrObj = vm.appliedFilters.length ? angular.toJson(vm.appliedFilters) : null;
        fltrs = fltrs ? angular.toJson(fltrs) : fltrs;
        let paramObj = {
            "filters": fltrs,
            "filterObject": fltrObj
        }
        paramObj = angular.merge($stateParams, paramObj);

        vm.srchParamObj = paramObj;
    }

    _showYMM() {
        let vm = this;
        let {appInfoService, $stateParams, $interval} = vm.DI();
        let intvl = $interval(() => {
            if (appInfoService.appInfo) {
                $interval.cancel(intvl);
                if (!$stateParams.ics && $stateParams.cat1.toString() === appInfoService.getYMMCatId().toString()) {
                    vm.showYMMFlag = true;
                }
                else
                    vm.showYMMFlag = false;
            }
        }, 100);
    }

    _hierarchyNavigation() {
        let vm = this, {SearchBarService, $stateParams, $interval, appInfoService} = vm.DI();
        let intObj = $interval(() => {
            if (angular.isDefined(appInfoService.appInfo) && angular.isDefined(appInfoService.appInfo.cats)) {
                $interval.cancel(intObj);
                SearchBarService.srchStr = "";
                SearchBarService.productLine = appInfoService.getCat1($stateParams.cat1);
                SearchBarService.productCategory = appInfoService.getCat3($stateParams.cat1, $stateParams.cat2, $stateParams.cat3);
                SearchBarService.productClass = appInfoService.getCat2($stateParams.cat1, $stateParams.cat2);

                vm.getParts(vm.resultStartIndex, vm.resultSetLimit, null, null, null, null);
            }
        }, 100);
    }

    _breadCrumbNavigation() {
        let vm = this, {SearchBarService} = vm.DI();
        vm.getParts(vm.resultStartIndex, vm.resultSetLimit, SearchBarService.selectdeFilters, null, null, null);
    }




}

SearchResultsController.$inject = ['$log', '$rootScope', '$state', '$scope', '$timeout', '$window', '$document', '$stateParams', '$interval', '$translate', 'dataServices', 'SearchBarService', 'appInfoService', 'BreadCrumbService', 'AftermarketConstants'];
