/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function FilterDirective() {
    let directive = {
        restrict: 'E',
        templateUrl: 'app/search-results/filter/filter.html',
        scope: {
            list: '=',
            category: '=',
            totalCount: '=',
            selectedItemsChanged: '&'
        },
        controller: FilterDirectiveController,
        controllerAs: 'vm',
        bindToController: true
    };
    return directive;
}

class FilterDirectiveController {
    constructor($log, $state, $location, $q, $stateParams, SearchBarService, dataServices, $scope, $rootScope, $timeout, $filter, $translate) {
        let vm = this;

        vm.DI = () => ({ $log, $state, $q, $location, $stateParams, SearchBarService, dataServices, $scope, $rootScope, $filter });

        vm.showTriangle = ($stateParams.cat1 === "0") ? false : true;
        let clearCat = $rootScope.$on("clearCategoryFilter", function () {
            vm.categoryPristine = [];
            SearchBarService.categoryfilters = [];
            SearchBarService.filters = [];
            vm.listPristine = [];
        });
        let nullSearch = $rootScope.$on("nullSearchCategory", function (event, category) {
            let tempChildren = [];
            if (category.children.length) {
                tempChildren = category.children.map(function (sub) {
                    return {
                        name: sub.name,
                        select: false,
                        id: sub.id
                    }
                });
            }
            let selectedCategory = {
                name: category.name,
                select: false,
                id: category.id,
                children: tempChildren
            }

            vm.categoryFilter(selectedCategory);
        });
        $rootScope.$on("nullSearchSubCategory", function (event, category, subcategory) {
            let selectedCategory = {
                id: category.id
            };
            let selectedSubCategory = {
                name: subcategory.name,
                select: false,
                id: subcategory.id
            };
            vm.subCategoryFilter(selectedCategory, selectedSubCategory);
        });
        $rootScope.$on("breadCrumSearchCategory", function (event, category) {
            for (let obj of vm.categoryPristine) {
                if (obj !== null && obj !== undefined && category.id.toString() === obj.id.toString()) {
                    $rootScope.$emit("nullSearchCategory", obj);
                    return;
                }
            }
        });
        $scope.$on("clearAttrFilter", (evt, appliedFilters) => {
            if (!appliedFilters.length) {
                vm.listPristine = vm.listPristine.map((prist) => {
                    prist.buckets = prist.buckets.map((buckt) => {
                        buckt.select = false;
                        return buckt;
                    });
                    return prist;
                });
                if (vm.listPristine.length) {
                    $rootScope.$emit("clearedFilter");
                }
            } else {
                vm.listPristine = vm.listPristine.map((prist) => {
                    let matched = false;
                    angular.forEach(appliedFilters, (appFil) => {
                        if (appFil.name.trim() === prist.name.trim()) {
                            matched = true;
                            prist.buckets = prist.buckets.map((buckt) => {
                                buckt.select = false;
                                angular.forEach(appFil.values, (val) => {
                                    if (val === "Spicer") {
                                    }
                                    if (val.trim() === buckt.key.trim()) {
                                        buckt.select = true;
                                    }
                                });
                                return buckt;
                            });
                        } else {
                            if (!matched) {
                                prist.buckets = prist.buckets.map((buckt) => {
                                    buckt.select = false;
                                    return buckt;
                                });
                            }
                        }
                    });
                    return prist;
                });
                if (vm.listPristine.length) {
                    $rootScope.$emit("clearedFilter");
                }
            }
        });
        $rootScope.$on("$destroy", () => {
            clearCat();
            nullSearch();
            clearAttrFilterEvt();
        });
        /* array which holds the updated attributes list */
        vm.listPristine = [];
        /* array which remembers the selected filters to update in new set of filters(retain filters) */
        vm.listPreviousFilter = [];
        /* array which holds the updated attributes category */
        vm.categoryPristine = [];
        $rootScope.$on("localizationChnged", () => {
            if (sessionStorage.categoryHint) {
                $translate('FILTER.TOOLTIP').then((txt) => {
                    vm.categoryHint = txt;
                    sessionStorage.categoryHint = txt;
                });
            } else {
                vm.categoryHint = "";
            }
        });
        if (sessionStorage.categoryHint) {
            vm.categoryHint = sessionStorage.categoryHint;
        } else {
            vm.categoryHint = "";
        }
        /* watch for the change in category */
        $scope.$watch(function () {
            return vm.category;
        }, function () {
            $timeout(function () {
                vm.resetCategory();
            }, 200);
        });

        /* watch for the change in list */
        $scope.$watch(function () {
            return vm.list;
        }, function () {
            if (SearchBarService.backBottonPressed) {
                vm.listPristine = SearchBarService.filters;
                SearchBarService.backBottonPressed = false;
            } else if (sessionStorage.refreshClickedSearch) {
                vm.listPristine = angular.fromJson(sessionStorage.filters);
            } else {
                vm.resetList();
                vm.listPreviousFilter = SearchBarService.listPreviousFilter;
                if (vm.listPreviousFilter && vm.listPreviousFilter.length >= 1) {
                    for (let x of vm.listPristine) {
                        if (x !== null && x !== undefined) {
                            x.priority = 2;
                            for (let y of vm.listPreviousFilter) {
                                if (y !== null && y !== undefined && x.name === y.name) {
                                    if (y.bucketChanged) {
                                        Object.assign(x, y);
                                        x.priority = 1;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                SearchBarService.filters = vm.listPristine;
            }
        });
    }

    /* convert categorty to object and put it in categoryPristine */
    resetCategory() {
        let vm = this;
        let { SearchBarService, $stateParams, $filter } = vm.DI();
        if (sessionStorage.refreshClickedSearch) {
            vm.categoryPristine = angular.fromJson(sessionStorage.categoryfilters);
            delete sessionStorage.refreshClickedSearch;
        } else {
            vm.categoryPristine = angular.fromJson(sessionStorage.categoryfilters);
        }
        if (vm.totalCount === 0) {
            vm.categoryPristine = [];
        }
        if (vm.category && vm.category.length > 0) {
            vm.categoryPristine = $filter("orderBy")(vm.category, 'name');
            if (vm.categoryPristine.length) {
                vm.categoryPristine = vm.categoryPristine.map(function (obj) {
                    let tempChildren = [];
                    if (obj.children && obj.children.length) {
                        tempChildren = obj.children.map(function (sub) {

                            return {
                                name: sub.name,
                                select: false,
                                id: sub.id
                            }
                        });
                    }
                    return {
                        name: obj.name,
                        select: false,
                        collapse: true,
                        id: obj.id,
                        children: tempChildren,
                        viewLimitName: "View less",
                        toggleView: true,
                        viewLimit: tempChildren.length
                    };
                });
            }

        } else {
            if (vm.categoryPristine) {
                if ($stateParams.cat3 && $stateParams.cat2) {
                    if (vm.categoryPristine.length) {
                        vm.categoryPristine = vm.categoryPristine.map(function (obj) {
                            let tempChildren = [];
                            if (obj.children.length) {
                                tempChildren = obj.children.map(function (sub) {
                                    return {
                                        name: sub.name,
                                        select: sub.id.toString() === $stateParams.cat3.toString() ? true : false,
                                        id: sub.id
                                    }
                                });
                            }

                            return {
                                name: obj.name,
                                select: obj.id.toString() === $stateParams.cat2.toString() ? true : false,
                                collapse: obj.id.toString() === $stateParams.cat2.toString() ? true : false,
                                id: obj.id,
                                children: tempChildren,
                                viewLimitName: "View all",
                                toggleView: false,
                                viewLimit: 1
                            };
                        });
                    }
                } else if ($stateParams.cat2) {
                    vm.categoryPristine = vm.categoryPristine.map(function (obj) {
                        let tempChildren = obj.children.map(function (sub) {
                            return {
                                name: sub.name,
                                select: false,
                                id: sub.id
                            }
                        });
                        return {
                            name: obj.name,
                            select: obj.id.toString() === $stateParams.cat2.toString() ? true : false,
                            collapse: obj.id.toString() === $stateParams.cat2.toString() ? true : false,
                            id: obj.id,
                            children: tempChildren,
                            viewLimitName: "View less",
                            toggleView: true,
                            viewLimit: tempChildren.length
                        };
                    });
                } else if ($stateParams.cat3) {
                    vm.categoryPristine = vm.categoryPristine.map(function (obj) {
                        let tempChildren = obj.children.map(function (sub) {
                            return {
                                name: sub.name,
                                select: sub.id.toString() === $stateParams.cat3.toString() ? true : false,
                                id: sub.id
                            }
                        });
                        return {
                            name: obj.name,
                            select: false,
                            collapse: obj.id.toString() === $stateParams.cat2.toString() ? true : false,
                            id: obj.id,
                            children: tempChildren,
                            viewLimitName: "View all",
                            toggleView: false,
                            viewLimit: 1
                        };
                    });
                }
            }
        }

        /* In case of only one product line filters should be shown along with the category 
        if (vm.category && vm.category.length === 1) {
        }*/
        SearchBarService.categoryfilters = vm.categoryPristine;

    }

    /* call api to get the filters for the selected category and selected category should be heighlighted */
    categoryFilter(selectedCategory) {
        let vm = this;
        let { $rootScope, $state, $scope, SearchBarService, $stateParams } = vm.DI();
        delete sessionStorage.categoryHint;
        vm.categoryHint = "";
        vm.listPreviousFilter = [];
        SearchBarService.sort = null;
        $scope.$emit("checkSearch", SearchBarService.srchStr);
        $rootScope.$broadcast("categoryFilterApplied", { obj: selectedCategory, catFilter: true });
        selectedCategory.select = !selectedCategory.select;
        if (SearchBarService.productLine && SearchBarService.productLine.id.toString() === (0).toString()) {
            SearchBarService.productLine = selectedCategory;
            let paramObj = { 'filters': "", 'filterObject': "", cat1: selectedCategory.id, "cat2": "", "cat3": "", "from": "", "size": "", "y": $stateParams.y, "mk": $stateParams.mk, "md": $stateParams.md, "sort": "" };
            if ($stateParams.ics) {
                paramObj.ics = $stateParams.ics;
            }
            $state.go("searchResults", paramObj);
        } else {
            if (selectedCategory.select) {
                SearchBarService.productClass = selectedCategory;
                SearchBarService.productCategory = 0;
                let paramObj = { 'filters': "", 'filterObject': "", "cat2": selectedCategory.id, "cat3": "", "from": "", "size": "", "y": $stateParams.y, "mk": $stateParams.mk, "md": $stateParams.md, "sort": "" };
                if ($stateParams.ics) {
                    paramObj.cat1 = selectedCategory.id;
                    paramObj.cat2 = "";
                }
                $state.go("searchResults", paramObj);
            } else {
                SearchBarService.productClass = 0;
                let paramObj = { 'filters': "", 'filterObject': "", "cat1": $stateParams.cat1, "cat2": "", "cat3": "", "from": "", "size": "", "y": $stateParams.y, "mk": $stateParams.mk, "md": $stateParams.md, "sort": "" };
                if ($stateParams.ics) {
                    angular.noop();
                }
                $state.go("searchResults", paramObj);
            }
        }

    }

    /* call api to get the filters for the selected subcategory and selected subcategory should be heighlighted */
    subCategoryFilter(category, selectedSubCategory) {
        let vm = this;
        let { $rootScope, $state, SearchBarService, $stateParams } = vm.DI();
        delete sessionStorage.categoryHint;
        vm.categoryHint = "";
        SearchBarService.listPreviousFilter = [];
        SearchBarService.sort = null;
        $rootScope.$broadcast("categoryFilterApplied");
        selectedSubCategory.select = !selectedSubCategory.select;
        if (selectedSubCategory.select) {
            SearchBarService.productCategory = selectedSubCategory;
            let paramObj = { 'filters': "", 'filterObject': "", "cat2": category.id, "cat3": selectedSubCategory.id, "from": "", "size": "", "y": $stateParams.y, "mk": $stateParams.mk, "md": $stateParams.md, "sort": "" };
            $state.go("searchResults", paramObj);
        } else {
            SearchBarService.productCategory = 0;
            let paramObj = { 'filters': "", 'filterObject': "", "cat1": $stateParams.cat1, "cat2": category.id, "cat3": "", "from": "", "size": "", "y": $stateParams.y, "mk": $stateParams.mk, "md": $stateParams.md, "sort": "" };
            $state.go("searchResults", paramObj);
        }
    }

    /* add extra properties to list and put in listPristine  */
    pushCheckboxData(x) {
        let vm = this;
        vm.tempBuckets = [];

        angular.forEach(x.buckets, function (obj) {
            vm.tempBuckets.push({
                key: obj.key,
                count: obj.count,
                select: false
            });
        });

        let obj = {
            name: x.name,
            type: x.type,
            buckets: vm.tempBuckets,
            viewSelect: x.buckets.length > 1 ? "Select All" : '',
            toggleSelect: false,
            viewLimitName: "View all",
            toggleView: false,
            viewLimit: 4
        };
        return obj;
    }

    /* categorize the filter based on their types */
    resetList() {
        let vm = this;
        vm.listPristine = [];
        for (let x of vm.list) {
            vm.listPristine.push(vm.pushCheckboxData(x));
        }
    }

    /* togglet the SelectAll and Unselect */
    toggleselect(list) {
        let vm = this;
        if (list.toggleSelect) {
            list.viewSelect = "Select All";
            angular.forEach(list.buckets, function (obj) {
                obj.select = false
            });
        } else {
            list.viewSelect = "Unselect All";
            angular.forEach(list.buckets, function (obj) {
                obj.select = true
            });
        }
        list.toggleSelect = !list.toggleSelect;
        vm.apicall();
    }

    toggleCategory(category) {
        if (category.toggleView) {
            category.viewLimitName = "View all"
            category.viewLimit = 1;
        } else {
            category.viewLimitName = "View less"
            category.viewLimit = category.children.length;
        }
        category.toggleView = !category.toggleView;
    }

    collapseCategory(category) {
        category.collapse = !category.collapse;
    }

    /* togglet the View all and View less */
    toggleview(list) {
        if (list.toggleView) {
            list.viewLimitName = "View all"
            list.viewLimit = 4;
        } else {
            list.viewLimitName = "View less"
            list.viewLimit = list.buckets.length;
        }
        list.toggleView = !list.toggleView;
    }

    /* call the api to get the filters and categories */
    apicall(selectedFilter) {
        let vm = this;
        let {$q} = vm.DI();
        let prms = () => {
            return $q((resolve) => {
                let vm = this;
                let { $scope, $state, $stateParams, SearchBarService} = vm.DI();
                let filterObjectArray = [];

                $scope.$emit("checkSearch", SearchBarService.srchStr);
                /* put all the selected filters in filterObjectArray */
                for (let x of vm.listPristine) {
                    if (x !== null && x !== undefined) {
                        let filterArray = [];
                        let filterObject;
                        for (let obj = 0; obj < x.buckets.length; obj++) {
                            x.buckets[obj].select ? filterArray.push(x.buckets[obj].key) : "";
                        }
                        if (filterArray.length) {
                            filterObject = {
                                name: x.name,
                                type: x.type,
                                values: filterArray
                            };
                            filterObjectArray.push(filterObject);
                            x.bucketChanged = true;
                        } else {
                            x.bucketChanged = false;
                        }
                    }
                }

                SearchBarService.listPreviousFilter = vm.listPristine;
                let temp;
                $stateParams.filters ? temp = $stateParams.filters : temp = [];
                temp = angular.fromJson(temp);
                angular.fromJson(temp).push(selectedFilter.key);
                let paramObj = { 'filters': angular.toJson(temp), 'filterObject': angular.toJson(filterObjectArray), "from": "", "size": "", "sort": $stateParams.sort };
                $state.go("searchResults", paramObj);
                SearchBarService.selectdeFilters = filterObjectArray;
                resolve();
            });
        }
        return prms();
    }

    launchTreeEvent() {
        let vm = this;
        let { $rootScope } = vm.DI();
        $rootScope.$emit("showOnlyTreeInBC", true);
    }
}

FilterDirectiveController.$inject = ['$log', '$state', '$location', '$q', '$stateParams', 'SearchBarService', 'dataServices', '$scope', '$rootScope', '$timeout', '$filter', '$translate'];
