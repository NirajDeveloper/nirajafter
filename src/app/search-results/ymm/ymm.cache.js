/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class ymmCacheFactory {
    constructor($cacheFactory) {
        this.$cacheFactory = $cacheFactory;

    }
     getYMMCache() {
        return this.$cacheFactory('cached');
    }

}

ymmCacheFactory.$inject = ['$cacheFactory'];









