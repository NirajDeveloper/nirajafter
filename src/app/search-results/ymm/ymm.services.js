/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class YmmService {

    constructor($http, $q, $stateParams, SearchBarService, apiConfig, ymmCacheFactory, appInfoService) {
        let vm = this;
        vm.DI = () => ({
            $http,
            $q,
            ymmCacheFactory,
            $stateParams,
            SearchBarService,
            appInfoService
        });
        this._yearSelected;
        this._makeSelected;
        this._modelSelected;
        this.$http = $http;
        this.$q = $q;
        this.ymmURL = apiConfig.BASEURL;
        this.endPoint = apiConfig.ENDPOINT;
        this.method = 'POST';
        this.params = "";
        this._currYMMOrder = [];
        this.level = [];
        this.currentCache = {};
        this._yearList = [];
        this._makeList = [];
        this._modelList = [];

    }

    set YMMOrder(order) {
        this._currYMMOrder = order;
    }

    get YMMOrder() {
        return this._currYMMOrder;
    }

    set yearList(year) {
        this._yearList = year;
        sessionStorage.yearList = angular.toJson(year);
    }

    get yearList() {
        return angular.fromJson(sessionStorage.yearList);
    }

    set makeList(make) {
        this._makeList = make;
        sessionStorage.makeList = angular.toJson(make);
    }

    get makeList() {
        return angular.fromJson(sessionStorage.makeList);
    }

    set modelList(model) {
        this._modelList = model;
        sessionStorage.modelList = angular.toJson(model);
    }

    get modelList() {
        return angular.fromJson(sessionStorage.modelList);
    }

    getYearData(q, cats, lvl1, lvl2, lvl3, from, size, isHeader = false) {
        let vm = this;
        let {
            $http,
            $q,
            SearchBarService,
            ymmCacheFactory,
            $stateParams,
            appInfoService
        } = vm.DI();

        vm.$q.defer();
        let selFilter = angular.fromJson(sessionStorage.selectdeFilters);
        if (isHeader) {
            sessionStorage.selectdeFilters = null;
            selFilter = null;
        }

        /*let tempCatId = "";
        if (SearchBarService.productCategory === undefined) {
            tempCatId = null;
        } else {

            tempCatId = SearchBarService.productCategory.id;
        }*/
        let cat1, cat2, cat3, srchStr;
        if (isHeader) {
            cat1 = appInfoService.getYMMCatId();
            cat2 = cat3 = 0;
            srchStr = null;
        } else {
            cat1 = $stateParams.cat1;
            cat2 = $stateParams.cat2;
            cat3 = $stateParams.cat3;
            srchStr = SearchBarService.srchStr;
        }
        return this.$http({
            url: this.ymmURL + this.endPoint + "/ymmList",
            method: 'POST',
            data: {
                "q": srchStr,
                "cats": [cat1, cat2, cat3],
                "filter": selFilter,
                "lvl1": this.level[1],
                'lvl2': this.level[2],
                "lvl3": (this.level[3] === undefined) ? (null) : (this.level[3]),
                "from": from,
                "size": size
            }
        })
    }


    getAPIConfigDataForYMM(q, cats, year, make, model, from, size) {
        let vm = this;
        let {
            $http,
            $q,
            SearchBarService
        } = vm.DI();

        vm.$q.defer();
        return this.$http({
            url: this.ymmURL + this.endPoint + "/configData",
            method: 'GET'
        })
    }

    setLevelData(param, data) {
        this.level[param + 1] = data;
    }

    emptyLevelData() {
        this.level = [];

    }
}

YmmService.$inject = ['$http', '$q', '$stateParams', 'SearchBarService', 'apiConfig', 'ymmCacheFactory', 'appInfoService'];