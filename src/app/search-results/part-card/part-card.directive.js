/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function PartCardDirective() {

    let directive = {
        restrict: 'E',
        templateUrl: 'app/search-results/part-card/part-card.html',
        scope: {
            part: '=',
            protocol: '@'
        },
        controller: SearchResultDirectiveController,
        controllerAs: 'partCard',
        bindToController: true
    };
    return directive;
}

class SearchResultDirectiveController {
    constructor($log, $timeout, $stateParams,  $state, $document, BreadCrumbService, SearchBarService, OrderListService, appInfoService, $uibModal, $location, $rootScope, dataServices, OrderlistModalFactory, PricingService, ProfileService, authenticationService, $sce, $scope, $translate, $filter) {
        let vm = this;
        vm.DI = () => ({ $log, $timeout, $stateParams,  $state, BreadCrumbService, SearchBarService, OrderListService, appInfoService, $uibModal, $location, $rootScope, dataServices, OrderlistModalFactory, PricingService, ProfileService, authenticationService, $sce, $scope, $translate, $filter });
        vm.partImage = appInfoService.appInfo.cdnBaseurl;
        if(this.part && (!this.part.packageQty || (this.part.packageQty && this.part.packageQty < 1))){
            this.part.packageQty = 1;
        }
        this.minQty = this.part.packageQty > 1 ? 0 : 1;
        this.showAvailable = true;
        this.showAvailableMsg = "Checking availability";
        this.specLimit = 5;
        this.toggle = false;
        this.specToggleName = "Expand";
        this.shwOrdrTxt = true;
        this.shwCartTxt  = localStorage["currentUserProfileData"] !== undefined ? true : false;
        this.shwQty = false;
        this.shwCartQty = false;
        this.shwMsg = false;
        this.shwRTBMsg = false;
        this.shwMyLists = false;
        //this.showTempList = false;
        this.shwOrderTypes = false;
        this.qty = 1;
        this.cartQty = 1;
        this.selectedList = "";
        this.lists = OrderListService.myLists;
        this.qtyNotAllowed = (this.qty > 0) ? false : true;
        this.cartQtyNotAllowed = (this.cartQty > 0) ? false : true;
        this.newLineId = "";
        this.selectedId = "";
        this.assignPartImage();
        this.action='';
        this.isRTBChecked = false;
        this.userType = '';
        this.setUserType();
        this.tooltipOpen = false;
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });
        let partCardLogin = $rootScope.$on("partCardLogIn",function(){
            vm.shwCartTxt  = true;
            //vm.showTempList = false;
            vm.shwOrdrTxt = true;
            vm.setUserType();
            vm.setTooltip();
        });

        $rootScope.$on("$destroy", partCardLogin);
        
        $timeout(() => {
            let partPics = $document[0].getElementsByClassName("default-pic");
            angular.forEach(partPics, (partPic) => {
                partPic.onerror = function () {
                    this.src = "https://placehold.it/160x160/dbdbdb/0099CC/?text=NO+IMAGE";
                }
            });
        });

        $timeout(() => {
            let brandLogos = $document[0].getElementsByClassName("brand-logo");
            angular.forEach(brandLogos, (brandLogo) => {
                brandLogo.onerror = function () {
                    angular.element(this).css("display", "none");
                }
            });
        });

        vm.customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            vm.customerId = authenticationService.associatedCustData.activeCustId;
        }
        
        vm.setTooltip();

        vm.checkAvlMsg='Checking availability...';
        vm.qtyAVL='Qty. available:';
        vm.tooltip1='Become a Dana customer - TBD';
        vm.tooltip2='Register as a user - TBD';
        vm.expand='Expand';
        vm.collapse='Collapse';
        $translate('common.CHECKAVL').then((MSG)=>{
           vm.checkAvlMsg=MSG;
        })
        $translate('common.QTYAVL').then((MSG)=>{
          vm.qtyavl= MSG;
        });
         $translate('TOOLTIP.PARTCARD1').then((MSG)=>{
           vm.tooltip1=MSG;
        })
        $translate('TOOLTIP.PARTCARD2').then((MSG)=>{
          vm.tooltip2= MSG;
        });
        $translate('common.EXPAND').then((MSG)=>{
          vm.expand= MSG;
        });
        $translate('common.COLLAPSE').then((MSG)=>{
          vm.collapse= MSG;
        });

       
    }

    setInterchange(part) {
        let vm = this;

        let interchange = angular.copy(part.interchanges);
        vm.superSession = false;
        if(interchange && interchange.length){
            for(let i=0;i<interchange.length;i++) {
                if(interchange[i].interChgComments === "Supersession") {
                    vm.superSession = true;
                    vm.superSessionPartNumber = interchange[i].partNumber;
                    break;
                }
            }
        }

        if(!vm.superSession) {
            vm.interChngPart = part.interChngPart;
        }
        else {
            vm.interChngPart = false;
        }
    }

    setUserType() {
        let vm = this;
        let { $rootScope, ProfileService } = vm.DI();
        ProfileService.setType($rootScope.currentUserProfileData);
        vm.shopUser = ProfileService.shopUser;
        vm.customer = ProfileService.customer;
        vm.admin = ProfileService.admin;
        vm.loggedIn = ProfileService.loggedIn;

        
    }

    setTooltip() {
        let vm = this;
        let {authenticationService} = vm.DI();
        //console.log(authenticationService);

        if(authenticationService.currentUserProfileData.userType !== undefined) {
            if(authenticationService.userPermissionsList.RequestQuote) {
                vm.tooltip = vm.tooltip1;
            }
        }
        else {
            vm.tooltip = vm.tooltip2;
        }
    }

    assignPartImage() {
        for (let key of this.part.attrList) {
            if (key === "Brand") {
                this.part.attrs[key] === 'SPL' ? this.partImage += "/logo/logo_Spicer" + ".jpg" : this.partImage += "/logo/logo_" + this.part.attrs[key] + ".jpg";
                return;
            }
        }
    }
    checkOrderlistVal() {
        if (this.qty > 0) {
            this.qtyNotAllowed = false;
        } else {
            this.qtyNotAllowed = true;

        }
    }

    checkAvailability(partCard){
        let vm = this,
        {
            dataServices,$sce, $filter
        } = vm.DI();
      
        vm.showAvailable = false;
        vm.showAvailableMsg = $sce.trustAsHtml("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i><span class='marginltgap'>"+vm.checkAvlMsg+"</span>");
        if(!partCard.part.packageQty || partCard.part.packageQty.toString().trim() == ""){
            partCard.part.packageQty = 1;
        }else{
            partCard.part.packageQty = partCard.part.packageQty;
        }
        dataServices.partAvailability(partCard.part.partNumber, vm.customerId ? vm.customerId : "",  "STK", vm.qtyReqd = 9999, partCard.part.corePartNumber,partCard.part.catPath, partCard.part.packageQty)
        .then(function(response){
            let availQty = response.totalQtyAvail ? response.totalQtyAvail : 0
            vm.showAvailableMsg = $sce.trustAsHtml("<span class='qtyAvailable'>"+ vm.qtyavl +"<strong class='fontbold'>"+ $filter('number')(availQty) + "</strong>" + "</span> <a class='glyphicon glyphicon-refresh marginrtgap' ng-click='partCard.checkAvailability(partCard)'></a>");
        },function(error){
            vm.showAvailableMsg = $sce.trustAsHtml("Error");
        });
    }   

    addToCartClick(){
        let vm = this,
        {
            dataServices,
        } = vm.DI();
        //Check RTB
        //vm.resetFlags();
        if(localStorage["currentUserProfileData"] !== undefined){
            this.shwOrdrTxt = true;
            this.shwQty = false;
            this.shwMsg = false;
            this.shwMyLists = false;
            this.shwRTBMsg = false;
            if(!vm.isRTBChecked) {
                let itemArr = [];
                itemArr.push(vm.part.partNumber);
                dataServices.bulkRTBCheck(itemArr).then(function (response) {
                    vm.isRTBChecked = true;
                    //response.data= {message: "Authorized", status: "SUCCESS"};
                    if (response && response.rtbParts &&  response.rtbParts.length > 0) {
                        vm.userType = 'RTB';
                        vm.showAddToCart(); 
                    } else {
                        
                        vm.userType = 'NON-RTB';
                        
                        vm.showAddToCart(); 
                    }     
                }, function (error) {
                    vm.shwRTBMsg = true;
                    vm.isRTBChecked = false;
                });
            }
            else {
               vm.showAddToCart(); 
            }
             
        }
    }
    showAddToCart(){
        let vm = this;
        if(vm.userType === 'RTB') {
                this.shwCartTxt = false;
                this.shwCartQty = true;                
                if (this.cartQty > 0) {
                    this.cartQtyNotAllowed = false;
                } else {
                    this.cartQtyNotAllowed = true;
                }
                if(vm.part && vm.part.packageQty > 1){
                   vm.cartQty = vm.part.packageQty;
                }
            }
            else {
                vm.shwRTBMsg = true;
                //No RTB permission
            }
    }
    checkCartQtyVal(fieldId) {
        let vm = this;
        console.log("document.getElementById('part_31872') : "+document.getElementById(fieldId).value);
        let fieldVal = document.getElementById(fieldId).value;
        if(fieldVal == "0"){
            vm.cartQty = vm.part.packageQty;
        }
        if (vm.cartQty > 0) {
            vm.cartQtyNotAllowed = false;
        }else{
            vm.cartQtyNotAllowed = true;
        }
        if(vm.part && vm.part.packageQty > 1 && vm.cartQty > 0 && (vm.cartQty % vm.part.packageQty != 0)){
            vm.isQtyNtMultplOfPkQty = true;
        }else{
            vm.isQtyNtMultplOfPkQty = false;
        }
    }
    makeQtyPositive(qtyNtPositive, fieldId){
        let vm = this;
        qtyNtPositive ? vm.cartQty = vm.part.packageQty: angular.noop();
        let fieldVal = document.getElementById(fieldId).value;
        // if(vm.cartQty % vm.part.packageQty != 0){
        //     let qtyFold = Math.floor(vm.cartQty/vm.part.packageQty);
        //     vm.cartQty = vm.part.packageQty * qtyFold + vm.part.packageQty;
        // }
        // if(vm.cartQty % vm.part.packageQty != 0){
        //     let qtyFold = Math.floor(vm.cartQty/vm.part.packageQty);
        //     if(qtyFold == 0){
        //         vm.cartQty = vm.part.packageQty;
        //     }
        // }
        if (vm.cartQty > 0) {
            vm.cartQtyNotAllowed = false;
        }else{
            vm.cartQtyNotAllowed = true;
        }
        if(vm.part && vm.part.packageQty > 1 && vm.cartQty > 0 && (vm.cartQty % vm.part.packageQty != 0)){
            vm.isQtyNtMultplOfPkQty = true;
        }else{
            vm.isQtyNtMultplOfPkQty = false;
        }
    }
    showOrderType(){
        let vm = this;
        vm.shwOrderTypes = true;
    }
    resetFlags(){
        this.shwOrdrTxt = true;
        this.shwQty = false;
        this.shwMsg = false;
        this.shwRTBMsg = false;
        //this.shwMyLists = false;
        this.shwCartTxt = true;
        this.shwCartQty = false;
        this.shwOrderTypes = false;
    }
    doOrderSelection(type){
        let vm = this,
        {
            dataServices,
            $rootScope,
            $scope,
            $uibModal, PricingService
        } = vm.DI();
        if(type === 'SO') {
            let payload = {
                "cartName":"STK",
                "cartType": "normal-cart",
                "orderType": "SO",
                "lineItems": [{
                            "partNo": vm.part.partNumber,
                            "partName": vm.part.partDesc,
                            "qtyReq": vm.cartQty,
                            "packageQty": vm.part.packageQty,
                            "qtyAdded": vm.cartQty,
                            "backOrderQty": 0,
                            "uom": vm.part.packageType ? vm.part.packageType : "",
                            "thumbnail": vm.part.tnImage ? vm.part.tnImage : "",
                            "wt": vm.part.wt ? vm.part.wt : 0,
                            "wtUom": vm.part.wtUom ? vm.part.wtUom : "",
                            "productCat": vm.part.catPath,
                            "checked": true,
                            "shipToDateReq": new Date().getTime()
                        }]
            };
            dataServices.addToCart(payload).then(function (response) {
                    $rootScope.$emit("gotCartCount");
                    vm.shwMsg = true;
                    vm.action = 'cart';
                    vm.cartStatus = 'Pass';
                    vm.cartQty = 1;
                   // $rootScope.$emit("displayAddToCartFrmEmergency");
            }, function (error) {
                vm.shwMsg = true;
                vm.action = 'cart';
                vm.cartStatus = 'Fail';
                vm.cartQty = 1;
            });
        }
        else if(type==='EO'){
            PricingService.qtyRequired = vm.cartQty;
            PricingService.partNumber = vm.part.partNumber;
            PricingService.packageQty = vm.part.packageQty;
            PricingService.partName = vm.part.partDesc;
            PricingService.orderType = 'EMG';
            PricingService.partCats =  '';
            PricingService.catPath =  vm.part.catPath; //'CVB.HA.DA';
            PricingService.orderTypeLbl = 'EMERGENCY ORDER';
            PricingService.userType = 'RTB';
            PricingService.uom = vm.part.packageType ? vm.part.packageType : "";
            PricingService.thumbnail = vm.part.tnImage ? vm.part.tnImage : "",
            PricingService.wt = vm.part.wt ? vm.part.wt : 0;
            PricingService.wtUom = vm.part.wtUom ? vm.part.wtUom : "";
            //let urlModalInstance = null;
            var modalInstance = $uibModal.open({
                templateUrl: 'app/pricing/availability/orderTypeSelection.html',
                size: 'lg',
                controller: 'orderTypeSelectionController',
                controllerAs: 'orderTypeCtrl',
                windowClass: 'ordertype-popup',
            resolve: {
                mode: function() {
                return 'normal';
                }
            }
            });

            ///let urlModalInstance = $timeout(OrderlistModalFactory.open("md", "app/pricing/availability/orderTypeSelection.html", "orderTypeSelectionController", "orderTypeCtrl", "availabilityOverlay", true, "edit"), 2000);
            modalInstance.result.then(function (operation) {
                //console.log("operation", operation);
                if(operation !== 'delete'){
                    vm.shwMsg = true;
                }
                vm.action = 'cart';
                vm.cartStatus = operation;
            
            }, function () {
                
            });
            //urlModalInstance = $timeout(OrderlistModalFactory.open("md", "app/pricing/availability/orderTypeSelection.html", "orderTypeSelectionController", "orderTypeCtrl", "availabilityOverlay", true), 2000);

        }
        vm.shwOrderTypes = false;
        vm.shwCartTxt = true;
        vm.shwCartQty = false;
    }
    addListClick() {
        let vm = this;
        let { $rootScope } = vm.DI();
        vm.resetFlags();
        //Check logged in status
        //if(sessionStorage["currentUserProfileData"] !== undefined){
        // var userInfo = JSON.parse(sessionStorage["currentUserProfileData"]);
        // debugger;
        // if(userInfo.userType) {
        //     vm.userName = userInfo.username;
        //     vm.userType = 'RTB';//userInfo.userType;
        // }
        //}
        //else {
        //  $rootScope.$emit("openSignin");
        //}

        if (localStorage["currentUserProfileData"] !== undefined) {
            vm.change("shwLists");
        }
        else {
            this.shwMyLists = false;
            vm.addToSessionList();
        }

    }
    change(msg) {
        let vm = this;
        let {OrderListService } = vm.DI();
        this.shwRTBMsg = false;
        if (msg === "shwQty") {
            this.shwOrdrTxt = false;
            this.shwQty = true;
            this.shwMsg = false;
            this.shwMyLists = false;            
            this.shwCartTxt = true;
            this.shwCartQty = false;
            this.shwOrderTypes = false;
            if (this.qty > 0) {
                this.qtyNotAllowed = false;
            } else {
                this.qtyNotAllowed = true;
            }
        } else if (msg === "shwMsg") {
            this.shwQty = false;
            this.shwMsg = true;
            this.shwOrdrTxt = true;
            let oldItem = false;
            this.shwMyLists = false;
            this.action='list';
            OrderListService.orderList.map(function (item) {
                if (item.partNumber === vm.part.partNumber) {
                    item.quantity = Number(item.quantity) + Number(vm.qty);
                    oldItem = true;
                }
            })
            if (oldItem === false) {
                let temp = {
                    id: OrderListService.orderList ? OrderListService.orderList.length : 0,
                    partNumber: vm.part.partNumber,
                    quantity: vm.qty,
                    partCategory: vm.part.categories[0].name,
                    partName: vm.part.partDesc,
                    partImageUrl: vm.part.imageUrl
                };
                OrderListService.orderList.push(temp);
            }

            sessionStorage.orderList = angular.toJson(OrderListService.orderList);
        } else if (msg == "shwLists") {

            // if (this.shwMyLists === true) {
            //     this.shwMyLists = false;
            // }
            // else {
                this.shwMyLists = true;
                this.shwMsg = false;
                this.lists = OrderListService.fetchMyLists();
            //}
        } else {


        }
    }

    addToSessionList() {
        let vm = this;
        let { $rootScope, OrderListService, dataServices } = vm.DI();
        vm.shwOrdrTxt = false;
        vm.shwQty = false;
        vm.part.showTempList = true;
        let tempItemList = [];
        if (sessionStorage["tempItemList"] !== undefined)
            tempItemList = JSON.parse(sessionStorage["tempItemList"]);

        let item = {
            partNo: vm.part.partNumber,
            quantity: 1,
            partName: vm.part.partDesc,
            uom: vm.part.packageType ? vm.part.packageType : "",
            thumbnail: vm.part.tnImage ? vm.part.tnImage : "",
            wt: vm.part.wt ? vm.part.wt : 0,
            wtUom: vm.part.wtUom ? vm.part.wtUom : "",
            catPath: vm.part.catPath     
        };
        tempItemList.push(item);
        $rootScope.$broadcast("addedTempItem", tempItemList);
        sessionStorage["tempItemList"] = JSON.stringify(tempItemList);
    }

    createNewList() {
        let vm = this;
        let { $uibModal, $location } = vm.DI();
       
        let modalInstance = $uibModal.open({
            templateUrl: 'app/mylist/createlist/createlist.html',
            controller: 'CreatenewlistController',
            controllerAs: 'newlist',
            size: 'md',
            windowClass: 'my-modal-popup',
            resolve: {
                tempItemList: function(){
                    //return payload;
                    return null;
                }
            }
        });
        modalInstance.result.then(function (resData) {
            vm.addToList(resData.code, resData.listName);
        }, function () {
        });
    }
    addToList(id, name) {
        let vm = this;
        let { OrderListService, dataServices } = vm.DI();
        this.shwQty = false;
        this.shwMsg = true;
        this.shwRTBMsg = false;
        this.shwOrdrTxt = true;
        this.shwMyLists = false;
        this.selectedList = name;
        this.selectedId = id;
        this.action='list';
        let temp = [{
            partNo: vm.part.partNumber,
            quantity: vm.part.packageQty,
            packageQty: vm.part.packageQty,
            partName: vm.part.partDesc,
            uom: vm.part.packageType ? vm.part.packageType : "",
            thumbnail: vm.part.tnImage ? vm.part.tnImage : "",
            wt: vm.part.wt ? vm.part.wt : 0,
            wtUom: vm.part.wtUom ? vm.part.wtUom : "",
            catPath: vm.part.catPath            
       }];

       //OrderListService.addItemToOrderList(temp,id);
        // let vm = this;
        // let {dataServices} = vm.DI();
        // let listNumber = listId;
        dataServices.addItemToOrderList(temp, id)
            .then(function (response) {
                //{code: "[524]", mess: "Created"}
                if (response.mess === "Created") {
                    OrderListService.incrementListCount(id);
                    vm.newLineId = response.code.replace(/[\[\]]/g, '');
                }
            }, function (error) {

                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });

    }
    removeItem() {
        let vm = this;
        let { OrderListService, dataServices } = vm.DI();

        this.shwMsg = false;
        dataServices.deleteItemFromOrderlist(vm.newLineId).then(function (response) {
            if (response.mess === "Accepted") {
                OrderListService.decrementtListCount(vm.selectedId);
            }


        }, function (error) {

        });
        //vm.newLineId
        // OrderListService.orderList.forEach(function (item, index, object) {
        //     if (item.partNumber == vm.part.partNumber) {
        //         object.splice(index, 1)
        //     }
        // })
        // OrderListService.orderList.splice(index,1);
        //sessionStorage.orderList = angular.toJson(OrderListService.orderList);


    }
    sort(sortObj) {
        let vm = this;
        let { SearchBarService } = vm.DI();
        if (sortObj = "")
            SearchBarService.sort = {
                sortType: sortObj.Name,
                sortName: sortObj.Type
            }
    }

    toggleSpecs() {
       let vm=this;

        if (this.toggle) {
            this.specToggleName = vm.expand;
            this.specLimit = 5;
        } else {
            this.specToggleName = vm.collapse;
            this.specLimit = this.part.attrList.length;
        }
        this.toggle = !this.toggle;
    }

    getImageUrl(part) {
        let vm=this;
        let retUrl = vm.protocol + "://placehold.it/160x160/dbdbdb/0099CC/?text=NO+IMAGE";
        switch (part.categories[2].name) {
            case 'Flanges':
                retUrl = "/assets/images/flange.png";
                break;
            case 'Universal Joints':
                retUrl = "/assets/images/u-joint.jpg";
                break;
            case 'Flange Yoke':
                retUrl = "/assets/images/flange_yoke.jpg";
                break;
            case 'Flange Yokes':
                retUrl = "/assets/images/flange_yoke.jpg";
                angular.noop();
                break;
            case 'Ring and Pinions':
                retUrl = "/assets/images/rangeNpinion.jpg";
                break;
            default:
                angular.noop();
                break;
        };
        return retUrl;
    }

    showBack() {
        this.BreadCrumbService.searchToResults = true;
    }

    gotoPart(part) {
        let vm = this;
        let { $stateParams, $state } = vm.DI();
        let ic = null;
        let cp = 0;
        if (part.interChngPart) {
            if (part.interchanges[0].partNumber) {
                ic = part.interchanges[0].partNumber;
            }
            else {
                cp = 1;
            }
        }
        let paramObj = { "id": part.id, "type": "id", "ic": ic, "cp": cp };
        angular.extend(paramObj, $stateParams);
        angular.forEach(paramObj, (value, key, obj) => {
            angular.isUndefined(value) ? delete obj[key] : angular.noop();
        });
        return $state.href("part", paramObj);
    }
    /*nospecial() {
        deb
        var regex = new RegExp("^[0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    }*/
    saveList() {
        let vm = this;
        let { $rootScope, $scope } = vm.DI();
        sessionStorage["saveList"] = "true";
        $scope.$emit("openSignin");
    }
    shareList() {
        let vm = this;
        let { $rootScope, $scope } = vm.DI();
        sessionStorage["shareList"] = "true";
        $scope.$emit("openSignin");
    }

    
 }

SearchResultDirectiveController.$inject = ['$log', '$timeout', '$stateParams', '$state', '$document', 'BreadCrumbService', 'SearchBarService', 'OrderListService', 'appInfoService', '$uibModal', '$location', '$rootScope', 'dataServices', 'OrderlistModalFactory', 'PricingService', 'ProfileService', 'authenticationService', '$sce', '$scope','$translate', '$filter']
