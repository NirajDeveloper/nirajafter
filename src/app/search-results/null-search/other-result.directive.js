/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function OtherResultDirective(){

	let directive = {
		restrict : 'E',
		templateUrl : 'app/search-results/null-search/other-result.html',
		controller : OtherResultDirectiveController,
		controllerAs : 'otherresult',
		bindToContrller : true
	};
	return directive;
}

class  OtherResultDirectiveController{
	constructor(){
	}	
}