/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function routerConfig($stateProvider) {
    $stateProvider
        .state('searchResults', {
            url: '/search?str&cat1&cat2&cat3&from&size&mode&filters&filterObject&y&mk&md&sort&ics',
            parent: 'aftermarket',
            templateUrl: 'app/search-results/search-results.html',
            controller: 'SearchResultsController',
            controllerAs: 'srchRes',
            resolve: {}
        });
}

routerConfig.$inject = ["$stateProvider"];