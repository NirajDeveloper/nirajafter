/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function SelectedFilter() {
    let directive = {
        restrict: 'E',
        templateUrl: 'app/search-results/selected-filter/selected-filter.html',
        scope: {},
        controller: SelectedFilterController,
        controllerAs: 'vm',
        bindToController: true
    };
    return directive;
}

class SelectedFilterController {
    constructor($log, $state, $location, $q, $stateParams, SearchBarService, dataServices, $scope, $rootScope, $timeout) {
        let vm = this;

        vm.DI = () => ({ $log, $state, $q, $location, $stateParams, SearchBarService, dataServices, $scope, $rootScope });

        vm.selectedFilter = angular.fromJson($stateParams.filterObject);
    }

    changeInService(deleteObj, deleteValue){
        let vm = this;
        let { SearchBarService, $stateParams, $state, $rootScope } = vm.DI();
        vm.listPreviousFilter = SearchBarService.listPreviousFilter;
        for(let obj of vm.listPreviousFilter){
            if(obj !== null && obj !== undefined && obj.name === deleteObj.name){
                for(let x of obj.buckets){
                    if(x !== null && x !== undefined && x.key === deleteValue){
                        x.select = false;
                        SearchBarService.listPreviousFilter = vm.listPreviousFilter;
                        let paramObj = {'filterObject': angular.toJson(vm.selectedFilter)};
                        $state.go('searchResults', paramObj);
                    }
                }
            }
        }
    }

    delete(deleteObj, deleteValue){
        let vm = this;
        let { SearchBarService, $stateParams, $state, $rootScope } = vm.DI();
        for(let obj of vm.selectedFilter){
            if(obj !== null && obj !== undefined && deleteObj.name === obj.name){
                for(let x of obj.values){
                    if(x === deleteValue){
                        obj.values.splice(obj.values.indexOf(x), 1);
                        vm.changeInService(deleteObj, deleteValue);
                    }
                }
            }
        }
    }
}

SelectedFilterController.$inject=['$log', '$state', '$location', '$q', '$stateParams', 'SearchBarService', 'dataServices', '$scope', '$rootScope', '$timeout'];