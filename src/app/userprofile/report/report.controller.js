/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// Author: Manjesh Kumar
export class ReportController {
  constructor($uibModal, $timeout, dataServices) {
    'ngInject';
    let vm = this;
    vm.DI = () => ({$uibModal , $timeout, dataServices});
    vm.searchByValue = {
      custUsrTxt: undefined,
      shopUsrTxt: undefined
    };
    vm.custHeaderList = [];
    vm.shopHeaderList = [];
    vm.paginationTbl = {
      danaUsrTbl:{
        currentPage: 1,
        itemsPerPage: 10,
        maxSize: 15
      },
      shopUsrTbl:{
        currentPage: 1,
        itemsPerPage: 10,
        maxSize: 15
      }
    };

    vm.currentTab = "CUSTOMERUSER";
  }

  pageChangedDanaTbl(){
    //let vm = this;
  }

  pageChangedShopTbl(){
    //let vm = this;
  }

  onChangeOfDisplayQty(tab){
    let vm = this;
    if(tab == 'SHOPUSER'){
        vm.paginationTbl.shopUsrTbl.currentPage = 1;
    }else{
      vm.paginationTbl.danaUsrTbl.currentPage = 1;
    }
  }

  fetchAllUserProfilesForReport(userType){
    let vm = this;
    let {dataServices} = vm.DI();
    let userDetails = {};
    userDetails.userType = userType;
    dataServices.getAllUserProfilesForReport(userDetails).then(function (response) {
        if(response){
          if(userDetails.userType == 'SHOPUSER'){
            vm.shopUsersProfilesData = response;
            vm.paginationTbl.shopUsrTbl.totalItems = vm.shopUsersProfilesData ? vm.shopUsersProfilesData.userdata.length : 0;
            vm.paginationTbl.shopUsrTbl.numOfPage = Math.ceil(vm.paginationTbl.shopUsrTbl.totalItems / vm.paginationTbl.shopUsrTbl.itemsPerPage);
          }else{
            vm.danaCustomerProfilesData = response;
            vm.paginationTbl.danaUsrTbl.totalItems = vm.danaCustomerProfilesData ? vm.danaCustomerProfilesData.userdata.length : 0;
            vm.paginationTbl.danaUsrTbl.numOfPage = Math.ceil(vm.paginationTbl.shopUsrTbl.totalItems / vm.paginationTbl.danaUsrTbl.itemsPerPage);
          }
        }else{
          // do something in case of null response
        }
    }, function () {

    });
  }
  onTabSwitch (tab) {
    let vm = this;
    let { $timeout } = vm.DI();
    $timeout(function() {
      vm.currentTab = tab;
      // here you can call api on the basis of current tab
      vm.fetchAllUserProfilesForReport(tab);
    });
  }

  downloadReport(userType, fileName){
    let vm = this;
    let {dataServices} = vm.DI();
    let userDetails = {};
    userDetails.userType = userType;
    let fileNameToDownlod = fileName;
    dataServices.downloadReportByUserType(userDetails).then(function (data) {
        var anchor = angular.element('<a/>');
        anchor.css({display: 'none'}); // Make sure it's not visible
        angular.element(document.body).append(anchor); // Attach to document
        anchor.attr({
            href: 'data:attachment/csv;charset=utf-8,' + encodeURIComponent(data),
            target: '_blank',
            download: fileNameToDownlod
        })[0].click();
        anchor.remove(); // Clean it up afterwards
    }, function () {

    });
  }
}
