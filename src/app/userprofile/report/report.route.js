/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

export function routeConfig($stateProvider){
	'ngInject';
	$stateProvider
	.state('report',{
		url: '/report',
		parent: 'aftermarket',
		templateUrl: 'app/userprofile/report/report.html',
		controller: 'ReportController',
		controllerAs: 'report',
        authenticate: true
	});
}