/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// Author: Manjesh Kumar
export class ProfileController {
  constructor($log, $scope, $filter, $uibModal, $timeout, $location, $window, authenticationService, dataServices) {
    'ngInject';
    let vm = this;
    vm.DI = () => ({ $log, $uibModal , $timeout, $window, authenticationService, dataServices});
    if(localStorage.currentUserProfileData){
        vm.profileDetails = angular.copy(authenticationService.currentUserProfileData);
    }else{
        $state.go('home');
    }
    vm.changePhoneFormat(true, 'mobile');
    vm.changePhoneFormat(true, 'phoneno');
    vm.pswrdDetails = {};
    vm.oldEmail = vm.profileDetails.emailId;
    vm.pswrdDetails.userId = vm.profileDetails.userId;
    vm.custUserDetails = {};
    vm.currentTab = "My Profile1";
    vm.responseMsg = "";
    vm.isFormSubmitted = false;
    vm.showResMsg = false;
    vm.countryList = [];
    vm.stateList = [];
    vm.isProfileChanged = false;
    vm.isEmailAvailChecked = true;
    vm.isEmailChanged = false;
    vm.isEmailExist = false;
    vm.init();
  }

  init(){
    let vm = this;
    vm.getCountryList();
    vm.getStateList('US');
  }

  onTabSwitch (tab) {
    let vm = this;
    let { $timeout } = vm.DI();
    $timeout(function() {
      //console.log('tab Switched : '+tab);
      vm.currentTab = tab;
      vm.isFormSubmitted = false;
    });
  };

  updateUserProfile(profileDetails, isNotReadyToSubmit){
    let vm = this;
    let { $timeout,$window, dataServices, authenticationService } = vm.DI();
    vm.profileFrmBtnClicked = true;
    if(isNotReadyToSubmit){
      // no need to call user profile update api
    }else{
      vm.resetErrorHandler();
      // remove below code if service bug is fixed, customerNumbers should come in getUserProfile api response
      if(profileDetails.roles[0] == 'PZV_SHOP_USER' || profileDetails.roles[0] == 'PZV_CSR' ){
        profileDetails.customerNumbers =[];
      }else{
        profileDetails.customerNumbers = [authenticationService.activeCustId];
      }
      // call get change Password API and after getting success response close pop-up and open login screen
      dataServices.createOrUpdateUser(profileDetails).then(function (response) {
          if(response && response.status == 'SUCCESS'){
            authenticationService.currentUserProfileData.firstName = profileDetails.firstName;
            authenticationService.setCurrentUserProfileData(profileDetails);
            vm.oldEmail = vm.profileDetails.emailId;
            vm.responseMsg = "PROFILE_UPDATED_SUCCESSFULLY";
            vm.showResMsg = true;
            vm.isProfileChanged = true;
            $window.scrollTo(0, 0);
          }else if(response && response.status == 'ERROR'){
            vm.responseMsg = response.errorCode;
            vm.showResMsg = true;
            $window.scrollTo(0, 0);
          }else{
            vm.responseMsg = "INTERNAL_SERVER_ERROR";
            vm.showResMsg = true;
            $window.scrollTo(0, 0);
          }
      }, function (error) {
        vm.responseMsg = "INTERNAL_SERVER_ERROR";
        vm.showResMsg = true;
        $window.scrollTo(0, 0);
      });
    }
  }

  submitChangedPswrd(pswrdDetails, isNotReadyToSubmit){
    let vm = this;
    let { $timeout,$window, dataServices } = vm.DI();
    vm.pswrdFrmBtnClicked = true;
    if(isNotReadyToSubmit){
      // no need to call user password change api
    }else{
      vm.resetErrorHandler();
      // call get change Password API and after getting success response close pop-up and open login screen
      dataServices.changePswrdForCp(pswrdDetails).then(function (response) {
          if(response && response.status == 'SUCCESS'){
            vm.responseMsg = "PSWRD_CHANGED_SUCCESSFULLY";
            vm.showResMsg = true;
            //vm.pswrdDetails.oldPwd = "";
            //vm.pswrdDetails.newPwd = "";
            //vm.pswrdDetails.confirmPwd = "";
            $window.scrollTo(0, 0);
          }else if(response && response.status == 'ERROR'){
            vm.responseMsg = response.errorCode;
            vm.showResMsg = true;
            $window.scrollTo(0, 0);
          }else{
            vm.responseMsg = 'INTERNAL_SERVER_ERROR';
            vm.showResMsg = true;
            $window.scrollTo(0, 0);
          }
      }, function (error) {
        vm.responseMsg = "INTERNAL_SERVER_ERROR";
        vm.showResMsg = true;
        $window.scrollTo(0, 0);
      });
    }
  }

  createCustomerUser(custUserDetails){
    let vm = this;
    let { $timeout,$window } = vm.DI();
    vm.resetErrorHandler();
    $window.scrollTo(0, 0);
  }

  resetErrorHandler(){
    let vm = this;
    vm.responseMsg = "";
    vm.isFormSubmitted = true;
    vm.showResMsg = false;
  }

  getCountryList(){
      // api to populate country list
    let vm = this;
    let {$scope, $rootScope, dataServices, $location, $state} = vm.DI();
    dataServices.fetchCountryList().then(function (response) {
        if (response) { 
            vm.countryList = response;
        } else {
            vm.countryList = []; 
           // console.log("NO country");
        }
    }, function (error) {
        vm.countryList = []; 
        //console.log("server error in fetching country list");
    });
  }

  getStateList(country){
      if(country){
          let vm = this;
          let {$scope, $rootScope, dataServices, $location, $state} = vm.DI();
          dataServices.fetchStateListByCountry(country).then(function (response) {
              if (response) { 
                  vm.stateList = response;
              } else {
                  vm.stateList = []; 
                  //console.log("NO state");
              }
          }, function (error) {
              vm.stateList = []; 
              //console.log("NO state");
          });
      }else{
        // not needed
      }
  }

  changePhoneFormat(isValid, fieldName){
    let vm = this;
    let {$scope, $rootScope, dataServices, $location, $state} = vm.DI();
    if(isValid && vm.profileDetails[fieldName] && vm.profileDetails[fieldName].length == 10){
      vm.profileDetails[fieldName] = vm.profileDetails[fieldName].slice(0,3) + '-' + vm.profileDetails[fieldName].slice(3,6)+ '-' + vm.profileDetails[fieldName].slice(6);
    }else{
      // no need to format
    }
  }

  onChangeOfEmailId(){
        //console.log("data changing");
        let vm = this;
        if(angular.equals(vm.oldEmail, vm.profileDetails.emailId)){
          vm.isEmailChanged = false;
          vm.isEmailAvailChecked = true;
          vm.isEmailExist = false;
        }else{
          vm.isEmailChanged = true;
          vm.isEmailAvailChecked = false;
          vm.isEmailExist = false;
        }
    }

  checkEmailIdAvailabilty(emailId){
        let vm = this;
        if(emailId && !angular.equals(vm.oldEmail, emailId)){
            // call check availabilty service
            let {$scope, $rootScope, dataServices, $location, $state} = vm.DI();
            vm.isEmailChanged = true;
            dataServices.checkEmailExistance(emailId).then(function (response) {
                if (response && response.status == 'SUCCESS') { 
                    vm.isEmailAvailChecked = true;
                    vm.isEmailExist = false;
                } else {
                    vm.isEmailAvailChecked = true;
                    vm.isEmailExist = true;
                    //console.log("User already exits.");
                }
            }, function (error) {
                vm.isEmailAvailChecked = false;
                vm.isEmailExist = false;
                //console.log("some service error while calling 'checkUserExistance'");
            });
        }else{
          vm.isEmailChanged = false;
          vm.isEmailAvailChecked = true;
          vm.isEmailExist = false;
        }
    }
}
