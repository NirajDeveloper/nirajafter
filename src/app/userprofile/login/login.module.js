/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author : Shaifali Jaiswal*/


import { LoginController } from './login.controller';
//import { loginDirective } from './login.directive';
import { AuthInterceptor } from './AuthInterceptor';
import { Session } from './Session';
import { authenticationService } from './authentication.service';
import {PasswordAssistController} from './password-assistance/passwordAssistancePopUp.controller';
import {VerifyOtpController} from './verify-otp/verifyOtpPopUp.controller';
import {CreatePasswordController} from './create-password/createPasswordPopUp.controller';
import {PasswordChangedSuccessController} from './pswrd-changed-success/PasswordChangedSuccessPopUp.controller';
import {LoginExpiredController} from './login-expired/loginExpiredPopUp.controller';
import {ShipToSoldToController} from './ship-to-sold-to/shipToSoldToPopUp.controller';

angular.module('aftermarket.login', [])
    .controller('LoginController',LoginController)
		.controller('PasswordAssistController',PasswordAssistController)
		.controller('VerifyOtpController',VerifyOtpController)
		.controller('CreatePasswordController',CreatePasswordController)
		.controller('PasswordChangedSuccessController',PasswordChangedSuccessController)
		.controller('LoginExpiredController',LoginExpiredController)
		.controller('ShipToSoldToController',ShipToSoldToController)
     //.directive('loginDirective', loginDirective)
    .service('authenticationService',authenticationService)
    .service('AuthInterceptor',AuthInterceptor)
		.service('Session',Session)

    .constant('USER_ROLES', {
	all : '*',
	admin : 'admin',
	editor : 'editor',
	guest : 'guest'
}).constant('AUTH_EVENTS', {
	loginSuccess : 'auth-login-success',
	loginFailed : 'auth-login-failed',
	logoutSuccess : 'auth-logout-success',
	sessionTimeout : 'auth-session-timeout',
	notAuthenticated : 'auth-not-authenticated',
	notAuthorized : 'auth-not-authorized'
})/* Adding the auth interceptor here, to check every $http request*/
.config(function ($httpProvider) {
  $httpProvider.interceptors.push([
    '$injector',
    function ($injector) {
      return $injector.get('AuthInterceptor');
    }
  ]);
})

.run(function($rootScope, $state, USER_ROLES,authenticationService, AUTH_EVENTS) {
	
	//before each state change, check if the user is logged in
	//and authorized to move onto the next state
	$rootScope.$on('$stateChangeStart', function (event, next) {
	    var authorizedRoles = [USER_ROLES.admin, USER_ROLES.editor];//next.data.authorizedRoles;
	    if (!authenticationService.isAuthorized(authorizedRoles)) {
	    //  event.preventDefault();
	      if (authenticationService.isAuthenticated()) {
	        // user is not allowed
	        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
	      } else {
	        // user is not logged in
	        $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
	      }
	    }
	  });
	
	/* To show current active state on menu */
	$rootScope.getClass = function(path) {
		if ($state.current.name == path) {
			return "active";
		} else {
			return "";
		}
	}
	
	$rootScope.logout = function(){
		authenticationService.logout();
	};
    })