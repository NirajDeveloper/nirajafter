/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class authenticationService {
    constructor($log, dataServices, $http, $rootScope, $window, $q, Session, AUTH_EVENTS, apiConfig) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            $log, dataServices, $http, $rootScope, $window, $q, Session, AUTH_EVENTS, apiConfig
        });
        vm.setUserFromSession();
        this.loginType = 'client';
        // This aatribute will store the current looged user info
        vm.currentUserProfileData = {};
        vm.associatedCustData = {};
        vm.userPermissionsList = {};
        vm.currentUserShortInfo = {};
        vm.shipToSoldToInfo = {};
    }

    checkUserStatus(userId) {
        let vm = this;
        let { $http, $q } = vm.DI();
        apiConfig.CHECK_USER_STATUS.setUrl(userId);
        return $q(function (resolve, reject) {
            $http(apiConfig.CHECK_USER_STATUS).then(function (response) {
                // DI.log.debug("response :", response.data.APIResponse);
                // start : hardcoding data since service is not ready
                response.data = {};
                response.data.APIResponse = {};
                response.data.APIResponse.status = "SUCCESS";
                // end : hardcoding data since service is not ready
                resolve(response.data.APIResponse);
            }, function (error) {
                console.log("error", error);
                reject(error);
            });
        });
    }

    login(userDtls) {
        let vm = this;
        let { $http, $q, apiConfig } = vm.DI();
        apiConfig.LOGIN_USER.setUrl(userDtls);
        return $q(function (resolve, reject) {
            $http(apiConfig.LOGIN_USER).then(function (response) {

                resolve(response.data);
            }, function (error) {
                console.log("error", error);
                reject(error);
            });
        });
    }

    setCurrentUserProfileData(userProfile) {
        let vm = this;
        let { $rootScope, AUTH_EVENTS } = vm.DI();
        vm.currentUserProfileData = userProfile; // need to be replaced with userProfile
        //console.log("set vm.currentUserProfileData : " + JSON.stringify(userProfile));
        $rootScope.currentUserProfileData = userProfile;
        localStorage.currentUserProfileData = JSON.stringify(userProfile);
        // $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
        // $cookieStore.put('globals', $rootScope.globals);
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
    }

    setCurrentUserShortInfo(userShortInfo) {
        let vm = this;
        let { $rootScope, AUTH_EVENTS } = vm.DI();
        vm.currentUserShortInfo = userShortInfo;
        localStorage.currentUserShortInfo = JSON.stringify(userShortInfo);
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
    }

    clearCurrentUserShortInfo() {
        let vm = this;
        let { $rootScope, AUTH_EVENTS } = vm.DI();
        vm.currentUserShortInfo = {};
        localStorage.removeItem('currentUserShortInfo');
    }

    setAssociatedCustIdToUser(associatedCustData) {
        let vm = this;
        let { $rootScope, AUTH_EVENTS } = vm.DI();
        vm.associatedCustData = associatedCustData;
        localStorage.associatedCustData = JSON.stringify(vm.associatedCustData);
        $rootScope.$broadcast("ASSOCIATED_CUSTOMER");
    }

    clearAssociatedCustIdToUser() {
        let vm = this;
        let { $rootScope, AUTH_EVENTS } = vm.DI();
        vm.associatedCustData = {};
        localStorage.removeItem('associatedCustData');
    }

    clearCurrentUserProfileData() {
        let vm = this;
        let { $rootScope, $http } = vm.DI();
        vm.currentUserProfileData = {};
        $rootScope.currentUserProfileData = {};
        localStorage.removeItem('currentUserProfileData');
        localStorage.removeItem('bearerToken');
        $http.defaults.headers.common.Authorization = "";
        // $cookieStore.remove('globals');
        // $http.defaults.headers.common.Authorization = 'Basic';
    }

    hasUserPermissionOf(permissionAskedFor){
        if(vm.userPermissionsList.find(function(permisnsAvailable) {return permisnsAvailable == permissionAskedFor;})){
            return true;
        }else{
            return false;
        }
    }

    setCurrentUserPermissions(permissionsRes){
        let vm = this;
        let { $rootScope, AUTH_EVENTS } = vm.DI();
        vm.userPermissionsList = permissionsRes;
        localStorage.userPermissionsList = JSON.stringify(vm.userPermissionsList);
        $rootScope.$emit("CURRENT_USER_PERMISSIONS");
        //$rootScope.$broadcast("ASSOCIATED_CUSTOMER");
    }

    clearCurrentUserPermissions(){
        let vm = this;
        let { $rootScope, AUTH_EVENTS } = vm.DI();
        vm.userPermissionsList = {};
        localStorage.removeItem('userPermissionsList');
    }

    setShipToSoldToInfo(customer, billTo, isEmitable){
        let vm = this;
        let { $rootScope, AUTH_EVENTS } = vm.DI();
        vm.shipToSoldToInfo.customer = customer;
        vm.shipToSoldToInfo.billTo = billTo;
        localStorage.shipToSoldToInfo = JSON.stringify(vm.shipToSoldToInfo);
        if(isEmitable){
            $rootScope.$emit("SHIP_TO_SOLD_TO_INFO");
        }
    }

    clearShipToSoldToInfo(){
        let vm = this;
        let { $rootScope, AUTH_EVENTS } = vm.DI();
        vm.shipToSoldToInfo = {};
        localStorage.removeItem('shipToSoldToInfo');
    }

    isUserAuthenticated() {
        let vm = this;
        if (vm.currentUserProfileData != null && vm.currentUserProfileData != undefined
            && !vm.isEmptyObj(vm.currentUserProfileData)) {
            return true;
        } else {
            return false;
        }
    }

    isEmptyObj(obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    };

    setCredentials(userDtls) {
        let vm = this;
        let { $rootScope } = vm.DI();
        $rootScope.currentUserProfileData = userDtls;
        localStorage.currentUserProfileData = JSON.stringify(userDtls);
    }

    clearCredentials() {
        let vm = this;
        let { $rootScope } = vm.DI();
        $rootScope.currentUserProfileData = {};
        localStorage.removeItem('currentUserProfileData');
    }

    setUserFromSession() {
        let vm = this,
            {
                $scope, $rootScope, $window, Session
            } = vm.DI();

        if (sessionStorage["userInfo"] !== undefined) {
            var userInfo = JSON.parse(sessionStorage["userInfo"]);
            vm._currentUser = userInfo;
        }
        else {
            vm._currentUser = "";
        }
    }
    isAuthenticated() {
        let vm = this,
            {
                $scope, $rootScope, $window, Session
            } = vm.DI();
        return !!Session.user;
    };

    isAuthorized(authorizedRoles) {
        let vm = this,
            {
                $scope, $rootScope, $window, Session
            } = vm.DI();
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }
        return (vm.isAuthenticated() &&
            authorizedRoles.indexOf(Session.userRole) !== -1);
    };

    logout() {
        let vm = this,
            {
                $scope, $rootScope, $window, Session, AUTH_EVENTS
            } = vm.DI();
        Session.destroy();
        $window.sessionStorage.removeItem("userInfo");
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
    }

    get currentUser() {
        return this._currentUser;
    }

    set currentUser(userName) {
        this._currentUser = userName;
    }
    setUser(userName) {
        let vm = this,
            {
                $scope, $rootScope, $window, Session
            } = vm.DI();
        vm._currentUser = userName;
    }
    SetCredentials(username, password) {
        let vm = this,
            {
                $scope, $rootScope
            } = vm.DI();

        $rootScope.globals = {
            currentUser: {
                username: username
            }
        };

    };

    isShopUser() {
        let vm = this,
            {
                $scope, $rootScope
            } = vm.DI();

        if (vm.currentUserProfileData.userType === "SHOPUSER") {
            return true;
        }
        else {
            return false;
        }
    };
    /*common calls */
}
