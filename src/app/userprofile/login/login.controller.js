/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author : Manjesh Kumar*/
export class LoginController {
    constructor(AftermarketConstants, $uibModal, $location, $rootScope, $scope, $log, $timeout,$q, $uibModalInstance,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            AftermarketConstants, $uibModal, $log, $scope, $rootScope, $timeout,$q, $uibModalInstance, $location,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices
        });
        this.userDetails = {};
        this.userDetails.tenant = AftermarketConstants.appSettings.tenant;;

        vm.isFormSubmitted = false;
        vm.dataLoading = false;
        vm.logInErrMsg = "";
        vm.isLogInFailed = false;
        if (localStorage.afterMarketRemME && localStorage.afterMarketRemME != '' && localStorage.afterMarketRemME != 'false') {
            vm.isRemembered = true;
            vm.userDetails.user = localStorage.afterMarketUsr == 'undefined' ? '' : localStorage.afterMarketUsr;
            vm.userDetails.password = localStorage.afterMarketPass == 'undefined' ? '' : localStorage.afterMarketPass;;
        } else {
            vm.isRemembered = false;
            vm.userDetails.user = '';
            vm.userDetails.password = '';
        }
        // console.log("currentUserProfileData from login control : "+JSON.stringify(vm.currentUserProfileData));
    }
    // Start: new implementation
    // Login Form submitted
    submitLoginDetails(isValidForm, userDtls){
        let vm = this,
        {
            $scope, $window, authenticationService, dataServices, $rootScope, $location, $q
        } = vm.DI();
        vm.isFormSubmitted = true;
        if(!isValidForm){
            return;
            // do not call api unless form is valid
        }else{
            vm.isLogInFailed = false;
            vm.dataLoading = true;
            if (vm.isRemembered) {
                // save username and password
                localStorage.afterMarketUsr = vm.userDetails.user;
                localStorage.afterMarketPass = vm.userDetails.password;
                localStorage.afterMarketRemME = vm.isRemembered;
            } else {
                localStorage.afterMarketUsr = '';
                localStorage.afterMarketPass = '';
                localStorage.afterMarketRemME = false;
            }
            authenticationService.login(vm.userDetails).then(function (loginRes) {
                if (loginRes && loginRes.status == 'SUCCESS') {
                    let bearerToken = "Bearer " + loginRes.message; 
                    localStorage.bearerToken = bearerToken.toString();
                    let orderType = "EO";
                    localStorage.checkedOutCartType = orderType.toString();
                    if(loginRes.customer && loginRes.customer.customerNumber && loginRes.customer.customerNumber.trim().length > 0){
                        vm.userDetails.customerId = loginRes.customer.customerNumber;
                        vm.userDetails.customerName = loginRes.customer.customerName;
                        vm.userDetails.dealerCode = loginRes.customer.dealerCode;
                    }
                    let permissionsObj = {};
                    //convert array to json obj
                    if(loginRes.permissions){
                        for(let i=0; i < loginRes.permissions.length; i++){
                            permissionsObj[loginRes.permissions[i]] = true;
                        }
                    }
                    vm.userShortInfo = {};
                    vm.userShortInfo.firstName = loginRes.firstName;
                    vm.userShortInfo.lastName = loginRes.lastName;
                    vm.userShortInfo.roles = loginRes.roles;
                    vm.isLogInFailed = false;
                    vm.dataLoading = false;
                    authenticationService.setCurrentUserPermissions(permissionsObj);
                    authenticationService.setShipToSoldToInfo(loginRes.customer, loginRes.billTo, false);
                    $rootScope.$emit("CURRENT_USER_PERMISSIONS"); // catch this event where it is required
                    $rootScope.$emit("auth-login-success");
                    $rootScope.$emit("showWhereToBuyBtn");
                    vm.cancel('logged-in'); // close pop-up before navigating anywhere
                } else {
                    // set error message here
                    if(loginRes && loginRes.status == 'ERROR'){
                        vm.logInErrMsg = loginRes.errorCode;
                        
                    }else{
                        vm.logInErrMsg = 'INTERNAL_SERVER_ERROR';
                    }
                    $window.scrollTo(0, 0);
                    vm.isLogInFailed = true;
                    vm.dataLoading = false;
                }
            }, function (error) {
                vm.logInErrMsg = 'INTERNAL_SERVER_ERROR';
                vm.isLogInFailed = true;
                vm.dataLoading = false;
                $window.scrollTo(0, 0);
            });
        }
    }

    goBackToRegistrationPopUp(){
        let vm = this;
        vm.cancel('open-register');
    }

    forgotPassword(){
        let vm = this;
        vm.cancel("forgot-password");
    }
    // End: new implementation
    cancel(action) {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        let data  = {};
        data.action = action;
        data.userId = vm.userDetails.user;
        data.tenant = vm.userDetails.tenant;
        data.customerId = vm.userDetails.customerId;
        data.customerName = vm.userDetails.customerName;
        data.dealerCode = vm.userDetails.dealerCode;
        data.userShortInfo = vm.userShortInfo;
        $uibModalInstance.close(data);

    };

    onChangeRememberMe(){
        let vm = this;
        if (vm.isRemembered) {
            // save username and password
            localStorage.afterMarketUsr = vm.userDetails.user;
            localStorage.afterMarketPass = vm.userDetails.password;
            localStorage.afterMarketRemME = vm.isRemembered;
        } else {
            localStorage.afterMarketUsr = '';
            localStorage.afterMarketPass = '';
            localStorage.afterMarketRemME = false;
        }
    }
}