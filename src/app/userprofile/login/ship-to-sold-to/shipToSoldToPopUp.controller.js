/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author : Manjesh Kumar*/
export class ShipToSoldToController {
    constructor(AftermarketConstants, $uibModal, $location, $rootScope, $scope, $log, $timeout, $uibModalInstance, $window,authenticationService, dataServices, shipToBillToInfo) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            $uibModal, $log, $scope, $rootScope, $timeout, $uibModalInstance, $location ,$window, authenticationService,dataServices, shipToBillToInfo
        });;
        vm.shipToBillToInfo = shipToBillToInfo;
        vm.justLoggedIn = shipToBillToInfo.justLoggedIn;
        //console.log("shipToBillToInfo : "+JSON.stringify(shipToBillToInfo));
    }

    cancel() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close(vm.justLoggedIn);
    };

    getTokenForSoldToCustomer(selectedBillTo){
        let vm = this,
        {
            $scope, $window, authenticationService, dataServices, $rootScope, $location
        } = vm.DI();
        vm.selectedBillTo = selectedBillTo;
        dataServices.fetchNewTokenForSoldToCustomer(selectedBillTo.customerNumber).then(function (response) {
            if (response && response.status == "SUCCESS") {
                let bearerToken = "Bearer " + response.message; 
                localStorage.bearerToken = bearerToken.toString();
                authenticationService.shipToSoldToInfo.selectedSoldTo = vm.selectedBillTo;
                localStorage.shipToSoldToInfo = JSON.stringify(authenticationService.shipToSoldToInfo);
                $rootScope.$emit("SHIP_TO_SOLD_TO_INFO");
                vm.cancel();
            } else {
                // display error in case of status failure
                //console.log("got failure response");
            }
        }, function (error) {
            // display error in case of status failure
            //console.log("some server error");
        });
    }
}