/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author : Manjesh Kumar*/
export class CreatePasswordController {
    constructor(AftermarketConstants, $uibModal, $location, $rootScope, $scope, $log, $timeout, $uibModalInstance,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices, loginId) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            $uibModal, $log, $scope, $rootScope, $timeout, $uibModalInstance, $location,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices
        });

        vm.resetPswrdError = false;
        vm.errorMsg = "Server Error";
        vm.pswrdDetails = {};
        vm.pswrdDetails.userId = loginId; // get it from isolate scope and assign here
    }

    cancel(action) {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        let data = {};
        data.action = action;
        $uibModalInstance.close(data);
    };

    resetPassword(pswrdDetails){
       let vm = this, { dataServices } = vm.DI();
       vm.resetPswrdError = false;
       // call get change Password API and after getting success response close pop-up and open login screen
       dataServices.resetPswrdForFp(pswrdDetails).then(function (response) {
           if(response && response.status == 'SUCCESS'){
            // if(true){
               vm.cancel('show-success-pop-up'); // close pop-up before navigating anywhere
           }else{
               vm.errorMsg = response.message;
               vm.resetPswrdError = true;
           }
        }, function (error) {
            vm.resetPswrdError = true;
        });
       
    }
}