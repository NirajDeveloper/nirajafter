/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author : Manjesh Kumar*/
export class PasswordAssistController {
    constructor(AftermarketConstants, $uibModal, $location, $rootScope, $scope, $log, $timeout, $uibModalInstance,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            $uibModal, $log, $scope, $rootScope, $timeout, $uibModalInstance, $location,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices
        });

        vm.userDetails = {};
        vm.loginId = "";
        vm.otpError = false;
        vm.errorMsg = "Server Error";
        vm.dataLoading = false;
    }


    cancel(action) {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        let data = {};
        data.action = action;
        data.loginId = vm.loginId;
        data.emailId = vm.userDetails.emailId;
        $uibModalInstance.close(data);
    };

    getOTP(emailId){
       let vm = this, { dataServices } = vm.DI();
       vm.otpError = false;
       vm.dataLoading = true;
       // call get otp api and after getting success response close pop-up and open verify OTP pop-up
       dataServices.getOtpForFp(emailId).then(function (otpRes) {
           if(otpRes && otpRes.status == 'SUCCESS'){ 
                vm.dataLoading = false;
                vm.loginId = otpRes.message;
                vm.cancel('verify-otp'); // close pop-up before navigating anywhere
           }else if(otpRes && otpRes.status == 'ERROR'){
                vm.errorMsg = otpRes.errorCode;
                vm.otpError = true;
                vm.dataLoading = false;
           }else{
                vm.errorMsg = "INTERNAL_SERVER_ERROR";
                vm.otpError = true;
                vm.dataLoading = false;
           }
        }, function (error) {
            vm.errorMsg = "INTERNAL_SERVER_ERROR";
            vm.otpError = true;
            vm.dataLoading = false;
        });
    }
}