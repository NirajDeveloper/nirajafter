/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class AuthInterceptor {

  /*@ngInject*/
  constructor($rootScope, $q, $location , Session, AUTH_EVENTS) {
  return {
    responseError : function(response) {
    //   $rootScope.$broadcast({
    //     401 : AUTH_EVENTS.notAuthenticated,
    //     403 : AUTH_EVENTS.notAuthorized,
    //     419 : AUTH_EVENTS.sessionTimeout,
    //     440 : AUTH_EVENTS.sessionTimeout
    //   }[response.status], response);
    //  // return $q.reject(response);
      if (response.data && response.data.error == 'invalid_token') {
            //console.log("session expired");
            //console.log("from interceptor : "+ response.data);
           // console.log("session expired");
            if(localStorage.currentUserProfileData){
                $rootScope.$emit("logout");
            }
           // $state.go("home");
            //$rootScope.$emit("logout");
            //$location.path("/home"); // display error mesaage "session expired, please login again to proceed further" and the display login overlay and dont forget to close any pop-up or loader
          return $q.reject(response);
      }else if(response.data && typeof response.data == "string" && response.data.indexOf("<!DOCTYPE html>") !== -1){
          // response.data = {};
          // response.data.status = "FAILURE";
          return $q.reject(response);
      }else{
          return response;
      }
    }
  };
} 
}