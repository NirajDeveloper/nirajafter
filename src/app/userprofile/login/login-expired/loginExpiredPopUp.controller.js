/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author : Manjesh Kumar*/
export class LoginExpiredController {
    constructor(AftermarketConstants, $uibModal, $location, $rootScope, $scope, $log, $timeout, $uibModalInstance,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            $uibModal, $log, $scope, $rootScope, $timeout, $uibModalInstance, $location,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices
        });
        // debugger;
        // this.userDetails = userDetails;
        // vm.isFormSubmitted = false;
        // vm.logInErrMsg = "";
        // vm.isLogInFailed = false;
        $timeout(function(){
            vm.cancel();
        }, 3000);
    }

    cancel() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close();
    };
}