/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author : Manjesh Kumar*/
export class VerifyOtpController {
    constructor(AftermarketConstants, $uibModal, $location, $rootScope, $scope, $log, $timeout, $uibModalInstance,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices, userData) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            $uibModal, $log, $scope, $rootScope, $timeout, $uibModalInstance, $location,USER_ROLES,AUTH_EVENTS,$window,authenticationService,dataServices, userData
        });
        vm.otpVerifyError = false;
        vm.resetPswrdError = false;
        vm.errorMsg = "";
        vm.otpDetails = {};
        vm.pswrdDetails = {};
        vm.otpDetails.loginId = userData.loginId; // get it from the isolated scope of directive
        vm.otpDetails.otpType = "PASSWORD_RESET";
        vm.emailId = userData.emailId;  // get it from the isolated scope of directive
        vm.dataLoading = false;
        vm.isOtpResent = false;
    }

    cancel(action) {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        let data = {};
        data.action = action;
        data.loginId = vm.otpDetails.loginId;
        $uibModalInstance.close(data);
    };

    validateOTPAndResetPswrd(otpDetails, pswrdDetails){
       let vm = this, { dataServices } = vm.DI();
       vm.otpVerifyError = false;
       vm.resetPswrdError = false;
       vm.dataLoading = true;
       vm.errorMsg = "";
       // call get otp api and after getting success response close pop-up and create password pop-up
       dataServices.verifyOtpForFp(otpDetails).then(function (response) {
           if(response && response.status == 'SUCCESS'){
               vm.otpVerifyError = false;
               vm.pswrdDetails.userId = vm.otpDetails.loginId;
               dataServices.resetPswrdForFp(vm.pswrdDetails).then(function (response) {
                    if(response && response.status == 'SUCCESS'){
                        vm.cancel('show-success-pop-up'); // close pop-up before navigating anywhere
                        vm.dataLoading = false;
                    }else if(response && response.status == 'ERROR'){
                        vm.errorMsg = response.errorCode;
                        vm.resetPswrdError = true;
                        vm.dataLoading = false;
                    }else{
                        vm.errorMsg = "INTERNAL_SERVER_ERROR";
                        vm.resetPswrdError = true;
                        vm.dataLoading = false;
                    }
                }, function (error) {
                    vm.errorMsg = "INTERNAL_SERVER_ERROR";
                    vm.resetPswrdError = true;
                    vm.dataLoading = false;
                });
           }else if(response && response.status == 'ERROR'){
               vm.errorMsg = response.errorCode;
               vm.otpVerifyError = true;
               vm.dataLoading = false;
           }else{
               vm.errorMsg = "INTERNAL_SERVER_ERROR";
               vm.otpVerifyError = true;
               vm.dataLoading = false;
           }
        }, function (error) {
            vm.errorMsg = "INTERNAL_SERVER_ERROR";
            vm.otpVerifyError = true;
            vm.dataLoading = false;
        });
       
    }

    resendOtp(){
        let vm = this, { dataServices, $timeout } = vm.DI();
        vm.otpVerifyError = false;
        vm.otpDetails.otp = "";
        vm.dataLoading = true;
        vm.isOtpResent = false;
        dataServices.getOtpForFp(vm.emailId).then(function (otpRes) {
           if(otpRes && otpRes.status == 'SUCCESS'){
               vm.otpDetails.loginId = otpRes.message;
               vm.isOtpResent = true;
               $timeout(function() {
                    vm.isOtpResent = false;
                }, 2000);
               vm.otpVerifyError = false;
               vm.dataLoading = false;
           }else if(otpRes && otpRes.status == 'ERROR'){
               vm.errorMsg = otpRes.errorCode;
               vm.otpVerifyError = true;
               vm.dataLoading = false;
           }else{
               vm.errorMsg = "INTERNAL_SERVER_ERROR";
               vm.otpVerifyError = true;
               vm.dataLoading = false;
           }
        }, function (error) {
            vm.errorMsg = "INTERNAL_SERVER_ERROR";
            vm.otpVerifyError = true;
            vm.dataLoading = false;
        });
    }
}