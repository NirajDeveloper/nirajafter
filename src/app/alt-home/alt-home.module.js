/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


import {routeConfig} from './alt-home.route';
import {AltHomeController} from './alt-home.controller';

angular.module('aftermarket.alt-home', [])
    .config(routeConfig)
    .controller('AltHomeController',AltHomeController);