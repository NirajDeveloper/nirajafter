/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class OrderDataService {
    constructor($log, $timeout, dataServices, $http, $q) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            $log, $timeout, dataServices, $http, $q
        });

        vm.tabControls = {
            "currentPage": 1,
            "activeTab": 0,
            "sortBy": undefined,
            "sortReverse": false,
            "filter": undefined
        };
        vm.recentOrderData;
        vm.backOrderData;
        vm.invoiceListData;
        vm.dtSpecificOrderData;
        vm.dateSelection = undefined;
    }
    resetData () {
        let vm = this;
        vm.recentOrderData = undefined;
        vm.backOrderData = undefined;
        vm.invoiceListData = undefined;
        vm.dtSpecificOrderData = undefined;
        vm.tabControls = {
            "currentPage": 1,
            "activeTab": 0,
            "sortBy": undefined,
            "sortReverse": false,
            "filter": undefined
        };
        vm.dateSelection = undefined;
    }
    getInvoiceData() {
        //invoiceData
        let vm = this;
        let {
            $http,
            $q
        } = vm.DI();
        return vm.invoiceData;
    }
    getInvoiceList() {
        let vm = this;
        let {
            $http,
            $q
        } = vm.DI();
        return vm.invoiceListData;
    }
    getOrderData() {
        let vm = this;
        let {
            $http,
            $q,
            dataServices
        } = vm.DI();
        return vm.data.APIResponse;
    }
    getInvoiceDetails() {
        let vm = this;
        let {
            $http,
            $q,
            dataServices
        } = vm.DI();
        return vm.invoiceDetails;
    }
    getBackOrderData () {
        let vm = this;
        let {
            $http,
            $q
        } = vm.DI();
        return vm.backOrderData;
    }



}
