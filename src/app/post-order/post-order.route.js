/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function routeConfig($stateProvider) {
    'ngInject';
    $stateProvider
        .state('postorder', {
            url: '/orderhistory/orders',
            parent: 'aftermarket',
            templateUrl: 'app/post-order/post-order.html',
            controller: 'PostOrderController',
            controllerAs: 'postorder',
            params: { refresh: false },
            authenticate: true
        })
        .state('orderdetail', {
            url: '/orderhistory/orders/:id?extlink',
            parent: 'aftermarket',
            templateUrl: 'app/post-order/orderdetails/detail.html',
            controller: 'OrderDetailController',
            controllerAs: 'orderdetail',
            params: { refresh: true },
            authenticate: true
        })
        .state('invoicedetail', {
            url: '/orderhistory/invoice/:id',
            parent: 'aftermarket',
            templateUrl: 'app/post-order/invoicedetails/invoice-detail.html',
            controller: 'InvoiceDetailController',
            controllerAs: 'invoicedetail',
            authenticate: true
        })
        .state('orderstatus', {
            url: '/orderhistory/orderstatus/:id?extlink',
            parent: 'aftermarket',
            templateUrl: 'app/post-order/orderStatus/orderstatus.html',
            controller: 'OrderStatusController',
            controllerAs: 'orderstatus',
            authenticate: true,
            params:{}
        })       
}   