/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import { PostOrderController } from './post-order.controller';
import { routeConfig } from './post-order.route';
import { OrderDataService } from './orderdata.service';
import { OrderDetailController } from './orderdetails/orderdetail.controller';
import { InvoiceListController } from './invoicedetails/invoice-list.controller';
import { InvoiceController } from './invoicedetails/invoice.controller';
import { InvoiceDetailController } from './invoicedetails/invoice-detail.controller';
import { PackingSlipController } from './packingSlip/packingslip.controller';
import { OrderStatusController } from './orderStatus/orderstatus.controller';


angular.module('aftermarket.postorder', [])
    .config(routeConfig)
    .controller('PostOrderController', PostOrderController)
    .controller('OrderDetailController', OrderDetailController)
    .controller('InvoiceListController', InvoiceListController)
    .controller('InvoiceController', InvoiceController)
    .controller('InvoiceDetailController', InvoiceDetailController)
    .controller('PackingSlipController', PackingSlipController)
    .controller('OrderStatusController', OrderStatusController)
    .service('OrderDataService', OrderDataService)
    .filter('slice', function () {
        return function (arr, start, end) {
            if (!arr || !arr.length) { return; }
            return arr.slice(start, end);

        };
    })
    .filter('startFrom', function () {
        return function (input, start) {
            if (input) {
                start = +start;
                return input.slice(start);
            }
            return [];
        };
    });



