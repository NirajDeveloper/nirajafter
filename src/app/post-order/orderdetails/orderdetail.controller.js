/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class OrderDetailController {
    constructor($stateParams,$scope, $rootScope, $state, dataServices, OrderDataService, $window, OrderlistModalFactory, authenticationService) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({$stateParams, $scope, $rootScope, $state, dataServices, OrderDataService, $window, OrderlistModalFactory, authenticationService});
        this.orderData = {};
        vm.orderTotal = {};
        vm.orderTotal.totalqtyordered = 0;
        vm.orderTotal.totalqtyshipped = 0;
        vm.orderTotal.totalWeight = 0;
        vm.orderTotal.total = 0;
        vm.orderTotal.net = 0;
        vm.loadingData = true;
        vm.error = false;
        $scope.$emit("showLoading", true);
        //extlink
        vm.extetnal_link;
        if($state.params.extlink){
            vm.extetnal_link = true;
        }
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });
        $scope.$on("$destroy", destroyUserPermissions);
        vm.getOrderDetails();        
    }
    backtohistory() {
        let vm = this,
      	{$window, $state} = vm.DI();
        //backtohistory
        if(vm.userPermissionsList.ViewOrderHistory){
            $window.history.back();
        }
        else {
           $state.go('home'); 
        }
        
    }
    goToHistory(){
        let vm = this;
        let {$state} = vm.DI();
        $state.go('postorder', {refresh: $state.params.refresh});
    }
    downloadOrder(ordernumber){
        let vm = this;
        let {dataServices} = vm.DI();
        let fileNameToDownlod = 'order-' + ordernumber + '.xlsx';
        dataServices.downloadOrderDetails(ordernumber).then(function (data) {
            var b = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
            saveAs(b, fileNameToDownlod);
        }, function (error) {

        });

   
    }
    getOrderDetails(){
    	let vm = this;
        let {$scope, $stateParams, dataServices, authenticationService} = vm.DI();
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        vm.error = false;
        dataServices.getOrderDetails(customerId, $stateParams.id)
            .then(function (response) {
                let {
                    $scope, $rootScope
                } = vm.DI();
                if(response.orderDtls){
                    vm.orderData = response;
                    vm.calculateTotalAmount();
                }
                else{
                   vm.error = true; 
                }
                if(vm.extetnal_link){
                    $rootScope.$emit("CALL_CUSTLIST_API");
                }
                vm.loadingData = false;
                $scope.$emit("showLoading", false);
                
                              
            }, function(error) {
                vm.error = true; 
                vm.loadingData = false;
                if(vm.extetnal_link){
                    $rootScope.$emit("CALL_CUSTLIST_API");
                }
                $scope.$emit("showLoading", false);
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
         
           
    }
    viewOrderStatus(orderId) {
        let vm = this;
        let { $uibModal} = vm.DI();
        let modalInstance = $uibModal.open({
            templateUrl: 'app/post-order/orderStatus/orderstatus.html',
            controller: 'OrderStatusController',
            controllerAs: 'orderstatus',
            size: 'md',
            windowClass: 'orderstatus-modal-popup',
            bindToController: true,
            resolve: {
                param: function () {
                    return {'orderId':orderId };
                }
            }
        });
        modalInstance.result.then(function (userData) {
          
        }, function () {
            
        });
    }
    viewInvoice(orderId){
        let vm = this;
        let {OrderDataService, OrderlistModalFactory, $timeout} = vm.DI();
        let urlModalInstance = null;
        OrderDataService.invoiceId = orderId;
        urlModalInstance = $timeout(OrderlistModalFactory.open("md", "app/post-order/invoicedetails/invoice.html", "InvoiceController", "invoiceCtrl", "invoiceOverlay", true), 2000);
    }
     calculateTotalAmount(){
        let vm = this;
        for (let value of vm.orderData.orderDtls) {
            if(value.qtyReq && value.qtyReq !== null){
                vm.orderTotal.totalqtyordered = vm.orderTotal.totalqtyordered + parseInt(value.qtyReq);
            }
            if(value.unitWeight && value.unitWeight !== null){
                vm.orderTotal.totalWeight = vm.orderTotal.totalWeight + Math.round(value.unitWeight);
            }
            if(value.qtyShipped && value.qtyShipped !== null){
                vm.orderTotal.totalqtyshipped = vm.orderTotal.totalqtyshipped + parseInt(value.qtyShipped);
            }
            if(value.unitPrice && value.unitPrice !== null){
                vm.orderTotal.total = vm.orderTotal.total + parseFloat(value.unitPrice);
            }
            if(value.extendedPrice && value.extendedPrice !== null){
                vm.orderTotal.net = vm.orderTotal.net + parseFloat(value.extendedPrice);
            }

        }
    }
}