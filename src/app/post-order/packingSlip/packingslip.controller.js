/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class PackingSlipController {
    constructor($uibModal, $uibModalInstance, $scope, $translate, $timeout, dataServices, $rootScope,AftermarketConstants, param ) {
        'ngInject';
        let vm = this;
         vm.DI = () => ({ $uibModal, $uibModalInstance, $scope, $timeout, dataServices, $rootScope,AftermarketConstants, param});
        this.newItem="";
        this.errMsg = "";
        this.deliveryList = [];
        this.packingList = {};
        this.selectedList;
        this.loadingListItems = true;
        this.isSlipsAvailable = false;
        vm.areYouCopied = false;
        vm.tenantAddress = AftermarketConstants.appSettings.tenant_address;
        vm.websiteLogo = AftermarketConstants.skin.logo;
        vm.orderId  = param.orderId;
        vm.fecthDeliveryData();

    }
    cancel() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        vm.newItem ="";
        $uibModalInstance.close();
    }
    fecthDeliveryData(){
        let vm = this;
        let {dataServices, $rootScope} = vm.DI();
        dataServices.getDeliveryNumbers(vm.orderId)
            .then(function (response) {
                // response = {
                //     "deliveryNo": [
                //         "0080091299",
                //         "0080091222",
                //         "0080091290"
                //     ]
                // };
               
                
                if(response && response.deliveryNo && response.deliveryNo.length > 0) {
                    vm.deliveryList = response.deliveryNo;
                    vm.getPackingSlips(vm.deliveryList[0]);
                    //vm.selectedList = vm.deliveryList[0];
                }
                else {
                    vm.loadingListItems = false;
                }

                }, function (error) {
                    vm.loadingListItems = false;
                    if (error.status === 500) {
                        //DO WHAT YOU WANT

                    }
                    if (error.status === 404) {
                        //DO WHAT YOU WANT
                    }
              });

    }
   
    getPackingSlips(deliveryNo) {
        let vm = this;
        let {dataServices, $rootScope} = vm.DI();
        vm.selectedList = deliveryNo;
        vm.loadingListItems =true;
        vm.isSlipsAvailable = false;
         dataServices.getPackingSlips(deliveryNo)
            .then(function (response) {
                vm.loadingListItems = false;
                if(response && response.deliveryNumber) {
                    vm.packingList = response;
                    vm.isSlipsAvailable = true;
                }
     
            }, function (error) {
                vm.loadingListItems = false;
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }
    openEmailContainer(event){
        let vm = this;
        let {$window} = vm.DI();
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
        vm.isEmailSent = false;
        vm.emailIdList = undefined;
        vm.areYouCopied = false;
    }

    onChangeOfEmailList(){
        let vm = this;
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
    }

    sendDeliveryDtlsViaEmail(deliveryno) {
        let vm = this;
        let {$state, dataServices, $timeout} = vm.DI();
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
        let payload = {};
        let emails, re = /^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z.]{1,5}$/, validityArr, atLeastOneInvalid;
        if(vm.emailIdList){
            let commaSep = vm.emailIdList.indexOf(',');
            if (commaSep !== -1) {
                if (commaSep !== -1) {
                    emails = vm.emailIdList.split(',');
                }
                validityArr = emails.map(function (str) {
                    return re.test(str.trim());
                });
                atLeastOneInvalid = false;
                for (let i = 0; i < validityArr.length ; i++) {
                    if (validityArr[i] === false){
                       atLeastOneInvalid = true; 
                       break;
                    }
                }
                if (atLeastOneInvalid) {
                    vm.isInvalidEmailList = true;
                    return;
                }else{
                    payload = {
                        "deliveryno": deliveryno,
                        "toEmail": emails,
                        "ccEnabled": vm.areYouCopied
                    };
                }
            }else if(re.test(vm.emailIdList.trim())){
                payload = {
                    "deliveryno": deliveryno,
                    "toEmail": [vm.emailIdList.trim()],
                    "ccEnabled": vm.areYouCopied
                };
            }else{
                vm.isInvalidEmailList = true;
                return;
            }
        }else{
            vm.isInvalidEmailList = true;
            return;
        }

        if(!vm.isInvalidEmailList) {
            dataServices.sendDeliveryNoToEmails(payload).then(function (response) {
                vm.isEmailOpen = false;
                vm.isSentEmailFailed = false;
                vm.isInvalidEmailList = false;
                vm.isEmailSent = true;
                $timeout(function() {
                        vm.isEmailSent = false;
                    }, 3000);
            }, function (error) {
                vm.isSentEmailFailed = true;
            });
        }
    }
}
