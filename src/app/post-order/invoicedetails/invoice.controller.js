/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class InvoiceController {
    constructor($log, $state, $stateParams, $location, $scope,AftermarketConstants, $window, $timeout, dataServices, authenticationService,$rootScope,$interval, OrderDataService, OrderlistModalFactory, $uibModalInstance) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $log, $scope, $state, $stateParams, $location, $window, $timeout,dataServices, authenticationService,$rootScope,$interval, OrderDataService, OrderlistModalFactory, $uibModalInstance });
        this.invoiceData= {};
       
        this.loadingData= true;
        vm.websiteLogo = AftermarketConstants.skin.logo;
        vm.selectedInvoiceId = OrderDataService.invoiceId;
        vm.invoiceTotal= {};
        vm.invoiceTotal.totalqtyordered = 0;
        vm.invoiceTotal.totalqtyshipped = 0;
        vm.invoiceTotal.total = 0;
        vm.invoiceTotal.net = 0;
        vm.areYouCopiedInv = false;
        vm.tenantAddress = AftermarketConstants.appSettings.tenant_address;
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });
         $scope.$emit("showLoading", true);
        vm.fetchInvoiceDetails(vm.selectedInvoiceId);
    };
    cancel() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close();
    }

     fetchInvoiceDetails(id) {
        let vm = this;
        let {$scope, dataServices, authenticationService} = vm.DI();
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        dataServices.getInvoiceDetails(customerId, id)
            .then(function (response) {
                let {
                    $scope,
                    OrderDataService
                } = vm.DI();
                 vm.invoiceData = response;
                 vm.loadingData = false;
                  $scope.$emit("showLoading", false);
                 // vm.totalBackOrders = vm.backOrderListData.length;
                // vm.numOfPage = Math.ceil(vm.totalBackOrders/vm.rowPerPage);
                 //OrderDataService.backOrderData = vm.backOrderListData;
                 vm.calculateTotalAmount();
              
            }, function(error) {
                vm.loadingData = false;
                  $scope.$emit("showLoading", false);
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }
    calculateTotalAmount(){
        let vm = this;
        for (let value of vm.invoiceData.invoiceDtl) {
            if(value.qtyReq && value.qtyReq !== null){
                vm.invoiceTotal.totalqtyordered = vm.invoiceTotal.totalqtyordered + parseInt(value.qtyReq);
            }
            if(value.qtyShipped && value.qtyShipped !== null){
                vm.invoiceTotal.totalqtyshipped = vm.invoiceTotal.totalqtyshipped + parseInt(value.qtyShipped);
            }
            if(value.unitPrice && value.unitPrice !== null){
                vm.invoiceTotal.total = vm.invoiceTotal.total + parseFloat(value.unitPrice);
            }
            if(value.netPrice && value.netPrice !== null){
                vm.invoiceTotal.net = vm.invoiceTotal.net + parseFloat(value.netPrice);
            }

        }
        if(vm.invoiceData.invoiceSummary.freightCharge){
            vm.invoiceTotal.totalInvoice = vm.invoiceTotal.net + parseFloat(vm.invoiceData.invoiceSummary.freightCharge);
        }
        else{
            vm.invoiceTotal.totalInvoice = vm.invoiceTotal.net;
        }
        
        
    }

    
    openEmailContainer(event){
        let vm = this;
        let {$window} = vm.DI();
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
        vm.isEmailSent = false;
        vm.emailIdList = undefined;
        vm.areYouCopiedInv = false;
    }

    onChangeOfEmailList(){
        let vm = this;
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
    }

    sendInvoiceViaEmail(invoiceNo) {
        let vm = this;
        let {$state, dataServices, $timeout} = vm.DI();
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
        let payload = {};
        let emails, re = /^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z.]{1,5}$/, validityArr, atLeastOneInvalid;
        if(vm.emailIdList){
            let commaSep = vm.emailIdList.indexOf(',');
            if (commaSep !== -1) {
                if (commaSep !== -1) {
                    emails = vm.emailIdList.split(',');
                }
                validityArr = emails.map(function (str) {
                    return re.test(str.trim());
                });
                atLeastOneInvalid = false;
                for (let i = 0; i < validityArr.length ; i++) {
                    if (validityArr[i] === false){
                       atLeastOneInvalid = true; 
                       break;
                    }
                }
                if (atLeastOneInvalid) {
                    vm.isInvalidEmailList = true;
                    return;
                }else{
                    payload = {
                        "invoiceNo": invoiceNo,
                        "toEmail": emails,
                        "ccEnabled": vm.areYouCopiedInv
                    };
                }
            }else if(re.test(vm.emailIdList.trim())){
                payload = {
                    "invoiceNo": invoiceNo,
                    "toEmail": [vm.emailIdList.trim()],
                    "ccEnabled": vm.areYouCopiedInv
                };
            }else{
                vm.isInvalidEmailList = true;
                return;
            }
        }else{
            vm.isInvalidEmailList = true;
            return;
        }

        if(!vm.isInvalidEmailList) {
            dataServices.sendInvoiceToEmails(payload).then(function (response) {
                vm.isEmailOpen = false;
                //console.log("vm.isEmailOpen : "+vm.isEmailOpen);
                vm.isSentEmailFailed = false;
                vm.isInvalidEmailList = false;
                vm.isEmailSent = true;
                $timeout(function() {
                        vm.isEmailSent = false;
                    }, 3000);
            }, function (error) {
                vm.isSentEmailFailed = true;
            });
        }
    }
}

