/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class InvoiceListController {
    constructor($log, $state, $stateParams, $location, $scope, $window, $timeout, dataServices,authenticationService,$rootScope,$interval, OrderDataService, OrderlistModalFactory) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $log, $scope, $state, $stateParams, $location, $window, $timeout,dataServices, authenticationService,$rootScope,$interval, OrderDataService, OrderlistModalFactory });
        this.invoiceListData = OrderDataService.getInvoiceList().invoices;
        this.sortKey="";
        this.reverse="ASC";
        vm.currentPage = 1;
        vm.totalItems = this.invoiceListData.length;
    };

    viewInvoice(orderId){
        let vm = this,
            size = "md";
        let {$state, $location, $scope, OrderDataService, OrderlistModalFactory, $timeout} = vm.DI();
        let urlModalInstance = null;
            urlModalInstance = $timeout(OrderlistModalFactory.open("md", "app/post-order/invoicedetails/invoice.html", "InvoiceController", "invoiceCtrl", "invoiceOverlay", true), 2000);
    }
    sort(keyname){
        let vm = this;
        let {} = vm.DI();
        vm.sortKey = keyname;   //set the sortKey to the param passed
        vm.reverse = !vm.reverse; //if true make it false and vice versa
    }
    // sorting functionality
    setPage(pageNo) {
        let vm = this;
        vm.currentPage = pageNo;
    };
    pageChanged() {
        let vm = this;
        //console.log('Page changed to: ' + vm.currentPage);
    };
}

