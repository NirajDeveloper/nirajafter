/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class InvoiceDetailController {
    constructor($log, $state, $stateParams, $location, $scope, $window, $timeout, dataServices,authenticationService,$rootScope,$interval, OrderDataService, OrderlistModalFactory) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $log, $scope, $state, $stateParams, $location, $window, $timeout,dataServices, authenticationService,$rootScope,$interval, OrderDataService, OrderlistModalFactory });
        //this.invoiceData = OrderDataService.getInvoiceDetails();
        this.invoiceData= {};
       
        this.loadingData= true;
        vm.invoiceTotal= {};
        vm.invoiceTotal.totalqtyordered = 0;
        vm.invoiceTotal.totalqtyshipped = 0;
        vm.invoiceTotal.totalWeight = 0;
        vm.invoiceTotal.total = 0;
        vm.invoiceTotal.net = 0;
        vm.error = false;
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });
        $scope.$on("$destroy", destroyUserPermissions);
         $scope.$emit("showLoading", true);
        vm.fetchInvoiceDetails($stateParams.id);
       // debugger;
    };

    viewInvoice(orderId){
        let vm = this;
        let {OrderlistModalFactory, OrderDataService, $timeout} = vm.DI();
        let urlModalInstance = null;
        OrderDataService.invoiceId = orderId;
            urlModalInstance = $timeout(OrderlistModalFactory.open("md", "app/post-order/invoicedetails/invoice.html", "InvoiceController", "invoiceCtrl", "invoiceOverlay", true), 2000);
    }
    fetchInvoiceDetails(id) {
        let vm = this;
        let {$scope, dataServices, authenticationService} = vm.DI();
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        vm.error = false;
        dataServices.getInvoiceDetails(customerId, id)
            .then(function (response) {
                let {
                    $scope
                } = vm.DI();
                if(response.invoiceDtl){
                    vm.invoiceData = response;
                    vm.calculateTotalAmount();
                }
                else{
                   vm.error = true; 
                }
                 
                 vm.loadingData = false;
                  $scope.$emit("showLoading", false);
                 // vm.totalBackOrders = vm.backOrderListData.length;
                // vm.numOfPage = Math.ceil(vm.totalBackOrders/vm.rowPerPage);
                 //OrderDataService.backOrderData = vm.backOrderListData;
                 
              
            }, function(error) {
                vm.loadingData = false;
                  $scope.$emit("showLoading", false);
                  vm.error = true; 
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }
    backtohistory() {
        let vm = this,
      	{$window} = vm.DI();
        
        $window.history.back();
    }
    calculateTotalAmount(){
        let vm = this;
        for (let value of vm.invoiceData.invoiceDtl) {
            if(value.qtyReq && value.qtyReq !== null){
                vm.invoiceTotal.totalqtyordered = vm.invoiceTotal.totalqtyordered + parseInt(value.qtyReq);
            }
            if(value.weight && value.weight !== null){
                vm.invoiceTotal.totalWeight = vm.invoiceTotal.totalWeight + Math.round(value.weight);
            }
            if(value.qtyShipped && value.qtyShipped !== null){
                vm.invoiceTotal.totalqtyshipped = vm.invoiceTotal.totalqtyshipped + parseInt(value.qtyShipped);
            }
            if(value.unitPrice && value.unitPrice !== null){
                vm.invoiceTotal.total = vm.invoiceTotal.total + parseFloat(value.unitPrice);
            }
            if(value.netPrice && value.netPrice !== null){
                vm.invoiceTotal.net = vm.invoiceTotal.net + parseFloat(value.netPrice);
            }

        }
        if(vm.invoiceData.invoiceSummary.freightCharge){
            vm.invoiceTotal.totalInvoice = vm.invoiceTotal.net + parseFloat(vm.invoiceData.invoiceSummary.freightCharge);
        }
        else{
            vm.invoiceTotal.totalInvoice = vm.invoiceTotal.net;
        }
    }
    
}

