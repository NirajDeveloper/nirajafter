/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class OrderStatusController {
    constructor($scope, $rootScope, $translate, dataServices, $stateParams, $window, $state, authenticationService, filterFilter) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $scope, $rootScope, dataServices, $stateParams, $window, $state, authenticationService, filterFilter });
        this.newItem = "";
        this.errMsg = "";
        this.orderStatusData = {};
        //this.packingList = {};
        this.selectedList;
        //this.loadingListItems = true;
        //this.isSlipsAvailable = false;
        vm.orderId = $stateParams.id
        vm.loadingData = true;
        vm.noData = false;
        vm.data = null;
        //vm.reverse = "DES";
        vm.extetnal_link;
        if ($state.params.extlink) {
            vm.extetnal_link = true;

        }
        vm.filter = {};
        vm.filter.orderLineNo = undefined;
        vm.filter.partNo = undefined;
        vm.filter.showShipped = undefined;
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });

        vm.checked = false;
        vm.fecthOrderStatus();
        vm.status = [
            {
                name: "ORDER_RECEIVED",
                value: "In Received"
            }, {
                name: "ORDER_BEING_PROCESSED",
                value: "In Process"
            }, {
                name: "ORDER_PARTIALLY_SHIPPED",
                value: "Partially Shipped"
            }, {
                name: "ORDER_COMPLETELY_SHIPPED",
                value: "Shipped"
            }
        ];

        vm.qtyShipped = 0;
        vm.qtyInProcess = 0;
        vm.qtyAwaiting = 0;


        vm.dynamicPopover = {
            templateUrl: 'myPopoverTemplate.html'
        };

        vm.partReceivedStatus = {
            templateUrl: 'partReceivedTemp.html'
        };
        vm.partProcessStatus = {
            templateUrl: 'partProcessTemp.html'
        };
        vm.partShippedStatus = {
            templateUrl: 'partShippedTemp.html'
        };


        //pagination

        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.entryLimit = 5;
        vm.noOfPages = 0;

        $scope.$watch(() => vm.filter, function (nv, ov) {
            if (vm.data) {
                vm.filteredData = filterFilter(vm.data.orderLineItemStatus, nv);
                vm.totalItems = vm.filteredData.length;
                vm.noOfPages = Math.ceil(vm.totalItems / vm.entryLimit);
                vm.currentPage = 1;
            }
        }, true);
    }

    closePopover() {
        let vm = this;
        vm.togglePopover = false;
    }

    hideShippedItems() {
        let vm = this;
        //vm.checked = !vm.checked;
        if (vm.checked) {
            vm.filter.showShipped = "0";
        }
        else {
            vm.filter.showShipped = undefined;
        }
    }
    cancel() {
        let vm = this,
            {

            } = vm.DI();
        vm.newItem = "";

    }
    backtohistory() {
        let vm = this,
            {$window, $state} = vm.DI();
        if (vm.userPermissionsList.ViewOrderHistory) {
            $window.history.back();
        }
        else {
            $state.go('home');
        }

        //$window.history.back();  
    }
    generateUiDataObject(response) {
        let vm = this;
        let { $scope } = vm.DI();
        vm.data = response;
        vm.paginate();
        for (let i = 0; i < vm.data.orderLineItemStatus.length; i++) {
            let status = vm.data.orderLineItemStatus[i].lineItemSts;
            let prcsDtls = vm.data.orderLineItemStatus[i].processingDtls;
            for (let j in prcsDtls) {
                switch (j) {
                    case "ORDER_COMPLETELY_SHIPPED":
                        vm.qtyShipped += prcsDtls["ORDER_COMPLETELY_SHIPPED"].qtyProcessed;
                        break;
                    case "ORDER_BEING_PROCESSED":
                        vm.qtyInProcess += prcsDtls["ORDER_BEING_PROCESSED"].qtyProcessed;
                        break;
                    case "ORDER_PARTIALLY_SHIPPED":
                        vm.qtyInProcess += prcsDtls["ORDER_PARTIALLY_SHIPPED"].qtyProcessed;
                        break;
                    case "ORDER_RECEIVED":
                        vm.qtyAwaiting += prcsDtls["ORDER_RECEIVED"].qtyProcessed;
                        break;
                    default:
                        angular.noop();
                        break;
                }
            }

        }
    }

    paginate() {
        let vm = this;
        //vm.pData = angular.copy(vm.data);

        let obj = angular.copy(vm.data.orderLineItemStatus);
        vm.totalItems = obj.length;
        vm.noOfPages = Math.ceil(vm.totalItems / vm.entryLimit);
        vm.filterData = [];
    }

    hoverIn(obj, idx) {
        if (obj.shippedData.length - 1 != idx) {
            obj.showLastNodeToolTip = false;
        }
    }
    hoverOut(obj, idx) {
        if (obj.shippedData.length - 1 != idx) {
            obj.showLastNodeToolTip = true;
        }
    }
    sort() {
        let vm = this;
        vm.reverse = !vm.reverse; //if true make it false and vice versa
    }
    fecthOrderStatus() {
        let vm = this;
        let {dataServices, $rootScope} = vm.DI();
        vm.noData = false;
        dataServices.getOrderStatus(vm.orderId)  //133270  // 118849
            .then(function (response) {
                vm.loadingData = false;
                if (response.code === undefined)
                    vm.generateUiDataObject(response);
                else
                    vm.noData = true;
                if(vm.extetnal_link){
                    $rootScope.$emit("CALL_CUSTLIST_API");
                }

            }, function (error) {
                vm.loadingData = false;
                if(vm.extetnal_link){
                    $rootScope.$emit("CALL_CUSTLIST_API");
                }
                if (error.status === 500) {

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });

    }


}
