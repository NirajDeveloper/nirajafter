/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class PostOrderController {
    constructor($state, $stateParams, $scope, $timeout, $uibModal, $window, dataServices, authenticationService, $interval, OrderDataService, OrderlistModalFactory, $filter, $rootScope) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $scope, $state, $stateParams, $timeout,$uibModal, $window, dataServices, authenticationService, $interval, OrderDataService, OrderlistModalFactory, $filter, $rootScope });
        
        //Common attributes
        vm.sortKey = "";
        vm.reverse = "ASC";

        vm.rowPerPage = 10;
        
        //params
        if ($state.params.refresh) {
           OrderDataService.resetData();
        }
        //OrderDataService.resetData();
        vm.currentPage = OrderDataService.tabControls.currentPage;
        vm.totalItems = 0;
        vm.totalItems_orig = 0;
        vm.numOfPage = 0;
        vm.maxpages = 10;
        vm.loadingData = true;
        $scope.$emit("showLoading", true);
        vm.currentDate = new Date();
        vm.invoiceToDate = new Date();  
        vm.invoiceFromDate = new Date((new Date()).setDate(vm.currentDate.getDate() - 60)); // two months ago date 
        let twoYrsAgoFrmInvToDate = vm.invoiceToDate - 1000 * 60 * 60 * 24 * 365 *2; // two years in milliseconds
        vm.invoiceMinFrmDate = new Date(twoYrsAgoFrmInvToDate);
        vm.areYouCopied = false;
        // vm.invToDtOptions = {
        //     dateDisabled: false,
        //     formatYear: 'yyyy',
        //     maxDate: new Date()
        // };
        // vm.invFrmDtOptions = {
        //     dateDisabled: false,
        //     formatYear: 'yyyy',
        //     maxDate: new Date()
        // };
        vm.format = 'MM/dd/yyyy';
        //vm.altInputFormats = ['M!/d!/yyyy'];
        vm.invToDtPicker = {
            opened: false
        };
        vm.invFrmDtPicker = {
            opened: false
        };
        vm.openInvFrmDtPicker = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            vm.invFrmDtPicker.opened = true;
            vm.invToDtPicker.opened = false;
        };
        vm.openInvToDtPicker = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            vm.invFrmDtPicker.opened = false;
            vm.invToDtPicker.opened = true;
        };
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });
        
        /* Recent order tab  block begins */
        vm.orderData = {};
        //vm.fetchOrderData();
        vm.showSearchBox = true;
        vm.searchBtnTxt = "Search Here"
        vm.searchFilter = {};
        vm.searchFilter.invoiceNumber = undefined;
        vm.searchFilter.processNumber = undefined;
        vm.searchFilter.orderNumber = undefined;
        vm.searchFilter.poNumber = undefined;
        vm.searchFilter.orderDate = undefined;
        vm.searchFilter.searchByPart = "";
        
        vm.recentPartNumber = "";
        
        /* Invoice list tab  block begins */
        vm.invoiceListData;
        vm.totalInvoices = 0;
        vm.invoiceFilter = {};
        vm.invoiceFilter.invoiceNumber = undefined;
        vm.invoiceFilter.invoiceDate = undefined;
        vm.invoiceFilter.invoiceType = undefined;
        vm.invoiceFilter.invoiceTypeDescr = undefined;
        vm.invoiceFilter.searchByPart = "";
        
        /* back order list tab  block begins */
        vm.backOrderListData;
        vm.totalBackOrders = 0;
        vm.backOrderFilter = {};
        vm.backOrderFilter.orderNo = undefined;
        vm.backOrderFilter.primaryPONumber = undefined;
        vm.backOrderFilter.soldToCustomerId = undefined;
        vm.backOrderFilter.shipToCustomerId = undefined;
        vm.backOrderFilter.partNo = undefined;
        vm.backOrderFilter.partDescr = undefined;
        vm.backOrderFilter.orderCreateDate = undefined;
        vm.backOrderFilter.promisedDate = undefined;
        vm.backSpecificPartNumber = "";
        
        

        /*Date specific tab :  date picker calender*/
        vm.orderDtData;
        vm.totalDtItems = 0;
        vm.totalDtItems_orig = OrderDataService.dtSpecificOrderData ? OrderDataService.dtSpecificOrderData.length:0;
        vm.dtFilter = {};
        vm.dtFilter.searchByPart = undefined;
        vm.dtSpecificPartNumber = "";
        vm._setDefaultDtOrderFilters();
        vm._setDefaultBackOrderFilters();
        vm._setDefaultInvoiceFilters();
       
    
        vm.dateRange = {};
        vm.dateRange.isTblHidden = true;
        vm.dateRange.fromDetails = {};
        vm.dateRange.toDetails = {};
        vm.dateRange.format = "MM/dd/yyyy";
        vm.dateRange.fromDetails.date = new Date();
        
        vm.dateRange.fromDetails.opened = false;
        vm.dateRange.toDetails.date = new Date();
        let twoYrsAgoFrmToDate = vm.dateRange.toDetails.date - 1000 * 60 * 60 * 24 * 365 *2; // two years in milliseconds
        vm.dateRange.fromDetails.minDate = new Date(twoYrsAgoFrmToDate);
        vm.dateRange.toDetails.opened = false;
        if(OrderDataService.dateSelection) {
            vm.dateRange.fromDetails.date = new Date(OrderDataService.dateSelection.fromDate);
            vm.dateRange.toDetails.date = new Date(OrderDataService.dateSelection.toDate);
            twoYrsAgoFrmToDate = vm.dateRange.toDetails.date - 1000 * 60 * 60 * 24 * 365 * 2; // two years in milliseconds
            vm.dateRange.fromDetails.minDate = new Date(twoYrsAgoFrmToDate);
        }
        vm.sortKey = OrderDataService.tabControls.sortBy;
        vm.reverse = OrderDataService.tabControls.sortReverse;
        vm.activeTab = OrderDataService.tabControls.activeTab;
        
        if(OrderDataService.tabControls.filter) {
            vm.searchFilter = OrderDataService.tabControls.filter.searchFilter;
            vm.backOrderFilter = OrderDataService.tabControls.filter.backOrderFilter;
            vm.dtFilter = OrderDataService.tabControls.filter.dtFilter;
        }
        vm.fetchTabData(vm.activeTab);
        $timeout(function(){
            vm.currentPage = OrderDataService.tabControls.currentPage;
        },1000);
        
    };
    formatDate(date) {
        let dt = new Date(date),
            month = '' + (dt.getMonth() + 1),
            day = '' + dt.getDate(),
            year = dt.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [month, day, year].join('/');
    };
    compareTwoDates(fromDtObj, toDtObj) {
        if(fromDtObj && toDtObj){
            if (toDtObj.getTime() >= fromDtObj.getTime()) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    }
    _setDefaultDtOrderFilters() {
        let vm = this;
        vm.dtFilter.invoiceNumber = undefined;
        vm.dtFilter.processNumber = undefined;
        vm.dtFilter.orderNumber = undefined;
        vm.dtFilter.poNumber = undefined;
        vm.dtFilter.orderDate = undefined;
    }
    _setDefaultInvoiceFilters() {
        let vm = this;
        vm.invoiceFilter.invoiceNumber = undefined;
        vm.invoiceFilter.invoiceDate = undefined;
        vm.invoiceFilter.invoiceType = undefined;
        vm.invoiceFilter.invoiceTypeDescr = undefined; 
    }
    _setDefaultBackOrderFilters() {
        let vm = this;
        vm.backOrderFilter.orderNo = undefined;
        vm.backOrderFilter.primaryPONumber = undefined;
        vm.backOrderFilter.soldToCustomerId = undefined;
        vm.backOrderFilter.shipToCustomerId = undefined;
        vm.backOrderFilter.partNo = undefined;
        vm.backOrderFilter.partDescr = undefined;
        vm.backOrderFilter.orderCreateDate = undefined;
        vm.backOrderFilter.promisedDate = undefined;
        
    }
    _setDefaultOrderFilters() {
        let vm = this;
        vm.searchFilter.invoiceNumber = undefined;
        vm.searchFilter.processNumber = undefined;
        vm.searchFilter.orderNumber = undefined;
        vm.searchFilter.poNumber = undefined;
        vm.searchFilter.orderDate = undefined; 
    }
    _search() {
        let vm = this;
        let {$scope, $filter} = vm.DI();
        vm.orderDataClient = [];
        vm.orderDataClient = $filter('filter')(vm.orderData, {poNumber: vm.searchFilter.poNumber});
        vm.orderDataClient = $filter('filter')(vm.orderDataClient, {processNumber: vm.searchFilter.processNumber});
        vm.orderDataClient = $filter('filter')(vm.orderDataClient, {orderNumber:vm.searchFilter.orderNumber});
        if(vm.searchFilter.invoiceNumber === "" || vm.searchFilter.invoiceNumber === null) {
            vm.searchFilter.invoiceNumber = undefined;
        }
        vm.orderDataClient = $filter('filter')(vm.orderDataClient, {invoiceNumber: vm.searchFilter.invoiceNumber});
        vm.orderDataClient = $filter('filter')(vm.orderDataClient, {orderDate:vm.searchFilter.orderDate});

        //vm.currentPage = 1;
        vm.totalItems = vm.orderDataClient.length; 
        vm.numOfPage = Math.ceil(vm.totalItems / vm.rowPerPage);
    }
    _dtSearch() {
        let vm = this;
        let {$scope, $filter} = vm.DI();
        vm.dtOrderDataClient = [];
        vm.dtOrderDataClient = $filter('filter')(vm.orderDtData, {poNumber:vm.dtFilter.poNumber});
        vm.dtOrderDataClient = $filter('filter')(vm.dtOrderDataClient, {processNumber: vm.dtFilter.processNumber});
        vm.dtOrderDataClient = $filter('filter')(vm.dtOrderDataClient, {orderNumber: vm.dtFilter.orderNumber});
        if(vm.dtFilter.invoiceNumber === "" || vm.dtFilter.invoiceNumber === null) {
            vm.dtFilter.invoiceNumber = undefined;
        }
        vm.dtOrderDataClient = $filter('filter')(vm.dtOrderDataClient, {invoiceNumber: vm.dtFilter.invoiceNumber});
        vm.dtOrderDataClient = $filter('filter')(vm.dtOrderDataClient, {orderDate:vm.dtFilter.orderDate});
        //vm.currentPage = 1;
        vm.totalDtItems = vm.dtOrderDataClient.length; 
        vm.numOfPage = Math.ceil(vm.totalDtItems / vm.rowPerPage);
    }
    _boSearch() {
        let vm = this;
        let {$scope, $filter} = vm.DI();
        vm.backOrderDataClient = [];
        let propToFilter = {
            orderCreateDate: vm.backOrderFilter.orderCreateDate,
            orderNo: vm.backOrderFilter.orderNo,
            primaryPONumber: vm.backOrderFilter.primaryPONumber,
            soldToCustomerId: vm.backOrderFilter.soldToCustomerId,
            shipToCustomerId: vm.backOrderFilter.shipToCustomerId,
            partNo: vm.backOrderFilter.partNo,
            partDescr: vm.backOrderFilter.partDescr,
            promisedDate: vm.backOrderFilter.promisedDate
        };
        vm.backOrderDataClient = $filter('filter')(vm.backOrderListData, propToFilter);
        vm.totalBackOrders = vm.backOrderDataClient.length; 
        vm.numOfPage = Math.ceil(vm.totalBackOrders / vm.rowPerPage);
    }
    _invSearch(){
        let vm = this;
        let {$scope, $filter} = vm.DI();
        vm.invListDataClient = [];
        if(vm.invoiceFilter.invoiceNumber === "" || vm.dtFilter.invoiceNumber === null) {
            vm.invoiceFilter.invoiceNumber = undefined;
        }
        let propToFilter = {
            invoiceNumber: vm.invoiceFilter.invoiceNumber,
            invoiceDate: vm.invoiceFilter.invoiceDate,
            invoiceType: vm.invoiceFilter.invoiceType,
            invoiceTypeDescr: vm.invoiceFilter.invoiceTypeDescr
        };
        vm.invListDataClient = $filter('filter')(vm.invoiceListData, propToFilter);
        vm.totalInvoices = vm.invListDataClient.length; 
        vm.numOfPage = Math.ceil(vm.totalInvoices / vm.rowPerPage);
    }
    searchByPart() {
        let vm = this;
        let {$scope, dataServices, OrderDataService, authenticationService} = vm.DI();
        if(vm.recentPartNumber !== vm.searchFilter.searchByPart) {
            vm.recentPartNumber = vm.searchFilter.searchByPart;
            vm._setDefaultOrderFilters();
            vm.fetchOrderData();
        }

    }
    backSearchByPart() {
        let vm = this;
        let {$scope, dataServices, OrderDataService, authenticationService} = vm.DI();
        if(vm.backSpecificPartNumber !== vm.backOrderFilter.searchByPart) {
            vm.backSpecificPartNumber = vm.backOrderFilter.searchByPart;
            vm._setDefaultBackOrderFilters();
            vm.fetchBackOrderData();
        }
    }
    dtSearchByPart() {
        let vm = this;
        let {$scope, dataServices, OrderDataService, authenticationService} = vm.DI();
        if(vm.dtSpecificPartNumber !== vm.dtFilter.searchByPart) {
            vm.dtSpecificPartNumber = vm.dtFilter.searchByPart;
            vm._setDefaultDtOrderFilters();
            vm.getOrderListByDateRange(vm.dateRange.fromDetails.date, vm.dateRange.toDetails.date);
        }
    }
    invSearchByPart(){
        let vm = this;
        let {$scope, dataServices, OrderDataService, authenticationService} = vm.DI();
        if(vm.invSpecificPartNumber !== vm.invoiceFilter.searchByPart) {
            vm.invSpecificPartNumber = vm.invoiceFilter.searchByPart;
            vm._setDefaultInvoiceFilters();
            vm.fetchInvoiceData(vm.invoiceFromDate, vm.invoiceToDate);
        }
    }
    clearSearchText() {
        let vm = this;
        vm.searchFilter.searchByPart = "";
        vm.searchByPart();
    }
    clearDtSearchText() {
        let vm = this;
        vm.dtFilter.searchByPart = "";
        vm.dtSearchByPart();
    }
    clearBackSearchText() {
        let vm = this;
        vm.backOrderFilter.searchByPart = "";
        vm.backSearchByPart();
    }
    clearInvSearchText() {
        let vm = this;
        vm.invoiceFilter.searchByPart = "";
        vm.invSearchByPart();
    }
    fetchOrderData() {
        let vm = this;
        let {$scope, dataServices, OrderDataService, authenticationService} = vm.DI();
        let currentDate = new Date();
        let toDate = currentDate.getTime();//vm.formatDate(new Date());
        let temDate = currentDate.setMonth(currentDate.getMonth() - 2);
        let fDate = currentDate.getTime(); // vm.formatDate(temDate);  //, "thruDate": toDate
        let customerId = null;
        $scope.$emit("showLoading", true);
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        let qryParams= {};
        qryParams.customerId = customerId;
        qryParams.fromDate = fDate;
        qryParams.toDate = toDate;
        if(vm.searchFilter.searchByPart && vm.searchFilter.searchByPart !== ""){
            qryParams.partNo = vm.searchFilter.searchByPart;
        }
        dataServices.getOrderHistory(qryParams)
            .then(function (response) {
                let {
                    $scope,
                    OrderDataService
                } = vm.DI();
                if(response && response.orderList) {
                    vm.orderData = response.orderList;
                    vm._search();
                    vm.totalItems = vm.orderData.length;
                    vm.totalItems_orig = vm.orderData.length;
                    vm.numOfPage = Math.ceil(vm.totalItems / vm.rowPerPage);
                    OrderDataService.recentOrderData = vm.orderData
                    
                }
                else {
                    vm.totalItems = 0;
                    vm.totalItems_orig = 0;
                }
                
                vm.loadingData = false;
                $scope.$emit("showLoading", false);

            }, function (error) {
                vm.loadingData = false;
                $scope.$emit("showLoading", false);
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });

    };
    fetchInvoiceData(fDateObj, tDateObj) {
        let vm = this,
            {$scope, dataServices, OrderDataService, authenticationService} = vm.DI();
        let date = new Date();
        let fDate = vm.convert(fDateObj);
        let tDate = vm.convert(tDateObj);
        vm._setDefaultInvoiceFilters();
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        vm.invoiceListData = undefined;
        vm.loadingData = true;
        $scope.$emit("showLoading", true);
        dataServices.getInvoiceList(customerId, fDate, tDate)
            .then(function (response) {
                let {
                    $scope,
                    OrderDataService
                } = vm.DI();
                if(response && response.invoiceList){
                    vm.invoiceListData = response.invoiceList; //OrderDataService.getInvoiceList().invoices;
                    vm._invSearch();
                    vm.totalInvoices = vm.invoiceListData.length;
                    vm.numOfPage = Math.ceil(vm.totalInvoices / vm.rowPerPage);
                    OrderDataService.invoiceListData = vm.invoiceListData
                }
                else{
                    vm.totalInvoices = 0;
                }
                vm.loadingData = false;
                $scope.$emit("showLoading", false);

            }, function (error) {
                vm.loadingData = false;
                $scope.$emit("showLoading", false);
                vm.totalInvoices =0;
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    };

    fetchBackOrderData() {
        let vm = this;
        let {$scope, dataServices, OrderDataService, authenticationService} = vm.DI();
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        dataServices.getBackOrderList(customerId)
            .then(function (response) {
                let {
                    $scope,
                    OrderDataService
                } = vm.DI();
                if(response.bckOrderSummary){
                    vm.backOrderListData = response.bckOrderSummary;
                    vm._boSearch();
                    vm.totalBackOrders = vm.backOrderListData.length;
                    vm.numOfPage = Math.ceil(vm.totalBackOrders / vm.rowPerPage);
                    OrderDataService.backOrderData = vm.backOrderListData;
                }
                else{
                    vm.totalBackOrders = 0;
                }
                vm.loadingData = false;
                $scope.$emit("showLoading", false);

            }, function (error) {
                vm.loadingData = false;
                $scope.$emit("showLoading", false);
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });

    };
    /* Start :  date picker calender*/
    openFromDtPicker($event) {
        let vm = this;
        $event.preventDefault();
        $event.stopPropagation();
        vm.dateRange.fromDetails.opened = true;
        vm.dateRange.toDetails.opened = false;
    };

    openToDtPicker($event) {
        let vm = this;
        $event.preventDefault();
        $event.stopPropagation();
        vm.dateRange.toDetails.opened = true;
        vm.dateRange.fromDetails.opened = false;
    };

    getOrderListByDateRange(fromDate, toDate) {

        let vm = this;
        vm.loadingData = true;
        
        let {OrderDataService, dataServices, authenticationService, $scope} = vm.DI();
        $scope.$emit("showLoading", true);
        let fDate = vm.convert(fromDate); //new Date(vm.convert(fromDate)).getTime();
        let tDate = vm.convert(toDate); //new Date(vm.convert(toDate)).getTime();
        OrderDataService.dateSelection = {};
        OrderDataService.dateSelection.fromDate = fDate;
        OrderDataService.dateSelection.toDate = tDate;
        vm._setDefaultDtOrderFilters();
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        
        let qryParams= {};
        qryParams.customerId = customerId;
        qryParams.fromDate = fDate;
        qryParams.toDate = tDate;
        if(vm.dtFilter.searchByPart && vm.dtFilter.searchByPart !== ""){
            qryParams.partNo = vm.dtFilter.searchByPart;
        }
        
        dataServices.getOrderHistory(qryParams)
            .then(function (response) {
                if(response.orderList) {
                    vm.orderDtData = response.orderList;
                    vm._dtSearch();
                    vm.totalDtItems = vm.orderDtData.length;
                    vm.totalDtItems_orig = vm.orderDtData.length;
                    vm.numOfPage = Math.ceil(vm.totalDtItems / vm.rowPerPage);
                    OrderDataService.dtSpecificOrderData = vm.orderDtData
                     vm.dateRange.isTblHidden = false;
                }
                else {
                    vm.totalDtItems = 0;
                    vm.totalDtItems_orig = 0;
                    vm.dateRange.isTblHidden = false
                }
                vm.loadingData = false;
                $scope.$emit("showLoading", false);
            }, function (error) {
                vm.loadingData = false;
                $scope.$emit("showLoading", false);
                vm.dateRange.isTblHidden = false;
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
        //vm.orderData = OrderDataService.getOrderData().orderData;
        
    }
    convert(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [mnth, day, date.getFullYear()].join("/");
    }


    /* End :  date picker calender*/

    viewDetails(orderId) {
        let vm = this;
        let {$state, OrderDataService} = vm.DI();
        OrderDataService.tabControls.filter = {};
        OrderDataService.tabControls.filter.searchFilter = vm.searchFilter;
        OrderDataService.tabControls.filter.backOrderFilter = vm.backOrderFilter;
        OrderDataService.tabControls.filter.dtFilter = vm.dtFilter;
        $state.go('orderdetail', { "id": orderId, refresh: false });
        
    }
    viewOrderStatus(orderId) {
        let vm = this;
        let {$state, OrderDataService} = vm.DI();
        OrderDataService.tabControls.filter = {};
        OrderDataService.tabControls.filter.searchFilter = vm.searchFilter;
        OrderDataService.tabControls.filter.backOrderFilter = vm.backOrderFilter;
        OrderDataService.tabControls.filter.dtFilter = vm.dtFilter;
        $state.go('orderstatus', { "id": orderId });
    }
    viewPackingSlip(orderId) {
        let vm = this;
        let { $uibModal} = vm.DI();
        let modalInstance = $uibModal.open({
            templateUrl: 'app/post-order/packingSlip/packingslip.html',
            controller: 'PackingSlipController',
            controllerAs: 'packingslip',
            size: 'md',
            windowClass: 'packingslip-modal-popup',
            bindToController: true,
            resolve: {
                param: function () {
                    return {'orderId':orderId };
                }
            }
        });
        modalInstance.result.then(function (userData) {
            // if(userData.indexOf(vm.selectedList) !== -1){
            //     vm.selectedList = OrderListService.defaultListId;
            //     vm.fetchListItems(vm.selectedList);                
            // }
        }, function () {
            
        });
        
    }
    viewInvoiceDetails(invoiceId) {
        let vm = this;
        let {$state, OrderDataService} = vm.DI();
        OrderDataService.tabControls.filter = {};
        OrderDataService.tabControls.filter.searchFilter = vm.searchFilter;
        OrderDataService.tabControls.filter.backOrderFilter = vm.backOrderFilter;
        OrderDataService.tabControls.filter.dtFilter = vm.dtFilter;
        $state.go('invoicedetail', { "id": invoiceId });
    }

    openEmailContainer(event){
        let vm = this;
        let {$window} = vm.DI();
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
        vm.isEmailSent = false;
        vm.emailIdList = undefined;
        vm.areYouCopied = false;
        let el = event.currentTarget;
        let windowEl = angular.element($window);
        // let scrollTop = event.pageY event.clientY $window.pageYOffset $window.innerHeight
        //if($window.pageYOffset > 0){
            $window.scrollTo(0, event.pageY - 300);
        //}

        //angular.element(windowEl).animate({scrollTop: event.pageY -300}, "slow");
   return true;
    }

    onChangeOfEmailList(){
        let vm = this;
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
    }

    sendInvoiceViaEmail(orderDetails) {
        let vm = this;
        let {$state, dataServices, $timeout} = vm.DI();
        vm.isInvalidEmailList = false;
        vm.isSentEmailFailed = false;
        let payload = {};
        let emails, re = /^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z.]{1,5}$/, validityArr, atLeastOneInvalid;
        if(vm.emailIdList){
            let commaSep = vm.emailIdList.indexOf(',');
            if (commaSep !== -1) {
                if (commaSep !== -1) {
                    emails = vm.emailIdList.split(',');
                }
                validityArr = emails.map(function (str) {
                    return re.test(str.trim());
                });
                atLeastOneInvalid = false;
                for (let i = 0; i < validityArr.length ; i++) {
                    if (validityArr[i] === false){
                       atLeastOneInvalid = true; 
                       break;
                    }
                }
                if (atLeastOneInvalid) {
                    vm.isInvalidEmailList = true;
                    return;
                }else{
                    payload = {
                        "invoiceNo": orderDetails.invoiceNumber,
                        "toEmail": emails,
                        "ccEnabled": vm.areYouCopied
                    };
                }
            }else if(re.test(vm.emailIdList.trim())){
                payload = {
                    "invoiceNo": orderDetails.invoiceNumber,
                    "toEmail": [vm.emailIdList.trim()],
                    "ccEnabled": vm.areYouCopied
                };
            }else{
                vm.isInvalidEmailList = true;
                return;
            }
        }else{
            vm.isInvalidEmailList = true;
            return;
        }

        if(!vm.isInvalidEmailList) {
            dataServices.sendInvoiceToEmails(payload).then(function (response) {
                for (let x of vm.dtOrderDataClient){
                    if(x.orderNumber == orderDetails.orderNumber && x.invoiceNumber == orderDetails.invoiceNumber){
                        x.isOpen = false;
                        break;
                    }
                }
                vm.isSentEmailFailed = false;
                vm.isInvalidEmailList = false;
                vm.isSentEmailFailed = false;
                vm.isEmailSent = true;
                $timeout(function() {
                        vm.isEmailSent = false;
                    }, 3000);
            }, function (error) {
                vm.isSentEmailFailed = true;
            });
        }
    }
   
    sort(keyname) {
        let vm = this;
        let {OrderDataService} = vm.DI();
        if(keyname === 'price' && !vm.userPermissionsList.CheckPrice){
            return;
        }
        vm.sortKey = keyname;   //set the sortKey to the param passed
        vm.reverse = !vm.reverse; //if true make it false and vice versa
        vm.currentPage = 1; // go back to first page of pagination
        OrderDataService.tabControls.sortBy = keyname;
        OrderDataService.tabControls.sortReverse = vm.reverse;
    }
    // sorting functionality
    setPage(pageNo) {
        let vm = this;
         vm.currentPage = pageNo;
    };
    pageChanged() {
        let vm = this;
        let {OrderDataService} = vm.DI();
        // vm.currentPage = OrderDataService.tabControls.currentPage;
        OrderDataService.tabControls.currentPage = vm.currentPage;
    };
    

    /*select(evt) {
        let vm = this;
        vm.currentPage = 1;
        console.log("select called and  to 1 : " + vm.currentPage);
    }*/
    viewInvoice(orderId) {
        let vm = this;
        let {OrderDataService, OrderlistModalFactory, $timeout} = vm.DI();
        OrderDataService.invoiceId = orderId;
        let urlModalInstance = null;
        urlModalInstance = $timeout(OrderlistModalFactory.open("md", "app/post-order/invoicedetails/invoice.html", "InvoiceController", "invoiceCtrl", "invoiceOverlay", true), 2000);
    }

    onTabSwitch(currentTabIdx, activeTabIdx) {
        let vm = this;
        let {OrderDataService} = vm.DI();
        OrderDataService.tabControls.activeTab = currentTabIdx;
        
        if (currentTabIdx != activeTabIdx) {
            // initialize page index or search filter or sorting if needed
            vm.backOrderFilter = {};
            //vm.searchFilter = {};
            vm._setDefaultOrderFilters();
            vm._setDefaultDtOrderFilters();
            vm._setDefaultBackOrderFilters();
            vm._setDefaultInvoiceFilters();
            //vm.dtFilter = {};
            vm.currentPage = 1;
            OrderDataService.tabControls.currentPage = 1;
            vm.fetchTabData(currentTabIdx);

        } else {
            // no need initialize pagination index
        }

    }

    fetchTabData(activeTab) {
        let vm = this;
        let {OrderDataService, authenticationService, $scope, $timeout} = vm.DI();
        vm.loadingData = true;
        $scope.$emit("showLoading", true);
        switch (activeTab) {
            case 0:                
                if (OrderDataService.recentOrderData !== undefined) {
                    vm.orderData = OrderDataService.recentOrderData;
                    
                    vm.totalItems = vm.orderData.length;         
                    vm.totalItems_orig = vm.orderData.length;           
                    vm.numOfPage = Math.ceil(vm.totalItems / vm.rowPerPage);
                    vm._search();
                    $scope.$emit("showLoading", false);
                    vm.loadingData = false;
                }
                else {
                    vm.loadingData = true;
                    $scope.$emit("showLoading", true);
                     if(authenticationService.associatedCustData.activeCustId){
                        vm.fetchOrderData();
                     }
                     else {
                         $timeout(function(){
                            vm.fetchOrderData();
                        },3000);
                     }
                }
                break;
            case 1:                 
                vm.dateRange.isTblHidden = true;
                if (OrderDataService.dtSpecificOrderData !== undefined) {
                    vm.orderDtData = OrderDataService.dtSpecificOrderData;
                    vm.totalDtItems = vm.orderDtData.length;
                    vm.numOfPage = Math.ceil(vm.totalDtItems / vm.rowPerPage);
                    vm._dtSearch();
                    vm.dateRange.isTblHidden = false;
                }
                $scope.$emit("showLoading", false);
                vm.loadingData = false;
                break;
            case 2:
                if (OrderDataService.backOrderData !== undefined) {
                    vm.backOrderListData = OrderDataService.backOrderData;
                    vm.totalBackOrders = vm.backOrderListData.length;
                    vm.numOfPage = Math.ceil(vm.totalBackOrders / vm.rowPerPage);
                    vm._boSearch();
                     $scope.$emit("showLoading", false);
                     vm.loadingData = false;
                }
                else {
                    vm.loadingData = true;
                    $scope.$emit("showLoading", true);
                    vm.fetchBackOrderData();
                }
                break;
            case 3:
                if (OrderDataService.invoiceListData !== undefined) {
                    vm.invoiceListData = OrderDataService.invoiceListData;
                    vm.totalInvoices = vm.invoiceListData.length;
                    vm.numOfPage = Math.ceil(vm.totalInvoices / vm.rowPerPage);
                    vm._invSearch();
                    $scope.$emit("showLoading", false);
                    vm.loadingData = false;
                }
                else {
                    vm.loadingData = true;
                    vm.fetchInvoiceData(vm.invoiceFromDate, vm.invoiceToDate);
                }
                break;
        }
    }

    onChangeOfToDate(){
        let vm = this;
        let twoYrsAgoFrmToDate = vm.dateRange.toDetails.date - 1000 * 60 * 60 * 24 * 365 * 2;
        vm.dateRange.fromDetails.minDate = new Date(twoYrsAgoFrmToDate);
    }

    onChangeOfInvToDate(){
        let vm = this;
        let twoYrsAgoFrmInvToDate = vm.invoiceToDate - 1000 * 60 * 60 * 24 * 365 * 2;
        vm.invoiceMinFrmDate = new Date(twoYrsAgoFrmInvToDate);
    }

    downloadOrderTblData(tab){
        let vm = this;
        let {dataServices, authenticationService} = vm.DI();
        let xlsxFileTxt = "";
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        let postData = {};
        if(tab == 'TAB1'){
            let currentDate = new Date();
            let toDate = currentDate.getTime();
            let temDate = currentDate.setMonth(currentDate.getMonth() - 2);
            let fDate = currentDate.getTime();
            postData.customerId = customerId;
            postData.fromDate = vm.convert(fDate);
            postData.toDate = vm.convert(toDate);
            postData.partNo = vm.searchFilter.searchByPart;
            xlsxFileTxt = "recent-orders";
        }else if(tab == 'TAB2'){
            postData.customerId = customerId;
            postData.fromDate = vm.convert(vm.dateRange.fromDetails.date);
            postData.toDate = vm.convert(vm.dateRange.toDetails.date);
            postData.partNo = vm.dtFilter.searchByPart;
            xlsxFileTxt = "orders-by-daterange";
        }else if(tab == 'TAB3'){
            postData.customerId = customerId;
            xlsxFileTxt = "back-orders";
        }else if(tab == 'TAB4'){
            postData.customerId = customerId;
            postData.fromDate = vm.convert(vm.invoiceFromDate);
            postData.thruDate = vm.convert(vm.invoiceToDate);
            xlsxFileTxt = "view-credits";
        }else{
            postData = {};
        }
        dataServices.downloadPostOrderData(tab, postData).then(function (response) {
            //console.log("tab : "+tab);
            var b = new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
            saveAs(b, xlsxFileTxt +".xlsx");
        }, function () {

        });
    }
}

