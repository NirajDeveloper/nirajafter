/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class SocketIOService {
    constructor(dataServices, $rootScope) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({dataServices, $rootScope});

        vm.init()
    }

    init() {
        console.log("socket");

        //let socket = io.connect();
    }
}
