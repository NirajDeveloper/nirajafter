/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import {SignupController} from './signup.controller';
// import {routeConfig} from './signup.route';
import {SignupSuccessController} from './signup-success-pop-up/signupSuccessPopUp.controller';

angular.module('aftermarket.signup', [])
    .controller('SignupController', SignupController)
    .controller('SignupSuccessController', SignupSuccessController);

   