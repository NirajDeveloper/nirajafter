/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

/*Author:Manjesh Kumar*/
export class SignupController {

    constructor($log, $state, $stateParams, $location, $scope, $window, $timeout, dataServices,$rootScope,$interval,$uibModalInstance, $uibModal) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $log, $scope, $state, $stateParams, $location, $window, $timeout,dataServices,$rootScope,$interval,$uibModalInstance, $uibModal });
        vm.userDetails = {};
        vm.userType = "USER";
        vm.isFormSubmitted = false;
        vm.isUserCreated = false;
        vm.usrSuccessMsg = "";
        vm.isUserAvailChecked = false;
        vm.isUserExist = false;
        vm.isEmailAvailChecked = false;
        vm.isEmailExist = false;
        vm.countryList = [];
        vm.init();
    };

    init(){
      let vm = this; 
      vm.getCountryList(); 
    }

    cancel(action) {
        let vm = this,
        { $uibModalInstance } = vm.DI();
        let userData = {};
        userData.isUserCreated = vm.isUserCreated;
        userData.userInfo = vm.userDetails;
        userData.action = action;
        $uibModalInstance.close(userData);
    };

    goBackToSignInPopUp(){
        let vm = this,
            size = "lg";
        vm.cancel("open-signin-popup"); // close signup pop-up before opening signin pop-up
    };

    createOrUpdateUser(isFromInvalid, userDetails){
        let vm = this;
        let {$scope, $rootScope, dataServices, $location, $state, $window} = vm.DI();
        vm.isFormSubmitted = true;
        if(isFromInvalid){
            return;
            // no need to call create update user api
        }else{
            vm.isUserCreated = false;
            vm.dataLoading = true;
            // once you get user details, first check whether the userId is already available or not then after only you can create new user else new user can't be created.
            if(vm.isUserAvailChecked && !vm.isUserExist && vm.isEmailAvailChecked && !vm.isEmailExist){
                dataServices.createOrUpdateShopUser(vm.userDetails).then(function (response) {
                    if (response && response.status == 'SUCCESS') { 
                        vm.dataLoading = false;
                        vm.isUserCreated = true;
                        vm.cancel("success-regiter"); // close sign up pop up and display
                    } else {
                        $window.scrollTo(0, 0);
                        vm.dataLoading = false;
                        vm.isUserCreated = false;
                        if(response && response.status == 'ERROR'){
                            vm.usrUnSuccessMsg = response.errorCode;
                        }else{
                            vm.usrUnSuccessMsg = "INTERNAL_SERVER_ERROR";
                        }
                        
                    }
                }, function (error) {
                    $window.scrollTo(0, 0);
                    vm.isUserCreated = false;
                    vm.dataLoading = false;
                    vm.usrUnSuccessMsg = "INTERNAL_SERVER_ERROR";
                });
            }
        }
        
    }

    checkLoginIdAvailabilty(userId){
        if(userId){
            // call check availabilty service
            let vm = this;
            let {$scope, $rootScope, dataServices, $location, $state} = vm.DI();
            dataServices.checkUserExistance(userId).then(function (existRes) {
                if (existRes && existRes.status == 'SUCCESS') { 
                    vm.isUserAvailChecked = true;
                    vm.isUserExist = false;
                } else {
                    vm.isUserAvailChecked = true;
                    vm.isUserExist = true;
                }
            }, function (error) {
                vm.isUserAvailChecked = false;
                vm.isUserExist = false;
                //console.log("some service error while calling 'checkUserExistance'");
            });
        }
    }

    checkEmailIdAvailabilty(emailId){
        if(emailId){
            // call check availabilty service
            let vm = this;
            let {$scope, $rootScope, dataServices, $location, $state} = vm.DI();
            dataServices.checkEmailExistance(emailId).then(function (response) {
                if (response && response.status == 'SUCCESS') { 
                    vm.isEmailAvailChecked = true;
                    vm.isEmailExist = false;
                } else {
                    vm.isEmailAvailChecked = true;
                    vm.isEmailExist = true;
                }
            }, function (error) {
                vm.isEmailAvailChecked = false;
                vm.isEmailExist = false;
                //console.log("some service error while calling 'checkUserExistance'");
            });
        }
    }

    onChangeOfUserId(){
       // console.log("data changing");
        let vm = this;
        vm.isUserAvailChecked = false;
        vm.isUserExist = false;
    }

    onChangeOfEmailId(){
       // console.log("data changing");
        let vm = this;
        vm.isEmailAvailChecked = false;
        vm.isEmailExist = false;
    }

    getCountryList(){
        // api to populate country list
        let vm = this;
        let {$scope, $rootScope, dataServices, $location, $state} = vm.DI();
        dataServices.fetchCountryList().then(function (response) {
            if (response) { 
                vm.countryList = response;
            } else {
               vm.countryList = []; 
               //console.log("NO country");
            }
        }, function (error) {
            //console.log("NO country");
        });
    }

    changePhoneFormat(isValid, fieldName){
        let vm = this;
        if(isValid && vm.userDetails[fieldName] && vm.userDetails[fieldName].length == 10){
            vm.userDetails[fieldName] = vm.userDetails[fieldName].slice(0,3) + '-' + vm.userDetails[fieldName].slice(3,6)+ '-' + vm.userDetails[fieldName].slice(6);
        }else{
            // no need to format
        }
    }
}
