/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// Author: Manjesh Kumar
export class CompletedOrder {
    constructor($log, $scope, $rootScope, $filter, $uibModal, $state, $stateParams, $q, PricingService) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $log, $scope, $rootScope, $uibModal, $state, $stateParams, $q, PricingService });
        // Hard coded value
        vm.orderId = $stateParams.id ? $stateParams.id : "";
        vm.code = $stateParams.code ? $stateParams.code : "";
        $scope.$emit("showLoading", false);
        vm.goToPath = (PricingService.cartType=='express-cart') ? 'expresscheckout' : 'ordermgt';
    }

    go(path){
    	let vm = this,
        {
            $scope, $state, $stateParams, PricingService
        } = vm.DI();
        path == '/' ? $scope.$emit("reachedhome"): $state.go(path);
    }

    goToDetail(){
        let vm = this,
        {
            $scope, $state, $stateParams, PricingService
        } = vm.DI();

        $state.go('orderdetail', {"id": vm.orderId});
    }

    saveOrder(){
        let vm = this;
        let {$scope, $state, $stateParams} = vm.DI();

        
    }
}
