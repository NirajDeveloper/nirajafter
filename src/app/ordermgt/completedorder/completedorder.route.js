/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

export function routeConfig($stateProvider){
	'ngInject';
	$stateProvider
	.state('completedOrder',{
		url: '/completedOrder?id?code',
		parent: 'aftermarket',
        authenticate: true,
		views: {
			'':{
				templateUrl: 'app/ordermgt/completedorder/completedorder.html',
				controller: 'CompletedOrder',
				controllerAs: 'compltdOrdr',
			},
			'header':{
				template: '<span><page-header></page-header></span>'
			}
		}
		
	});
}