//Author: Niraj Kumar
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import {routeConfig} from './expresscheckout.route';
import {ExpressCheckOutController} from './expresscheckout.controller';
import {ExpressCheckOutService} from './expresscheckout.service';
import {autofocus} from './expresscheckout.directive';

angular.module('aftermarket.expresscheckout', [])
    .config(routeConfig)
    .controller('ExpressCheckOutController', ExpressCheckOutController)
    .service('ExpressCheckOutService', ExpressCheckOutService)
    .directive('autofocus', autofocus)
