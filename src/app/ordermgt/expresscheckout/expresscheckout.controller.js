//Author: Niraj Kumar
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class ExpressCheckOutController {
    constructor($document, $rootScope, $state, dataServices, ExpressCheckOutService, $translate, $scope, appInfoService, AvailabilityService, authenticationService, $q, $interval, PricingService, $sce, $timeout, $stateParams) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $document, $rootScope, $state, dataServices, ExpressCheckOutService, $translate, $scope, appInfoService, AvailabilityService, authenticationService, $q, $interval, PricingService, $sce, $timeout, $stateParams });

        vm.dateFormate = 'MM/dd/yyyy';
        vm.dateOptions = {
            showWeeks: false
        };
        vm.valuationDate = new Date();
        vm.isDataLoading = true;
        vm.isNonEditable = false;
        vm.dateOptions.minDate = vm.dateOptions.minDate ? null : new Date();
        vm.partno = { inputlength: '' };
        vm.isPartRemove = false;
        vm.isAllCartItmChecked = false;
        vm.isCheckAvlApiComlt = false;
        vm.isPriceCheckApiCompt = false;
        vm.partSearch = "";
        vm.showAvailableMsg = 'spin';
        vm.addCallCounter = 0;
        vm.partdetails = [
            {
                cartId: 0,
                partNo: '',
                avlQty: 0,
                qtyReq: '',
                customerPartNumber :'',
                partDesc: '',
                unitPrice: 0,
                uom: '',
                wtUom: '',
                coreCharge: 0,
                partWeight: 0,
                prodTotal: 0,
                productCat: '',
                backOrderQty: 0,
                qtyAdded: 0,
                shipToDateReq: new Date(),
                isNonEditable: false,
                checked: false,
                isPartNoValid: false,
                isQtyInputDisable: true,
                qtyStatus: '',
                isQtyStatusIconShow: false,
                isBackOrderCheckbox: false,
                isBackOrder: false,
                calanderSelect: false,
                showAvailableMsg: '',
                packageQty: 1,
                isQtyNtMultplOfPkQty: false
            }];

        vm.totNoOfParts = 0;
        vm.totWgtOfParts = 0;
        vm.totWgtOfPartsInLb = 0;
        vm.sumOfTotal = 0;
        vm.totalOrderQTY=0;
        vm.getCartData();
        vm.customerId = null;
        vm.addCallCounter = 0;
        vm.cartQtyNotAllowed=false;
        if (authenticationService.shipToSoldToInfo.selectedSoldTo && authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber) {
            vm.customerId = authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber;
        }
        else if (authenticationService.associatedCustData.activeCustId) {
            vm.customerId = authenticationService.associatedCustData.activeCustId;
        }
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });

        vm.stickyAd = false;
        let deregistrationCallback = $rootScope.$on("isHeaderSticky", (evt, isHeaderSticky) => {
            let adSec = $document[0].getElementById("ad-section2");
            if (isHeaderSticky.state) {
                vm.stickyAd = true;
                if (angular.isDefined(isHeaderSticky.bottomOffset)) {
                    angular.noop();
                }
            } else {
                vm.stickyAd = false;
                angular.element(adSec).css("bottom", 'auto');
            }
            //console.log(vm.stickyAd);
        });

        $scope.$on("$destroy", deregistrationCallback);
    }

    ///not in use
    onFocus(index, obj) {
        let vm = this;
        if (obj.partDesc) {
            if (obj.isNonEditable) {
                vm.partdetails[index].isNonEditable = false;
                obj.isNonEditable = false;
            } else {
                vm.partdetails[index].isNonEditable = true;
            }

        }
    }


    getPartNoDetails(index, obj) {
        let vm = this;
        let { ExpressCheckOutService, dataServices, $sce, $timeout, $translate } = vm.DI();
        if (obj.partNo) {
            vm.isDataLoading = false;
            vm.onFocus(index, obj);
            let payload = {
                "partNumber": obj.partNo
            };
            dataServices.getPartDetails(payload).then(function (response) {
                let data = response.data;
                if (response.status == 200) {
                    if(data.partNumber){
                        vm.isNonEditable = true;
                        vm.partdetails[index].partNo = angular.copy(data.partNumber);
                        vm.partdetails[index].customerPartNumber = angular.copy(data.customerPartNumber);
                        vm.partdetails[index].partDesc = angular.copy(data.partDesc);
                        vm.partdetails[index].unitPrice = angular.copy(data.unitPrice);
                        vm.partdetails[index].uom = angular.copy(data.packageType);
                        vm.partdetails[index].wtUom = angular.copy(data.wtUom);
                        vm.partdetails[index].productCat = angular.copy(data.catPath);
                        //vm.partdetails[index].coreCharge=angular.copy(data[0].coreCharges);
                        vm.partdetails[index].packageQty = angular.copy(data.packageQty);
                        vm.partdetails[index].partWeight = angular.copy(data.wt);
                        vm.partdetails[index].isNonEditable = true;
                        vm.partdetails[index].isPartNoValid = false;
                        vm.partdetails[index].isQtyInputDisable = false;
                        $timeout(() => {
                            document.getElementById("qty_" + index).focus();
                        }, 100);
                        vm.partdetails[index].isBackOrderCheckbox = true;
                        vm.isDataLoading = false;
                    }
                    else{
                        vm.partdetails[index].isPartNoValid = true;
                        vm.partdetails[index].isNonEditable = false;
                        vm.partdetails[index].isQtyInputDisable = true;
                        //document.getElementById("qty_" + index).focus();
                        
                        vm.partdetails[index].isBackOrderCheckbox = false;
                        vm.isDataLoading = false;
                        $translate('ERRORMSG.PARTNOTVALID').then((ERRORMSG)=>{
                          vm.partvalidateMsg= $sce.trustAsHtml(ERRORMSG);
                        })
                    }
                }
                if (response.status == 404) {
                    vm.partdetails[index].isPartNoValid = true;
                    vm.partdetails[index].isNonEditable = false;
                    vm.partdetails[index].isQtyInputDisable = true;
                    //document.getElementById("qty_" + index).focus();
                    
                    vm.partdetails[index].isBackOrderCheckbox = false;
                    vm.isDataLoading = false;
                  
                    $translate('ERRORMSG.PARTNOTVALID').then((ERRORMSG)=>{
                     vm.partvalidateMsg= $sce.trustAsHtml(ERRORMSG);
                    })
                   
                   
                }
                if(response.status == 403){
                    vm.partdetails[index].isPartNoValid = true;
                    vm.partdetails[index].isNonEditable = false;
                    vm.partdetails[index].isQtyInputDisable = true;
                    //document.getElementById("qty_" + index).focus();
                    vm.partdetails[index].isBackOrderCheckbox = false;
                    vm.isDataLoading = false;
                    $translate('ERRORMSG.NOTRTBERRORMSG').then((ERRORMSG)=>{
                         vm.partvalidateMsg= $sce.trustAsHtml(ERRORMSG);
                    })
                   
                }

                // if((response.status==200) && ((index)==(vm.partdetails.length-1))){
                //     vm.addnewrow();
                // }
            }, function (reason) {
                vm.produdtdata = [];
                vm.partdetails[index].isPartNoValid = true;
                vm.partdetails[index].isNonEditable = false;
                vm.partdetails[index].isQtyInputDisable = true;
                vm.partdetails[index].isBackOrderCheckbox = false;
                vm.isDataLoading = false;

            });

        }
    }

    removeSeletedItem() {
        let vm = this;
        vm.isAllCartItmChecked = false;
        var newDataList = [];
        for (let item of vm.partdetails) {
            if (!item.checked) {
                newDataList.push(item);
            } else {
                vm.deleteItem(item);
            }
        }
        vm.partdetails = newDataList;
       // document.getElementById('part_'+vm.partdetails.length-1).focus();
        vm.checkOutCalculation();
    };

    deleteItem(obj) {
        let vm = this,
            {
                dataServices, $state, PricingService, $scope, $timeout
            } = vm.DI();
        vm.isDataLoading = true;
        dataServices.deleteCart(obj.cartId).then(function (response) {
            vm.isDataLoading = false;
            vm.isPartRemove = false;
        }, function (error) {
            vm.isDataLoading = false;
        });
    }

    selectCartItem(obj) {
        let vm = this;
        for(let y of vm.partdetails){
           if(y.checked){
                vm.isPartRemove = true;
                break;
           }else{
                vm.isPartRemove = false;
           }
        }
        for (let x of vm.partdetails){
            if(x.cartId!=0){
               if(x.checked){
                vm.isAllCartItmChecked = true;
            }else{
                vm.isAllCartItmChecked = false;
                break;
                }
            }
        }
    }

    selectAllCartItem() {
        let vm = this;
        let {$scope} = vm.DI();
        if (vm.isAllCartItmChecked) {
            vm.isAllCartItmChecked = true;
            vm.isPartRemove = true;
        } else {
            vm.isAllCartItmChecked = false;
            vm.isPartRemove = false;
        }
        if (vm.partdetails.length == 1) {
            vm.isPartRemove = false;
        }
        for (let i = 0; i < vm.partdetails.length - 1; i++) {
            vm.partdetails[i].checked = vm.isAllCartItmChecked;
        }
    };

    addnewrow() {
        let vm = this;
        let {$scope} = vm.DI();

        vm.partdetails.push({
            cartId: 0,
            partNo: '',
            qtyReq: '',
            customerPartNumber : '',
            partDesc: '',
            unitPrice: 0,
            uom: '',
            wtUom: '',
            coreCharge: 0,
            partWeight: 0,
            prodTotal: 0,
            productCat: '',
            avlQty: 0,
            backOrderQty: 0,
            qtyAdded: 0,
            shipToDateReq: new Date(),
            isNonEditable: false,
            checked: false,
            isPartNoValid: false,
            isQtyInputDisable: true,
            qtyStatus: '',
            isQtyStatusIconShow: false,
            isBackOrderCheckbox: false,
            calanderSelect: false,
            showAvailableMsg: '',
            packageQty: 1,
            isQtyNtMultplOfPkQty: false
            //minQty = vm.packageQty > 1 ? 0 : 1;
        });
    }

    checkAvailability(index, obj) {
        let vm = this,
            { dataServices, authenticationService, $interval, $sce} = vm.DI();
        let orderType = "STK";
        let catArr = [];
       // vm.addCallCounter = 0;
        vm.isDataLoading = false;
        obj.showAvailableMsg = $sce.trustAsHtml("<i class='fa fa-spinner fa-spin fa-1x fa-fw'></i>");
        obj.enable = true;
        let packageQty = obj.packageQty;
        if(!packageQty || packageQty.toString().trim() == ""){
            packageQty = 1;
        }else{
            packageQty = packageQty;
        }
        dataServices.partAvailability(obj.partNo, vm.customerId, orderType, obj.qtyReq, "", obj.productCat, packageQty)
            .then(function (response) {
                vm.addCallCounter++;
                if (response) {
                    let avlQty = response.totalQtyAvail ? response.totalQtyAvail : 0;
                    let isAvlQty = avlQty >= obj.qtyReq ? true : false;
                    vm.partdetails[index].backOrderQty = obj.qtyReq > avlQty ? obj.qtyReq - avlQty : 0;
                    vm.partdetails[index].qtyAdded = obj.qtyReq < avlQty ? obj.qtyReq : avlQty;
                    vm.partdetails[index].avlQty = obj.qtyReq < avlQty ? 10 : avlQty;

                    vm.partdetails[index].isQtyStatusIconShow = true;
                    vm.partdetails[index].prodTotal = obj.prodTotal;
                    vm.isCheckAvlApiComlt = true;

                    if (isAvlQty) {
                        vm.partdetails[index].qtyStatus = 'fullQtyAvl';
                        vm.partdetails[index].isBackOrder = false;
                    } else {
                        vm.partdetails[index].qtyStatus = 'partialQtyAvl';
                        vm.partdetails[index].isBackOrder = true;
                    }
                    //vm.addCallCounter++;
                    if (vm.addCallCounter%2 === 0) {
                        //vm.addCallCounter=0;
                        if (obj.cartId) {
                            vm.updateLineItemQty(index, obj);
                        } else {
                            vm.addToCart(index, obj);
                        }
                    }


                    // vm.addToCart(index);


                } else {
                    vm.isDataLoading = false;
                    vm.addCallCounter++;

                }
                if (vm.addCallCounter%2 === 0) {
                    //vm.addCallCounter = 0;
                    obj.showAvailableMsg = '';
                }

            }, function (error) {
                vm.addCallCounter++;
                vm.isDataLoading = false;
                if (vm.addCallCounter%2 === 0) {
                    //vm.addCallCounter = 0;
                    obj.showAvailableMsg = '';
                }
            });

        dataServices.partPricing(obj.partNo, vm.customerId, orderType, obj.qtyReq, "", catArr)
            .then(function (response) {
                vm.addCallCounter++;
                if (response && response.partPriceDtl && response.partPriceDtl.priceDtlMap) {
                    vm.partdetails[index].unitPrice = response.partPriceDtl.priceDtlMap.STANDARD_BASE;
                    vm.partdetails[index].coreCharge = response.partPriceDtl.priceDtlMap.CORE_BASE;
                    vm.isPriceCheckApiCompt = true;
                    vm.checkOutCalculation();
                    // vm.addCallCounter++;
                    if (vm.addCallCounter%2 === 0) {
                        //vm.addCallCounter=0;
                        if (obj.cartId) {
                            vm.updateLineItemQty(index, obj);
                        } else {
                            vm.addToCart(index, obj);
                        }
                        obj.showAvailableMsg = '';
                    }


                } else {
                    //vm.partdetails[index].isNonEditable=false;
                    //vm.isDataLoading = false;
                    vm.checkOutCalculation();
                    if (vm.addCallCounter%2 === 0) {
                        //vm.addCallCounter = 0;
                        obj.showAvailableMsg = '';
                        vm.partdetails[index].unitPrice = 0;
                        vm.partdetails[index].coreCharge = 0;
                        if (obj.cartId) {
                            vm.updateLineItemQty(index, obj);
                        } else {
                            vm.addToCart(index, obj);
                        }
                    }
                    vm.isDataLoading = false;
                }
                
            }, function (error) {
                vm.addCallCounter++;
                vm.checkOutCalculation();
                if (vm.addCallCounter%2 === 0) {
                    //vm.addCallCounter = 0;
                    obj.showAvailableMsg = '';
                    vm.partdetails[index].unitPrice = 0;
                    vm.partdetails[index].coreCharge = 0;
                    if (obj.cartId) {
                        vm.updateLineItemQty(index, obj);
                    } else {
                        vm.addToCart(index, obj);
                    }
                }
                // vm.partdetails[index].isNonEditable=false;


            })
       

    }
    gotoPart(obj){
        let vm = this;
        let { $stateParams, $state } = vm.DI();
        
        let paramObj = { "id": obj.partNo, "type": "partnum", "val":obj.partNo };
        return $state.href("part", paramObj);
    }

     onBlurOfQtyField(obj, fieldId) {
        let vm = this;
        let { $timeout } = vm.DI();
        let fieldVal = document.getElementById(fieldId).value;
        if((fieldVal == "0") || (obj.qtyReq == undefined) || (obj.qtyReq <= 0)){
            obj.qtyReq = obj.packageQty;
        }
        if(((obj.packageQty >= 1) && (obj.qtyReq > 0)) && ((obj.qtyReq % obj.packageQty) !== 0)){
           vm.cartQtyNotAllowed = true;
        }else{
            $timeout(function(){
                if(document.getElementsByClassName('qty-err').length > 0){
                    vm.cartQtyNotAllowed = true;
                }else{
                    vm.cartQtyNotAllowed = false;
                }
            }, 0);  
        }
    }
    
    addToCart(index, obj) {

        let vm = this,
            {
                dataServices,
                $rootScope
            } = vm.DI();
        let lineitem = vm.partdetails[index];
        let payload = {
            "cartName": "ORDERLIST",//change acq
            "cartType": "express-cart",//change acq
            "orderType": "SO",
            "lineItems": [{
                "partNo": lineitem.partNo,
                "partName": lineitem.partDesc,
                customerPartNo : lineitem.customerPartNumber,
                "qtyReq": lineitem.qtyReq,
                "qtyAdded": lineitem.qtyAdded,
                "backOrderQty": lineitem.backOrderQty,
                "productCat": lineitem.productCat,
                "uom": lineitem.uom,
                "wt": lineitem.partWeight,
                "packageQty": lineitem.packageQty,
                "wtUom": 'LB',
                "prices": [{
                    "type": "",
                    "unitPrice": lineitem.unitPrice,
                    "extdPrice": (lineitem.qtyAdded ? lineitem.qtyAdded : 0 )*(lineitem.unitPrice ? lineitem.unitPrice :0 + lineitem.coreCharge ? lineitem.coreCharge :0) ,
                    "corePrice": lineitem.coreCharge ? lineitem.coreCharge : 0,
                    "currency": lineitem.currency ? lineitem.currency : "USD"
                }],
                 "checked": true,
                 "shipToDateReq": new Date(lineitem.shipToDateReq).getTime()
            }]
        };

        dataServices.addToCart(payload).then(function (response) {
            if (response) {
                vm.partdetails[index].cartId = response.ids[0];
                if ((response.ids[0]) && ((index) == (vm.partdetails.length - 1))) {
                    vm.addnewrow();
                }
                vm.isDataLoading = false;

            }

        }, function (error) {
            vm.isDataLoading = false;
        });
    }

    updateLineItemQty(index, lineItmObj) {
        let vm = this,
            {
                dataServices, $state, PricingService, $scope, $rootScope
            } = vm.DI();
        if (lineItmObj.qtyReq && lineItmObj.qtyReq >= 1) {
            let objToUpdate = lineItmObj;
            let payload = [{
                "id": lineItmObj.cartId,
                "qtyAdded": lineItmObj.qtyAdded,
                "qtyReq": lineItmObj.qtyReq,
                "backOrderQty": lineItmObj.backOrderQty,
                "packageQty": lineItmObj.packageQty,
                "checked": true,
                "shipToDateReq": new Date(lineItmObj.shipToDateReq).getTime(),
                "prices": [
                    {
                        "unitPrice": lineItmObj.unitPrice ? parseFloat(lineItmObj.unitPrice) : 0,
                        "extdPrice": (lineItmObj.qtyAdded ? lineItmObj.qtyAdded : 0 )*(lineItmObj.unitPrice ? lineItmObj.unitPrice :0 + lineItmObj.coreCharge ? lineItmObj.coreCharge :0) ,
                        "corePrice": lineItmObj.coreCharge ? parseFloat(lineItmObj.coreCharge) : 0,
                        "currency": lineItmObj.currency ? lineItmObj.currency : "USD"
                    }
                ]
            }];
            dataServices.updateCart(payload, lineItmObj.cartId).then(function (response) {
                if (response) {
                    vm.isDataLoading = false;
                } else {
                    vm.isDataLoading = false;
                }
            }, function (error) {
                vm.isDataLoading = false;
            });
        }
    }

    checkOutCalculation() {
        let vm = this;
        vm.totWgtOfParts = 0;
        vm.totWgtOfPartsInLb = 0;
        vm.sumOfTotal = 0;
        vm.totNoOfParts = 0;
        vm.totalOrderQTY=0;
        let i = 0;
        for (let part of vm.partdetails) {
            if(part.qtyReq) {
                i++;
                part.coreCharge = part.coreCharge ? part.coreCharge : 0;
                part.unitPrice = part.unitPrice ? part.unitPrice : 0;
                part.qtyReq = part.qtyReq ? part.qtyReq : 0;
                part.partWeight = part.partWeight? part.partWeight:0;
                
                if (part.wtUom == 'LB') {
                    vm.totWgtOfParts += Math.round(part.qtyReq * part.partWeight);
                } else {
                    vm.totWgtOfParts += Math.round(part.qtyReq * (part.partWeight / 0.4536));
                }                
                vm.totalOrderQTY+=part.qtyReq
                vm.sumOfTotal += (part.qtyReq * (part.unitPrice + part.coreCharge));
            }
        }
        vm.totNoOfParts = i;
        vm.totWgtOfPartsInLb = vm.totWgtOfParts;
    }


    open(obj) {
        let vm = this;
        vm.calanderSelect = true;
        for (let i of vm.cartData) {
            if (i.id == obj.id) {
                i.calanderSelect = true;
            } else {
                i.calanderSelect = false;
            }
        }
    }

    open1(index, $event) {
        let vm = this;
        if ($event) {
            $event.preventDefault();
            $event.stopPropagation(); // This is the magic
        }

        vm.partdetails[index].calanderSelect = true;
    }

    getCartData() {
        let vm = this;
        let orderType = "SO";
        let cartType = "express-cart";
        let operation = false;
        vm.cartQtyNotAllowed=false;
        let { dataServices, PricingService, $scope, $rootScope, authenticationService, $state, appInfoService} = vm.DI();
        //PricingService.orderType =  "STK" ;
        localStorage.checkedOutCartType = orderType.toString();
        dataServices.getCart(cartType, orderType).then(function (response) {
            let pciceRequest = [];
            let cartData = response.data;
            vm.isDataLoading = false;
            vm.totNoOfParts = cartData.lineItems ? cartData.lineItems.length : 0;
            for (let i = 0; i < cartData.lineItems.length; i++) {

                let temp = {
                    cartId: cartData.lineItems[i].id,
                    partNo: cartData.lineItems[i].partNo,
                    packageQty: cartData.lineItems[i].packageQty,
                    qtyReq: cartData.lineItems[i].qtyReq,
                    partDesc: cartData.lineItems[i].partName,
                    customerPartNumber: cartData.lineItems[i].customerPartNo,
                    uom: cartData.lineItems[i].uom,
                    wtUom: cartData.lineItems[i].wtUom,
                    unitPrice: cartData.lineItems[i].prices[0].unitPrice,
                    coreCharge: cartData.lineItems[i].prices[0].corePrice,
                    partWeight: cartData.lineItems[i].wt,
                    prodTotal: ((cartData.lineItems[i].prices[0].unitPrice) + (cartData.lineItems[i].prices[0].corePrice)) * cartData.lineItems[i].qtyReq,
                    productCat: cartData.lineItems[i].productCat,
                    avlQty: (cartData.lineItems[i].qtyReq - cartData.lineItems[i].backOrderQty),
                    //avlQty:verifyPartAvailbility(),
                    backOrderQty: cartData.lineItems[i].backOrderQty,
                    qtyAdded: cartData.lineItems[i].qtyAdded,
                    //shipToDate: cartData.lineItems[i].shipToDateReq,
                    shipToDateReq: vm.validatePastDate(cartData.lineItems[i].shipToDateReq),
                    
                    isNonEditable: true,
                    checked: false,
                    isPartNoValid: false,
                    isQtyInputDisable: false,
                    qtyStatus: cartData.lineItems[i].backOrderQty ? 'partialQtyAvl' : 'fullQtyAvl',
                    isQtyStatusIconShow: true,
                    isBackOrderCheckbox: true,
                    isBackOrder: cartData.lineItems[i].backOrderQty ? true : false,
                    calanderSelect: false
                }

                vm.partdetails.unshift(temp);
                let priceData = {
                    "uiId": cartData.lineItems[i].id,
                    "customerCid": authenticationService.shipToSoldToInfo.selectedSoldTo ? authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber : vm.customerId,
                    "partNumber": cartData.lineItems[i].partNo,
                    "orderType": "STK",
                    "quantityReq": cartData.lineItems[i].qtyReq,
                    "unitOfMeasure": null
                }

                pciceRequest.push(priceData);
            }//end for loop

            let payload = {
                "pricingReqDtlList": pciceRequest
            };
            dataServices.getCartPrice(payload).then(function (response) {
                let data = response.data;
                for (let i = 0; i < data.length; i++) {
                    if (data[i].partNumber == vm.partdetails[i].partNo) {
                        vm.partdetails[i].unitPrice = data[i].partPriceDtl.priceDtlMap.STANDARD_BASE;
                        vm.partdetails[i].coreCharge = data[i].partPriceDtl.priceDtlMap.CORE_BASE;
                    }

                }

            });
             vm.checkOutCalculation();
           
        });
    }

    validatePastDate(inputDate) {
        let dateStr = "";
        if (inputDate) {
            let selectedDate = new Date(inputDate);
            var now = new Date();
            if (selectedDate < now) {
                dateStr = now;
            } else {
                dateStr = inputDate;
            }

        }
        return dateStr;
    }

    checkout() {
        let vm = this;
        let {  $state, PricingService, dataServices } = vm.DI();
        let partLength = vm.partdetails.length;
        let i = 0;
    
        PricingService.orderType = 'STK';
        PricingService.sumOfTotal = vm.sumOfTotal;
        PricingService.noOfParts = vm.totNoOfParts;
        PricingService.totWgtOfPartsInKg = vm.totWgtOfParts;
        PricingService.totWgtOfPartsInLb = vm.totWgtOfPartsInLb;
        PricingService.totalOrderQTY = vm.totalOrderQTY;
        PricingService.cartType = "express-cart";
        vm.isDataLoading = true;
        let checkoutData = [];
        let payload = [];
        for (let obj of vm.partdetails) {

            Object.assign(obj, { "qtyAvailable": false });
            Object.assign(obj, { "partAvailable": false });
            Object.assign(obj, { "totalQtyAvailable": 0 });
            if (true) {
                checkoutData.push(obj);
            }

            let temp = {
                "id": obj.cartId,
                "qtyAdded": obj.qtyAdded,
                "qtyReq": obj.qtyReq,
                "backOrderQty": obj.backOrderQty ? obj.backOrderQty : 0,
                //"checked": obj.unitPrice ? true : false,
                "checked": true,
                "uom": obj.uom,
                "wt": obj.partWeight,
                "wtUom": "LB",
                "shipToDateReq": new Date(obj.shipToDateReq).getTime(),
                "prices": [
                    {
                        "type": obj.prices ? obj.prices[0].type : "",
                        "unitPrice": obj.unitPrice,
                        "extdPrice": obj.qtyReq * obj.unitPrice,
                        "corePrice": obj.coreCharge,
                        "currency": obj.prices ? obj.prices[0].currency : ""
                    }
                ]
            };
            payload.push(temp);

            //iterate loop by less than 1 by length
            i++;
            if (i === (partLength - 1))
                break;

        }
        sessionStorage.checkoutData = angular.toJson(checkoutData);
        dataServices.updateCart(payload, 0).then(function (res) {
            if (res) {
                vm.isDataLoading = false;

            } else {
                vm.isDataLoading = false;
            }
        }, function (error) {
            vm.isDataLoading = false;
        });
        $state.go('previewOrder');

    }

    doBlur($event) {
        let target = $event.target;
        target.blur();
    }



}
