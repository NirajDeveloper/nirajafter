//Author: Niraj Kumar
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function autofocus($timeout) { 

    let directive ={
        restrict : 'A',
        link : function($scope, $element){
            debugger
            $timeout(function() {
            $element[0].focus();
          });
        }

    }

    return directive

} 

autofocus.$inject = ['$timeout'];

