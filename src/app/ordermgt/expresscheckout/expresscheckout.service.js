//Author: Niraj Kumar
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/
export class ExpressCheckOutService {
    constructor(dataServices, $http, $rootScope, $window, Session, AUTH_EVENTS, $q) {
        let vm = this;
        vm.prodUrl="http://localhost:4000/expressCheckout";
        vm.DI = () => ({
            dataServices, $http, $rootScope, $window, Session, AUTH_EVENTS, $q
        });

    }


     getPartDetails(prodtno) {
        let vm = this;
       
        let { dataServices,   $http, $q} = vm.DI();
        let defer=$q.defer();
        return $http.get(vm.prodUrl+'?partNumber='+prodtno)

        // dataServices.getWhereToBuyData(payload).then(function (response) {
        //     callback(response);
        // });
     }
 
}

ExpressCheckOutService.$inject = ['dataServices', '$http','$rootScope' , '$window', 'Session', 'AUTH_EVENTS', '$q'];
