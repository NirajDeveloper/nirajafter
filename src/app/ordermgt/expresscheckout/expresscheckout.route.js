//Author: Niraj Kumar
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/
export function routeConfig($stateProvider){
	'ngInject';
	$stateProvider
	.state('expresscheckout',{
		url: '/expresscheckout',
		parent: 'aftermarket',
		templateUrl: 'app/ordermgt/expresscheckout/expresscheckout.html',
		controller: 'ExpressCheckOutController',
		controllerAs: 'expco',
	});
}