/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

export class OrdermgtController {
    constructor($log, $uibModal, dataServices, $state, OrderlistModalFactory, $timeout, $scope, $rootScope, $window, PricingService, authenticationService, $stateParams, $document, $filter, appInfoService) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $uibModal, $log, dataServices, $state, OrderlistModalFactory, $timeout, $scope, $rootScope, $window, PricingService, authenticationService, $stateParams, $document, $filter, appInfoService });
        $window.scrollTo(0, 0);
        vm.format = 'MM/dd/yyyy';
        vm.dateOptions = {
            showWeeks: false
        };
        vm.noOfSelectedParts = 0;
        vm.imageBaseUrl = "";
        if (appInfoService.appInfo && appInfoService.appInfo.cdnBaseurl) {
              vm.imageBaseUrl = appInfoService.appInfo.cdnBaseurl;
        }
        vm.dateOptions.minDate = vm.dateOptions.minDate ? null : new Date();

        let reqOrderType = 'EO';
        if(($rootScope.fromState == "previewOrder" || $rootScope.fromState == "incompleteOrder") && $state.params && !$state.params.isOrderPlaced){
            if(localStorage.checkedOutCartType && localStorage.checkedOutCartType == "EO"){
                reqOrderType = localStorage.checkedOutCartType;
            }else{
                reqOrderType = "SO";
            }
            vm.init(reqOrderType);
        }else{
            $scope.$emit("showLoading", true);
            dataServices.cartCount().then(function (response) {
                if(response && response.data && response.data.itemLevel){
                    vm.soCount = response.data.itemLevel.SO ? response.data.itemLevel.SO : 0;
                    vm.eoCount = response.data.itemLevel.EO ? response.data.itemLevel.EO : 0;
                }else{
                    vm.soCount = 0;
                    vm.eoCount = 0;
                }
                if(vm.eoCount > 0 || (vm.eoCount == vm.soCount)){
                    reqOrderType = 'EO';
                }else{
                    reqOrderType = 'SO';
                }
                vm.init(reqOrderType);
                $scope.$emit("showLoading", false);
            }, function (error) {
                vm.init(reqOrderType);
                $scope.$emit("showLoading", false);
            });
        }
        // PricingService.orderType = PricingService.orderType ? PricingService.orderType : "STK";
        // //let reqOrderType = PricingService.orderType=='STK' ? 'SO' : 'EO';
        // vm.activeTab = '';
        // // do not persist data if tab switching happens
        // vm.initialActvTab = reqOrderType;
        // vm.isTabSwitchedOnce = false;
        // if($rootScope.fromState && ($rootScope.fromState === 'previewOrder' || $rootScope.fromState === 'incompleteOrder') && PricingService.isCartPersisted){
        //     vm.isCheckedPersisted = true;
        // }else{
        //     vm.isCheckedPersisted = false;
        // }
        // if(PricingService.isCartPersisted){
        //     vm.isCheckedPersisted = true;
        // }else{
        //     vm.isCheckedPersisted = false;
        // }
        // vm.getCartData(reqOrderType);
        // vm.sortType = 'partNo';
        // vm.sortReverse = false;
        // vm.orderType = PricingService.orderType;
        // vm.cartData = [];
        // vm.isDataLoading = false;
        // vm.selected = undefined;
        // vm.bad400 = false;
        // vm.isDirty = false;
        // vm.date = new Date(); 
        // vm.userPermissionsList = authenticationService.userPermissionsList;
        // let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
        //     vm.userPermissionsList = authenticationService.userPermissionsList;
        // });

        // vm.customerId = null;
        
        // if(authenticationService.shipToSoldToInfo.selectedSoldTo && authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber){
        //     vm.customerId = authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber;
        // }
        // else if(authenticationService.associatedCustData.activeCustId){
        //     vm.customerId = authenticationService.associatedCustData.activeCustId;
        // }

        // vm.stickyAd = false;
        // let deregistrationCallback = $rootScope.$on("isHeaderSticky", (evt, isHeaderSticky) => {
        //     let adSec = $document[0].getElementById("ad-section1");
        //     if (isHeaderSticky.state) {
        //         vm.stickyAd = true;
        //         if (angular.isDefined(isHeaderSticky.bottomOffset)) {
        //             angular.noop();
        //         }
        //     } else {
        //         vm.stickyAd = false;
        //         angular.element(adSec).css("bottom", 'auto');
        //     }
        // });

        // $scope.$on("$destroy", deregistrationCallback);

        // vm.getCartCount();
    }

    init(reqOrderType){
        let vm = this;
        let  { $log, $scope, $rootScope, $document, authenticationService, PricingService } = vm.DI();
        PricingService.orderType = reqOrderType == "SO" ? "STK" : "EMG";
        vm.activeTab = '';
        vm.initialActvTab = reqOrderType;
        vm.isTabSwitchedOnce = false;
        if($rootScope.fromState && ($rootScope.fromState === 'previewOrder' || $rootScope.fromState === 'incompleteOrder') && PricingService.isCartPersisted){
            vm.isCheckedPersisted = true;
        }else{
            vm.isCheckedPersisted = false;
        }
        if(PricingService.isCartPersisted){
            vm.isCheckedPersisted = true;
        }else{
            vm.isCheckedPersisted = false;
        }
        vm.getCartData(reqOrderType);
        vm.sortType = 'partNo';
        vm.sortReverse = false;
        vm.orderType = PricingService.orderType;
        vm.cartData = [];
        vm.isDataLoading = false;
        vm.selected = undefined;
        vm.bad400 = false;
        vm.isDirty = false;
        vm.date = new Date(); 
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });

        vm.customerId = null;
        
        if(authenticationService.shipToSoldToInfo.selectedSoldTo && authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber){
            vm.customerId = authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber;
        }
        else if(authenticationService.associatedCustData.activeCustId){
            vm.customerId = authenticationService.associatedCustData.activeCustId;
        }

        vm.stickyAd = false;
        let deregistrationCallback = $rootScope.$on("isHeaderSticky", (evt, isHeaderSticky) => {
            let adSec = $document[0].getElementById("ad-section1");
            if (isHeaderSticky.state) {
                vm.stickyAd = true;
                if (angular.isDefined(isHeaderSticky.bottomOffset)) {
                    angular.noop();
                }
            } else {
                vm.stickyAd = false;
                angular.element(adSec).css("bottom", 'auto');
            }
        });

        $scope.$on("$destroy", deregistrationCallback);

        //vm.getCartCount();
    }

    getCartCount(){
        let vm = this;
        let  { $log, $state, PricingService, dataServices } = vm.DI();
        dataServices.cartCount().then(function (response) {
                vm.soCount = response.data.itemLevel ? response.data.itemLevel.SO : 0;
                vm.eoCount = response.data.itemLevel ? response.data.itemLevel.EO : 0;
        }, function (error) {

        });
    }

    open2(obj) {
        let vm = this;
        let { $log } = vm.DI();
        vm.calanderSelect = true;
        for (let i of vm.cartData) {
            if(i.id == obj.id){
                i.calanderSelect = true;
            }else{
                i.calanderSelect = false;
            }
        }
    }
     open3(index, $event) {
        let vm = this;
        if ($event) {
            $event.preventDefault();
            $event.stopPropagation(); // This is the magic
        }

        vm.cartData[index].calanderSelect = true;
    }
    getCartData(orderType = "EO", cartType = "normal-cart", operation = false, isFromEdit = false){
        let vm = this;
        vm.noOfSelectedParts = 0;
        vm.isPartRemove = false;
        //vm.isAllCartItmChecked = false; no need to make it false, we are doing it later
        if(vm.activeTab == orderType && operation){
        //    vm.activeTab =  orderType == 'SO' ? 0 : 1;

        }else{
            let { $log, $uibModal, dataServices, PricingService, $scope, $rootScope ,authenticationService, $filter, $state, appInfoService} = vm.DI();
            localStorage.checkedOutCartType = orderType.toString();
            vm.cartQtyNotAllowed = false;
            if(vm.isTabSwitchedOnce && !isFromEdit){
                vm.initialActvTab = orderType;
                vm.isCheckedPersisted = true;
            }
            if(vm.initialActvTab != orderType && !isFromEdit){
                vm.isCheckedPersisted = false;
                vm.isTabSwitchedOnce = true;
            }
            PricingService.orderType = orderType == "SO" ? "STK" : "EMG";
            vm.activeTab = orderType;
            // persist data on switching tab
            // vm.activeTab =  orderType == 'SO' ? 0 : 1;
            vm.orderType = PricingService.orderType;
            $scope.$emit("showLoading", true);
            vm.getCartCount();
            if(vm.isTabSwitchedOnce && vm.isCheckedPersisted && !isFromEdit){ // update cartData locally
            //if(vm.isTabSwitchedOnce && !isFromEdit){ // update cartData locally
                if(orderType == "SO"){
                    vm.cartData = angular.copy(PricingService.stkCartData);
                    vm.priceResponse = angular.copy(PricingService.stkPriceData);
                    if(vm.cartData && vm.cartData.length > 0){
                        vm.calculateSummaryDtls('SO');
                        vm.getCart = true;
                    }else{
                         vm.getCart = false;
                        
                    }
                   
                    $scope.$emit("showLoading", false);
                    return;
                }else{
                    vm.cartData = angular.copy(PricingService.emgCartData);
                    vm.priceResponse = angular.copy(PricingService.emgPriceData);
                    if(vm.cartData && vm.cartData.length > 0){
                        vm.calculateSummaryDtls('EO');
                        vm.getCart = true;
                    }else{
                          vm.getCart = false;
                    }
                    $scope.$emit("showLoading", false);
                    return;
                }
            }
            dataServices.getCart(cartType,orderType).then(function (response) {
                let pciceRequest = [];
                if (appInfoService.appInfo && appInfoService.appInfo.cdnBaseurl) {
                    vm.imageBaseUrl = appInfoService.appInfo.cdnBaseurl;
                }
                if(response && response.data){
                    vm.cartData = response.data.lineItems;
                }else{
                    // or catch exception here
                    vm.cartData = [];
                }

                if(vm.cartData && vm.cartData.length > 0){
                    vm.getCart = true;
                }else{
                    vm.getCart = false;
                    $scope.$emit("showLoading", false);
                    return;
                }
                for (let x of vm.cartData){
                    // if(isFromEdit){
                    //     if(PricingService.eoItemSelection.indexOf(x.id) !== -1){
                    //         //Object.assign(x,{"checked": true});
                    //         x.checked = true;
                    //     }
                    //     else{
                    //         //Object.assign(x,{"checked": false});
                    //          x.checked = false;
                    //     }
                    // }
                    x.checked = true;
                    x.deletechecked = false;
                    let temp = {
                        "uiId": x.id,
                        "customerCid": authenticationService.shipToSoldToInfo.selectedSoldTo ? authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber : vm.customerId,
                        "partNumber":x.partNo,
                        "orderType":PricingService.orderType,
                        "quantityReq":x.qtyReq,
                        "unitOfMeasure": null
                    }
                    pciceRequest.push(temp);
                    Object.assign(x,{"cartId":response.data.id});
                    Object.assign(x, {"STANDARD_BASE": 0});
                    Object.assign(x, {"CORE_BASE": 0});
                    Object.assign(x, {"calanderSelect": false});
                };

                let payload = {
                    "pricingReqDtlList" : pciceRequest
                };
                vm.sumOfTotal = 0;
                vm.totWgtOfPartsInKg = 0;
                PricingService.sumOfTotal = vm.sumOfTotal;
                PricingService.totWgtOfPartsInKg = vm.totWgtOfPartsInKg;
                PricingService.noOfParts = vm.cartData.length;
                PricingService.totalOrderQTY = vm.totalOrderQTY;
                dataServices.getCartPrice(payload).then(function(priceRes){
                    // if(priceRes){
                    //     vm.priceResponse = priceRes.data;                      
                        
                    //     if(priceRes.status == 500 || priceRes.status == 400){
                    //         $scope.$emit("showLoading", false);
                    //         $state.go('poError');
                    //     }
                    //     if(vm.priceResponse){
                    //         //vm.calculateSummaryDtls(vm.isCheckedPersisted, vm.activeTab);
                    //         vm.calculateSummaryDtls( vm.activeTab);
                    //     }else{
                    //         $scope.$emit("showLoading", false);
                    //     }
                    // }else{
                    //     $scope.$emit("showLoading", false);
                    //     $state.go('poError');
                    // }
                    if(priceRes.data){
                        vm.priceResponse = priceRes.data; 
                    }else{
                        vm.priceResponse = {};
                    }
                    vm.calculateSummaryDtls( vm.activeTab);
                    $scope.$emit("showLoading", false);

                }, function(error){
                    console.log("in erroe", typeof(vm.priceResponse));
                    vm.priceResponse = {};
                    vm.calculateSummaryDtls(vm.activeTab);
                    $scope.$emit("showLoading", false);
                });
            
            }, function (error) {
                vm.getCart = false;
                $scope.$emit("showLoading", false);
            });
        }
    }

    shippedList() {
        let vm = this,
            size = "md";
        let { $log, $uibModal } = vm.DI();
        var modalInstance = $uibModal.open({
            templateUrl: 'app/ordermgt/processpopup/proceedmsg.html',
            size: size,
            controller: 'ShippedListEdit',
            controllerAs: 'listedit',
            windowClass: 'my-modal-popup',
            resolve: {
                items: function() {
                    return vm.items;
                }
            }
        });
    }
    saveData() {
        let vm = this;
        vm.checkout(false);
    }
    checkout(flag){
        let vm = this;
        let  { $log, $state, $scope, PricingService, dataServices } = vm.DI();

        //PricingService.checkoutData = [];
        vm.isDataLoading = true;
        //PricingService.noOfParts = vm.cartData.filter(vm.sumOfCheckedParts).length;
        PricingService.noOfParts = vm.cartData.length;
        PricingService.cartType='';
        let checkoutData = [];
        let payload = [];
        for(let obj of vm.cartData){
            if(1){
                Object.assign(obj, {"qtyAvailable":false});
                Object.assign(obj, {"partAvailable":false});
                Object.assign(obj, {"totalQtyAvailable":0});
                //if(obj.checked){
                    checkoutData.push(obj);
                //}
               
                let temp  = {
                   "id": obj.id,
                   "qtyAdded": obj.qtyAdded,
                   "qtyReq": obj.qtyReq,
                   "backOrderQty":obj.backOrderQty? obj.backOrderQty : 0,
                   //"checked":obj.checked,
                   "checked":true,
                   "uom":null,
                   "shipToDateReq": new Date(obj.shipToDateReq).getTime(),
                   "prices": [
                    {
                      "type": obj.prices ? obj.prices[0].type : "",
                      "unitPrice": obj.STANDARD_BASE,
                      "extdPrice": obj.qtyReq * obj.STANDARD_BASE,
                      "corePrice": obj.CORE_BASE,
                      "currency": obj.prices ? obj.prices[0].currency: ""
                    }
                  ]
                };
                payload.push(temp);
            }
        } 
        sessionStorage.checkoutData = angular.toJson(checkoutData);
        dataServices.updateCart(payload, vm.cartData[0].cartId).then(function(res){
            if(res){
                vm.isDataLoading = false;
                if(flag){
                    $state.go('previewOrder')
                }
            }else{
                vm.isDataLoading = false;
            }
            if(PricingService.orderType == 'STK'){
                vm.isDirty = false;
            }
        },function(error){
            vm.isDataLoading = false;
        });
        
    }
    process() {
        let vm = this,
            size = "md";
        let { $log, $uibModal } = vm.DI();
        var modalInstance = $uibModal.open({

            templateUrl: 'app/ordermgt/processorderpopup/processorder.html',

            size: size,
            controller: 'ShippedListEdit',
            controllerAs: 'listedit',
            windowClass: 'my-modal-popup',
            resolve: {
                items: function() {
                    return vm.items;
                }
            }
        });
    }
    cancel() {
        let vm = this,
            { $uibModalInstance } = vm.DI();
        $uibModalInstance.close();
    }

    deleteCartItem(obj){
        let vm = this,
        { PricingService } = vm.DI();

        /*if(obj.deletechecked){
            vm.isPartRemove = true;
        }*/

        for(let y of vm.cartData){
           if(y.deletechecked){
                vm.isPartRemove = true;
                break;
           }else{
                vm.isPartRemove = false;
           }
        }
        for (let x of vm.cartData){
            if(x.cartId!=0){
               if(x.deletechecked){
                vm.isAlldeleteCartItmChecked = true;
            }else{
                vm.isAlldeleteCartItmChecked = false;
                break;
                }
            }
        }

        if(PricingService.orderType == "STK"){
            PricingService.stkCartData = angular.copy(vm.cartData);
        }else{
            PricingService.emgCartData = angular.copy(vm.cartData);
        }
    }

    selectCartItem(orderType){
        let vm = this,
            { PricingService } = vm.DI();
        vm.sumOfTotal = 0;
        vm.totWgtOfPartsInKg = 0;
        vm.totalOrderQTY=0;
        for (let i of vm.cartData) {
            //if(i.checked){
                //vm.totWgtOfPartsInKg += i.wt;
                if(i.wt){
                    if(i.wtUom == 'LB'){
                        i.weight = i.qtyReq * i.wt;
                    }
                    else{
                        i.weight = i.qtyReq * (i.wt / 0.4536);
                    }
                    vm.totWgtOfPartsInKg += Math.round(i.weight); 
                }
                //vm.totWgtOfPartsInKg += i.weight ? i.weight : 0;
                //console.log("i.weight ", i.weight);
                if(PricingService.orderType == 'STK'){
                    vm.sumOfTotal +=  i.qtyReq * (i.STANDARD_BASE+ i.CORE_BASE);
                    vm.totalOrderQTY+=i.qtyReq;
                }else{
                    vm.sumOfTotal += i.qtyReq * ( i.STANDARD_BASE+ i.CORE_BASE);
                    // vm.totalOrderQTY+=i.qtyAdded;
                    vm.totalOrderQTY+=i.qtyReq;
                }
                //vm.updateSelectionArray(orderType, i.id, 'Add');
            // }
            // else {
            //     vm.updateSelectionArray(orderType, i.id, 'Delete');
                
            // }
        }
        PricingService.sumOfTotal = vm.sumOfTotal;
        PricingService.totWgtOfPartsInKg = vm.totWgtOfPartsInKg;
        PricingService.totalOrderQTY = vm.totalOrderQTY;

        // for (let x of vm.cartData){
        //     if(x.checked){
        //         vm.isAllCartItmChecked = true;
        //     }else{
        //         vm.isAllCartItmChecked = false;
        //         break;
        //     }
        // }
        //vm.noOfSelectedParts = vm.cartData.filter(vm.sumOfCheckedParts).length;
        vm.noOfSelectedParts = vm.cartData.length;
        if(orderType == "SO"){
            PricingService.stkCartData = angular.copy(vm.cartData);
        }else{
            PricingService.emgCartData = angular.copy(vm.cartData);
        }
    }

    // sumOfCheckedParts(v){
    //     return v.checked == true;
    // }

    sumOfDeleteCheckedParts(v){
        return v.deletechecked == true;
    }

    // getCartItemById(v){
    //     return v.checked == true;
    // }
    pushToSelectionArray(orderType, id){
         let vm = this,
            { PricingService } = vm.DI();
        if(orderType === 'SO') {
            PricingService.stkItemSelection.push(id); 
        }
        else {
            PricingService.eoItemSelection.push(id);
        }
    }
    // updateSelectionArray(orderType, id, action){
    //     let vm = this,
    //         { PricingService } = vm.DI();
    //     if(orderType === 'SO') {
    //         if(PricingService.stkItemSelection.indexOf(id) !== -1){
    //             if(action === 'Delete'){
    //                 PricingService.stkItemSelection.splice(PricingService.stkItemSelection.indexOf(id), 1);
    //             }
    //         } else {
    //             if(action === 'Add'){
    //                 PricingService.stkItemSelection.push(id);
    //             }
    //         } 
    //     }
    //     else {
    //         if(PricingService.eoItemSelection.indexOf(id) !== -1){
    //             if(action === 'Delete'){
    //                 PricingService.eoItemSelection.splice(PricingService.eoItemSelection.indexOf(id), 1);
    //             }
    //         } else {
    //             if(action === 'Add'){
    //                 PricingService.eoItemSelection.push(id);
    //             }
    //         } 
    //     }
    // }

    selectAllDeleteCartItem(orderType){
        let vm = this,
            { PricingService } = vm.DI();
        if(vm.isAlldeleteCartItmChecked){
            vm.isPartRemove = true;
            if(orderType === 'SO') {
                PricingService.stkItemSelection = [];
            }
            else {
                PricingService.eoItemSelection = [];
            }
            for (let item of vm.cartData) {
                item.deletechecked = true;
                vm.pushToSelectionArray(orderType, item.id);
            }
        }else{
            vm.isPartRemove = false;
            if(orderType === 'SO') {
                PricingService.stkItemSelection = [];
            }
            else {
                PricingService.eoItemSelection = [];
            }
            for (let item of vm.cartData) {
                item.deletechecked = false;
            }
        }

        if(PricingService.orderType == "STK"){
            PricingService.stkCartData = angular.copy(vm.cartData);
        }else{
            PricingService.emgCartData = angular.copy(vm.cartData);
        }
    }
    
    selectAllCartItem(orderType){
        let vm = this,
            { PricingService } = vm.DI();
        // if(vm.isAllCartItmChecked){
        //     if(orderType === 'SO') {
        //         PricingService.stkItemSelection = [];
        //     }
        //     else {
        //         PricingService.eoItemSelection = [];
        //     }
        //     for (let item of vm.cartData) {
        //         item.checked = true;
        //         vm.pushToSelectionArray(orderType, item.id);
        //     }
        // }else{
        //     if(orderType === 'SO') {
        //         PricingService.stkItemSelection = [];
        //     }
        //     else {
        //         PricingService.eoItemSelection = [];
        //     }
        //     for (let item of vm.cartData) {
        //         item.checked = false;
        //     }
        // }

        vm.sumOfTotal = 0;
        vm.totWgtOfPartsInKg = 0;
        vm.totalOrderQTY=0;
        for (let i of vm.cartData) {
             i.checked = true;
            //if(i.checked){
                vm.totWgtOfPartsInKg += i.wt;
                if(PricingService.orderType == 'STK'){
                    vm.sumOfTotal +=  i.qtyReq * (i.STANDARD_BASE+ i.CORE_BASE);
                    vm.totalOrderQTY+= i.qtyReq;
                }else{
                    vm.sumOfTotal += i.qtyReq * ( i.STANDARD_BASE+ i.CORE_BASE);
                    // vm.totalOrderQTY+= i.qtyAdded;
                    vm.totalOrderQTY+= i.qtyReq;
                }
           // }
        }
        PricingService.sumOfTotal = vm.sumOfTotal;
        PricingService.totWgtOfPartsInKg = vm.totWgtOfPartsInKg;
        PricingService.totalOrderQTY = vm.totalOrderQTY;
        //vm.noOfSelectedParts = vm.cartData.filter(vm.sumOfCheckedParts).length;
        vm.noOfSelectedParts = vm.cartData.length;
        if(orderType == "SO"){
            PricingService.stkCartData = angular.copy(vm.cartData);
        }else{
            PricingService.emgCartData = angular.copy(vm.cartData);
        }

    }

    editItem(editObj){
        let vm = this,size = "md";
        let { $log, $scope, OrderlistModalFactory, $timeout, $uibModal, PricingService } = vm.DI();
        vm.selectedObj = editObj;
        
        PricingService.qtyRequired = PricingService.orderType == 'STK' ? editObj.qtyReq : editObj.qtyReq;
        PricingService.partNumber = editObj.partNo;
        PricingService.partName = editObj.partName;
        PricingService.packageQty = editObj.packageQty;
        PricingService.partToUpdateId = editObj.id;
        PricingService.cartId = editObj.cartId;
        PricingService.plantCode = editObj.plantCode;
        PricingService.catPath = editObj.productCat;      
        PricingService.uom = editObj.uom;
        PricingService.wt = editObj.wt;
        PricingService.wtUom = editObj.wtUom;
        PricingService.userType = 'RTB'
       // vm.updateSelectionArray(PricingService.orderType, editObj.id, 'Add');
        var modalInstance = $uibModal.open({
            templateUrl: 'app/pricing/availability/orderTypeSelection.html',
            size: 'md',
            controller: 'orderTypeSelectionController',
            controllerAs: 'orderTypeCtrl',
            windowClass: 'my-modal-popup editAvailabilityOverlay',
           resolve: {
            mode: function() {
              return 'edit';
            }
          }
        });

        ///let urlModalInstance = $timeout(OrderlistModalFactory.open("md", "app/pricing/availability/orderTypeSelection.html", "orderTypeSelectionController", "orderTypeCtrl", "availabilityOverlay", true, "edit"), 2000);
        modalInstance.result.then(function (operation) {
        //    vm.isCheckedPersisted = false;
        //    vm.isTabSwitchedOnce = false;
           operation == 'Edit' ? vm.getCartData(PricingService.orderType == 'STK' ? 'SO' : 'EO', "normal-cart", false, true) : "";
           
        }, function () {
             
        });
    }

    deleteSelectedItems(orderType){
        let vm = this,
        {
            dataServices, $state, PricingService, $scope, $timeout
        } = vm.DI();

        vm.isPartRemove = false;
        for(let obj of vm.cartData){
            if(obj.deletechecked){
                obj.deleted = true;
                dataServices.deleteCart(obj.id).then(function(response){
                    if(response){
                        vm.cartData = vm.cartData.filter(function(v){return v.id !== obj.id;});
                        //vm.cartData.splice(index, 1);
                        if(PricingService.orderType == "STK"){
                            PricingService.stkCartData = angular.copy(vm.cartData);
                        }else{
                            PricingService.emgCartData = angular.copy(vm.cartData);
                        }
                        if(PricingService.orderType == 'STK'){
                            vm.soCount = vm.cartData.length;
                        }else{
                            vm.eoCount = vm.cartData.length;
                        }
                        if(vm.cartData && vm.cartData.length > 0){
                            vm.getCart = true;
                            vm.selectCartItem();
                        }else{
                            vm.getCart = false;
                            return;
                        }
                        $scope.$emit("gotCartCount");
                    }
                });
            }
        }
    }

    deleteItem(obj, index, orderType){
        let vm = this,
        {
            dataServices, $state, PricingService, $scope, $timeout
        } = vm.DI();
        obj.deleted = true;
        dataServices.deleteCart(obj.id).then(function(response){
            if(response){
                vm.cartData = vm.cartData.filter(function(v){return v.id !== obj.id;});
                //vm.cartData.splice(index, 1);
                if(orderType == "SO"){
                    PricingService.stkCartData = angular.copy(vm.cartData);
                }else{
                    PricingService.emgCartData = angular.copy(vm.cartData);
                }
                if(PricingService.orderType == 'STK'){
                    vm.soCount = vm.cartData.length;
                }else{
                    vm.eoCount = vm.cartData.length;
                }
                if(vm.cartData && vm.cartData.length > 0){
                    vm.getCart = true;
                    vm.selectCartItem();
                }else{
                    vm.getCart = false;
                    return;
                }

                /*else{
                    vm.getCartData(PricingService.orderType == 'STK' ? 'SO' : 'EO', "normal-cart", false);
                }*/
                $scope.$emit("gotCartCount");
            } 
        },function(error){
            // display error if delete api fails
        });

       /* $timeout(() => {
            
        }, 0);*/
    }

    calculateSummaryDtls(orderType){
        let vm = this;
        let { $log, PricingService, $scope, $rootScope , $filter, $state} = vm.DI();
        // This calculation happens when user land from preview order/incomplete order/ update qty

        //if(isCheckedPersisted){
            //vm.noOfSelectedParts = vm.cartData.filter(vm.sumOfCheckedParts).length;
            vm.noOfSelectedParts = vm.cartData.length;
            let totalPartsInCart = vm.cartData.length;
            vm.isAlldeleteCartItmChecked = false;
            // if(totalPartsInCart == vm.noOfSelectedParts){
            //     vm.isAllCartItmChecked = true;
            // }else{
            //     vm.isAllCartItmChecked = false;
            // }

            // if(totalPartsInCart == vm.cartData.filter(vm.sumOfDeleteCheckedParts).length){
            //     vm.isAlldeleteCartItmChecked = true;
            // }
            // else{
            //     vm.isAlldeleteCartItmChecked = false;
            // }
        // }else{
        //     vm.noOfSelectedParts = 0;
        //     vm.isAllCartItmChecked = false;
        //     vm.isAlldeleteCartItmChecked = false;
        // }
        vm.sumOfTotal = 0;
        vm.totWgtOfPartsInKg = 0;
        vm.totalOrderQTY=0;
        PricingService.sumOfTotal = vm.sumOfTotal;
        PricingService.totWgtOfPartsInKg = vm.totWgtOfPartsInKg;
        PricingService.totalOrderQTY = vm.totalOrderQTY;
        for(let i = 0; i < vm.cartData.length; i++){
            vm.cartData[i].checked = true;
            vm.cartData[i].deletechecked = false;
            // if(!isCheckedPersisted){
            //     vm.cartData[i].checked = false;
            //     if(orderType == 'SO'){
            //         if(PricingService.stkItemSelection && PricingService.stkItemSelection.length >0) {
            //             if(PricingService.stkItemSelection.indexOf(vm.cartData[i].id) !== -1){
            //                 vm.cartData[i].checked = true;
            //             }
            //         }                   
            //     }else {
            //         if(PricingService.eoItemSelection && PricingService.eoItemSelection.length >0) {
            //             if(PricingService.eoItemSelection.indexOf(vm.cartData[i].id) !== -1){
            //                 vm.cartData[i].checked = true;
            //             }
            //         }   
            //     }
            // }
            Object.assign(vm.cartData[i], {"deleted": false});
            //vm.cartData[i].checked ? vm.totWgtOfPartsInKg += vm.cartData[i].wt : vm.totWgtOfPartsInKg +=0;           
            
            vm.totalOrderQTY += (vm.cartData[i].qtyAdded+(vm.cartData[i].backOrderQty ? vm.cartData[i].backOrderQty: 0));
            //Object.assign(vm.cartData[i], {"weight": vm.cartData[i].wtUom == 'LB' ? vm.cartData[i].qtyReq * vm.cartData[i].wt : vm.cartData[i].qtyReq * (vm.cartData[i].wt / 0.4536)});
            if(vm.cartData[i].wt){
                if(vm.cartData[i].wtUom == 'LB'){
                    vm.cartData[i].weight = vm.cartData[i].qtyReq * vm.cartData[i].wt;
                }
                else{
                    vm.cartData[i].weight = vm.cartData[i].qtyReq * (vm.cartData[i].wt / 0.4536);
                }
                vm.totWgtOfPartsInKg += Math.round(vm.cartData[i].weight); 
            }
            
            if(vm.cartData[i].shipToDateReq instanceof Date){
            }else{
                let actualDate = vm.cartData[i].shipToDateReq;
                let currentDate = new Date().getTime();
               vm.cartData[i].shipToDateReq =  (actualDate > currentDate) ? new Date(parseInt(vm.cartData[i].shipToDateReq)): new Date();
            }
            if(vm.cartData[i].refData && vm.cartData[i].refData.length){
                vm.cartData[i].refData[0].estAvlDate = vm.cartData[i].refData[0].estAvlDate ? new Date(parseInt(vm.cartData[i].refData[0].estAvlDate)) : "";
            }

            for(let j = 0; j < vm.priceResponse.length; j++){
                if(vm.cartData[i].id == vm.priceResponse[j].uiId){
                    if(vm.priceResponse[j].errMsg){
                        Object.assign(vm.cartData[i], {"STANDARD_BASE": 0});
                        Object.assign(vm.cartData[i], {"CORE_BASE": 0});

                        if(vm.cartData[i].prices){
                            vm.cartData[i].prices[0].totalPrice = 0;
                            vm.cartData[i].prices[0].extdPrice = 0;
                            vm.cartData[i].prices[0].totalPrice += vm.cartData[i].prices[0].extdPrice + vm.cartData[i].prices[0].corePrice;
                        }
                        
                        if(j == (vm.priceResponse.length-1) && i == (vm.cartData.length-1)){
                            $scope.$emit("showLoading", false);
                        }
                        break;
                    }else{
                        Object.assign(vm.cartData[i], {"STANDARD_BASE": vm.priceResponse[j].partPriceDtl.priceDtlMap.STANDARD_BASE? vm.priceResponse[j].partPriceDtl.priceDtlMap.STANDARD_BASE: 0});
                        Object.assign(vm.cartData[i], {"CORE_BASE": vm.priceResponse[j].partPriceDtl.priceDtlMap.CORE_BASE ? vm.priceResponse[j].partPriceDtl.priceDtlMap.CORE_BASE :0});
                        vm.sumOfTotal += (vm.cartData[i].STANDARD_BASE + vm.cartData[i].CORE_BASE) * (vm.cartData[i].qtyAdded + (vm.cartData[i].backOrderQty ? vm.cartData[i].backOrderQty: 0));
                        if(vm.cartData[i].prices){
                            vm.cartData[i].prices[0].totalPrice = 0;
                            vm.cartData[i].prices[0].extdPrice = vm.cartData[i].qtyReq * vm.cartData[i].STANDARD_BASE;
                            vm.cartData[i].prices[0].totalPrice += vm.cartData[i].prices[0].extdPrice + vm.cartData[i].prices[0].corePrice;    
                        }
                        if(j == (vm.priceResponse.length-1) && i == (vm.cartData.length-1)){
                            $scope.$emit("showLoading", false);
                        }
                        break;
                    }
                }
            }
        }
        // if(!isCheckedPersisted){
        //     if(orderType == 'SO'){
        //         if(PricingService.stkItemSelection && PricingService.stkItemSelection.length >0) {
        //             //vm.noOfSelectedParts = vm.cartData.filter(vm.sumOfCheckedParts).length;
        //             vm.noOfSelectedParts = vm.cartData.length;
        //             let totalPartsInCart = vm.cartData.length;
        //             if(totalPartsInCart == vm.noOfSelectedParts){
        //                 vm.isAllCartItmChecked = true;
        //             }else{
        //                 vm.isAllCartItmChecked = false;
        //             }
        //         }                   
        //     }else {
        //         if(PricingService.eoItemSelection && PricingService.eoItemSelection.length >0) {
        //             //vm.noOfSelectedParts = vm.cartData.filter(vm.sumOfCheckedParts).length;
        //             vm.noOfSelectedParts = vm.cartData.length;
        //             let totalPartsInCart = vm.cartData.length;
        //             if(totalPartsInCart == vm.noOfSelectedParts){
        //                 vm.isAllCartItmChecked = true;
        //             }else{
        //                 vm.isAllCartItmChecked = false;
        //             }
        //         }   
        //     } 
        // }
        PricingService.sumOfTotal = vm.sumOfTotal;
        PricingService.totWgtOfPartsInKg = vm.totWgtOfPartsInKg;
        PricingService.totalOrderQTY = vm.totalOrderQTY;
        
       // persist data on switching tab
        if(orderType == "SO"){
            PricingService.stkCartData = angular.copy(vm.cartData);
            PricingService.stkPriceData = angular.copy(vm.priceResponse);
        }else{
            PricingService.emgCartData = angular.copy(vm.cartData);
            PricingService.emgPriceData = angular.copy(vm.priceResponse);
        }
        //$scope.$emit("showLoading", false);
    }

    gotoPart(part) {
        let vm = this;
        let { $stateParams, $state } = vm.DI();
        
        let paramObj = { "id": part.partNo, "type": "partnum", "val":part.partNo };
        return $state.href("part", paramObj);
    }
    changeDate() {
        let vm = this;
        vm.isDirty = true;
    }

    onBlurOfQtyField(obj, fieldId) {
        let vm = this;
        let { $timeout } = vm.DI();
        let fieldVal = document.getElementById(fieldId).value;
        if(fieldVal == "0" || obj.qtyReq == undefined || obj.qtyReq <= 0){
            obj.qtyReq = obj.packageQty;
        }
        if(obj.packageQty >= 1 && obj.qtyReq > 0 && (obj.qtyReq % obj.packageQty != 0)){
            vm.cartQtyNotAllowed = true;
        }else{
            $timeout(function(){
                if(document.getElementsByClassName('qty-err').length > 0){
                    vm.cartQtyNotAllowed = true;
                }else{
                    vm.cartQtyNotAllowed = false;
                }
            }, 0);  
        }
    }

    // makeQtyPositive(qtyNtPositive, lineItmObj, fieldId){
    //     let vm = this;
    //     qtyNtPositive ? vm.qtyReqd = vm.packageQty: angular.noop();
    //     let fieldVal = document.getElementById(fieldId).value;
    //     if (lineItmObj.qtyReq > 0) {
    //         vm.cartQtyNotAllowed = false;
    //     }else{
    //         vm.cartQtyNotAllowed = true;
    //     }
    //     if(lineItmObj.packageQty > 1 && lineItmObj.qtyReq > 0 && (lineItmObj.qtyReq % lineItmObj.packageQty != 0)){
    //         vm.isQtyNtMultplOfPkQty = true;
    //     }else{
    //         vm.isQtyNtMultplOfPkQty = false;
    //     }
    // }

    updateLineItemQty(lineItmObj, index, orderType, fieldId){
        let vm = this,
        {
            dataServices, $state, PricingService, $scope, $rootScope, $timeout
        } = vm.DI();
        let fieldVal = document.getElementById(fieldId).value;
        if(fieldVal == "0"){
            lineItmObj.qtyReq = lineItmObj.packageQty;
        }
        if(!lineItmObj.qtyReq || lineItmObj.qtyReq <= 0  || (lineItmObj.packageQty >= 1 && lineItmObj.qtyReq > 0 && (lineItmObj.qtyReq % lineItmObj.packageQty != 0))){
            vm.cartQtyNotAllowed = true;
        }else{
            $timeout(function(){
                if(document.getElementsByClassName('qty-err').length > 0){
                    vm.cartQtyNotAllowed = true;
                }else{
                    vm.cartQtyNotAllowed = false;
                }
            }, 0);
        }
        lineItmObj.enable = true;
        if(lineItmObj.qtyReq && lineItmObj.qtyReq >= 1){
            let objToUpdate = lineItmObj;
            let backOrdQty = 0;
            vm.isDirty = true;
            if(lineItmObj.backOrderQty){
                backOrdQty = lineItmObj.qtyReq - lineItmObj.qtyAdded;
                backOrdQty > 0 ? backOrdQty : backOrdQty = 0;
            }
            vm.cartData[index].weight =  vm.cartData[index].qtyReq * (vm.cartData[index].wtUom == 'LB' ? vm.cartData[index].wt : vm.cartData[index].wt / 0.4536);
            vm.cartData[index].qtyAdded = backOrdQty ? lineItmObj.qtyAdded : lineItmObj.qtyReq;
            vm.cartData[index].backOrderQty = backOrdQty > 0 ? backOrdQty : 0;

            // vm.cartData.filter(function(v){return v.id == lineItmObj.id;})[0].qtyAdded = payload[0].qtyAdded;
            // vm.cartData.filter(function(v){return v.id == lineItmObj.id;})[0].backOrderQty = payload[0].backOrderQty;
            vm.selectCartItem(orderType);
            //console.log("vm.cartData[index].weight ", vm.cartData[index].weight, vm.cartData[index].qtyReq, vm.cartData[index].wt);
        } 
    }

    goToHome(){
        let vm = this,
        {
            dataServices, $state, PricingService, $scope, $rootScope
        } = vm.DI();

        $state.go('home');      
    }
    // updateLineItemQty(lineItmObj){
    //     let vm = this,
    //     {
    //         dataServices, $state, PricingService, $scope, $rootScope
    //     } = vm.DI();
    //     if(lineItmObj.qtyReq && lineItmObj.qtyReq >= 1){
    //         let objToUpdate = lineItmObj;
    //         let backOrdQty = 0;
    //         if(lineItmObj.backOrderQty){
    //             backOrdQty = lineItmObj.qtyReq - lineItmObj.qtyAdded;
    //             backOrdQty > 0 ? backOrdQty : backOrdQty = 0;
    //         }
    //         let payload = [{
    //             "id": lineItmObj.id,
    //             "qtyAdded": backOrdQty ? lineItmObj.qtyAdded : lineItmObj.qtyReq,
    //             "qtyReq": lineItmObj.qtyReq,
    //             "backOrderQty": backOrdQty,
    //             "checked":lineItmObj.checked,
    //             "shipToDateReq": new Date(lineItmObj.shipToDateReq).getTime(),
    //             "prices": [
    //                 {
    //                     "unitPrice": lineItmObj.STANDARD_BASE ? parseInt(lineItmObj.STANDARD_BASE) : 0,
    //                     "extdPrice": (lineItmObj.STANDARD_BASE && lineItmObj.qtyAdded && lineItmObj.backOrderQty) ? parseInt((lineItmObj.qtyAdded + lineItmObj.backOrderQty) * lineItmObj.STANDARD_BASE) : 0,
    //                     "corePrice": lineItmObj.CORE_BASE ? parseInt(lineItmObj.CORE_BASE) : 0,
    //                     "currency": lineItmObj.currency ? lineItmObj.currency : "USD"
    //                 }
    //             ]
    //         }];
    //         vm.cartData.filter(function(v){return v.id == lineItmObj.id;})[0].qtyAdded = payload[0].qtyAdded;
    //         vm.cartData.filter(function(v){return v.id == lineItmObj.id;})[0].backOrderQty = payload[0].backOrderQty;
    //         dataServices.updateCart(payload, lineItmObj.cartId).then(function(response){
    //             if(response){
    //                 console.log("close loading from update qty");
    //                 vm.calculateSummaryDtls(true);
    //             }else{
    //             }
    //         },function(error){
    //         });
    //     } 
    // }
}
