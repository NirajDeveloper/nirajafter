/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// Author: Manjesh Kumar
export class IncompleteOrder {
    constructor($document, $log, $scope, dataServices, $state, PricingService, $filter, $uibModal, $timeout, $q, appInfoService, authenticationService, $rootScope) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $document, $log, $state, dataServices, PricingService, $uibModal, $timeout, $q, $scope, appInfoService, authenticationService, $rootScope });
        
        if($rootScope.fromState == 'completedOrder'){
            $state.go('ordermgt');
            return;
        }
        //Remove
        //PricingService.orderType = 'EMG';

        // Hard coded value
        vm.orderId = "39KSKS930DLA0S80"
        vm.incompeteData = [];
        vm.backOrder = false;
        PricingService.orderType = localStorage.checkedOutCartType == "SO" ? "STK" : "EMG";
        vm.orderType = PricingService.orderType;
        vm.available = false;
        vm.checkoutData = [];
        vm.noItemsInCart = false;
        vm.totalAmount = 0;
        vm.imageBaseUrl = "";
        PricingService.isCartPersisted = true;
        vm.totWgtOfPartsInKg=PricingService.totWgtOfPartsInKg;
        vm.totalOrderQTY=PricingService.totalOrderQTY;
        if (appInfoService.appInfo && appInfoService.appInfo.cdnBaseurl) {
              vm.imageBaseUrl = appInfoService.appInfo.cdnBaseurl;
        }
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });
        vm.getData();

        vm.stickyAd = false;
        let deregistrationCallback = $rootScope.$on("isHeaderSticky", (evt, isHeaderSticky) => {
            let adSec = $document[0].getElementById("ad-section2");
            if (isHeaderSticky.state) {
                vm.stickyAd = true;
                if (angular.isDefined(isHeaderSticky.bottomOffset)) {
                    angular.noop();
                }
            } else {
                vm.stickyAd = false;
                angular.element(adSec).css("bottom", 'auto');
            }
            //console.log(vm.stickyAd);
        });
        //PricingService.orderType = 'EMG';
        //vm.orderType = 'EMG';
    }

    calculatePrice(){
        let vm = this,
        {
            $scope, $state, $timeout, dataServices
        } = vm.DI();
        let sum = 0;

        
        let checkoutData = angular.fromJson(sessionStorage.checkoutData);
            if(vm.orderType == 'STK'){
                for(let obj of vm.incompeteData){
                   // console.log("calculatePrice ", obj);
                    if(obj.partAvailable && !obj.qtyAvailable){
                        if(obj.radio == "backOrder"){
                            sum += (obj.totalQtyAvailable  + obj.backQty)* obj.STANDARD_BASE + obj.CORE_BASE;
                        }
                    }else{
                        sum += (obj.qtyAdded  + obj.backQty)* obj.STANDARD_BASE + obj.CORE_BASE;
                    }
                }
            }else{
                for(let obj of checkoutData){
                    sum += obj.totalQtyAvailable * obj.STANDARD_BASE + obj.CORE_BASE;
                }
            }
            vm.totalAmount = sum;
    }

    getData(){
        let vm = this,
        {
            $scope, $state, $timeout, dataServices
        } = vm.DI();
        let sum = 0;
        //$scope.$emit("showLoading", true);
        //console.log("getData");
        let checkoutData = angular.fromJson(sessionStorage.checkoutData);
        vm.checkoutDataLength = checkoutData ? checkoutData.length : 0;
        let temp = [];
        let promise = new Promise(function(resolve, reject) {
            let count = 0;
            if(vm.orderType == 'STK'){
                for(let obj of checkoutData){
                    count++;
                    if(obj.partAvailable && !obj.qtyAvailable){
                        temp.push(obj);
                        sum += (obj.totalQtyAvailable  + obj.backQty)* obj.STANDARD_BASE + obj.CORE_BASE;
                    }else{
                        sum += (obj.qtyAdded  + obj.backQty)* obj.STANDARD_BASE + obj.CORE_BASE;
                    }
                    obj.radio = 'backOrder';
                    checkoutData.length == count ? resolve() : angular.noop();
                }
            }else{
                //console.log("else ",checkoutData);
                for(let obj of checkoutData){
                    count++;
                    if(obj.partAvailable && !obj.qtyAvailable){
                        temp.push(obj);
                    }
                    sum += obj.totalQtyAvailable * obj.STANDARD_BASE + obj.CORE_BASE;
                    checkoutData.length == count ? resolve() : angular.noop();
                }
            }
        });

        promise.then(function() {
           $scope.$apply(function() {
                //console.log("vm.incompeteData ", vm.incompeteData);
                vm.incompeteData = temp;
                vm.totalAmount = sum;
            });
            $scope.$emit("showLoading", false);
        });

    }

    go(path){
    	let vm = this,
        {
            $scope, $state, PricingService
        } = vm.DI();
        //$state.go(path);

        if(PricingService.cartType=="express-cart"){
            $state.go('expresscheckout');
        }else{
            $state.go('ordermgt');
        }
    }

    deleteItem(id){
        let vm = this,
        {
            dataServices, $state, PricingService, $timeout, $q
        } = vm.DI();
        //console.log("deleteItem ", id);
        return $q(function(resolve, reject){
            dataServices.deleteCart(id).then(function(response){
                if(response.mess == "Accepted"){
                let checkoutData = angular.fromJson(sessionStorage.checkoutData);
                    for(let obj of checkoutData){
                        if(obj.id == id){
                            checkoutData.splice(checkoutData.indexOf(obj), 1);
                            sessionStorage.checkoutData = angular.toJson(checkoutData);
                            //console.log("deleted ", checkoutData);
                            if(checkoutData.length){
                                vm.getData();
                            }else{
                                vm.noItemsInCart = true;
                            }
                            break;
                        }
                    }
                    resolve();
                }
            },function(error){
                reject();
                $state.go('poError');
            });
        });
    }

    updateItem(obj){
        let vm = this,
        {
            dataServices, $state, PricingService, $timeout, $q
        } = vm.DI();
        //console.log("updateItem ", obj);
        let payload = [{
           "id": obj.id,
           "qtyAdded": obj.totalQtyAvailable,
           //"qtyReq": vm.orderType == 'STK' ? obj.qtyReq : obj.totalQtyAvailable,
           "qtyReq": vm.orderType == 'STK' ? obj.qtyReq : obj.qtyReq,
           "backOrderQty":obj.backQty? obj.backQty: 0,
           "checked":true,
           "plantCode":obj.plantCode ? obj.plantCode : 1601,
           "uom": obj.uom ? obj.uom : null,
           "shipToDateReq": obj.shipToDateReq ? obj.shipToDateReq: new Date().getTime(),
           "prices":[{
               "unitPrice":obj.STANDARD_BASE,
               "extdPrice":obj.STANDARD_BASE * (obj.totalQtyAvailable + (obj.backQty ? obj.backQty: 0)),
               "corePrice":obj.CORE_BASE,
               "currency":obj.currency ? obj.currency : "USD"
           }]
        }];
        return $q(function(resolve, reject){
            dataServices.updateCart(payload, obj.cartId).then(function(response){
                if(response.status == 404 || response.status == 500 || response.status == 400){
                    reject();
                    $state.go('poError');
                }else if(response.status == 200){
                    resolve();
                }else{
                    reject();
                    $state.go('poError');
                }
            },function(error){
                reject();
            });
        });
    }

    updateCart(){
        let vm = this,
        {
            dataServices, $state, PricingService, $timeout, $q
        } = vm.DI();

        let count = 0;
        return $q(function(resolve, reject){
            for(let obj of vm.incompeteData){
                count++;
                let payload = [{
                   "id": obj.id,
                   "qtyAdded": obj.totalQtyAvailable,
                   "qtyReq": vm.orderType == 'STK' ? obj.qtyReq : obj.totalQtyAvailable,
                   "backOrderQty":obj.backQty? obj.backQty: 0,
                   "checked":true,
                   "plantCode":obj.plantCode ? obj.plantCode : 1601,
                   "uom": obj.uom ? obj.uom : null,
                   "shipToDateReq": obj.shipToDateReq ? obj.shipToDateReq: new Date().getTime(),
                   "prices":[{
                       "unitPrice":obj.STANDARD_BASE,
                       "extdPrice":obj.STANDARD_BASE * (obj.totalQtyAvailable + (obj.backQty ? obj.backQty: 0)),
                       "corePrice":obj.CORE_BASE,
                       "currency":obj.currency ? obj.currency : "USD"
                   }]
                }];
                dataServices.updateCart(payload, obj.cartId).then(function(response){
                    if(response.status == 404 || response.status == 500 || response.status == 400){
                        reject();
                    }
                    if(count == vm.incompeteData.length){
                        resolve();
                    }
                },function(error){
                    reject();
                }); 
            }
        });
    }

    proceed(){
    	let vm = this,
        {
            dataServices, $state, PricingService, $timeout, $q, $scope
        } = vm.DI();
        let checkoutData = angular.fromJson(sessionStorage.checkoutData);
        $scope.$emit("showLoading", true);
        
        
        vm.beforePlaceOrder();
        /*if(checkoutData.length){
            if(vm.backOrder){
                if(vm.incompeteData.length){
                    let promise = vm.updateCart();
                    promise.then(function(){
                        vm.placeOrder();
                    },function(reason){
                        $state.go('poError');
                    });
                }else{
                    vm.placeOrder();
                }
            }else{
                vm.placeOrder();
            }
        }else{
            $scope.$emit("showLoading", false);
        }*/
    }

    beforePlaceOrder(){
        let vm = this,
        {
            dataServices, $state, PricingService, $timeout, $scope, $q
        } = vm.DI();
        let promises = [];
        for(let obj of vm.incompeteData){
            //console.log("beforePlaceOrder ", obj);
            if(PricingService.orderType == 'STK'){
                if(obj.radio == 'remove'){
                    promises.push(vm.deleteItem(obj.id));
                }else{
                    promises.push(vm.updateItem(obj));
                }
            }else{
                if(obj.radio == 'remove'){
                    promises.push(vm.deleteItem(obj.id));
                }else{
                    promises.push(vm.updateItem(obj));
                }
            }
        }
        $q.all(promises).then((values) => {
            
            // for (var i = 0; i < values.length; i++){
            //     console.log("value ", i);
            // }
            let checkoutData = angular.fromJson(sessionStorage.checkoutData);
            //console.log("$q.all ",values, checkoutData.length);
            if(checkoutData.length){
                 vm.placeOrder();
            }else{
               $scope.$emit("showLoading", false);
            }
        });
    }

    placeOrder(){
        let vm = this,
        {
            dataServices, $state, PricingService, $timeout, $scope
        } = vm.DI();

        //console.log("place order");
        let placeOrderDate = angular.fromJson(sessionStorage.placeOrderDate);
        dataServices.placeOrder(placeOrderDate, PricingService.orderType).then(function(response){
            if(response.status == 500){
                $state.go('poError');
            }else{
                let paramObj = {"id": response.data.orderNo, "code": response.data.processNo };
                if(PricingService.orderType == "STK"){
                    PricingService.stkCartData = [];
                    PricingService.stkPriceData = [];
                }else{
                    PricingService.emgCartData = [];
                    PricingService.emgPriceData = [];
                }
                $state.go("completedOrder", paramObj);   
            }
            $scope.$emit("showLoading", false);
        },function(error){
            $state.go('poError');
            $scope.$emit("showLoading", false);
        });   
    }

    editItem(editObj){
        let vm = this,size = "md";
        let { $log, $scope,$timeout, $uibModal, PricingService } = vm.DI();
        vm.selectedObj = editObj;
        //console.log("editObj ", editObj);
        PricingService.qtyRequired = editObj.qtyAdded;
        PricingService.partNumber = editObj.partNo;
        PricingService.partName = editObj.partName;
        PricingService.partToUpdateId = editObj.id;
        PricingService.plantCode = editObj.plantCode;
        PricingService.cartId = editObj.cartId;
        PricingService.catPath = editObj.productCat;
        PricingService.uom = editObj.uom ? editObj.uom : "";
        PricingService.wt = editObj.wt ? editObj.wt : "";
        PricingService.wtUom = editObj.wtUom ? editObj.wtUom : "";
        PricingService.userType = 'RTB';

        var modalInstance = $uibModal.open({
            templateUrl: 'app/pricing/availability/orderTypeSelection.html',
            size: 'lg',
            controller: 'orderTypeSelectionController',
            controllerAs: 'orderTypeCtrl',
            windowClass: 'my-modal-popup',
           resolve: {
            mode: function() {
              return 'incomplete';
            }
          }
        });

        ///let urlModalInstance = $timeout(OrderlistModalFactory.open("md", "app/pricing/availability/orderTypeSelection.html", "orderTypeSelectionController", "orderTypeCtrl", "availabilityOverlay", true, "edit"), 2000);
        modalInstance.result.then(function (response) {
            //console.log("response ", response);
            if(response){
                if(vm.orderType == 'STK'){
                    let promise = vm.updateList(response);
                    promise.then(function(){
                        vm.getData();
                    }, function(error){

                    });
                }else if(vm.orderType == 'EMG'){
                    //console.log("call to merge cart");
                    vm.mergeCart(PricingService.orderType == 'STK' ? 'SO' : 'EO', "normal-cart", response);
                }else{

                }
            }
        }, function () {
             
        });
    }

    manupulateData(response){
        let vm = this,
        {
            dataServices, $state, PricingService, $timeout
        } = vm.DI();
        Object.assign(response.data[0], {"STANDARD_BASE": response.data[0].prices[0].unitPrice});
        Object.assign(response.data[0], {"CORE_BASE": response.data[0].prices[0].corePrice});
        
        Object.assign(response.data[0], {"partAvailable": true});
        Object.assign(response.data[0], {"qtyAvailable": false});
        Object.assign(response.data[0], {"cartId": PricingService.cartId});
        
        //vm.incompeteData[vm.incompeteData.length] = response.data[0];
        if(vm.orderType == 'STK'){
            Object.assign(response.data[0], {"totalQtyAvailable": response.data[0].qtyAdded});
            Object.assign(response.data[0], {"qtyAdded": response.data[0].qtyReq});
            Object.assign(response.data[0], {"backQty": response.data[0].backOrderQty});
            
        }else{
            Object.assign(response.data[0], {"totalQtyAvailable": response.data[0].refData ? response.data[0].refData[0].qtyAvl : response.data[0].qtyAdded}); 
            Object.assign(response.data[0], {"qtyAdded": response.data[0].qtyReq});  
        }
        return response.data[0];
    }

    addToList(creatArr){
        let vm = this,
        {
            dataServices, $state, PricingService, $timeout 
        } = vm.DI();
        return new Promise(function(resolve, reject) {
            if(creatArr.length){
                for(let x=0; x<creatArr.length; x++){
                    dataServices.getCartById(creatArr[x]).then(function(response){
                        let checkoutData = angular.fromJson(sessionStorage.checkoutData);
                        checkoutData[checkoutData.length] = vm.manupulateData(response);
                        sessionStorage.checkoutData = angular.toJson(checkoutData);
                        creatArr.length-1 == x ? resolve() : angular.noop();
                    },function(error){

                    });
                }   
            }else{
                resolve();
            }
        });
    }

    updateList(updateArr){
        let vm = this,
        {
            dataServices, $state, PricingService, $timeout
        } = vm.DI();

        return new Promise(function(resolve, reject) {
            if(updateArr.length){
                for(let x=0; x<updateArr.length; x++){
                    let checkoutData = angular.fromJson(sessionStorage.checkoutData);
                    dataServices.getCartById(updateArr[x]).then(function(response){
                        for(let y=0; y<checkoutData.length; y++){
                            /*if(response.data[0].id == checkoutData[y].id && checkoutData[y].plantCode == response.data[0].refData[0].plantCode){
                                checkoutData[y] = vm.manupulateData(response);
                                sessionStorage.checkoutData = angular.toJson(checkoutData);
                                break;
                            }*/
                            //console.log("response.data[0] ", response.data[0], checkoutData[y]);
                            if(vm.orderType == 'STK' && response.data[0].id == checkoutData[y].id){
                                
                                checkoutData[y] = vm.manupulateData(response);
                                sessionStorage.checkoutData = angular.toJson(checkoutData);
                                break;
                            }else if(vm.orderType == 'EMG' && response.data[0].id == checkoutData[y].id && checkoutData[y].plantCode == response.data[0].plantCode){
                                
                                checkoutData[y] = vm.manupulateData(response);
                                sessionStorage.checkoutData = angular.toJson(checkoutData);
                                break;
                            }else{
                                
                            }
                        }
                        //updateArr.length-1 == x ? resolve() : angular.noop();
                        if(updateArr.length-1 == x){
                            //console.log("resolve");
                            resolve();
                        }else{
                            angular.noop();
                        }
                    },function(error){
                        reject();
                    });
                }
            }else{
                resolve();
            }
            
        });
    }

    mergeCart(orderType = "SO", cartType = "normal-cart", response){
        let vm = this,
        {
            dataServices, $state, PricingService, $timeout, $q
        } = vm.DI();

        let creatArr =  response.created;
        let updateArr = response.updated;
        
        var addToListTemp = vm.addToList(creatArr).then(function(result){
            return result;
        });

        var updateListTemp = vm.updateList(updateArr).then(function(result){
            return result;
        });

        Promise.all([addToListTemp, updateListTemp]).then(function(result){
            vm.getData();
        });
    }
}
        
