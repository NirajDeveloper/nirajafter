/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

import {OrdermgtController} from './ordermgt.controller';
import {routeConfig} from './ordermgt.route';
import {ShippedListEdit} from './shippedlistedit/shippedlist.controller';
import { validateNumberDirective } from './validate-number.directive';

angular.module('aftermarket.ordermgt', [])
	.config(routeConfig)
	.controller('OrdermgtController', OrdermgtController)
	.controller('ShippedListEdit', ShippedListEdit)
	.directive('validateNumberDirective', validateNumberDirective);

