/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

import {routeConfig} from './poerror.route';
import {PoErrorController} from './poerror.controller';

angular.module('aftermarket.poError', [])
.config(routeConfig)
.controller('PoErrorController', PoErrorController);
