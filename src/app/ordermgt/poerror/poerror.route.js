/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

export function routeConfig($stateProvider){
	'ngInject';
	$stateProvider
	.state('poError',{
		url: '/poerror',
		parent: 'aftermarket',
        authenticate: true,
		views: {
			'':{
				templateUrl: 'app/ordermgt/poerror/poerror.html',
				controller: 'PoErrorController',
				controllerAs: 'poerr',
			},
			'header':{
				template: '<span><page-header-order></page-header-order></span>'
			}
		}
	});
}

