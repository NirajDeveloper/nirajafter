/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// Author: Manjesh Kumar
export class PoErrorController {
  constructor($log, $filter, $uibModal, $timeout, $location, $window, $scope) {
    'ngInject';
    let vm = this;
    vm.DI = () => ({ $log, $uibModal , $timeout, $window, $scope});
    $scope.$emit("showLoading", false);
  }
}
