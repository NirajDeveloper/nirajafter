/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

export class ShippedListEdit {
    constructor($log, $uibModal, $uibModalInstance, $state, $timeout) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $uibModal, $log, $uibModalInstance, $state, $timeout});

        $timeout(function(){vm.cancel();$state.go('completedOrder')}, 2000);
    }

    cancel(){
        let vm = this, size = "md";
        let {$log, $uibModal, $uibModalInstance} = vm.DI();
         $uibModalInstance.close();
    }
}
