/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

export function routeConfig($stateProvider){
	'ngInject';
	$stateProvider
	.state('placedOrder',{
		url: '/placedOrder',
		parent: 'aftermarket',
        authenticate: true,
		templateUrl: 'app/ordermgt/placedOrder/placedOrder.html',
		// controller: 'PlacedOrder',
		// controllerAs: 'placed',
	});
}