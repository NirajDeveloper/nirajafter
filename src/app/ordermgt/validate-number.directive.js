/*Author:Rohit Rane*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function validateNumberDirective() {

    let directive = {
        restrict: 'A',
        require: 'ngModel',
        compile: function(element, attr) {
          element.tooltip({ placement: 'top', title:'Ordered quantity for this part exceeds 9999', trigger:'manual'});
          function emailValid(number) {
             var valid = number > 9999 ? false : true;
             return valid;
          }
          return  function(scope, element,attr, ngModel) {
              ngModel.$validators.qty = function(val) {

                return emailValid(val);
              }
              
              scope.$watch(function() {
                return ngModel.$error.qty;
              }, function(val) {
                if (val)
                   element.tooltip('show');
                else
                   element.tooltip('hide');
                    
              });
          }
        }
    };
    return directive;
}