/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

import {routeConfig} from './previewOrder.route';
import {PreviewOrder} from './previewOrder.controller';
import {AdvancedSearchPopUp} from './advancedsearchpopup/advancedsearchpopup.controller';

angular.module('aftermarket.previewOrder', [])
.config(routeConfig)
.controller('PreviewOrder', PreviewOrder)
.controller('AdvancedSearchPopUp', AdvancedSearchPopUp);
