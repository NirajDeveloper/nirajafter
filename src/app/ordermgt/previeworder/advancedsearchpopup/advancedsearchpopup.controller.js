/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// Author: Manjesh Kumar
export class AdvancedSearchPopUp {
    constructor($log, $uibModal, $uibModalInstance, advSrchResultList) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $uibModal, $log, $uibModalInstance,advSrchResultList});
        vm.advSrchResultList = advSrchResultList;
        vm.sortType = 'address.state'; // set the default sort type
        vm.sortReverse = false;  // set the default sort order
        vm.search = {};
        vm.search.state = undefined;
        vm.search.city = undefined;
        vm.search.zipCode = undefined;
        vm.search.addr1 = undefined;
        vm.search.addr2 = undefined;
        vm.search.country = undefined;
    }

    cancel(selectedResult){
        let vm = this, size = "lg";
        let {$log, $uibModal, $uibModalInstance} = vm.DI();
         $uibModalInstance.close(selectedResult);
    }

    shipToSelectedAddress(searchResult){
        let vm = this;
        vm.cancel(searchResult);
    }
    
}
