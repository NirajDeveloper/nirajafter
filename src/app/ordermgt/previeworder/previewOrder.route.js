/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->

export function routeConfig($stateProvider){
	'ngInject';
	$stateProvider
	.state('previewOrder',{
		url: '/previewOrder',
		parent: 'aftermarket',
        authenticate: true,
		views: {
			'':{
				templateUrl: 'app/ordermgt/previeworder/previewOrder.html',
				controller: 'PreviewOrder',
				controllerAs: 'preview',
			},
			'header':{
				template: '<span><page-header-order></page-header-order></span>'
			}
		}
	});
}
