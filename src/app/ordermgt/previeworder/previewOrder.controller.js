/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// Author: Manjesh Kumar
export class PreviewOrder {
    constructor($log, $window, $scope, $rootScope, $state, $filter, $uibModal, PricingService, dataServices, $timeout, authenticationService, $q) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $log, $window, $state, $uibModal, PricingService, $scope, $rootScope, dataServices, $timeout, authenticationService, $q});
        if($rootScope.fromState == 'completedOrder'){
            $state.go('ordermgt');
            return;
        }
        PricingService.isCartPersisted = true;
        $window.scrollTo(0, 0);
        if(PricingService.noOfParts && PricingService.noOfParts > 0){
            // user can only proceed if qty and price is valid
        }else{
            $state.go('home');
            return;
        }

        /*vm.stickyAd = false;
        let deregistrationCallback = $rootScope.$on("isHeaderSticky", (evt, isHeaderSticky) => {
            let adSec = $document[0].getElementById("ad-section1");
            if (isHeaderSticky.state) {
                vm.stickyAd = true;
                if (angular.isDefined(isHeaderSticky.bottomOffset)) {
                    angular.noop();
                }
            } else {
                vm.stickyAd = false;
                angular.element(adSec).css("bottom", 'auto');
            }
        });

        $scope.$on("$destroy", deregistrationCallback);*/
        
        vm.shipComplete = 1;
        vm.shipOverride = false;
        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });
        vm.completeOrder = true;
        vm.shipDateOriginal = new Date();
        vm.format = 'MM/dd/yyyy';
        vm.dateOptions = {
            showWeeks: false,
            // altInputFormats : ['MM/dd/yyyy'],
            // pattern:"MM/dd/yyyy"
        };
        vm.dateOptions.minDate = vm.dateOptions.minDate ? null : new Date();
        vm.countryList = [];
        vm.stateList = [];
        vm.showDateErr = false;
        PricingService.orderType = localStorage.checkedOutCartType == "SO" ? "STK" : "EMG";
        vm.orderType = PricingService.orderType;
        vm.sumOfTotal = PricingService.sumOfTotal;
        vm.totalOrderQTY=PricingService.totalOrderQTY;
        vm.wt = PricingService.wt;
        vm.noOfParts = PricingService.noOfParts;
        vm.totWgtOfPartsInKg=PricingService.totWgtOfPartsInKg;
        vm.totWgtOfPartsInLb=PricingService.totWgtOfPartsInLb;
        
        
        vm.orderTypeSummary = vm.orderType == 'STK' ? "Stock" : "Emergency";
        vm.selectedShippingMethod = {
            "name": "Select"
        };
        vm.selectedCarrier = "Select";
        vm.selectedEmailId = "";
        vm.isShipMethodPristine = true;
        vm.isCarrierPristine = true;
        // dummy data for advanced search result
        vm.advSrchResultList = [];
        vm.emailDetailsList = [];

        // dummy data for shipping method list
        vm.shippingMethodList = [];
        vm.selectedShippingCarrier = {};
        vm.selectedShippingMethod = {};

        vm.selected = undefined;
        vm.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut'];
        vm.getCountryList();
        vm.getStateList('US');

        vm.checkoutData= angular.fromJson(sessionStorage.checkoutData);
        
        dataServices.checkOut(PricingService.orderType, PricingService.cartType).then(function(response){
            if(response.status == 500){
                $state.go('poError');
            }
            vm.headerResult = response.data;
            vm.selectedAdvSrchRes = vm.headerResult.shipTo.address;
            vm.selectedAdvSrchRes.customerId = vm.headerResult.shipTo.customerId;
            vm.emailDetailsList = vm.headerResult.emails ? vm.headerResult.emails : [];
            vm.emailDetailsListObj = [];
            for(var i = 0; i < vm.emailDetailsList.length; i++ ){
                vm.emailDetailsListObj.push({
                    name: vm.emailDetailsList[i],
                    select: true
                });
            };
            vm.selectedEmailId = vm.emailDetailsList[0];
            vm.advSrchRsltAddress = vm.headerResult.shipTo.customerId + ' ' + vm.headerResult.shipTo.name + ' ' + vm.headerResult.shipTo.address.addr1 + ' '+ vm.headerResult.shipTo.address.addr2 +' '+ vm.headerResult.shipTo.address.city +' '+vm.headerResult.shipTo.address.state + ', '+ vm.headerResult.shipTo.address.country +' '+vm.headerResult.shipTo.address.zipCode;
            vm.shipToName = vm.headerResult.shipTo.name;
            vm.shipToCustomerId = vm.headerResult.shipTo.customerId;
            
            vm.shipToFirstAddress = vm.headerResult.shipTo.address.addr1 +' '+ vm.headerResult.shipTo.address.addr2;
            vm.shipToSecondAddress = vm.headerResult.shipTo.address.city +' '+vm.headerResult.shipTo.address.state +' '+vm.headerResult.shipTo.address.zipCode;
            vm.shipToAddress = vm.headerResult.shipTo.address.addr1 + ' '+ vm.headerResult.shipTo.address.addr2 +' '+ vm.headerResult.shipTo.address.city +' '+vm.headerResult.shipTo.address.state +' '+vm.headerResult.shipTo.address.zipCode;
            
            vm.soldToFirstAddress = vm.headerResult.soldTo.address.addr1 +' '+ vm.headerResult.soldTo.address.addr2;
            vm.soldToSecondAddress = vm.headerResult.soldTo.address.city + ' '+vm.headerResult.soldTo.address.state  +' '+vm.headerResult.soldTo.address.zipCode;
            vm.soldToAddress = vm.headerResult.soldTo.address.addr1 + ' '+ vm.headerResult.soldTo.address.city + ' '+vm.headerResult.soldTo.address.state + ', '+ vm.headerResult.soldTo.address.country +' '+vm.headerResult.soldTo.address.zipCode;
            
            vm.billToFirstAddress = vm.headerResult.billTo.address.addr1 +' '+ vm.headerResult.billTo.address.addr2;
            vm.billToSecondAddress = vm.headerResult.billTo.address.city + ' '+vm.headerResult.billTo.address.state  +' '+vm.headerResult.billTo.address.zipCode;
            vm.billToAddr = vm.headerResult.billTo.customerId +' '+ vm.headerResult.billTo.name +' '+ vm.headerResult.billTo.address.addr1 + ' '+ vm.headerResult.billTo.address.city + ' '+vm.headerResult.billTo.address.state +' '+vm.headerResult.billTo.address.zipCode;
            vm.soldToAddr = vm.headerResult.soldTo.customerId +' '+ vm.headerResult.soldTo.name +' '+ vm.headerResult.soldTo.address.addr1 + ' '+ vm.headerResult.soldTo.address.city + ' '+vm.headerResult.soldTo.address.state + ', '+ vm.headerResult.soldTo.address.country +' '+vm.headerResult.soldTo.address.zipCode;
            vm.shipToDate = vm.DateinDDMMYY(vm.headerResult.shipToDate);
            vm.getShipAddress();
        },function(error){

        });
        let customerId = null;
        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        dataServices.getCarrier(customerId, PricingService.orderType == 'STK' ? "SO" : "EO").then(function(response){
            vm.carrier = response.data.carriers;
            vm.shippingMethodList = response.data.friehgts;
            vm.carrierList = response.data.carriers;

            //let defaultCarrier = PricingService.orderType == 'STK' ? response.data.defaultCarrier? response.data.defaultCarrier : response.data.carriers[0].code : "FDEN10"; 
            let defaultCarrier = response.data.defaultCarrier? response.data.defaultCarrier : response.data.carriers[0].code; 
            let defaultFrieght = response.data.defaultFrieght ? response.data.defaultFrieght : response.data.friehgts[0].code;

            vm.selectedShippingCarrier = response.data.carriers[0] ? response.data.carriers[0] : "";
            vm.selectedShippingMethod = response.data.friehgts[0] ? response.data.friehgts[0] : "";

            for(let obj of response.data.carriers){
                if(obj.code === defaultCarrier){
                    vm.selectedShippingCarrier = obj;
                    break;
                }
            }
            for(let obj of response.data.friehgts){
                if(obj.code === defaultFrieght){
                    vm.selectedShippingMethod = obj;
                    break;
                }
            }
        },function(error){

        });

    }

    getCountryList(){
      // api to populate country list
    let vm = this;
    let {$scope, $rootScope, dataServices, $location, $state} = vm.DI();
    dataServices.fetchCountryList().then(function (response) {
        if (response) { 
            vm.countryList = response;
        } else {
            vm.countryList = []; 
           // console.log("NO country");
        }
    }, function (error) {
        vm.countryList = []; 
        //console.log("server error in fetching country list");
    });
  }

  getStateList(country){
      if(country){
          let vm = this;
          let {$scope, $rootScope, dataServices, $location, $state} = vm.DI();
          dataServices.fetchStateListByCountry(country).then(function (response) {
              if (response) { 
                  vm.stateList = response;
              } else {
                  vm.stateList = []; 
                  //console.log("NO state");
              }
          }, function (error) {
              vm.stateList = []; 
              //console.log("NO state");
          });
      }else{
        // not needed
      }
  }

    compareDate() {
        //debugger;
        let vm = this;
        let { $log } = vm.DI();

        let currentDate = new Date((vm.shipToDate));
        
        let shipDateOriginal = new Date((vm.shipDateOriginal));
        if(currentDate > shipDateOriginal || currentDate == shipDateOriginal){
            vm.showDateErr = false;
        }else{
            vm.showDateErr = true;
        }
    }

    DateinDDMMYY(noOfDays) {
        //debugger;
        let vm = this;
        let { $log } = vm.DI();
        vm.shipDateOriginal = new Date(parseInt(noOfDays));
        //$filter('date')(alignFillDate, DATE_FORMAT);
        return vm.shipDateOriginal;
    }

    open2() {
        let vm = this;
        let { $log } = vm.DI();
        vm.opened = true;
    }

    process() {
        let vm = this,
            size = "md";
        let { $log, $uibModal } = vm.DI();
        /*vm.modalInstance = $uibModal.open({
            templateUrl: 'app/ordermgt/processorderpopup/processorder.html',
            size: size,
            controller: 'ShippedListEdit',
            controllerAs: 'listedit',
            windowClass: 'my-modal-popup',
            resolve: {
                items: function() {
                    return vm.items;
                }
            }
        });*/
    }
    cancel() {
        let vm = this,
            { $uibModalInstance } = vm.DI();
        vm.modalInstance.close();
    }

    // add email address implementation
    removeEmailDetails(index){
        let vm = this;
        vm.emailDetailsListObj.splice(index, 1);
    }
    // create a new email field 
    addEmailDetailsToList(){
        let vm = this;
        if(!vm.emailDetailsListObj){
            vm.emailDetailsListObj = [];
        }
        vm.emailDetailsListObj.push({name: "", select: true});
    }

    getShipAddress(){
        let vm = this;
        let { $log, $uibModal, dataServices, $q } = vm.DI();
        return $q(function(resolve, reject){
            dataServices.getShipAddr(vm.headerResult.billTo.customerId).then(function(response){
                if(response.status == 200){
                    vm.advSrchResultList = response.data;
                }else{
                }
                resolve();
            },function(error){
                reject(error);
            });
        });
            
    }

    // 'advanced search' pop-up
    openAdvancedSearchPopUp(){
        let vm = this,
            size = "lg";
        let { $log, $uibModal, dataServices } = vm.DI();

        /*let promise = vm.getShipAddress();
        promise.then(function(){
        }, function(error){

        });*/
        var modalInstance = $uibModal.open({
            templateUrl: 'app/ordermgt/previeworder/advancedsearchpopup/advancedsearchpopup.html',
            size: size,
            controller: 'AdvancedSearchPopUp',
            controllerAs: 'advancedsearchpopup',
            animation: false,
            windowClass: 'my-modal-popup advanced-search-pop-up',
            resolve: {
                advSrchResultList: function() {
                    return vm.advSrchResultList;
                }
            }
        });

        modalInstance.result.then(function (selectedAdvResult) {

            if(selectedAdvResult){
                //vm.shipOverride = true;
                vm.selectedAdvSrchRes = selectedAdvResult;
                vm.advSrchRsltAddress = selectedAdvResult.customerId + " " + selectedAdvResult.name + " " + selectedAdvResult.address.addr1+ " "+ selectedAdvResult.address.addr2 + " "+ selectedAdvResult.address.city+" "+selectedAdvResult.address.state + " "+ selectedAdvResult.address.country + " " + selectedAdvResult.address.zipCode;
                vm.shipToAddress = selectedAdvResult.address.addr1+ " "+ selectedAdvResult.address.addr2 + " "+ selectedAdvResult.address.city+" "+selectedAdvResult.address.state + " " + selectedAdvResult.address.zipCode;
                vm.shipToFirstAddress = selectedAdvResult.address.addr1+ " "+ selectedAdvResult.address.addr2;
                vm.shipToSecondAddress = selectedAdvResult.address.city+" "+selectedAdvResult.address.state + " " + selectedAdvResult.address.zipCode;
                
                vm.shipToName = selectedAdvResult.name;
                vm.shipToCustomerId = selectedAdvResult.customerId;
            }
        }, function () {

        });
    }


    closeAlert () {
        let vm = this;
        vm.advSrchRsltAddress = undefined;
    };

    
    // On change of Shipping method functionality
    onChangeOfShippingMethod(selectedMethod){
        let vm = this;
        vm.isShipMethodPristine = false;
        vm.selectedShippingMethod = selectedMethod;
        //vm.selectedShippingMethod = selectedMethod;
    }

    // On change of Shipping method functionality
    onChangeOfCarrier(selectedCarrier){
        let vm = this;
        vm.selectedCarrier = selectedCarrier;
        vm.isCarrierPristine = false;
        vm.selectedShippingCarrier = selectedCarrier;
        if(vm.selectedShippingCarrier.code == 'XXXX1'){
            let pickupShipMethod = vm.shippingMethodList.filter(function(obj){
                return obj.code == "PU";
            });
            if(pickupShipMethod && pickupShipMethod.length > 0){
                vm.selectedShippingMethod = pickupShipMethod[0];
            }
        }
        /*for(let obj of vm.carrier){
            //console.log(obj);
            if(obj.name == selectedCarrier){
                vm.shippingMethodList = obj.shipMethods;
                vm.selectedShippingMethod = {
                    "name": obj.shipMethods[0].name,
                    "code": obj.shipMethods[0].code,
                    "id": obj.shipMethods[0].id
                };
                break;
            }
        }*/
    }

    addNewShipAddressPopUp(){
        let vm = this,
            size = "md";
        let { $log, $uibModal, $scope } = vm.DI();
        vm.modalInstance = $uibModal.open({
            templateUrl: 'app/ordermgt/previeworder/newshipaddress/newshipaddress.html',
            size: size,
            animation: false,
            scope: $scope,
            windowClass: 'my-modal-popup new-ship-address-pop-up',
            backdrop  : 'true',
            keyboard : 'true'
        });
    }

    addNewShipAddress(){
        let vm = this,
            size = "lg";
       let { $log, dataServices, $timeout } = vm.DI();

        let payload={"address":
                        {
                            "name": vm.shipname,
                            "state": vm.state,
                            "city": vm.city,
                            "zipCode": vm.pin,
                            "addr1": vm.addr1,
                            "addr2": vm.addr2 ? vm.addr2 : "",
                            "country": vm.country ? vm.country : "",
                            "email": null
                        }
                    };  

        vm.shipOverride = true;
        vm.selectedAdvSrchRes = payload.address;
        
        vm.advSrchRsltAddress = payload.address.name+ " "+ payload.address.addr1+ " "+ payload.address.addr2 + " "+ payload.address.city+" "+payload.address.state + " "+ payload.address.country + " " + payload.address.zipCode;
        
        vm.shipToSecondAddress = payload.address.city+" "+payload.address.state + " " + payload.address.zipCode;
        vm.shipToFirstAddress = payload.address.addr1+ " "+ payload.address.addr2;
        vm.shipToAddress = payload.address.addr1+ " "+ payload.address.addr2 + " " +  payload.address.name+ " "+ payload.address.addr1+ " "+ payload.address.addr2 + " "+ payload.address.city+" "+payload.address.state + " " + payload.address.zipCode;

        vm.shipToName = payload.address.name;
        vm.advSrchResultList.push(payload);
        vm.modalInstance.close(payload);

        /*dataServices.addShipAddr(payload).then(function(){

        },function(error){

        });*/
    }

    processOrder(){
        let vm = this;
        let { $log, dataServices, PricingService, $scope, $state, $timeout } = vm.DI();
    }

    bulkAvailabilityCheckEo(){
        let vm = this;
        let { $log, dataServices, PricingService, $scope, $state, $timeout, authenticationService, $q } = vm.DI();
        
        let checkoutData = angular.fromJson(sessionStorage.checkoutData);
        let data = [];
        let temp = {};
        let customerId = null;

        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }

        for(let obj of checkoutData){
            let packageQty = obj.packageQty;
            if(!packageQty || packageQty.toString().trim() == ""){
                packageQty = 1;
            }else{
                packageQty = packageQty;
            }
            temp = {
                "partNumber": obj.partNo,
                "orderType":PricingService.orderType,
                "quantityReq":obj.qtyReq,
                //"quantityReq":obj.qtyAdded,
                "unitOfMeasure":obj.uom ? obj.uom : null,
                "reqestedDate":obj.shipToDateReq ? new Date(obj.shipToDateReq).getTime() : "",
                "productCat":obj.productCat,
                "plantERPName":obj.plantERPName,
                "plantCd": [obj.plantCode],
                "packageQty": packageQty    
            };
            data.push(temp);
       }
        let availabilityPayload = {
            "checkByPlantCode": PricingService.orderType =='STK' ? 0 : 1,
            "availReqDtlList" :data
        };

        let count = 0 ;
        let availabilityResponse = [];
        return $q(function(resolve, reject){
            dataServices.bulkAvailabilityCheckEo(availabilityPayload).then(function(response){
                if(response.status == 500 || response.status == 400){
                    reject();
                }
                availabilityResponse = response.data;
                for(let i of checkoutData){
                    count++;
                    for(let j of availabilityResponse){
                        if(j.partNumber == i.partNo && j.plantCd == i.plantCode){
                            i.partAvailable = true;
                            if(j.quantityAvbl >= i.qtyAdded ){
                                i.qtyAvailable = true;
                            }else{
                                vm.completeOrder = false;
                            }
                            i.totalQtyAvailable = j.quantityAvbl;
                            break;
                        }
                    }
                    checkoutData.length == count ? resolve(checkoutData) : "";
                }
            },function(error){
                reject();
            });
        });
    }

    bulkAvailabilityCheck(){
        let vm = this;
        let { $log, dataServices, PricingService, $scope, $state, $timeout, authenticationService, $q } = vm.DI();
        
        let checkoutData = angular.fromJson(sessionStorage.checkoutData);
        let data = [];
        let temp = {};
        let customerId = null;

        if(authenticationService.associatedCustData.activeCustId){
            customerId = authenticationService.associatedCustData.activeCustId;
        }

        for(let obj of checkoutData){
            let packageQty = obj.packageQty;
            if(!packageQty || packageQty.toString().trim() == ""){
                packageQty = 1;
            }else{
                packageQty = packageQty;
            }
            if(PricingService.orderType == 'STK'){
                temp = {
                    //"customerCid":customerId,
                    "partNumber": obj.partNo,
                    "orderType":PricingService.orderType,
                    "quantityReq":obj.qtyReq,
                    "reqestedDate":obj.shipToDateReq ? new Date(obj.shipToDateReq).getTime() : "",
                    "productCat":obj.productCat,
                    "unitOfMeasure":obj.uom ? obj.uom : null,
                    "packageQty": packageQty
                };
            }else{
                temp = {
                    //"customerCid":customerId,
                    "partNumber": obj.partNo,
                    "orderType":PricingService.orderType,
                    "quantityReq":obj.qtyReq,
                    //"quantityReq":obj.qtyAdded,
                    "unitOfMeasure":obj.uom ? obj.uom : null,
                    "reqestedDate":obj.shipToDateReq ? new Date(obj.shipToDateReq).getTime() : "",
                    "productCat":obj.productCat,
                    "plantERPName":obj.plantERPName,
                    "plantCd": [obj.plantCode],
                    "packageQty": packageQty  
                };
            }
            data.push(temp);
        }

        let availabilityPayload = {
            "checkByPlantCode": PricingService.orderType =='STK' ? 0 : 1,
            "availReqDtlList" :data
        };
        let count = 0 ;
        let availabilityResponse = [];
        return $q(function(resolve, reject){
            dataServices.bulkAvailabilityCheck(availabilityPayload).then(function(response){

                if(response.status == 500 || response.status == 400){
                    reject();
                }
                availabilityResponse = response.data;
                for(let i of checkoutData){
                    count++;
                    for(let j of availabilityResponse){
                        if(j.partNumber == i.partNo){
                            i.partAvailable = true;
                            Object.assign(i, {"radio": "backOrder"});
                            if(j.partAvailDtlList.length > 0) {
                                if(j.partAvailDtlList[0].quantityAvbl >= i.qtyReq ){
                                    i.qtyAvailable = true;
                                    Object.assign(i, {"backQty": 0});
                                }
                                /*else if(j.partAvailDtlList[0].quantityAvbl == i.qtyAdded){
                                    i.qtyAvailable = false;
                                    Object.assign(i, {"backQty": i.backOrderQty});
                                    vm.completeOrder = false;
                                }*/
                                else{
                                    if(PricingService.orderType == 'STK'){
                                        let newBackOrder = i.backOrderQty + i.qtyAdded - j.partAvailDtlList[0].quantityAvbl;
                                        if(newBackOrder > i.backOrderQty){
                                            vm.completeOrder = false;
                                        }else {
                                            i.qtyAvailable = true;
                                        }
                                        Object.assign(i, {"backQty": i.backOrderQty + i.qtyAdded - j.partAvailDtlList[0].quantityAvbl});
                                    }else{
                                        vm.completeOrder = false;
                                        Object.assign(i, {"backQty": i.backOrderQty + i.qtyAdded - j.partAvailDtlList[0].quantityAvbl});
                                    }
                                }
                                i.totalQtyAvailable = j.partAvailDtlList[0].quantityAvbl;
                            }
                            else {
                                if(i.qtyAdded !== 0 ){
                                    vm.completeOrder = false;
                                }
                                else {
                                    i.qtyAvailable = true;
                                }
                                
                                if(PricingService.orderType == 'STK'){
                                     Object.assign(i, {"backQty": i.backOrderQty + i.qtyAdded});
                                }
                                i.totalQtyAvailable = 0;
                            }
                            break;
                        }
                    }
                    checkoutData.length == count ? resolve(checkoutData) : "";
                }
            },function(error){
                reject();
            });
        });
    }

    placeOrder(validForm){
        let vm = this;
        let { $log, dataServices, PricingService, $scope, $state, $timeout, authenticationService, $q } = vm.DI();
        
        var emailList = [];
        vm.isDataLoading = true;
        console.log("validForm ", validForm.$valid, validForm.priNum.$valid, validForm.priNum.$dirty);
        if(validForm.$valid && validForm.priNum.$valid){
        }else{
            validForm.priNum.$invalid = true;
            validForm.priNum.$pristine = false;
            vm.isDataLoading = false;
            return
        }
        for(var obj of vm.emailDetailsListObj){
            if( obj.select && obj.name && obj.name.length){
                emailList.push(obj.name);
            }
        };
        
        let payload= {
            "billTo": vm.headerResult.billTo,
            "shipTo": {
                //"customerId": authenticationService.associatedCustData.activeCustId,
                "customerId": vm.shipToCustomerId,
                "name": vm.shipToName,
                "address": vm.selectedAdvSrchRes
            },
            "soldTo": vm.headerResult.soldTo,
            "shipToDate": new Date(vm.shipToDate).getTime(),
            "notifEmails":emailList,
            "shipInstn":vm.instruction,
            "primePoNo":vm.primaryNo,
            "secPoNo": vm.secondaryNo,
            "carrier":vm.selectedShippingCarrier.code,
            "frieght": vm.selectedShippingMethod.code,
            "shipOverride": vm.shipOverride,
            "shipComplete":vm.shipComplete == 1 ? false : true,
            "parcelAcNo":vm.caNumber
        };
        
        sessionStorage.placeOrderDate = angular.toJson(payload);

        dataServices.placeOrder(payload, PricingService.orderType, PricingService.cartType).then(function(response){
            if(response && (response.status == 500 || response.status == 400 || response.status == 406)){
                vm.isDataLoading = false;
                $state.go('poError');
            }
            /*else if(response.status == 406){
                if(response.data.mess == 'no.cart.item.found'){

                }else if(response.data.mess == 'order.not.placed'){

                }else if(response.data.mess == 'customer.on.hold'){

                }else if(response.data.mess == 'di.service.unavailable'){

                }else{

                }
            }*/
            else if(response && response.data && response.data.orderNo){
                vm.isDataLoading = false;
                let paramObj = {"id": response.data.orderNo, "code": response.data.processNo ? response.data.processNo : "" };
                if(PricingService.orderType == "STK"){
                    PricingService.stkCartData = [];
                    PricingService.stkPriceData = [];
                }else{
                    PricingService.emgCartData = [];
                    PricingService.emgPriceData = [];
                }
                $state.go("completedOrder", paramObj);
            }else{
                vm.isDataLoading = false;
                $state.go('poError');
            }
        },function(error){
            vm.isDataLoading = false;
            $state.go('poError');
        });
        //$scope.$emit("showLoading", true);
        
        //let promise = vm.bulkAvailabilityCheck();
        //let promise = PricingService.orderType == 'STK' ? vm.bulkAvailabilityCheck() : vm.bulkAvailabilityCheckEo();
        
        /*promise.then(function(checkoutData){
            if(vm.completeOrder){
                dataServices.placeOrder(payload, PricingService.orderType, PricingService.cartType).then(function(response){
                    if(response && (response.status == 500 || response.status == 400)){
                        vm.isDataLoading = false;
                        $state.go('poError');
                    }
                    else if(response && response.data){
                        vm.isDataLoading = false;
                        let paramObj = {"id": response.data.orderNo, "code": response.data.processNo };
                        $state.go("completedOrder", paramObj);
                    }else{
                        vm.isDataLoading = false;
                        $state.go('poError');
                    }
                },function(error){
                    vm.isDataLoading = false;
                    $state.go('poError');
                });
            }else{
                vm.isDataLoading = false;
                sessionStorage.checkoutData = angular.toJson(checkoutData);
                $state.go('incompleteOrder');
            }
        },function(error){
            vm.isDataLoading = false;
            $state.go('poError');
        });*/
    }
    
    go(){
        let vm = this;
        let {$state,PricingService} = vm.DI();

       // $state.go('ordermgt');
       if(PricingService.cartType=="express-cart"){
           $state.go('expresscheckout');
       }else{
           $state.go('ordermgt');
       }
    }
}
