/*Author:Rohit Rane*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class HomeController {
    constructor($rootScope, $scope, $window, $interval, AftermarketConstants, SearchBarService, appInfoService, $state, ngAnimate) {
        let vm = this;
        $window.scrollTo(0, 0);
        vm.dummyHome = "";
        vm.slides = [];
        
        /*let goto = $state.params.goto;
        
        
                if (localStorage["currentUserProfileData"] === undefined) {
                    switch (goto) {
                        case "rfq":
                            $scope.$emit("openSignin");
                            break;
                        default:
                            angular.noop();
                            break;
                    }
                }*/
        vm.slidesTimer = AftermarketConstants.skin.bannerSliderTimer;
        let intvl = $interval(() => {
            if (appInfoService.appInfo && appInfoService.appInfo.cdnBaseurl) {
                $interval.cancel(intvl);
                for(let i=0;i<AftermarketConstants.skin.banner.length;i++) {
                    vm.slides.push({image: $rootScope.protocol + "://" + appInfoService.appInfo.cdnBaseurl + '/home/' + AftermarketConstants.skin.banner[i]});
                }
                vm.dummyHome = $rootScope.protocol + "://" + appInfoService.appInfo.cdnBaseurl + '/' + AftermarketConstants.skin.home;
            }
        }, 100);


    }
}

HomeController.$inject = ['$rootScope', '$scope', '$window', '$interval', 'AftermarketConstants', 'SearchBarService', 'appInfoService', '$state'];