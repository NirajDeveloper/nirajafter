/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import {HomeController} from './home.controller';
import {routeConfig} from './home.route';

angular.module('aftermarket.home', [])
    .config(routeConfig)
    .controller('HomeController', HomeController);