/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function routeConfig($stateProvider) {
    $stateProvider
        .state('home', {
            url: '/?goto&refUrl',
            parent: 'aftermarket',
            templateUrl: 'app/home/home.html',
            controller: 'HomeController',
            controllerAs: 'home'
        });
}   

routeConfig.$inject = ['$stateProvider'];