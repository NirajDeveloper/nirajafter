/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class WhereToBuyService {
    constructor(dataServices, $http, $rootScope, $window, Session, AUTH_EVENTS, $q) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            dataServices, $http, $rootScope, $window, Session, AUTH_EVENTS, $q
        });

    }
    get dealerLocatorData() {
        return this._dealerLocatorData;
    }
    set dealerLocatorData(data) {
        this._dealerLocatorData = data;
    }

     getDealerLocatorData(payload, callback) {
        let vm = this;
        let { dataServices } = vm.DI();

        dataServices.getWhereToBuyData(payload).then(function (response) {
            callback(response);
        });
     }

}