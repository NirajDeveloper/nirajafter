//author: Gautham Manivannan
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function routeConfig($stateProvider){
	'ngInject';
	$stateProvider
	.state('wheretobuy',{
		url: '/wheretobuy',
		parent: 'aftermarket',
		templateUrl: 'app/wheretobuy/wheretobuy.html',
		controller: 'WhereToBuyController',
		controllerAs: 'wtb',
	});
}