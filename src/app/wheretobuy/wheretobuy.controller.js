//Author: Gautham Manivannan
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class WhereToBuyController {
  constructor($rootScope, $translate, $scope, appInfoService, authenticationService, $q, ReqquoteService, $interval, WhereToBuyService, $timeout) {
    'ngInject';
    let vm = this;
    vm.DI = () => ({ $rootScope, $translate, $scope, appInfoService, authenticationService, $q, ReqquoteService, $interval, WhereToBuyService, $timeout });
  }

}