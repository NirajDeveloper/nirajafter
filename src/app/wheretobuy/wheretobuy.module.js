// <!-- Author: Gautham Manivannan -->
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import {routeConfig} from './wheretobuy.route';
import {WhereToBuyController} from './wheretobuy.controller';
import {WhereToBuyService} from './wheretobuy.service';

angular.module('aftermarket.wheretobuy', [])
    .config(routeConfig)
    .controller('WhereToBuyController', WhereToBuyController)
    .service('WhereToBuyService', WhereToBuyService);
