/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export let appConstants = {
    skin:skin,
    localization:localization,
    appSettings: appSettings
}

/*Uncomment the skin you want loaded in the application*/

import { skin } from '../assets/skins/dana/dana';
//import { skin } from '../assets/skins/meritor/meritor';

//import { localization } from '../assets/localization/meritor/local';
import { localization } from '../assets/localization/dana/local';
import { appSettings } from './application.config';
