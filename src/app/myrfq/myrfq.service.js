/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class MyRFQService {
    constructor(dataServices, $q) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            dataServices,
            $q
        });

        this._openRFQ = [];
        this._closedRFQ = [];
    }

    get openRFQ() {
        return this._openRFQ;
    }

    set openRFQ(obj) {
        this._openRFQ = obj;
    }

    get closedRFQ() {
        return this._closedRFQ;
    }

    set closedRFQ(obj) {
        this._closedRFQ = obj;
    }

    getOpenRFQ(obj) {
        let vm = this,
            {
                dataServices,
                $q
            } = vm.DI();
        
        return $q(function (resolve, reject) {
            dataServices.openRFQ(obj).then(function(response) {
                /*let i,j,receiverDTOs = [],receiverDTO;
                for(i =0;i<response.length;i++) {
                    for(j=0;j<response[i].rfqReciverDTOs.length;j++){
                        receiverDTO = response[i].rfqReciverDTOs[j];
                        receiverDTO.rfqItemsDTOs = response[i].rfqItemsDTOs;
                        receiverDTO.status = response[i].status;
                        receiverDTO.company = response[i].company;
                        receiverDTO.initiatedOn = response[i].initiatedOn;
                        receiverDTO.deadlineOn = response[i].deadlineOn;
                        receiverDTO.userMail = response[i].userMail;
                        receiverDTO.rfqUniqueId = response[i].rfqUniqueId;
                        receiverDTO.id = response[i].id;
                        receiverDTO.userId = response[i].userId;
                        receiverDTOs.push(receiverDTO);
                    }
                }*/
                vm.openRFQ = response;
                resolve(response);
            });
        });        
    }

    getClosedRFQ(obj) {
        let vm = this,
            {
                dataServices,
                $q
            } = vm.DI();
        
        return $q(function (resolve, reject) {
            dataServices.closedRFQ(obj).then(function(response) {
                /*let i,j,receiverDTOs = [],receiverDTO;
                for(i =0;i<response.length;i++) {
                    for(j=0;j<response[i].rfqReciverDTOs.length;j++){
                        receiverDTO = response[i].rfqReciverDTOs[j];
                        receiverDTO.rfqItemsDTOs = response[i].rfqItemsDTOs;
                        receiverDTO.status = response[i].status;
                        receiverDTO.company = response[i].company;
                        receiverDTO.initiatedOn = response[i].initiatedOn;
                        receiverDTO.deadlineOn = response[i].deadlineOn;
                        receiverDTO.userMail = response[i].userMail;
                        receiverDTO.rfqUniqueId = response[i].rfqUniqueId;
                        receiverDTO.id = response[i].id;
                        receiverDTO.userId = response[i].userId;
                        receiverDTOs.push(receiverDTO);
                    }
                }*/
                vm.closedRFQ = response;
                resolve(response);
            });
        });        
    }

    updateRFQStatus(payload) {
        let vm = this,
            {
                dataServices,
                $q
            } = vm.DI();
        return $q(function (resolve, reject) {
            dataServices.updateRFQStatus(payload).then(function(response) {
                resolve(response);
            });
        });
    }

    getRFQDetails(id) {
        let vm = this;
        let { dataServices, $q } = vm.DI();

        return $q(function(resolve, reject) {
            dataServices.getRFQDetails(id).then(function(response) {
                resolve(response);
            }); 
        });
    }
}