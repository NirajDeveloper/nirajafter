// <!-- Author: Gautham Manivannan -->
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import {routeConfig} from './myrfq.route';
import { MyRFQController } from './myrfq.controller';
import { MyRFQService } from './myrfq.service';
import { RFQStatusController } from './rfqstatus/rfq.status.controller'; 

angular.module('aftermarket.myrfq', [])
.config(routeConfig)
.service('MyRFQService', MyRFQService)
.controller('MyRFQController', MyRFQController)
.controller('RFQStatusController', RFQStatusController);
