/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class RFQStatusController {
    constructor( $scope, $uibModal, $uibModalInstance, details, MyRFQService) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $scope, $uibModal, $uibModalInstance, details, MyRFQService });
        vm.details = details;
    }

    closeStatus() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close();
    }

    confirmReceipt() {
        let vm = this;
        let { MyRFQService, $scope, $uibModalInstance } = vm.DI();

        let payload = {
            "rfqReciverId": vm.details.id,
            "rfqUserStatus": "CONFIRM_RECEIPT"
        };
        let reminder = MyRFQService.updateRFQStatus(payload);
        //$scope.$emit("sentConfirmReceipt", {});
        reminder.then(function (response) {
            $scope.$emit("reloadOpenRFQ");
            $scope.$emit("rfqNotfDetails");
            $uibModalInstance.close();
        });
    }


}