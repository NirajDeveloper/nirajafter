//Author: Gautham Manivannan
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class MyRFQController {
  constructor($rootScope, $translate, $scope, $uibModal, MyRFQService) {
    'ngInject';
    let vm = this;
    vm.DI = () => ({ $rootScope, $translate, $scope, $uibModal, MyRFQService });
    vm.init("open");
    let reloadOpenRFQ = $rootScope.$on("reloadOpenRFQ", function (evt, args) {
      let payload = vm.getPayload();
      vm.getOpenRFQ(payload);
    });
    $scope.$on("$destroy", reloadOpenRFQ);

  }

  init(type) {
    let vm = this;
    vm.reset();
    let payload = vm.getPayload();
    if (type === "open")
      vm.getOpenRFQ(payload);
    else if (type === "closed")
      vm.getClosedRFQ(payload);
  }

  reset() {
    let vm = this;
    
    vm.searchUid = "";
    vm.index = 1;
    vm.size = 10;
    vm.openRFQ = [];
    vm.closedRFQ = [];
    vm.sortOrder = true;
    vm.sortAttr = "DATE_INITIATED";
  }

  getPayload() {
    let vm = this;
    let from = 10 * (vm.index - 1);
    let obj = {
      from: from,
      size: vm.size,
      sortParam: vm.sortAttr,
      sortDsc: vm.sortOrder,
      aggrReq: false,
      filter: {}
    };

    if (vm.searchUid !== "")
      obj.filter.rfqUid = vm.searchUid;

    return obj;
  }

  getSortOrder(key) {
    let vm = this;
    let bool;
    let sortAttr = vm.getSortAttr(key);
    if (vm.sortAttr === sortAttr) {
      bool = (vm.sortOrder) ? false : true;
    }
    else {
      bool = false;
    }
    return bool;
  }

  getSortAttr(key) {
    let val;
    switch (key) {
      case 0:
        val = "RFQ_ID";
        break;
      case 1:
        val = "DISTRIBUTOR";
        break;
      case 2:
        val = "DATE_INITIATED";
        break;
      case 3:
        val = "DUE_IN";
        break;
      case 4:
        val = "STATUS";
        break;
      case 5:
        val = "DATE_CLOSED";
        break;
      default:
        val = "DATE_INITIATED";
        break;
    }

    return val;
  }

  sortOpenRFQ(key) {
    let vm = this;
    vm.sortOrder = vm.getSortOrder(key);
    vm.sortAttr = vm.getSortAttr(key);
    vm.index = 1;
    let payload = vm.getPayload();
    vm.getOpenRFQ(payload);
  }

  sortClosedRFQ(key) {
    let vm = this;
    vm.sortOrder = vm.getSortOrder(key);
    vm.sortAttr = vm.getSortAttr(key);
    vm.index = 1;
    let payload = vm.getPayload();
    vm.getClosedRFQ(payload);
  }

  filterOpenRFQ() {
    let vm = this;
    let { MyRFQService } = vm.DI();
    let payload = vm.getPayload();
    let openRFQ = MyRFQService.getOpenRFQ(payload);
    openRFQ.then(function (resolve) {
      vm.openRFQ = MyRFQService.openRFQ;
    });
  }

  filterClosedRFQ() {
    let vm = this;
    let { MyRFQService } = vm.DI();
    let payload = vm.getPayload();
    let closedRFQ = MyRFQService.getClosedRFQ(payload);
    closedRFQ.then(function (resolve) {
      vm.closedRFQ = MyRFQService.closedRFQ;
    });
  }

  onOpenClear() {
    let vm = this;
    if (vm.searchUid === "")
      vm.filterOpenRFQ();
  }

  onClosedClear() {
    let vm = this;
    if (vm.searchUid === "")
      vm.filterClosedRFQ();
  }

  navOpenRFQ() {
    let vm = this;

    let payload = vm.getPayload();
    vm.getOpenRFQ(payload);
  }

  navClosedRFQ() {
    let vm = this;

    let payload = vm.getPayload();
    vm.getClosedRFQ(payload);
  }

  status(key) {
    let rStr;
    switch (key) {
      case "DRAFT":
        rStr = "MYRFQ.STATUS.DRAFT";
        break;
      case "SENT_TO_DISTRIBUTOR":
        rStr = "MYRFQ.STATUS.SENT_TO_DISTRIBUTOR";
        break;
      case "CANCELLED":
        rStr = "MYRFQ.STATUS.CANCELLED";
        break;
      case "CLOSED":
        rStr = "MYRFQ.STATUS.CLOSED";
        break;
      case "QUOTE_SENT_BY_DISTRIBUTOR":
        rStr = "MYRFQ.STATUS.QUOTE_SENT_BY_DISTRIBUTOR";
        break;
      case "OPENED_BY_DISTRIBUTOR":
        rStr = "MYRFQ.STATUS.OPENED_BY_DISTRIBUTOR";
        break;
      case "CONFIRM_RECEIPT":
        rStr = "MYRFQ.STATUS.CONFIRM_RECEIPT";
        break;
      default:
        rStr = "";
        break;
    }
    return rStr;
  }

  getOpenRFQ(obj) {
    let vm = this,
      {
        MyRFQService,
        $rootScope
      } = vm.DI();

    let payload = obj;
    let openRFQ = MyRFQService.getOpenRFQ(payload);
    openRFQ.then(function (resolve) {
      vm.openRFQ = MyRFQService.openRFQ;
    });
  }

  getClosedRFQ(obj) {
    let vm = this,
      {
        MyRFQService,
        $rootScope
      } = vm.DI();

    let payload = obj;
    let closedRFQ = MyRFQService.getClosedRFQ(payload);
    closedRFQ.then(function (resolve) {
      vm.closedRFQ = MyRFQService.closedRFQ;
    });
  }

  openRFQDetails(obj) {
    let vm = this,
      size = "md";
    let { $uibModal } = vm.DI();
    var uibModalInstance = $uibModal.open({
      templateUrl: 'app/mylist/rfqdetails/rfqdetails.html',
      size: size,
      controller: 'RFQDetailsController',
      controllerAs: 'RFQD',
      backdrop: 'static',
      animation: false,
      windowClass: 'my-modal-popup rfq-details-modal',
      resolve: {
        details: function () {
          return obj;
        }
      }
    });
  }

  openRFQStatus(obj) {
    let vm = this,
      size = "md";
    let { $uibModal, $rootScope } = vm.DI();
    var uibModalInstance = $uibModal.open({
      templateUrl: 'app/myrfq/rfqstatus/rfq.status.html',
      size: size,
      controller: 'RFQStatusController',
      controllerAs: 'RFQS',
      backdrop: 'static',
      animation: false,
      windowClass: 'my-modal-popup rfq-status-modal',
      resolve: {
        details: function () {
          return obj;
        }
      }
    });
  }

  sendReminder(items) {
    let vm = this;
    let { $rootScope, MyRFQService } = vm.DI();
    let payload = {
      "rfqReciverId": items.id,
      "rfqUserStatus": "REMINDER"
    };

    let reminder = MyRFQService.updateRFQStatus(payload);
    reminder.then(function (response) {
      let payload = vm.getPayload();
      vm.getOpenRFQ(payload);
      $scope.$emit("rfqNotfDetails");
    });

  }

  cancelRFQ(items) {
    let vm = this;
    let { $rootScope, MyRFQService } = vm.DI();
    let payload = {
      "rfqReciverId": items.id,
      "rfqUserStatus": "CANCELLED"
    };

    let reminder = MyRFQService.updateRFQStatus(payload);
    reminder.then(function (response) {
      let payload = vm.getPayload();
      vm.getOpenRFQ(payload);
      $scope.$emit("rfqNotfDetails");
    });
  }
}