//author: Gautham Manivannan
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function routeConfig($stateProvider){
	'ngInject';
	$stateProvider
	.state('myrfq',{
		url: '/myrfq',
		parent: 'aftermarket',
		templateUrl: 'app/myrfq/myrfq.html',
		controller: 'MyRFQController',
		controllerAs: 'rfq',
        authenticate: true
	});
}