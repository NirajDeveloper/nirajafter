/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function routeConfig($stateProvider) {
    $stateProvider
        .state('category', {
            url: '/category/:catName',
            parent: 'aftermarket',
            templateUrl: 'app/category/category.html',
            controller: 'CategoryController',
            controllerAs: 'category'

        });
}   

routeConfig.$inject = ['$stateProvider'];