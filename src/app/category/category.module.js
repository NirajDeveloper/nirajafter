/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import {CategoryController} from './category.controller';
import {routeConfig} from './category.route';

angular.module('aftermarket.category', [])
    .config(routeConfig)
    .controller('CategoryController', CategoryController);