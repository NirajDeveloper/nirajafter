/*Author:Rohit Rane*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class CategoryController {
    constructor($log,$stateParams) {
        
        let vm = this;
        
        vm.categoryName = $stateParams.catName;
    }
}

CategoryController.$inject=["$log","$stateParams"];