/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function nospecialcharDirective() {
    //var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;
    //removed as per sonar report - no scope variable
    let directive = {
        require: 'ngModel',
      restrict: 'A',

        // optional compile function
        compile(elem, attrs) {
            return this.linkFunction;
        },

        linkFunction(scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function(inputValue) {
          if (inputValue === undefined)
            return ''
         var cleanInputValue = inputValue.replace(/[^\w\s]/gi, '');
          if (cleanInputValue !== inputValue) {
            modelCtrl.$setViewValue(cleanInputValue);
            modelCtrl.$render();
          }
          return cleanInputValue;
        });
      }
    };
    return directive;
}
