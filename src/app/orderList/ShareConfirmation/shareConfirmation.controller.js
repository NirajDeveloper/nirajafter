/*Author : Shaifali Jaiswal*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class ShareConfirmationController {
    constructor($stateParams, $scope,dataServices, SearchBarService, OrderListService,$uibModalInstance, $translate) {
        let vm = this;
        vm.DI = () => ({$stateParams, $scope, dataServices, OrderListService,$uibModalInstance});
         $scope.orderId  = OrderListService.orderId;
       $scope.quantity  = OrderListService.orderList.length;

       $scope.cancel = function(){
            $uibModalInstance.close();
        }

    }
  
}

ShareConfirmationController.$inject = ['$stateParams', '$scope','dataServices', 'SearchBarService', 'OrderListService','$uibModalInstance', '$translate'];
