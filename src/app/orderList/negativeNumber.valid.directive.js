/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export function nonegativeDirective() {
  
    let directive = {
        require: 'ngModel',

        // optional compile function
        compile(elem, attrs) {
            return this.linkFunction;
        },

        linkFunction(scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$validators.positive = function(value) {
                let ret = true;
                if(angular.isDefined(value) && value!=="" && value !== 0){
                    ret =  (value > 0);
                }
                 
                return ret;
                
            }
        }
    
}
return directive;
}
