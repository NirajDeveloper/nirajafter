/*Author : Shaifali Jaiswal*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function routeConfig($stateProvider){
	$stateProvider
		.state('orderList', {
			url: '/orderlist',
			parent: 'aftermarket',
			templateUrl: 'app/orderList/orderList.html',
			controller:'OrderListController',
			controllerAs:'orderlist',
        	authenticate: true
		})
		.state('sharedOrderList', {
			url: '/sharedOrderList/:id',
			parent: 'aftermarket',
			templateUrl: 'app/orderList/sharedOrderList/sharedOrderList.html',
			controller:'SharedOrderListController',
			controllerAs:'sharedOrderlist',
        	authenticate: true
		});
}

routeConfig.$inject = ['$stateProvider'];
