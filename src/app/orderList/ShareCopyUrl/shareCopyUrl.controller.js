/*Author : Shaifali Jaiswal*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class ShareUrlController{
    constructor($scope,$stateParams, dataServices, SearchBarService, OrderListService,$uibModalInstance) {
        let vm = this;
        vm.DI = () => ({$scope,$stateParams, dataServices, OrderListService,$uibModalInstance});
        $scope.OrderId = OrderListService.orderId;
         $scope.Url = location.host + "/" + "sharedOrderList/" + OrderListService.orderId;
         $scope.quantity = OrderListService.orderList.length;

        $scope.cancel = function(){
        	$uibModalInstance.close();
        }
    }
}

ShareUrlController.$inject = ['$scope','$stateParams', 'dataServices', 'SearchBarService', 'OrderListService','$uibModalInstance'];
