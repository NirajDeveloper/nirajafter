/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class OrderListService {
    constructor(dataServices, $http, $rootScope, $window, Session, AUTH_EVENTS, authenticationService) {
        'ngInject';

        let vm = this;
        vm.DI = () => ({
            dataServices, $http, $rootScope, $window, Session, AUTH_EVENTS, authenticationService
        });
        this._orderList = [];
        this._orderId = 0;
        this._myLists = [];
        this.updated = false;
        this.listItems = [];
        this.listCount = 0;
        this.defaultListId;
        this.selectedList = {};
        $rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
            vm._myLists = [];
            vm.getMyLists();
        });
        $rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
            vm._myLists = [];
            vm.updated = false;
        });

    }

    get orderList() {
        return this._orderList;
    }

    set orderList(newList) {
        this._orderList = newList;
        //sessionStorage.orderList = angular.toJson(this._orderList);
    }
    get myLists() {
        return this._myLists;
    }
    set myLists(newList) {
        this._myLists = newList;
        //sessionStorage.myLists = angular.toJson(this._myLists);
    }
    get orderId() {
        return this._orderId;
    }

    set orderId(newId) {
        this._orderId = newId;
        sessionStorage.orderId = angular.toJson(this._orderId);
    }
    getMyLists(obj) {
        let vm = this;
        let {dataServices, $rootScope} = vm.DI();
        dataServices.getLists('ORDER_LIST')
            .then(function (response) {
                if (response && response[0] && response[0].id) {
                    vm.defaultListId = response[0].id;
                    vm.selectedList.id = response[0].id;
                    vm.selectedList.name = response[0].cartName;
                    vm._myLists = response;
                }
                else {
                    vm._myLists = [];
                }

                vm.updated = true;
                $rootScope.$broadcast('refreshList', obj);
                $rootScope.$emit("updateListInPopUp");
            }, function (error) {

                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });


    }
    fetchMyLists() {
        let vm = this;
        if (this._myLists.length > 0) {
            vm.defaultListId = this._myLists[0].id;
        }
        return this._myLists;
    }
    createNewList(listName) {
        let vm = this;
        let {dataServices, $rootScope} = vm.DI();
        dataServices.createList({ "cartName": listName }, listName)
            .then(function (response) {
                if (response.mess === 'Created') {
                    vm._myLists.push({
                        "id": response.code,
                        "cartName": listName,
                        "cartType": "ORDER_LIST",
                        "cartLineCount": 0
                    });
                    $rootScope.$broadcast('refreshList');

                }
                $rootScope.$broadcast('listCreateAPIMessage', { mess: response.mess });

            }, function (error) {
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }
    fetchListItems(listid) {
        let vm = this;
        let {dataServices, $rootScope} = vm.DI();
        dataServices.fetchListItems(listid)
            .then(function (response) {
                if (response.lineItems.length > 0) {
                    vm._orderList = response.lineItems;
                    vm.listCount = response.lineItems.length;
                    $scope.$emit('refreshListDetails');
                }
                else {
                    vm._orderList = [];
                    vm.listCount = 0;
                }
            }, function (error) {
                vm._orderList = [];
                vm.listCount = 0;
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }
    addItemToOrderList(partObj, listId) {
        let vm = this;
        let {dataServices} = vm.DI();
        let listNumber = listId;
        dataServices.addItemToOrderList(partObj, listId)
            .then(function (response) {
                //{code: "[524]", mess: "Created"}
                if (response.mess === "Created") {
                    vm.incrementListCount(listNumber);
                }
            }, function (error) {

                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }
    incrementListCount(id) {
        let vm = this;
        let lid = id;
        // vm._myLists.map(function(item, index) {
        //    if(item.id == lid) {
        //        item.cartLineCount = item.cartLineCount + 1;
        //    }
        // })
        vm.getMyLists();
    }
    decrementtListCount(id) {
        let vm = this;
        let lid = id;
        // vm._myLists.map(function(item, index) {
        //    if(item.id == lid) {
        //        item.cartLineCount = item.cartLineCount - 1;
        //    }
        // })
        vm.getMyLists();
    }
    deleteItemFromOrderList(partId, listId) {
        let vm = this;
        let {dataServices} = vm.DI();
        let listNumber = listId;
        dataServices.fetchListItems(listId)
            .then(function (response) {
                if (response) {
                    if (response.mess === "Accepted") {
                        vm.decrementtListCount(listNumber);
                    }

                }
                else {
                    vm._orderList = [];
                    vm.listCount = 0;
                }
            }, function (error) {

                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }
    deleteOrderList(id) {
        let vm = this;
        let {dataServices, $rootScope} = vm.DI();
        dataServices.deleteList(id)
            .then(function (response) {
                if (response.mess === "Accepted") {
                    vm.getMyLists();
                    $rootScope.$broadcast('listDeletedAPIMessage', { mess: response.mess });
                }
            }, function (error) {

                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }
    updateOrderListItem() {
        let vm = this;
        let {dataServices} = vm.DI();
        dataServices.fetchListItems(listId)
            .then(function (response) {

            }, function (error) {

                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }

}




