/*Author : Shaifali Jaiswal*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class CopyOrderListURLController {
    constructor($scope, $timeout, dataServices, SearchBarService, OrderListService, $uibModalInstance, OrderlistModalFactory) {
        let vm = this;
        vm.DI = () => ({
            $scope, $timeout, $uibModalInstance, dataServices, OrderListService, OrderlistModalFactory
        });
        $scope.shareURL = location.host + "/" + "sharedOrderList/" + OrderListService.orderId;
 $scope.orderId  = OrderListService.orderId;
       $scope.quantity  = OrderListService.orderList.length;

        vm.copySelectionText = function() {
            var emailfield = document.querySelector("#shreURL")
            emailfield.focus() // this is necessary in most browsers before setSelectionRange() will work
            emailfield.setSelectionRange(0, emailfield.value.length) // select the 5th to last characters of input field

            if (emailfield.length > 0)
                document.execCommand("copy");
            $uibModalInstance.close();
            $timeout(OrderlistModalFactory.open("lg", "app/orderList/ShareCopyUrl/shareUrl.html", "ShareUrlController", "shareUrl"), 2000);

        }

        vm.clipCopyHandler = function() {
            angular.noop();
        }
    }
    cancel() {}
}

CopyOrderListURLController.$inject = ['$scope', '$timeout', 'dataServices', 'SearchBarService', 'OrderListService', '$uibModalInstance', 'OrderlistModalFactory'];