/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class OrderlistModalFactory {

  /*@ngInject*/
    constructor($uibModal,$timeout) {
        //this.$timeout = $timeout;
         return {
      open: function(size, templateParam, controllerParam, controlleras, className ="", staticBackdrop = false, mode = "normal") {
        return $uibModal.open({
          animation: true,
         /* templateUrl: 'app/orderList/ShareGetUrl/getUrl.html',
          controller: 'CopyOrderListURLController',
          controllerAs: 'copyUrl',*/
            backdrop  : staticBackdrop ? 'static': 'true',
            keyboard  : staticBackdrop ? false: true,
           templateUrl: templateParam,
          controller: controllerParam,
          controllerAs: controlleras,
          windowClass: className,
          size: size,
          resolve: {
            mode: function() {
              return mode;
            }
          }
        });
      }
    };
  }
}
OrderlistModalFactory.$inject = ['$uibModal', '$timeout'];









