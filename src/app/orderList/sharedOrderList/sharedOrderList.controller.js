/*Author : Shaifali Jaiswal*/

/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class SharedOrderListController {
    constructor($stateParams, dataServices, SearchBarService, OrderListService, $translate) {
        let vm = this;
        vm.DI = () => ({$stateParams, dataServices, OrderListService});
        vm.getSharedList();
    }

    getSharedList(){
    	let vm = this,
      	{ $stateParams, dataServices, OrderListService } = vm.DI();
    	dataServices.sharedOrderList($stateParams.id).then(function (response) {
            vm.sharedList = response.data.APIResponse;
        }, function (error) {
        });
    }
}

SharedOrderListController.$inject = ['$stateParams', 'dataServices', 'SearchBarService', 'OrderListService', '$translate'];
