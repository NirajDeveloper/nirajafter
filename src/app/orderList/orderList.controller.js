/*Author : Shaifali Jaiswal*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class OrderListController {
    constructor($uibModal, $log, OrderListService, $translate) {
        let vm = this;
        vm.DI = () => ({ $uibModal, $log, OrderListService });
        vm.getOrderList();
        vm.isOrderlistEnabled = () => OrderListService.orderList.length > 0 ? true : false;
   	}

    getOrderList() {
        let vm = this;
        let {$log, $uibModal, OrderListService} = vm.DI();
        let storedOrderList = sessionStorage.orderList?angular.fromJson(sessionStorage.orderList):[];
        let emptyCheck = (OrderListService.orderList.length === 0 && storedOrderList.length === 0) ? true
            : false;
        vm.isOrderlistEnabled = () => emptyCheck ? true : false;
        if (OrderListService.orderList.length === 0 && storedOrderList.length === 0) return false;
        vm.orderId = OrderListService.orderId;
        vm.orderList = OrderListService.orderList = angular.fromJson(sessionStorage.orderList);
    }

    remove(itemIndex) {
        let vm = this;
        let {$log, $uibModal, OrderListService} = vm.DI();
        OrderListService.orderList.forEach(function (item, index, object) {
            if (item.id.toString() === itemIndex.toString()) {
                object.splice(index, 1)
            }
        })
        sessionStorage.orderList = angular.toJson(OrderListService.orderList);
        vm.orderList = OrderListService.orderList;
    }
    save(index, qty) {
        let vm = this;
        let {$log, $uibModal, OrderListService} = vm.DI();
        for (var i in OrderListService.orderList) {
            if (OrderListService.orderList[i].id.toString() === index.toString()) {
                OrderListService.orderList[i].quantity = qty;
            }
        }
        sessionStorage.orderList = angular.toJson(OrderListService.orderList);
        vm.orderList = OrderListService.orderList;
    }

    edit(part, qty) {
        let vm = this;
        let {$log, $uibModal, OrderListService} = vm.DI();
    }

   	open() {
        let vm = this, size = "md";
        let {$log, $uibModal} = vm.DI();
        $uibModal.open({
            templateUrl: 'app/orderList/shareOrderList/shareOrderList.html',
            controller: 'ShareOrderlistController',
            controllerAs: 'shareList',
            size: size,
            windowClass: 'my-modal-popup',
            resolve: {
                items: function () {
                    return vm.items;
                }
            }
        });
   	}

    cancel() {
        let vm = this,
            {$uibModalInstance} = vm.DI();
        $uibModalInstance.close();
    }
}

OrderListController.$inject = ['$uibModal', '$log', 'OrderListService', '$translate'];