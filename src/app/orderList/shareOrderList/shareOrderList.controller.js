/*Author : Shaifali Jaiswal*/
/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class ShareOrderlistController {
    constructor($uibModalInstance, $scope, $uibModal, $timeout, $location, items, dataServices, SearchBarService, OrderListService, OrderlistModalFactory, $translate, $rootScope) {
        let vm = this;
        vm.DI = () => ({
            $uibModalInstance, $scope, $timeout, $location, dataServices, OrderListService, OrderlistModalFactory, $rootScope
        });
        vm.orderId = OrderListService.orderId;
        vm.date = new Date();
        $scope.emails = null;
        $scope.isThisDisabled = () => { OrderListService.orderList.length > 0 ? true : false }

        if (sessionStorage.userService !== undefined) {
            var data = angular.fromJson(sessionStorage.userService);
            $scope.fName = data.fName;
            $scope.lName = data.lName;
            $scope.fEmail = data.fEmail;
            $scope.descText = data.descText;
            $scope.phoneNumber = data.phoneNumber;
        }

    }

    cancel() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close();
    }

    shareOrderList(flag) {
        let vm = this,
            {
                $location, $scope, $uibModalInstance, dataServices, SearchBarService, OrderListService, OrderlistModalFactory, $timeout, $rootScope
            } = vm.DI();
        sessionStorage.orderList = OrderListService.orderList;
        let tempOrd = OrderListService.orderId.toString();
        //let tempOrd = "";
        let payload = {
            "uuid": tempOrd,
            "fromFirstName": $scope.fName,
            "fromLastName": $scope.lName,
            "fromEmail": $scope.fEmail,
            "fromPhoneNumber": $scope.phoneNumber,
            "customerCallbackRequired": $scope.callback,
            "description": $scope.descText,
            "sendTo": $scope.emails.split(","),
            "sharedURL": $rootScope.protocol + "://" + location.host + "/" + "sharedOrderList",
            "orderParts": OrderListService._orderList, //OrderListService.orderList,
            "createdOn": new Date().toJSON()
        };


        var model = {
            fName: $scope.fName,
            lName: $scope.lName,
            fEmail: $scope.fEmail,
            text: $scope.descText,
            phoneNumber: $scope.phoneNumber
        };


        sessionStorage.userService = angular.toJson(model);
        let promise = dataServices.shareList(payload).then(function (response) {
            OrderListService.orderId = response;
        });

        /* Once orderlist changed we should get new orderlist */
        promise.then(() => {
            dataServices.orderList().then(function (response) {
                OrderListService.orderId = response;
                sessionStorage.orderId = angular.toJson(response);
                OrderListService.orderList = [];
            });
        });

        $uibModalInstance.close();
        if (flag !== true) {
            $timeout(OrderlistModalFactory.open("lg", "app/orderList/ShareGetUrl/getUrl.html", "CopyOrderListURLController", "copyUrl"), 2000);
        } else {
            $timeout(OrderlistModalFactory.open("lg", "app/orderList/ShareConfirmation/shareConfirmation.html", "ShareConfirmationController", "shareConfirm"), 2000);
        }

    }
}

ShareOrderlistController.$inject = ['$uibModalInstance', '$scope', '$uibModal', '$timeout', '$location', 'items', 'dataServices', 'SearchBarService', 'OrderListService', 'OrderlistModalFactory', '$translate', '$rootScope'];


