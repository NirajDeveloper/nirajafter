/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { appConstants } from './index.constants';
import { MainController } from './main/main.controller';
import { mainConfig } from './main/main.config';
import {httpInterceptor} from './http.interceptor';
import "babel-polyfill";
import { SocketIOService } from "./socketio/socketio.service.js";


angular.module('aftermarket',
    ['ui.router',
        'ui.bootstrap',
        'ngAnimate',
        'angular.filter',
        'imageZoom',
        'rzModule',
        'ngSanitize',
        'breadCrumb',
        'loading',
        'sending',
        'form-controls',
        'pascalprecht.translate',
        'aftermarket.header',
        'aftermarket.login',
        'aftermarket.home',
        'aftermarket.searchResults',
        'aftermarket.part',
        'aftermarket.category',
        'aftermarket.core',
        'aftermarket.footer',
        'aftermarket.pricing',
        'aftermarket.orderList',
        'aftermarket.alt-home',
        'aftermarket.signup',
        'aftermarket.ordermgt',
        'aftermarket.previewOrder',
        'aftermarket.completedOrder',
        'aftermarket.incompleteOrder',
        'aftermarket.postorder',
        'aftermarket.mylist',
        'aftermarket.userProfile',
        'aftermarket.wheretobuy',
        'aftermarket.poError',
        'aftermarket.report',
        'aftermarket.myrfq',
        'aftermarket.managerfq',
         'aftermarket.expresscheckout',
         'aftermarket.dealerLocator',
         'aftermarket.imageGallery'
        
    ]
)
    .constant('AftermarketConstants', appConstants)
    .config(config)
    .config(routerConfig)
    .run(runBlock)
    .controller('MainController', MainController)
    .config(mainConfig)
    .service('httpInterceptor',httpInterceptor)
    .service('SocketIOService',SocketIOService);

//Main module.

/*Tagging for qa-06-Jul-2016-1.0*/
