/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class MainController {
    constructor($scope, $rootScope, $state, $timeout, $interval, $log, $document, $window, $translate, BreadCrumbService, dataServices, appInfoService) {
        let vm = this;
        vm.DI = () => ({ $scope, $timeout, $log, $document });

        vm.landscape = true;

        $translate('FILTER.TOOLTIP').then((txt)=>{
            sessionStorage.categoryHint = txt;
        });        

        if ($window.innerHeight > $window.innerWidth) {
            vm.landscape = false;
        }
        // $window.onscroll = function (ev) {
        //     if (($window.innerHeight + $window.scrollY) >= $document[0].body.offsetHeight - 90) {
        //         // you're at the bottom of the page
        //         let bottomOffset = $document[0].body.offsetHeight - ($window.innerHeight + $window.scrollY);
        //         $rootScope.$emit("isHeaderSticky", { bottomOffset: bottomOffset, state: true });
        //     } else {
        //         $rootScope.$emit("isHeaderSticky", { state: true });
        //     }
        // };

        dataServices.appInfo().then(response => {
            appInfoService.appInfo = response;
            if ($state.is("aftermarket")) {
                $state.go("home");
            }
        }, error => {
            angular.noop();
        });

        let showTree = $rootScope.$on("showOnlyTreeInBC", (evt, status) => {
            BreadCrumbService.showOnlyTree = status;
        });

        $rootScope.$on("$destroy", () => {
            showTree();
        });
    }


    addOpaqueOverlay() {
        var vm = this;
        let {$timeout, $log, $document} = vm.DI();
        var mc = $document[0].getElementById('main-content');
        var mcHeight = mc.offsetHeight;
        vm.putOverlay = true;
        $timeout(function () {
            var overlay = angular.element($document[0].getElementById('overlay'));
            overlay.css("height", mcHeight + 'px');
        });
    }

    clearOpaqueOverlay() {
        let vm = this;
        vm.putOverlay = false;
    }
}

MainController.$inject = ['$scope', '$rootScope', '$state', '$timeout', '$interval', '$log', '$document', '$window', '$translate', 'BreadCrumbService', 'dataServices', 'appInfoService'];
