/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

import {routeConfig} from './mylist.route';
import {MylistController} from './mylist.controller';
import {CreatenewlistController} from './createlist/createlist.controller';
import {ManagelistController} from './managelist/managelist.controller';
import { printDirective } from './print.directive';
 import {ReqquoteController} from './requestquote/sendrequestquote.controller';
 import {RFQDetailsController} from './rfqdetails/rfqdetails.controller';
 import {ShareListController} from './sharelist/share.controller';
 import {ReqquoteService} from './requestquote/sendrequestquote.service';
 import {SessionListController} from './sessionlist/sessionlist.controller';
import {AddedRFQController} from './requestquote/addedrfq/addedrfq.controller';
import { ConfirmDeleteController } from './confirmdelete/confirmdelete.controller';  

angular.module('aftermarket.mylist', [])
	.config(routeConfig)
	.controller('MylistController', MylistController)
	.controller('CreatenewlistController', CreatenewlistController)
	.controller('ManagelistController', ManagelistController)
    .directive('printDirective', printDirective)
    .controller('ReqquoteController', ReqquoteController)
	.controller('RFQDetailsController', RFQDetailsController)
	.controller('ShareListController', ShareListController)
	.service('ReqquoteService', ReqquoteService)
	.controller('SessionListController', SessionListController)
    .controller('AddedRFQController', AddedRFQController)
	.controller('ConfirmDeleteController', ConfirmDeleteController);