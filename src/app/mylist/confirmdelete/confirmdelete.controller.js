/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->
export class ConfirmDeleteController {
    constructor($uibModal, $uibModalInstance, $rootScope, $scope, listData, OrderListService, dataServices) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $uibModal,  $uibModalInstance, $rootScope, $scope, listData, OrderListService, dataServices});
        vm.listId = OrderListService.selectedList.id;
        vm.listName = OrderListService.selectedList.name;
        vm.listData = listData;
    }
    close() {
        let vm = this;
        let { $uibModalInstance } = vm.DI();
        $uibModalInstance.close();
    }

    delete() {
        let vm = this;
        let { $rootScope, $scope, $uibModalInstance } = vm.DI();
        $scope.$emit("confirmDelete", {listData: vm.listData});
        $uibModalInstance.close();
    }


}