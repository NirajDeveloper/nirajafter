/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function routeConfig($stateProvider) {
	'ngInject';
	$stateProvider
		.state('mylist', {
			url: '/mylist',
			parent: 'aftermarket',
			templateUrl: 'app/mylist/mylist.html',
			controller: 'MylistController',
			controllerAs: 'mylist',
            params: { listId: 0 },
			authenticate: true
		})
		.state('sendrfq', {
			url: '/mylist/sendrfq',
			parent: 'aftermarket',
			params: {
				rfqItems: null
			},
			templateUrl: 'app/mylist/requestquote/sendrequestquote.html',
			controller: 'ReqquoteController',
			controllerAs: 'sendrfq',
			authenticate: true
		})
	/*.state('zeroitemlist', {
		url: '/zeroitemlistsearch',
		parent: 'aftermarket',
		templateUrl: 'app/mylist/zeroitemlist/zeroitemlistsearch.html'

	})
	.state('requestquote', {
		url: '/requestquote',
		parent: 'aftermarket',
		templateUrl: 'app/mylist/requestquote/sendrequestquote.html',
		controller: 'ReqquoteController',
		controllerAs: 'reqquote'
	})
	.state('sendquoteconfirm', {
		url: '/quoteconfirm',
		parent: 'aftermarket',
		templateUrl: 'app/mylist/sendquoteconfirm/quoteconfirmation.html'
		// controller: 'QuoteconfirmController',
		// controllerAs: 'quoteconfirm'
	});*/
};