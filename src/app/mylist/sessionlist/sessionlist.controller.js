/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class SessionListController {
    constructor($uibModal, $uibModalInstance, $translate){
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $uibModal, $uibModalInstance, $translate});
    }
    closeSession() {
        let vm = this,
            {
                $uibModalInstance, $uibModal
            } = vm.DI();
        sessionStorage.removeItem("saveList");
        sessionStorage.removeItem("shareList");
        sessionStorage.removeItem("tempItemList");
        $uibModalInstance.close();        
    }

    createlist() {
        let vm = this;
        let { $uibModal, $uibModalInstance } = vm.DI();
        $uibModalInstance.close();     
        let tempItemList = JSON.parse(sessionStorage["tempItemList"]);
        let modalInstance = $uibModal.open({
            templateUrl: 'app/mylist/createlist/createlist.html',
            controller: 'CreatenewlistController',
            controllerAs: 'newlist',
            size: 'md',
            windowClass: 'my-modal-popup',
            resolve: {
                tempItemList: function () {
                    return tempItemList;
                }
            }
        });
    }
}