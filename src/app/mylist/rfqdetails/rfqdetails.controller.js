/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class RFQDetailsController {
    constructor( $uibModal, $uibModalInstance, details, $translate, ReqquoteService, AftermarketConstants) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            $uibModal,  $uibModalInstance, details, $translate, ReqquoteService, AftermarketConstants
        });
        vm.loading = false;
        vm.websiteLogo = AftermarketConstants.skin.logo;
        vm.getRFQDetails = details.getRFQDetails;
        if(details.getRFQDetails) {
            let rfqReceiver = ReqquoteService.getRFQReceiver(details.id);
            rfqReceiver.then(function(response) {
                vm.details = response;
            });
        }
        else {
            vm.details = details;
        }
          
    }

    closeDetails() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close();
    }
}