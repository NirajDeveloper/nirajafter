/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class ManagelistController {
    constructor($uibModal, $uibModalInstance, $scope, $q, $filter, $translate, dataServices, OrderListService, $rootScope, authenticationService) {
        'ngInject';
        let vm = this;
         vm.DI = () => ({ $uibModal, $uibModalInstance, $scope, $q, $filter, dataServices, OrderListService, $rootScope, authenticationService });
         vm.lists = [];
         vm.selected = {};
         vm.fetchLists();
         vm.formSubmitMsg = undefined;
         vm.searchFilterListTxt = undefined;
         vm.renamedResponse = {};
         vm.renamedResponse.renameList = [];
         vm.deletedIds = [];
         vm.userPermissionsList = authenticationService.userPermissionsList;
         vm.renameListId = [];
         $rootScope.$on("updateListInPopUp", function (evt, args) {
             vm.fetchLists();
        });
         
        //  let bb =  $rootScope.$on('listDeletedAPIMessage', function(event, args){ 
        //      debugger;
        //         if(args.mess === 'Accepted'){
        //                 $uibModalInstance.close();
        //         }
        //         else {
        //             vm.errMsg = "Failed!!";
        //         }
        // });
        // $rootScope.$on("$destroy",()=>{
        //     bb();
        // });

    }
    cancel() {
        let vm = this,
            {
                $uibModalInstance
            } = vm.DI();
        $uibModalInstance.close();
    }
    renameList(list) {
        let vm = this;
        let { $filter, OrderListService } = vm.DI();
        let oldListObj = $filter('filter')(vm.oldlists, function (listObj) {return listObj.id === list.id;})[0];
        if(list && oldListObj && list.cartName && oldListObj.cartName){
            if(list.cartName.trim() != oldListObj.cartName.trim()){
                let index = vm.renameListId.indexOf(list.id);
                if(index == -1){
                    vm.renameListId.push(list.id);
                }else{
                    // no need to add this element because it is already added
                }
            }else{
                let index = vm.renameListId.indexOf(list.id);
                if(index != -1){
                    vm.renameListId.splice(index, 1);
                }else{
                    // no need to worry
                }
            }
        }else{
            // no need to rename cartName Since name did not change.
        }
    }
     fetchLists() {
        let vm = this;
        let { OrderListService } = vm.DI();
        
        vm.lists = angular.copy(OrderListService.fetchMyLists());
        vm.oldlists = angular.copy(OrderListService.fetchMyLists());

        let lists = angular.copy(vm.lists);

        if(lists.length === 0) {
            vm.cancel();
        }
    }
    showSelected() {
        let vm = this;

        let { $scope, $q, $filter, OrderListService, dataServices, $rootScope, $uibModalInstance } = vm.DI();
        vm.listSelected = false;
        vm.formSubmitMsg = undefined;
        vm.renamedResponse = {};
         vm.renamedResponse.renameList = [];
        vm.deletedIds = [];
        if(vm.selected && vm.selected.hasOwnProperty.length > 0){
            let tempObj= vm.selected;
            for(var key in tempObj) {
                if(tempObj.hasOwnProperty(key) && tempObj[key]) {
                    vm.deletedIds.push(key);
                    let index = vm.renameListId.indexOf(parseInt(key));
                    if(index !== -1){
                        vm.renameListId.splice(index, 1);
                    }
                }
            }
        }
        let listToDelete = undefined;
        let listToRenameArr = undefined;
        if(vm.deletedIds.length > 0) {
            listToDelete = vm.deletedIds.toString();
        }

        if(vm.renameListId.length > 0) {
            listToRenameArr = [];
            for(let i = 0; i < vm.renameListId.length; i++){
                let obj = $filter('filter')(vm.lists, function (listObj) {return listObj.id === vm.renameListId[i];})[0];
                let renameListObj = {};
                renameListObj.id = obj.id;
                //renameListObj.oldName = "Shefali List 1"; // not manadatory
                renameListObj.newName = obj.cartName;
                renameListObj.type = "ORDER_LIST";
                listToRenameArr.push(renameListObj);
            }
        }
        if(listToDelete && listToRenameArr){
            let renameListObj = {};
            renameListObj.renameList = listToRenameArr;
            let promises = [dataServices.deleteList(listToDelete), dataServices.renameList(renameListObj)];
            $q.all(promises).then((response) => {
                let deletedResponse = response[0];
                vm.renamedResponse = response[1]; // play with renamed response to display error or successful message
                vm.deletedIds = []; // empty stored data
                vm.selected = {};
                vm.renameListId = []; // empty stored data
                if(deletedResponse && deletedResponse.mess === "Accepted") {
                    vm.formSubmitMsg = $filter('translate')('LISTSETTING.DELETED_MSG');
                    //vm.formSubmitMsg = "Selected List(s) has been deleted."
                    OrderListService.getMyLists();
                }
                // do something on the basis of response
            });
        }else if(listToDelete){
            let promises = [dataServices.deleteList(listToDelete)];
            $q.all(promises).then((response) => {
                let deletedResponse = response[0];
                // do something on the basis of response
                vm.deletedIds = []; // // empty stored data
                vm.selected = {};
                if(deletedResponse && deletedResponse.mess === "Accepted") {
                    vm.formSubmitMsg = $filter('translate')('LISTSETTING.DELETED_MSG');
                    //vm.formSubmitMsg = "Selected List(s) has been deleted."
                    OrderListService.getMyLists();
                    //vm.cancel();
                   // vm.lists = angular.copy(OrderListService.fetchMyLists()); // new updated list 
                   // $uibModalInstance.close(vm.deletedIds);
                }
            });
        }else if(listToRenameArr){
            let renameListObj = {};
            renameListObj.renameList = listToRenameArr;
            let promises = [dataServices.renameList(renameListObj)];
            $q.all(promises).then((response) => {
                vm.renamedResponse = response[0];
                vm.renameListId = []; // empty stored data
                OrderListService.getMyLists();
                //vm.cancel(); // close overlay.
            });
        }else{
            // no need to call any api
        }
    };

    onChange() {
        let vm = this;
        vm.listSelected = false;
        if(typeof vm.selected === 'object') {
            for(let i in vm.selected) {
                if(vm.selected[i] === true) {
                    vm.listSelected = true;
                    break;
                }                    
            }
        }
        
        for(let i=0;i<vm.oldlists.length;i++) {
            for(let j=0;j<vm.lists.length;j++) {
                if(i===j&&vm.oldlists[i].cartName !== vm.lists[j].cartName) {
                    vm.listSelected = true;
                    break;
                }
            }
        }
    }
    // deleteOrderList(id){
    //     let vm = this;
    //     let {dataServices, OrderListService, $rootScope, $uibModalInstance} = vm.DI();
    //     dataServices.deleteList(id)           
    //         .then(function (response) {
    //             if(response.mess === "Accepted") {
    //                 OrderListService.getMyLists();
    //                 //vm.cancel();
    //                 $uibModalInstance.close(vm.deletedIds);
    //                 //$rootScope.$broadcast('listDeletedAPIMessage', {mess:response.mess});
    //             }
    //         }, function (error) {
               
    //             if (error.status === 500) {
    //                 //DO WHAT YOU WANT

    //             }
    //             if (error.status === 404) {
    //                 //DO WHAT YOU WANT
    //             }
    //         });     
    // }
    // renameOrderList(renameList){
    //     let vm = this;
    //     let {dataServices, OrderListService, $rootScope, $uibModalInstance} = vm.DI();
    //     let ids= renameList.toString();
    //     let updatedList = [];
    //     vm.lists.map(function (item, index) {
    //         if (renameList.indexOf(item.id) !== -1) {
    //             let temp ={"cartName":item.cartName};
    //             updatedList.push(temp);
    //         }
    //     });
    // }
}
