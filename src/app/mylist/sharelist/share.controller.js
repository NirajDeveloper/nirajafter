/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class ShareListController {
    constructor($uibModal, $uibModalInstance, $translate, authenticationService, listData, dataServices, $timeout, OrderListService) {
        'ngInject';
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $uibModal, $uibModalInstance, authenticationService, listData, dataServices, $timeout, OrderListService });
        vm.userIds = "";
        vm.userMsg = "";
        vm.invalid = false;
        vm.isSuccess = false;
        vm.copyRequired = false;
        
        vm.isSessionList = listData.isSessionList;
        vm.listName = (vm.isSessionList || OrderListService.selectedList.name === undefined)?"Untitled List":OrderListService.selectedList.name;
    }

    close(isEmailSent) {
        let vm = this,
            {
                $uibModalInstance,
                authenticationService
            } = vm.DI();
        let data  = {};
        data.isEmailSent = isEmailSent;  
        $uibModalInstance.close(data);
    }

    removeError(){
        let vm = this;
        vm.invalid = false;
    }

    clearEmailTxtArea(){
        let vm = this;
        vm.userIds = '';
        vm.invalid = false;
    }
    share() {
        let vm = this,
            {
                $uibModalInstance,
                authenticationService,
                listData,
                dataServices,
                $timeout,
                OrderListService
            } = vm.DI();
        let listItems = [];
        for (let i = 0; i < listData._orderList.length; i++) {
            listItems.push({
                "partNumber": listData._orderList[i].partNo,
                "partName": listData._orderList[i].partName,
                "quantity": listData._orderList[i].quantity
            });
        }
        listItems.reverse();
        let emailIds = document.getElementById("shareToEmailIds").value;
        let commaSep = emailIds.indexOf(',');
        let semcolSep = emailIds.indexOf(';');
        let emails, re = /^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z.]{1,5}$/, validityArr, atLeastOneInvalid, payload;
        if (commaSep !== -1) {
            if (commaSep !== -1) {
                emails = emailIds.split(',');
            }
            validityArr = emails.map(function (str) {
                return re.test(str.trim());
            });
            atLeastOneInvalid = false;
            angular.forEach(validityArr, function (value) {
                if (value === false)
                    atLeastOneInvalid = true;
            });

            if (!atLeastOneInvalid) {
                payload = {
                    "listName": vm.listName,
                    "userId": authenticationService.currentUserProfileData.userId,
                    "email": authenticationService.currentUserProfileData.emailId,
                    "sentToEmails": emails.toString(),
                    "url": "http://52.9.66.239/",
                    "copyRequired": vm.copyRequired,
                    "userType": authenticationService.currentUserProfileData.userType,
                    "description": vm.userMsg
                };
                vm.invalid = false;
            }
            else {
                vm.invalid = true;
            }
        }
        else if (re.test(emailIds.trim())) {
            payload = {
                "listName": vm.listName,
                "userId": authenticationService.currentUserProfileData.userId,
                "email": authenticationService.currentUserProfileData.emailId,
                "sentToEmails": emailIds,
                "url": "http://52.9.66.239/",
                "copyRequired": vm.copyRequired,
                "description": vm.userMsg
            };
            vm.invalid = false;
        }
        else {
            vm.invalid = true;
        }


        if(!vm.invalid) {
            if(vm.isSessionList) {
                payload.orderListItems = listItems;
            }
            else {
                payload.id = OrderListService.selectedList.id;
            }
            dataServices.shareOrderList(payload).then(function (response) {
                vm.isSuccess = true;
                sessionStorage.removeItem("tempItemList");
                vm.close(true);
            });
        }
    }

}


