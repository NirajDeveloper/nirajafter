/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

// <!-- Author: Shaifali Jaiswal -->
export class MylistController {
    constructor(PricingService, $uibModal, $log, $state, $location, $scope, $translate, dataServices, OrderListService, $rootScope, $timeout, AftermarketConstants, authenticationService, ProfileService, $filter, appInfoService) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ PricingService, $uibModal, $state, $log, $location, $scope, dataServices, OrderListService, $rootScope, $timeout, AftermarketConstants, authenticationService, ProfileService, $filter, appInfoService });
        vm.lists = [];
        vm.listData = [];
        OrderListService._orderList = [];
        vm.selectedList;
        if ($state.params.listId !== 0) {
            OrderListService.defaultListId = $state.params.listId;
            OrderListService.selectedList.id = $state.params.listId;
            vm.selectedList = $state.params.listId;
        }
        vm.hasRFQ = false;
        vm.allAddedRFQ = false;
        vm.showRFQAdded = false;
        vm.hideCartBtn = true;
        vm.rtbParts = [];
        ProfileService.setType($rootScope.currentUserProfileData);
        vm.fetchLists('fetch');
        vm.fetchDraftLists('fetch');
        vm.priceCheck = AftermarketConstants.appSettings.my_list.price_check;
        //vm.listData = OrderListService._orderList;
        vm.totalItemCount = OrderListService.listCount;
        vm.loadingInstance;
        vm.loadingListItems = false;
        vm.orderListMsg = {};
        vm.orderListMsg.show = false;
        vm.orderListMsg.msg = "";
        vm.FilterList = "";
        vm.orderListErrMsg = {};
        vm.orderListErrMsg.show = false;
        vm.orderListErrMsg.msg = "";
        vm.reverse = "ASC";
        vm.partSearch = "";
        vm.cartQtyNotAllowed = false;
        let refreshList = $scope.$on('refreshList', function (evt, args) {
            if (OrderListService.myLists.length === 0) {
                vm.totalItemCount = 0;
            }
            vm.fetchLists('refresh', args);
        });
        $scope.$on("$destroy", refreshList);

        let saveList = $rootScope.$on("saveList", function (evt, args) {
            vm.createlist();
        });
        $scope.$on("$destroy", saveList);

        let shareList = $rootScope.$on("shareList", function (evt, args) {
            vm.openShareList();
        });
        $scope.$on("$destroy", shareList);


        let confirmDelete = $rootScope.$on("confirmDelete", function (evt, args) {
            vm.deleteCartItem(args.listData);
        });
        $scope.$on("$destroy", confirmDelete);

        vm.userPermissionsList = authenticationService.userPermissionsList;
        let destroyUserPermissions = $rootScope.$on("CURRENT_USER_PERMISSIONS", function (event) {
            vm.userPermissionsList = authenticationService.userPermissionsList;
        });

        let showList = $scope.$on("showList", function (evt, args) {
            vm.showList(args.id, args.name);
        });
        $scope.$on("$destroy", showList);

        if (vm.userPermissionsList.RequestQuote) {
            vm.showRFQ = true;
        }
        else {
            vm.showRFQ = false;
        }
        vm.addedRFQ = false;
        vm.listFlag = true;
        vm.imageBaseUrl = "";
        if (appInfoService.appInfo && appInfoService.appInfo.cdnBaseurl) {
              vm.imageBaseUrl = appInfoService.appInfo.cdnBaseurl;
        }
    }

    open() {
        let vm = this, size = "md";
        let {$uibModal} = vm.DI();
        var modalInstance = $uibModal.open({
            templateUrl: 'app/orderList/shareOrderList/shareOrderList.html',
            controller: 'ShareOrderlistController',
            controllerAs: 'shareList',
            // templateUrl: 'app/orderList/ShareError/shareError.html',
            // controller: 'ShareErrorController',
            size: size,
            windowClass: 'share-overlay',
            resolve: {
                items: function () {
                    return vm.items;
                }
            }
        });
   	}

    clear() {
        let vm = this;
        vm.FilterList = "";
    }
    sort(keyname) {
        let vm = this;
        vm.reverse = !vm.reverse; //if true make it false and vice versa

    }
    createlist() {
        let vm = this;
        let { $uibModal, $location } = vm.DI();
        let modalInstance = $uibModal.open({
            templateUrl: 'app/mylist/createlist/createlist.html',
            controller: 'CreatenewlistController',
            controllerAs: 'newlist',
            size: 'md',
            windowClass: 'my-modal-popup',
            resolve: {
                tempItemList: function () {
                    return null;
                }
            }
        });
    }
    fetchLists(type, obj) {
        let vm = this;
        let { OrderListService } = vm.DI();
        vm.lists = OrderListService.fetchMyLists();
        if (vm.isListAvlbl(vm.lists, vm.selectedList)) {
            if (vm.selectedList === undefined && vm.lists.length > 0) {
                vm.selectedList = vm.lists[0].id;
            }
        }
        else {
            if (vm.lists.length > 0) {
                vm.selectedList = vm.lists[0].id
            }                
        }
        debugger;
        if(obj !== undefined && obj.listCreated) {
            debugger;
            vm.selectedList = vm.lists[0].id
        }
        vm.fetchListItems(vm.selectedList);
    }

    isListAvlbl(lists, id) {
        let flag = false;
        if (id !== undefined) {
            for (let i = 0; i < lists.length; i++) {
                if (parseInt(lists[i].id) === parseInt(id)) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    showList(id, name) {
        let vm = this;
        //event.preventDefault();
        let { OrderListService } = vm.DI();
        vm.partSearch = "";
        vm.selectedList = id;
        vm.fetchListItems(id);
        OrderListService.selectedList.id = id;
        OrderListService.selectedList.name = name;
        //debugger;
    }
    managelist() {
        let vm = this;
        let { $uibModal, OrderListService } = vm.DI();
        let modalInstance = $uibModal.open({
            templateUrl: 'app/mylist/managelist/managelist.html',
            controller: 'ManagelistController',
            controllerAs: 'managelist',
            size: 'md',
            windowClass: 'my-modal-popup manage-mylist-popup',
            bindToController: true
        });
        modalInstance.result.then(function (userData) {
            if (userData && userData.indexOf(vm.selectedList) !== -1) {
                vm.selectedList = OrderListService.defaultListId;
                vm.fetchListItems(vm.selectedList);

            }
        }, function () {

        });

    }
    initList() {
        let vm = this;
        let { OrderListService } = vm.DI();
        vm.selectedList = OrderListService.defaultListId;
        if (vm.lists && vm.lists.length >= 1) {
            vm.fetchListItems(vm.selectedList);
        }
    }
    fetchDraftLists(type) {
        let vm = this;
        let { dataServices } = vm.DI();
        vm.hasRFQ = false;
        vm.draftRFQData = [];
        dataServices.getDraftList().then(function (response) {
            if (response.rfqItemsDTOs === null)
                response.rfqItemsDTOs = [];
            if (response.rfqReciverDTOs === null)
                response.rfqReciverDTOs = [];
            if (response.rfqItemsDTOs.length !== 0)
                vm.hasRFQ = true;
            vm.draftRFQData = response;
        });
    }
    blurQty(list, fieldId) {
        let vm = this;
        let { $timeout } = vm.DI();
        let fieldVal = document.getElementById(fieldId).value;
        if(fieldVal == "0" || list.quantity == undefined || list.quantity <= 0){
            list.quantity = list.packageQty;
        }
        if(list.packageQty >= 1 && list.quantity > 0 && (list.quantity % list.packageQty != 0)){
            vm.cartQtyNotAllowed = true;
        }else{
            $timeout(function(){
                if(document.getElementsByClassName('qty-err').length > 0){
                    vm.cartQtyNotAllowed = true;
                }else{
                    vm.cartQtyNotAllowed = false;
                }
            }, 0);  
        }
    }

    delRFQItem(items) {
        let vm = this;
        let { dataServices } = vm.DI();
        items.isDeleted = true;
        dataServices.deleteRFQItem(items.id).then(function (response) {

            let rfqItemsData = [];
            if (response === "") {
                for (var i in vm.draftRFQData.rfqItemsDTOs) {
                    if (vm.draftRFQData.rfqItemsDTOs[i].id !== items.id) {
                        rfqItemsData.push(vm.draftRFQData.rfqItemsDTOs[i]);
                    }
                }
                if (rfqItemsData.length === 0) {
                    vm.hasRFQ = false;
                }
                vm.draftRFQData.rfqItemsDTOs = rfqItemsData;
                vm.checkAddedToRFQ();

            }
        });
    }



    loading() {
        let vm = this;
        let {$uibModal } = vm.DI();
        vm.loadingInstance = $uibModal.open({
            templateUrl: 'app/components/loading/loading-overlay.html',
            size: "md",
            windowClass: 'my-modal-popup'
        });
    }
    fetchListItems(id) {
        let vm = this;
        let {dataServices, OrderListService, ProfileService, appInfoService} = vm.DI();
        if (id === undefined) {
            return;
        }
        vm.hideCartBtn = true;
        vm.rtbParts = [];
        vm.loadingListItems = true;
        vm.orderListMsg = {};
        vm.orderListMsg.show = false;
        vm.orderListMsg.msg = "";

        vm.orderListErrMsg = {};
        vm.orderListErrMsg.show = false;
        vm.orderListErrMsg.msg = "";
        if (appInfoService.appInfo && appInfoService.appInfo.cdnBaseurl) {
              vm.imageBaseUrl = appInfoService.appInfo.cdnBaseurl;
        }
        dataServices.fetchListItems(id)
            .then(function (response) {
                if (response.lineItems.length > 0) {
                    OrderListService.selectedList.name = response.cartName;
                    vm.selectedListName = response.cartName;
                    OrderListService._orderList = response.lineItems;
                    OrderListService.listCount = response.lineItems.length;
                    vm.listData = OrderListService._orderList;
                    vm.totalItemCount = OrderListService.listCount;
                    if (ProfileService.shopUser) {
                        vm.checkAddedToRFQ();
                    }
                    else {
                        vm.bulkRtbCheck();
                        // if (vm.priceCheck) {
                        //     vm.updatePricing();
                        // }
                    }

                }
                else {
                    OrderListService._orderList = [];
                    OrderListService.listCount = 0;
                    vm.listData = [];
                    vm.totalItemCount = 0;
                }
                vm.loadingListItems = false;
            }, function (error) {
                OrderListService._orderList = [];
                OrderListService.listCount = 0;
                vm.listData = [];
                vm.totalItemCount = 0;
                vm.loadingListItems = false;
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });

    }
    updatePricing() {
        let vm = this;
        let { OrderListService, dataServices, authenticationService } = vm.DI();
        let pciceRequest = [];
        //vm.listData
        let customerId = null;

        if (authenticationService.shipToSoldToInfo.selectedSoldTo && authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber) {
            customerId = authenticationService.shipToSoldToInfo.selectedSoldTo.customerNumber;
        }
        else if (authenticationService.associatedCustData.activeCustId) {
            customerId = authenticationService.associatedCustData.activeCustId;
        }
        //rtbParts
        for (let x of vm.listData) {
            if (vm.rtbParts.indexOf(x.partNo) !== -1) {
                let temp = {
                    "customerCid": customerId,
                    "partNumber": x.partNo,
                    "orderType": 'STK',
                    "quantityReq": x.quantity,
                    "unitOfMeasure": null
                }
                pciceRequest.push(temp);
            }
        };

        let payload = {
            "pricingReqDtlList": pciceRequest
        };
        dataServices.getCartPrice(payload).then(function (response) {
            vm.priceResponse = response.data;
            if (vm.priceResponse.code !== "PRICE_UNAVAILABLE" || "INVALID_INPUT_PARAMS") {
                if (vm.priceResponse) {
                    for (let i = 0; i < vm.priceResponse.length; i++) {
                        if (vm.priceResponse[i].partPriceDtl) {
                            let partNum = vm.priceResponse[i].partNumber;
                            let priceObj = vm.priceResponse[i].partPriceDtl.priceDtlMap;
                            vm.updatePartlinePrice(partNum, priceObj);
                        }
                    }
                }
            }
        }, function (error) {

        });
    }
    updatePartlinePrice(no, pObj) {
        let vm = this;
        for (let i = 0; i < vm.listData.length; i++) {
            if (vm.listData[i].partNo == no) {
                if (pObj.STANDARD_BASE) {
                    vm.listData[i].price = pObj.STANDARD_BASE;
                }
                if (pObj.CORE_BASE) {
                    vm.listData[i].core_price = pObj.CORE_BASE;
                }
            }

        }
    }
    goToCartPage() {
        let vm = this;
        let {$state, PricingService} = vm.DI();
        PricingService.orderType = 'STK';
        $state.go('ordermgt');
    }
    deleteCartItem(item) {
        let vm = this,
            {
                dataServices, OrderListService
            } = vm.DI();
        let tempID = item.id;
        let id = item.id;
        item.isDeleted = true;
        dataServices.deleteItemFromOrderlist(id).then(function (response) {
            if (response.mess === "Accepted") {
                OrderListService.decrementtListCount(vm.selectedList);
                vm.fetchListItems(vm.selectedList);
                //vm.fetchLists('refresh');
            }


        }, function (error) {

        });
    }
    addToCart(list) {
        let vm = this,
            {
                dataServices,
                $rootScope,
                $filter
            } = vm.DI();
        //dataServices.getRtbEntitlementAccess(list.partNo).then(function (response) {
        // if (response.message && response.message === "Authorized") {
        let payload = {
            "cartName": "ORDERLIST",
            "cartType": "normal-cart",
            "orderType": "SO",
            "lineItems": [{
                "partNo": list.partNo,
                "partName": list.partName,
                "qtyReq": list.quantity,
                "packageQty": list.packageQty,
                "backOrderQty": 0,
                "qtyAdded": list.quantity,
                "productCat": list.catPath,
                "uom": list.uom,
                "wt": list.wt,
                "wtUom": list.wtUom,
                "thumbnail": list.thumbnail? list.thumbnail: "",
                "checked": true,
                "shipToDateReq": new Date().getTime()
            }]
        };

        dataServices.addToCart(payload).then(function (response) {
            $rootScope.$emit("gotCartCount");
            vm.orderListMsg.show = true;
            vm.orderListErrMsg.show = false;
            vm.orderListMsg.msg = $filter('translate')('MYLIST.ADDED_CART1');
            //vm.orderListMsg.msg = " 1 item(s) added to the cart!";
            list.addedToCart = true;
        }, function (error) {
        });
        // } else {
        //     vm.orderListErrMsg.show = true;
        //     vm.orderListMsg.show = false;
        //     vm.orderListErrMsg.msg = "Authorization required to order this part(s).";
        //     list.RTBFailed = true;
        // }
        // }, function (error) {
        //     vm.orderListErrMsg.show = true;
        //     vm.orderListMsg.show = false
        //     vm.orderListErrMsg.msg = " Authorization required to order this part(s).";
        // });

    }
    bulkRtbCheck() {
        let vm = this,
            {
                dataServices,
                $rootScope
            } = vm.DI();
        let itemArr = [];
        vm.rtbParts = [];
        let cnt = 0;
        vm.listData.map(function (item, index) {
            itemArr.push(item.partNo);
        });
        dataServices.bulkRTBCheck(itemArr).then(function (response) {
            if (response && response.rtbParts && response.rtbParts.length > 0) {
                vm.hideCartBtn = false;
                vm.listData.map(function (item, index) {
                    if (response.rtbParts.indexOf(item.partNo) !== -1) {
                        vm.rtbParts.push(item.partNo);
                        cnt++;
                    }
                    else {
                        item.RTBFailed = true;
                    }

                });
                if (vm.priceCheck && vm.rtbParts.length > 0) {
                    vm.updatePricing();
                }

            }
            else {
                vm.listData.map(function (item, index) {
                    item.RTBFailed = true; //RTBFailed
                });
                vm.hideCartBtn = true;

            }
        }, function (error) {
            //RTB CHeck failed
            vm.hideCartBtn = true;
        });
    }
    addAllToCart() {
        let vm = this,
            {
                dataServices,
                $rootScope,
                $filter
            } = vm.DI();

        let lineItems = [];
        let cnt = 0;
        vm.orderListErrMsg.show = false;
        vm.orderListMsg.show = false
        //bulkRTBCheck
        let itemArr = [];
        vm.listData.map(function (item, index) {
            itemArr.push(item.partNo);
        });
        //dataServices.bulkRTBCheck(itemArr).then(function (response) {
        //if (response && response.rtbParts.length > 0) {
        if (vm.rtbParts.length > 0) {
            vm.listData.map(function (item, index) {
                if (vm.rtbParts.indexOf(item.partNo) !== -1) {
                    let temp = {
                        "partNo": item.partNo,
                        "partName": item.partName,
                        "qtyReq": item.quantity,
                        "packageQty": item.packageQty,
                        "qtyAdded": item.quantity,
                        "productCat": item.catPath,
                        "uom": item.uom,
                        "wt": item.wt,
                        "wtUom": item.wtUom,
                        "thumbnail": item.thumbnail? item.thumbnail: "",
                        "checked": true,
                        "shipToDateReq": new Date().getTime()
                    };
                    lineItems.push(temp);
                    item.addedToCart = true;
                    cnt++;
                }
                else {
                    item.RTBFailed = true; //RTBFailed
                }
            });
            let payload = {
                "cartName": "ORDERLIST",
                "cartType": "normal-cart",
                "orderType": "SO",
                "lineItems": lineItems
            };
            dataServices.addToCart(payload).then(function (response) {
                $rootScope.$emit("gotCartCount");
                //cnt + items(s) added to the cart!
                vm.orderListMsg.show = true;
                vm.orderListErrMsg.show = false;
                if (vm.totalItemCount === cnt) {
                    vm.orderListMsg.msg = cnt + $filter('translate')('MYLIST.CART_MSG1');
                    //vm.orderListMsg.msg = cnt + " item(s) added to the cart!";
                }
                else {
                    //vm.orderListMsg.msg = $filter('translate')('MYLIST.CART_MSG_OUTOF') ;
                    vm.orderListMsg.msg = $filter('translate')('MYLIST.CART_MSG_OUTOF') + vm.totalItemCount + $filter('translate')('MYLIST.CART_MSG_ONLY') + cnt + $filter('translate')('MYLIST.CART_MSG1');
                }
                //Out of 10 only 5 item(s) added to cart!
            }, function (error) {

            });
        }
        else {
            vm.listData.map(function (item, index) {
                item.RTBFailed = true; //RTBFailed
            });
            vm.orderListErrMsg.show = true;
            vm.orderListMsg.show = false;
            vm.orderListErrMsg.msg = $filter('translate')('MYLIST.CART_MSG_AUTH');
            //vm.orderListErrMsg.msg = " Authorization required to order this part(s).";
        }
        // }, function (error) {
        //     //RTB CHeck failed
        //     vm.orderListErrMsg.show = true;
        //     vm.orderListMsg.show = false;
        //     vm.orderListErrMsg.msg = "Authorization required to order this part(s).";
        // });
        //vm.listData;

    }
    listMenu(elem) {
        console.log(angular.element(elem).hasClass("list-active"));
    }

    checkAddedToRFQ() {
        let vm = this,
            {
                dataServices,
                $rootScope
            } = vm.DI();
        for (let i in vm.listData) {
            vm.listData[i].addedRFQ = false;
            if (vm.draftRFQData !== null) {
                for (let j in vm.draftRFQData.rfqItemsDTOs) {
                    if (vm.listData[i].partNo === vm.draftRFQData.rfqItemsDTOs[j].partNumber) {
                        vm.listData[i].addedRFQ = true;
                        break;
                    }
                }
            }
        }
        vm.allAddedRFQ = false;
        let addedCount = 0;
        for (let k in vm.listData) {
            if (vm.listData[k].addedRFQ) {
                addedCount++;
            }
        }
        if (vm.listData.length === addedCount) {
            vm.allAddedRFQ = true;
        }
    }

    openShareList() {
        let vm = this;
        let size = "md";
        let { $rootScope, $uibModal, OrderListService, $timeout } = vm.DI();
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/mylist/sharelist/share.html',
            size: size,
            controller: 'ShareListController',
            controllerAs: 'SLC',
            backdrop: 'static',
            animation: false,
            //windowClass: 'my-modal-popup rfq-details-modal',
            windowClass: 'share-overlay',
            resolve: {
                listData: function () {
                    return OrderListService;
                }
            }
        });

        uibModalInstance.result.then(function (emailResData) {
            if (emailResData.isEmailSent) {
                vm.isEmailSent = true;
                $timeout(function () {
                    vm.isEmailSent = false;
                }, 3000);
            }
        }, function () {

        });
    }

    addAllToRFQ() {
        let vm = this;
        let { $rootScope, dataServices } = vm.DI();

        let i, payload = [];

        for (i = 0; i < vm.listData.length; i++) {
            if (!vm.listData[i].addedRFQ) {
                vm.listData[i].addedRFQ = true;
                payload.push({
                    "partNumber": vm.listData[i].partNo,
                    "partName": vm.listData[i].partName,
                    "qty": vm.listData[i].quantity,
                    "packageQty": vm.listData[i].packageQty
                });
            }
        }
        dataServices.addToRFQ(vm.draftRFQData.id, payload).then(function (response) {
            if (response !== undefined) {
                vm.hasRFQ = true;
                vm.draftRFQData.rfqItemsDTOs = vm.draftRFQData.rfqItemsDTOs.concat(response);
                vm.checkAddedToRFQ();
            }
        }, function (error) {

        });
    }

    addToRFQ(list) {
        let vm = this,
            {
                dataServices,
                $rootScope
            } = vm.DI();
        let payload = [];
        let obj = {
            "partNumber": list.partNo,
            "partName": list.partName,
            "qty": list.quantity,
            "packageQty": list.packageQty
        };

        for (let i in vm.listData) {
            if (vm.listData[i].partNo === list.partNo) {
                vm.listData[i].addedRFQ = true;
            }
        }

        payload.push(obj);
        dataServices.addToRFQ(vm.draftRFQData.id, payload).then(function (response) {
            if (response !== undefined) {
                vm.draftRFQData.rfqItemsDTOs = vm.draftRFQData.rfqItemsDTOs.concat(response);
                vm.hasRFQ = true;
                vm.checkAddedToRFQ();
            }
        }, function (error) {

        });
    }

    locateDealers(data) {
        let vm = this;
        let { $rootScope, dataServices, $state } = vm.DI();
        //debugger;
        $state.go('sendrfq', { rfqItems: vm.draftRFQData });
    }

    confirmDelete(list) {
        let vm = this;
        let { $uibModal } = vm.DI();
        let size = "sm";
        vm.confirmModalInstance = $uibModal.open({
            templateUrl: 'app/mylist/confirmdelete/confirmdelete.html',
            size: size,
            backdrop: 'static',
            controller: 'ConfirmDeleteController',
            controllerAs: 'cfdc',
            animation: false,
            windowClass: 'my-modal-popup confirm-delete-modal',
            resolve: {
                listData: list
            }
        });
    }

    closeConfirmDelete() {
        let vm = this;
        vm.confirmModalInstance.close();
    }

    updateQuantity() {
        let vm = this;
        let { dataServices, $uibModal, $timeout } = vm.DI();
        vm.isEditableQty = false;
        let payload = [];

        for (let i = 0; i < vm.listData.length; i++) {
            payload.push({
                "quantity": vm.listData[i].quantity,
                "uom": vm.listData[i].uom,
                "id": vm.listData[i].id,
                "packageQty": vm.listData[i].packageQty
            });
        }
        dataServices.updateListItemQuantity(payload).then(function (response) {
            let confirmUpdateList = $uibModal.open({
                templateUrl: 'app/mylist/confirmupdate/confirmupdate.html',
                size: "sm",
                backdrop: 'static',
                windowClass: 'my-modal-popup confirm-update-list'
            });

            $timeout(() => {
                confirmUpdateList.close();
            }, 1000);
        });

    }

    onQtyChange(obj, fieldId) {
        let vm = this;
        let { $timeout } = vm.DI();
        let fieldVal = document.getElementById(fieldId).value;
        if(fieldVal == "0"){
            obj.quantity = obj.packageQty;
        }
        if(!obj.quantity || obj.quantity <= 0  || (obj.packageQty >= 1 && obj.quantity > 0 && (obj.quantity % obj.packageQty != 0))){
            vm.cartQtyNotAllowed = true;
        }else{
            //vm.cartQtyNotAllowed = false;
            $timeout(function(){
                if(document.getElementsByClassName('qty-err').length > 0){
                    vm.cartQtyNotAllowed = true;
                }else{
                    vm.cartQtyNotAllowed = false;
                }
            }, 0);
        }
        obj.enable = true;
        vm.isEditableQty = true;
    }

    gotoPart(obj){
        let vm = this;
        let { $stateParams, $state } = vm.DI();
        let paramObj = { "id": obj.partNo, "type": "partnum", "val":obj.partNo };
        return $state.href("part", paramObj);
    }

}
