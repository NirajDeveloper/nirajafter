/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export function printDirective($rootScope) {
    'ngInject';
    let directive = {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrm, vm) {
            let printSection = document.getElementById('printSection');
            element.on('click', function () {
                printSection.innerHTML = '';
                let elemToPrint = document.getElementById(attrm.printElementId);
                if (elemToPrint) {
                    let domClone = elemToPrint.cloneNode(true);
                    printSection.innerHTML = '';
                    printSection.appendChild(domClone);
                    window.print();
                }
            });
            window.onafterprint = function () {
                // clean the print section before adding new content
                printSection.innerHTML = '';
            }
        }
    };
    return directive;
}