/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class CreatenewlistController {
    constructor($location, $uibModal, $uibModalInstance, $scope, $translate, dataServices, OrderListService, $rootScope, tempItemList, $state, authenticationService) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $location, $uibModal, $uibModalInstance, $scope, dataServices, OrderListService, $rootScope, tempItemList, $state, authenticationService });
        this.newItem = "";
        this.errMsg = "";

        // $rootScope.$on('listCreateAPIMessage', function(event, args){ 
        //    if(args.mess === 'Created'){
        //         $uibModalInstance.close();
        //    }
        //    else if(args.mess === 'list.already.exist'){
        //        vm.errMsg = "List already exists!!";
        //    }
        // });
        vm.isListCreated = false;
        vm.tempItemList = tempItemList;
        
        vm.isShopUser = (authenticationService.userPermissionsList.RequestQuote) ? true : false;
    }
    cancel() {
        let vm = this,
            {
                $uibModalInstance, $uibModal, $state
            } = vm.DI();
        vm.newItem = "";
        if (sessionStorage["saveList"] === "true") {
            $uibModalInstance.close();
            $uibModal.open({
                templateUrl: 'app/mylist/sessionlist/sessionlist.html',
                size: 'md',
                backdrop: 'static',
                animation: false,
                controller: "SessionListController",
                controllerAs: "slcont",
                windowClass: 'my-modal-popup session-list-modal'
            });
        }
        else {
            $uibModalInstance.close();
        }
    }



    createNewList() {
        //   let vm = this,
        //     {
        //         OrderListService
        //     } = vm.DI();
        //     OrderListService.createNewList(vm.newItem)
        let vm = this;
        let {dataServices, OrderListService, $rootScope, $uibModalInstance} = vm.DI();
        dataServices.createList({ "cartName": vm.newItem }, vm.newItem)
            .then(function (response) {
                if (response.mess === 'Created') {
                    OrderListService._myLists.push({
                        "id": response.code,
                        "cartName": vm.newItem,
                        "cartType": "ORDER_LIST",
                        "cartLineCount": 0
                    });
                    $rootScope.$broadcast('refreshList');
                    let data = {};
                    data.listName = vm.newItem;
                    data.code = response.code;

                    if (vm.tempItemList) {
                        vm.addTempItemsToList(response);
                    }
                    else {
                        $uibModalInstance.close(data);
                        OrderListService.getMyLists({listCreated: true});
                    }
                }
                else if (response.mess === 'list.already.exist') {
                    vm.errMsg = "List already exists!!";
                }
                //$rootScope.$broadcast('listCreateAPIMessage', {mess:response.mess});

            }, function (error) {
                if (error.status === 500) {
                    //DO WHAT YOU WANT

                }
                if (error.status === 404) {
                    //DO WHAT YOU WANT
                }
            });
    }

    addTempItemsToList(obj) {
        let vm = this;
        let {dataServices, $location, $rootScope, $scope, OrderListService, $uibModalInstance} = vm.DI();
        //debugger;
        if (vm.tempItemList !== undefined && vm.tempItemList !== null) {
            dataServices.addItemToOrderList(vm.tempItemList, obj.code)
                .then(function (response) {
                    if (response.mess === "Created") {
                        vm.isListCreated = true;
                        sessionStorage.removeItem("saveList");
                        sessionStorage.removeItem("shareList");
                        sessionStorage.removeItem("tempItemList");
                        for (let i = 0; i < OrderListService._myLists.length; i++) {
                            if (OrderListService._myLists[i].id === obj.code) {
                                OrderListService._myLists[i].cartLineCount = vm.tempItemList.length;
                            }
                        }
                        vm.newLineId = response.code.replace(/[\[\]]/g, '');
                        OrderListService.getMyLists();
                    }
                }, function (error) {
                    if (error.status === 500) {
                        //DO WHAT YOU WANT
                    }
                    if (error.status === 404) {
                        //DO WHAT YOU WANT
                    }
                });
        }
        else {
            $uibModalInstance.close();
        }
    }
}
