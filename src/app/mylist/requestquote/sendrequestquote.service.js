/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class ReqquoteService {
    constructor(dataServices, $http, $rootScope, $window, Session, AUTH_EVENTS, $q) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({
            dataServices, $http, $rootScope, $window, Session, AUTH_EVENTS, $q
        });
        vm._dealerLocatorData = {};
    }
    get dealerLocatorData() {
        return this._dealerLocatorData;
    }
    set dealerLocatorData(data) {
        this._dealerLocatorData = data;
    }

    shareRFQ(rfqId, quoteBy) {
        let vm = this;
        let { dataServices, $q } = vm.DI();

        return $q(function (resolve, reject) {
            dataServices.shareRFQ(rfqId, quoteBy).then(function (response) {
                resolve(response);
            });
        });


    }

    getDealerLocatorData(payload, callback) {
        let vm = this;
        let { dataServices } = vm.DI();

        dataServices.getDealerLocatorData(payload).then(function (response) {
            let data = response;
            for (let i = 0; i < data.length; i++) {
                data[i].selected = false;
            }
            vm.dealerLocatorData = data;
            callback(data);
        });
    }

    addRFQReceiver(rfqId, custId) {
        let vm = this;
        let { dataServices, $q } = vm.DI();
        return $q(function (resolve, reject) {
            dataServices.addRFQReceiver(rfqId, custId).then(function (response) {
                resolve(response);
            });
        });
    }

    deleteRFQReceiver(custId) {
        let vm = this;
        let { dataServices, $q } = vm.DI();
        return $q(function (resolve, reject) {
            dataServices.deleteRFQReceiver(custId).then(function (response) {
                resolve(response);
            });
        });
    }

    getRFQReceiver(rfqId) {
        let vm = this;
        let { dataServices, $q } = vm.DI();
        return $q(function(resolve, reject) {
            dataServices.getRFQReceiver(rfqId).then(function(response) {
                resolve(response);
            });
        });
    }
}