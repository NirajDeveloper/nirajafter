/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/

export class AddedRFQController {
    constructor($rootScope, $scope, $translate, rfqdata, $uibModalInstance) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $rootScope, $scope, $translate, rfqdata, $uibModalInstance });
        vm.rfqData = rfqdata;
    }
    
    close() {
        let vm = this;
        let { $uibModalInstance } = vm.DI();
        $uibModalInstance.close();
    }
}