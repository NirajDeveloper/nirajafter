/***************************************************************
* Proprietary & Confidential  |  © 2016 PhaseZero Ventures LLC 
* 
* This file is part of Tire Advisor project
* 
* This file cannot be copied and/or distributed without 
* the express permission of PhaseZero Ventures LLC
***************************************************************/


export class ReqquoteController {
    constructor($rootScope, $translate, $scope, $uibModal, $stateParams, ReqquoteService, $state, appInfoService, authenticationService, $location, dataServices, AftermarketConstants, $q, $interval, MyRFQService) {
        'ngInject';
        let vm = this;
        vm.DI = () => ({ $rootScope, $translate, $scope, $uibModal, $stateParams, ReqquoteService, $state, appInfoService, authenticationService, $location, dataServices, AftermarketConstants, $q, $interval, MyRFQService });
        vm.rfqData = null;
        vm.dealers = [];
        vm.selectedDateLimit = "0";
        if ($state.params.rfqItems !== null) {
            vm.rfqData = $state.params.rfqItems;
            vm.selectedDealers = vm.rfqData.rfqReciverDTOs;
        }
        else {
            dataServices.getDraftList().then(function(response) {
                vm.rfqData = response;
                vm.selectedDealers = vm.rfqData.rfqReciverDTOs;
            });
        }

        let intvl = setInterval(() => {
            if (appInfoService.appInfo) {
                clearInterval(intvl);
                vm.appInfo = appInfoService.appInfo;
            }
        }, 100);


        $scope.$watch(() => vm.dealers, function(nv, ov) {
            if (vm.dealers.length > 0) {
                vm.checkAdded(vm.dealers);
            }
        });

        $scope.$watch(() => vm.dealer, function(nv, ov) {
            if (vm.dealer) {
                vm.selectedDealer();
            }
        });
    }

    saveRFQData() {
        let vm = this;
        let { $rootScope, ReqquoteService, dataServices } = vm.DI();
        dataServices.addToRFQ(vm.rfqData).then(function(response) {
            if (response !== undefined) {
                vm.rfqData = response;
            }
        }, function(error) {

        });
    }

    getReceiverData() {
        let vm = this;
        let { $rootScope, ReqquoteService, $q } = vm.DI();

        return $q(function(resolve, reject) {
            ReqquoteService.getRFQReceiver(vm.rfqData.id, function(response) {
                resolve(response);
            });
        });
    }

    openRFQDetails(obj) {
        let vm = this,
            size = "md";
        let { $log, $uibModal } = vm.DI();
        var uibModalInstance = $uibModal.open({
            templateUrl: 'app/mylist/rfqdetails/rfqdetails.html',
            size: size,
            controller: 'RFQDetailsController',
            controllerAs: 'RFQD',
            backdrop: 'static',
            animation: false,
            windowClass: 'my-modal-popup rfq-details-modal',
            resolve: {
                details: function() {
                    return obj;
                }
            }
        });
    }

    selectedDealer() {
        let vm = this;

        let obj = vm.dealer;
        let { $rootScope, $location, appInfoService, AftermarketConstants, dataServices, ReqquoteService } = vm.DI();
        obj.isDeleted = false;
        let rfqAddReceiver = ReqquoteService.addRFQReceiver(vm.rfqData.id, obj.customerId);
        rfqAddReceiver.then(function(response) {
            delete obj.reciverId;
            obj.reciverStatus = "NEW";
            obj.rfqUserStatus = "RECEIVED_BY_DEALER";
            obj.reciverId = response;
            vm.selectedDealers.push(obj);
            /*for (let i = 0; i < vm.dealerLocatorData.length; i++) {
                if (vm.dealerLocatorData[i].customerId === obj.customerId) {
                    vm.dealerLocatorData[i].selected = true;
                    break;
                }
            }*/
            let day = vm.appInfo.rfqReceiveWithin[parseInt(vm.selectedDateLimit)];
            let nextDay = new Date();
            nextDay.setDate(nextDay.getDate() + day);
            vm.rfqData.rfqReciverDTOs = vm.selectedDealers;
            vm.rfqData.deadlineOn = nextDay.valueOf();
            vm.rfqData.baseUrl = $rootScope.protocol + "://" + $location.host() + AftermarketConstants.skin.root + '/?goto=rfq';
            vm.rfqData.responseRequiredBy = day;
        });

        //vm.saveRFQData();
    }

    checkAdded(sData) {
        let vm = this;

        for (let i = 0; i < sData.length; i++) {
            sData[i].selected = false;
            for (let j = 0; j < vm.selectedDealers.length; j++) {
                if (sData[i].customerId === vm.selectedDealers[j].customerId) {
                    sData[i].selected = true;
                    break;
                }
            }
        }
        return sData;
    }

    removeAddedDealers(items, index) {
        let vm = this;
         vm.dealer = null;
        let { $rootScope, ReqquoteService } = vm.DI();
        let custId = items.reciverId;
        let deleteDealer = ReqquoteService.deleteRFQReceiver(custId);
        items.isDeleted = true;
        deleteDealer.then(function(response) {
            for (let i = 0; i < vm.dealers.length; i++) {
                if (items.customerId === vm.dealers[i].customerId) {
                    vm.dealers[i].selected = false;
                    break;
                }
            }
            vm.selectedDealers.splice(index, 1);
        });
    }

    shareRFQ() {
        let vm = this;
        let { $rootScope, $scope, ReqquoteService, $location, appInfoService, AftermarketConstants } = vm.DI();
        let shareRFQ = ReqquoteService.shareRFQ(vm.rfqData.id, vm.appInfo.rfqReceiveWithin[parseInt(vm.selectedDateLimit)]);
        shareRFQ.then(function(response) {
            if (response === "") {
                $scope.$emit("rfqNotfDetails");
                sessionStorage.removeItem("paramsRFQItems");
                vm.goToRFQSuccess();
            }
        });
    }

    goToRFQSuccess() {
        let vm = this;
        let { $rootScope, ReqquoteService } = vm.DI();
        vm.finalData = [];
        let response = vm.rfqData.rfqReciverDTOs;
        let i, j, receiverDTOs = [], receiverDTO = {};
        for (i = 0; i < response.length; i++) {
            receiverDTO = response[i];
            receiverDTO.rfqUniqueId = vm.rfqData.rfqUniqueId;
            receiverDTO.rfqItemsDTOs = vm.rfqData.rfqItemsDTOs;
            receiverDTO.deadlineOn = vm.rfqData.deadlineOn;
            receiverDTO.initiatedOn = vm.rfqData.initiatedOn;
            receiverDTO.deadlineOn = vm.rfqData.deadlineOn;
            receiverDTOs.push(receiverDTO);
        }
        vm.finalData = receiverDTOs;
        vm.isRFQSuccess = true;
    }

    change(type) {
        let vm = this;

        switch (type) {
            case 0:
                vm.isSortFilter = false;
                break;
            case 1:
                vm.isSortFilter = true;
                break;
            default:
                vm.isSortFilter = false;
                break;
        }
        vm.init();
    }

    clear() {
        let vm = this;
        vm.reset();
        vm.init();
    }

    viewPartsAdded() {
        let vm = this;
        let { $uibModal } = vm.DI();
        let rfqData = vm.rfqData;

        let modalInstance = $uibModal.open({
            templateUrl: 'app/mylist/requestquote/addedrfq/addedrfq.html',
            size: 'md',
            windowClass: 'added-rfq-popup',
            controller: 'AddedRFQController',
            controllerAs: 'arfq',
            resolve: {
                rfqdata: function() {
                    return rfqData;
                }
            }
        });
    }

    cancelRFQ(items) {
        let vm = this;
        let { MyRFQService } = vm.DI();
        let payload = {
            "rfqReciverId": items.reciverId,
            "rfqUserStatus": "CANCELLED"
        };
        let reminder = MyRFQService.updateRFQStatus(payload);
        reminder.then(function(response) {
            items.isCancelled = true;
        });
    }

}