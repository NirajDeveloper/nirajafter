'use strict';

var gulp = require('gulp'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    rename = require('gulp-rename'),
    fs = require('fs'),
    uglify = require('gulp-uglify');

gulp.task('gen-env', function () {
    var args = Object.keys(argv);
    if(args[1] !== "$0"){
        console.log("API Environment : "+args[1]);
        fs.createReadStream('src/env/'+args[1]+'.js').pipe(fs.createWriteStream('src/env/env.js'));
    }
    else {
        console.log("API Environment : dev");
        fs.createReadStream('src/env/development.js').pipe(fs.createWriteStream('src/env/env.js'));
    }
});